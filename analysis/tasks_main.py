import os
from itertools import product
import law
import luigi
import numpy as np

np.random.seed(101)
from tensorflow.keras import backend as K
from tensorflow import set_random_seed

from analysis.tasks_data_external import SimulatedData
from analysis.tasks_base import (
    BaseTask,
    BaseData,
    BaseGAN,
    BaseNetwork,
    GLOBAL_SEED,
    HTCondorBaseTask,
)
from analysis.framework import HTCWGPU
from utils.processing.dataset import Dataset
from utils.processing.sanitize import sanitize_input

law.contrib.load("numpy", "slack")


class TrainClassifierSimSim(BaseData, BaseGAN, BaseNetwork, HTCWGPU):
    """description for TrainClassifier"""

    """
    The following parameters are the ons available
    for different architectures
    """

    def create_branch_map(self):
        """
        Branches are used for the k_split folding!
        Branch 0: [train, test]
        Branch 1: [test, train]
        """
        return {k_split_number: k_split_number for k_split_number in range(self.k_split_folds)}

    def requires(self):
        return {
            "data_a": SimulatedData.req(self, dataset=self.dataset, datapolicy=self.datapolicy),
            "data_b": SimulatedData.req(self, dataset=self.dataset, datapolicy=self.datapolicy),
        }

    def output(self):
        return law.TargetCollection(
            {
                "training_history": self.local_target("training_history.pkl"),
                "summary_dir": law.LocalDirectoryTarget(self.local_path("summary")),
                "checkpoint_dir": law.LocalDirectoryTarget(self.local_path("checkpoints")),
                "test_metrics": self.local_target("test_metrics.pkl"),
                "config": self.local_target("config.json"),
            }
        )

    def run(self):
        import tensorflow as tf
        import numpy as np
        from model.models import BinaryClassifier
        from model.logger import Logger
        from model.trainer import BinaryClassifierTrainer
        from model.evaluater import BinaryClassifierEvaluater

        set_random_seed(self.seed)
        self.config = {
            "layers": self.layers,
            "nodes": self.nodes,
            "activation": self.activation,
            "LBN": self.lbn,
            "input_dim": self.input_dim,
            "LBN_particles": self.lbn_particles,
            "LBN_features": self.lbn_features,
            "epochs": self.epochs,
            "batch_size": self.batch_size,
            "learning_rate": self.learning_rate,
            "L2": self.L2,
            "batch_norm": self.batch_norm,
            "summary_dir": self.local_path("summary"),
            "checkpoint_dir": self.local_path("checkpoints"),
        }
        """
        Load Data
        """
        train_data_a = self.input()["data_a"]["train"]["x"].load()
        train_label_a = self.input()["data_a"]["train"]["y"].load()
        train_data_b = self.input()["data_b"]["train"]["x"].load()
        train_label_b = self.input()["data_b"]["train"]["y"].load()

        """
        split data
        """
        data_a = train_data_a[np.where(train_label_a == 1)[0]]
        label_a = train_label_a[np.where(train_label_a == 1)[0]]
        data_b = train_data_b[np.where(train_label_b == 0)[0]]
        label_b = train_label_b[np.where(train_label_b == 0)[0]]

        split_idx_a = k_fold_split(self.k_split_folds, data_a, self.branch, seed=self.seed)
        split_idx_b = k_fold_split(self.k_split_folds, data_b, self.branch, seed=self.seed)
        data_a = data_a[split_idx_a]
        label_a = label_a[split_idx_a]
        data_b = data_b[split_idx_b]
        label_b = label_b[split_idx_b]

        train_data = np.concatenate((data_a, data_b))
        train_label = np.concatenate((label_a, label_b))
        """
        preprocess data
        """
        train_data, scaling_factor = preprocessing(train_data)
        self.config["data"]["scaling_factor"] = float(scaling_factor)
        """
        split test data
        """
        test_fold = (self.branch + 1) % self.k_split_folds

        split_idx_a_test = k_fold_split(self.k_split_folds, data_a, test_fold, seed=self.seed)
        split_idx_b_test = k_fold_split(self.k_split_folds, data_b, test_fold, seed=self.seed)

        assert np.any(np.in1d(split_idx_a_test, split_idx_a)), "Dataset folding not working properly!"
        assert np.any(np.in1d(split_idx_b_test, split_idx_b)), "Dataset folding not working properly!"

        data_a_test = data_a[split_idx_a_test]
        label_a_test = label_a[split_idx_a_test]
        data_b_test = data_b[split_idx_b_test]
        label_b_test = label_b[split_idx_b_test]

        test_data = np.concatenate((data_a_test, data_b_test))
        test_data /= scaling_factor
        test_label = np.concatenate((label_a_test, label_b_test))
        """
        pre-shuffle data
        """
        permutation = np.random.permutation(len(train_data))
        val_permutation = np.random.permutation(len(test_data))

        val_split = int(len(test_data) * 0.1)
        data = {
            "x_train": train_data[permutation],
            "y_train": train_label[permutation],
            "x_val": test_data[val_permutation[0:val_split]],
            "y_val": test_label[val_permutation[0:val_split]],
        }
        test_data = {
            "x_test": test_data[val_permutation[val_split:]],
            "y_test": test_label[val_permutation[val_split:]],
        }
        """
        create needed directories
        """
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        # create Session
        config = tf.ConfigProto()
        # # dynamically grow the memory used on the GPU
        config.gpu_options.allow_growth = True
        # config.gpu_options.per_process_gpu_memory_fraction = 0.45

        with tf.Session(config=config) as sess:
            # create model instance
            BC = BinaryClassifier(self.config)
            # create tensorboard logger
            logger = Logger(sess, self.config)
            # create trainer
            trainer = BinaryClassifierTrainer(sess, BC, data, self.config, logger)
            # load model if exists
            BC.load(sess)
            # train model
            trainer.train()
            training_history = trainer.get_training_history()
            """
            Evaluate on test-set
            """
            # BC.load(sess)
            evaluater = BinaryClassifierEvaluater(sess, BC, test_data, self.config)
            test_metrics = evaluater.evaluate()
            self.output()["test_metrics"].dump(test_metrics)
            self.output()["training_history"].dump(training_history)
        self.output()["config"].dump(self.config, indent=4)
        return

    def store_parts(self):
        return super(TrainClassifierSimSim, self).store_parts() + ("{}".format(self.branch),)


class EvaluateModel(BaseData, BaseNetwork):
    """description"""

    def requires(self):
        from analysis.tasks_main import TrainClassifierSimSim

        return {
            "data": SimulatedData.req(self, dataset=self.dataset),
            "model": TrainClassifierSimSim.req(
                self,
                layers=self.layers,
                lbn=self.lbn,
                nodes=self.nodes,
                activation=self.activation,
                input_dim=self.input_dim,
                lbn_features=self.lbn_features,
                epochs=self.epochs,
                dataset=self.dataset,
            ),
        }

    def output(self):
        return law.TargetCollection({"test_metrics": self.local_target("test_metrics.pkl")})

    def run(self):
        import tensorflow as tf
        import numpy as np
        from model.models import BinaryClassifier
        from model.logger import Logger
        from model.trainer import BinaryClassifierTrainer
        from model.evaluater import BinaryClassifierEvaluater

        set_random_seed(self.seed)
        self.config = self.input()["model"].config
        """
        Load Data and shuffle
        """
        test_data = self.input()["data"]["test"]["x"].load()
        test_label = self.input()["data"]["test"]["y"].load()
        permutation = np.random.permutation(len(test_data))
        val_permutation = np.random.permutation(len(test_data))
        val_split = int(len(test_data) * 0.1)
        test_data = {"x_test": test_data, "y_test": test_label}
        """
        create needed directories
        """
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        # create Session
        config = tf.ConfigProto()
        # dynamically grow the memory used on the GPU
        config.gpu_options.allow_growth = True
        # config.gpu_options.per_process_gpu_memory_fraction = 0.45
        # to log device placement (on which device the operation ran)

        # create model instance
        # tf.reset_default_graph()
        with tf.Session(config=config) as sess:
            BC = BinaryClassifier(self.config)
            # create tensorboard logger
            logger = Logger(sess, self.config)
            # load model if exists
            BC.load(sess)
            """
            Evaluate on test-set
            """
            evaluater = BinaryClassifierEvaluater(sess, BC, test_data, self.config)
            test_metrics = evaluater.evaluate()
            self.output()["test_metrics"].dump(test_metrics)
            self.output()["training_history"].dump(training_history)
        self.output()["config"].dump(self.config, indent=4)
        return

    def store_parts(self):
        return super(EvaluateModel, self).store_parts() + ()


class TrainWGAN(BaseData, BaseGAN, HTCWGPU, law.LocalWorkflow):
    """description for TrainClassifierSimSim"""

    """
    The following parameters are the ons available
    for different architectures
    """

    def create_branch_map(self):
        return [0]

    def requires(self):
        return {"data": SimulatedData.req(self)}

    def output(self):
        ret_dict = {
            "training_history": self.local_target("training_history.pkl"),
            "checkpoint_dir": law.LocalDirectoryTarget(self.local_path("checkpoints")),
            "config": self.local_target("config.json"),
            "success": self.local_target("success.json"),
        }
        if self.verbose is True:
            ret_dict["summary_dir"] = (law.LocalDirectoryTarget(self.local_path("summary")),)
        return law.TargetCollection(ret_dict)

    def run(self):
        import tensorflow as tf
        import numpy as np
        import six
        from model.models import WGAN, DetectorAcceptanceWGAN
        from model.logger import Logger
        from model.trainer import WGANTrainer
        from model.evaluater import WGANGenerator

        set_random_seed(self.seed)
        kwargs = {}
        self.config_general["summary_dir"] = self.local_path("summary")
        self.config_general["checkpoint_dir"] = self.local_path("checkpoints")
        """
        Load Data and shuffle
        """
        dataset = Dataset(
            self.input()["data"],
            self,
            preprocessing=True,
            validation_split=0.1,
            data_split=True,
            folds=self.k_split_folds,
            stage=self.k_split_number,
            seed=GLOBAL_SEED,
            test=self.test,
        )
        self.config_data["scaling_factor"] = float(dataset.scaling_factor)
        data = dataset.data

        if "momentum transformation" in self.architecture_config_gen["callbacks"]:
            kwargs["rv_histograms"] = dataset.rv_histograms

        """
        create needed directories
        """
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        # create Session
        config = tf.ConfigProto()
        # dynamically grow the memory used on the GPU
        config.gpu_options.allow_growth = True
        # config.gpu_options.per_process_gpu_memory_fraction = 0.45
        # to log device placement (on which device the operation ran)
        sess = tf.Session(config=config)
        # from tensorflow.python import debug as tf_debug
        # sess = tf_debug.LocalCLIDebugWrapperSession(sess)
        # sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

        # sess = tf_debug.TensorBoardDebugWrapperSession(sess, "priv-3a-199.physik.rwth-aachen.de:7000", send_traceback_and_source_code=False)
        # Register session with Keras
        # sess = tf_debug.TensorBoardDebugWrapperSession(sess, "Niclass-MacBook-Pro.local:7000", send_traceback_and_source_code=False)
        self.config = {
            "critic": self.architecture_config_crit,
            "generator": self.architecture_config_gen,
            "general": self.config_general,
            "data": self.config_data,
        }
        K.set_session(sess)
        with sess:
            # create model instance
            if "detector_acceptance_only" in self.architecture_config_gen["callbacks"]:
                wgan = DetectorAcceptanceWGAN(self.config, **kwargs)
            else:
                wgan = WGAN(self.config, **kwargs)
            # create tensorboard logger
            logger = Logger(sess, self.config)
            # create trainer
            trainer = WGANTrainer(sess, wgan, data, self.config, logger)
            # load model if exists
            # WGAN.load()
            # train model
            success = trainer.train()
            training_history = trainer.get_training_history()
            self.output()["training_history"].dump(training_history)
        self.config = sanitize_input(self.config)
        self.output()["config"].dump(self.config, indent=4)
        self.publish_message("running {}".format(self.__class__.__name__))
        self.output()["success"].dump({"status": success}, indent=2)
        return

    def store_parts(self):
        return super(TrainWGAN, self).store_parts()


class TrainMass(TrainWGAN):
    def create_branch_map(self):
        return [0]

    def requires(self):
        return {"data": SimulatedData.req(self)}

    def run(self):
        import tensorflow as tf
        import numpy as np
        import six
        from model.models import MassWGAN
        from model.logger import Logger
        from model.trainer import WGANTrainer

        set_random_seed(self.seed)
        kwargs = {}
        self.config_general["summary_dir"] = self.local_path("summary")
        self.config_general["checkpoint_dir"] = self.local_path("checkpoints")
        """
        Load Data and shuffle
        """
        dataset = Dataset(
            self.input()["data"],
            self,
            preprocessing=True,
            validation_split=0.1,
            data_split=True,
            folds=self.k_split_folds,
            stage=self.k_split_number,
            seed=GLOBAL_SEED,
            test=self.test,
        )
        self.config_data["scaling_factor"] = float(dataset.scaling_factor)
        data = dataset.data
        """
        create needed directories
        """
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        # create Session
        config = tf.ConfigProto()
        # dynamically grow the memory used on the GPU
        config.gpu_options.allow_growth = True
        # config.gpu_options.per_process_gpu_memory_fraction = 0.45
        # to log device placement (on which device the operation ran)
        sess = tf.Session(config=config)
        # from tensorflow.python import debug as tf_debug
        # sess = tf_debug.LocalCLIDebugWrapperSession(sess)
        # sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)

        # sess = tf_debug.TensorBoardDebugWrapperSession(sess, "priv-3a-199.physik.rwth-aachen.de:7000", send_traceback_and_source_code=False)
        # Register session with Keras
        # sess = tf_debug.TensorBoardDebugWrapperSession(sess, "Niclass-MacBook-Pro.local:7000", send_traceback_and_source_code=False)
        self.config = {
            "critic": self.architecture_config_crit,
            "generator": self.architecture_config_gen,
            "general": self.config_general,
            "data": self.config_data,
        }
        self.config["data"]["input_dim"] = (1,)
        K.set_session(sess)
        with sess:
            # create model instance
            wgan = MassWGAN(self.config, **kwargs)
            # create tensorboard logger
            logger = Logger(sess, self.config)
            # create trainer
            trainer = WGANTrainer(sess, wgan, data, self.config, logger)
            # load model if exists
            # WGAN.load()
            # train model
            success = trainer.train()
            training_history = trainer.get_training_history()
            self.output()["training_history"].dump(training_history)
        self.config = sanitize_input(self.config)
        self.output()["config"].dump(self.config, indent=4)
        self.publish_message("running {}".format(self.__class__.__name__))
        self.output()["success"].dump({"status": success}, indent=2)
        return
