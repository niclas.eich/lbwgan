import os

import luigi
import law

# the htcondor workflow implementation is part of a law contrib package
# so we need to explicitly load it
law.contrib.load("htcondor")


class HTCondorWorkflow(law.htcondor.HTCondorWorkflow):
    """
    Batch systems are typically very heterogeneous by design, and so is HTCondor. Law does not aim
    to "magically" adapt to all possible HTCondor setups which would certainly end in a mess.
    Therefore we have to configure the base HTCondor workflow in law.contrib.htcondor to work with
    the VISPA environment. In most cases, like in this example, only a minimal amount of
    configuration is required.
    """

    htcondor_logs = luigi.BoolParameter()
    htcondor_logs = True

    test = luigi.BoolParameter()
    htcondor_test = luigi.BoolParameter()
    # htcondor_test = False

    # Only use the regular GPUs
    htcondor_hosts = law.CSVParameter(
        cls=luigi.Parameter,
        default=["vispa-gpu{0:02d}".format(i) for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]]
        + ["vispa-worker{0:02d}".format(i) for i in [1, 2, 3]],
        significant=False,
    )
    # htcondor_gpus = luigi.IntParameter(default=law.NO_INT, significant=False, description="number "
    htcondor_gpus = luigi.IntParameter(
        default=0,
        significant=False,
        description="number "
        "of GPUs to request on the VISPA cluster, leave empty to use only CPUs",
    )

    def workflow_requires(self):
        return self.requires_from_branch()

    def htcondor_output_directory(self):
        # the directory where submission meta data should be stored
        return law.LocalDirectoryTarget(self.local_path())

    def htcondor_bootstrap_file(self):
        # each job can define a bootstrap file that is executed prior to the actual job
        # in order to setup software and environment variables
        return law.util.rel_path(__file__, "bootstrap.sh")

    def htcondor_job_config(self, config, job_num, branches):
        # render_data is rendered into all files sent with a job
        config.render_variables["analysis_path"] = os.getenv("LBWGAN_BASE")
        # copy the entire environment
        config.custom_content.append(("getenv", "true"))
        # tell the job config if GPUs are requested
        # condor logs
        if self.htcondor_logs:
            config.stdout = "out.txt"
            config.stderr = "err.txt"
            config.log = "log.txt"
        if self.htcondor_test is True or self.test is True:
            config.custom_content.append(("+runtime", "600"))
        if not law.is_no_param(self.htcondor_gpus):
            config.custom_content.append(("request_gpus", self.htcondor_gpus))
        config.custom_content.append(("RequestMemory", "10000"))
        if not (self.htcondor_hosts is None):
            if len(self.htcondor_hosts) == 1:
                host_req = ""
            else:
                host_req = "("
            for i, host in enumerate(self.htcondor_hosts):
                if i != 0:
                    host_req += " || "
                host_req += 'TARGET.Machine == "{}.physik.rwth-aachen.de"'.format(host)
            if len(self.htcondor_hosts) == 1:
                pass
            else:
                host_req += ")"
            config.custom_content.append(("requirements", host_req))
        return config


class HTCWGPU(HTCondorWorkflow):

    htcondor_gpu_ram = luigi.IntParameter(default=6000)

    def htcondor_job_config(self, config, job_num, branches):
        # render_data is rendered into all files sent with a job
        config.render_variables["analysis_path"] = os.getenv("LBWGAN_BASE")
        # copy the entire environment
        config.custom_content.append(("getenv", "true"))
        # tell the job config if GPUs are requested
        # condor logs
        if self.htcondor_logs:
            config.stdout = "out.txt"
            config.stderr = "err.txt"
            config.log = "log.txt"
        if self.htcondor_test is True or self.test is True:
            config.custom_content.append(("+runtime", "600"))
        if not law.is_no_param(self.htcondor_gpus):
            config.custom_content.append(("request_gpus", self.htcondor_gpus))
        config.custom_content.append(("request_gpumemory", self.htcondor_gpu_ram))
        config.custom_content.append(("RequestMemory", "10000"))
        if not (self.htcondor_hosts is None):
            if len(self.htcondor_hosts) == 1:
                host_req = ""
            else:
                host_req = "("
            for i, host in enumerate(self.htcondor_hosts):
                if i != 0:
                    host_req += " || "
                host_req += 'TARGET.Machine == "{}.physik.rwth-aachen.de"'.format(host)
            if len(self.htcondor_hosts) == 1:
                pass
            else:
                host_req += ")"
            config.custom_content.append(("requirements", host_req))
        return config


class HTCWCPU(HTCondorWorkflow):
    def htcondor_job_config(self, config, job_num, branches):
        # render_data is rendered into all files sent with a job
        config.render_variables["analysis_path"] = os.getenv("LBWGAN_BASE")
        # copy the entire environment
        config.custom_content.append(("getenv", "true"))
        # tell the job config if GPUs are requested
        # condor logs
        if self.htcondor_logs:
            config.stdout = "out.txt"
            config.stderr = "err.txt"
            config.log = "log.txt"
        if self.htcondor_test is True or self.test is True:
            config.custom_content.append(("+runtime", "600"))
        if not law.is_no_param(self.htcondor_gpus):
            config.custom_content.append(("request_gpus", 0))
        config.custom_content.append(("RequestMemory", "6000"))
        if not (self.htcondor_hosts is None):
            if len(self.htcondor_hosts) == 1:
                host_req = ""
            else:
                host_req = "("
            for i, host in enumerate(self.htcondor_hosts):
                if i != 0:
                    host_req += " || "
                host_req += 'TARGET.Machine == "{}.physik.rwth-aachen.de"'.format(host)
            if len(self.htcondor_hosts) == 1:
                pass
            else:
                host_req += ")"
            config.custom_content.append(("requirements", host_req))
        return config
