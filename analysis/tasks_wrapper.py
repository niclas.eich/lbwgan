import os
from itertools import product
from collections import ChainMap

import six
import law
import luigi
from analysis.tasks_base import BaseTask, BaseData, BaseGAN, BaseNetwork
from analysis.tasks_data_external import SimulatedData
from analysis.tasks_data import GeneratedData
from analysis.tasks_main import TrainClassifierSimSim, TrainWGAN
from analysis.tasks_summaries import (
    CreateClassifierSummary,
    CreateWGANSummary,
    CreateDataSummary,
    CreateMassHistogram,
    CreateMassGANSummary,
)
from analysis.framework import HTCondorWorkflow

law.contrib.load("numpy", "slack")


class CompareNetworks(BaseData, BaseNetwork, BaseTask):
    """description for CompareNetworks"""

    metrics = ["loss", "acc", "auc"]
    category = "LBN"

    def requires(self):
        from analysis.tasks_main import TrainClassifierSimSim

        # layers = [8]
        # L2 = [1e-4, 1e-3]
        # lbn = [True, False]
        # nodes = [1024, 512]
        # activations = ["elu"]
        # input_dim = [[32]]
        # batch_norm = [True, False]
        param_dict = {
            "layers": [8],
            "L2": [1e-4],
            "lbn": [True, False],
            "nodes": [1024],
            "activation": ["elu"],
            "input_dim": [[32]],
            "batch_norm": [True, False],
        }
        return {
            "data": SimulatedData.req(self, dataset=self.dataset),
            "architectures": [
                TrainClassifierSimSim.req(self, **dict(zip(param_dict.keys(), vals))) for vals in product(*param_dict.values())
            ],
            "summaries": [
                CreateClassifierSummary.req(self, **dict(zip(param_dict.keys(), vals))) for vals in product(*param_dict.values())
            ],
        }

    def output(self):
        return {met: self.local_target("comparison_{}.png".format(met)) for met in self.metrics}

    def run(self):
        from utils.plotting.training import plot_comparison

        benchmarks = []
        configs = []
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        # inputs = self.input()["architectures"][1]["collection"].targets
        for inputs in self.input()["architectures"]:
            for i, inp in enumerate(six.itervalues(inputs["collection"].targets)):
                load_target = inp["test_metrics"].load()
                benchmarks.append(load_target)
                load_target = inp["config"].load()
                configs.append(load_target)
        """
        bring histories and configs in right order
        """
        summary_data = {met: [] for met in self.metrics}
        summary_data[self.category] = []
        for bench, conf in zip(benchmarks, configs):
            for met in self.metrics:
                summary_data[met].append(bench[met])
            if conf[self.category] is True:
                cat = "LBN"
            else:
                cat = "No LBN"
            summary_data[self.category].append(cat)
        """
        Plot comparison of different metrics
        """
        for met in self.metrics:
            plot_comparison(summary_data, met, self.category, fname=self.output()[met].path)
        return

    def store_parts(self):
        return BaseTask.store_parts(self) + ()


class TrainingSession(BaseData, BaseNetwork, BaseTask, law.WrapperTask):
    def requires(self):
        from analysis.tasks_summaries import CreateClassifierSummary

        # layers = [8]
        # L2 = [1e-4]
        # lbn = [True]
        # nodes = [20, 40]
        # activations = ["elu"]
        # input_dim = [[32]]
        # batch_norm = [True]
        param_dict = {
            "layers": [8, 7],
            "L2": [1e-4, 1e-3],
            "lbn": [True, False],
            "nodes": [1024, 888],
            "activation": ["elu"],
            "input_dim": [[32]],
            "batch_norm": [True, False],
        }
        return {
            "data": SimulatedData.req(self, dataset=self.dataset),
            "architectures": [
                CreateClassifierSummary.req(self, **dict(zip(param_dict.keys(), vals))) for vals in product(*param_dict.values())
            ],
        }


class TrainingSessionWGAN(BaseGAN, BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)

    def requires(self):
        from analysis.tasks_summaries import CreateWGANSummary, CreateHistograms

        self.config_general["epochs"] = self.epochs
        return {
            "summaries": CreateWGANSummary.req(self),
            "histograms": CreateHistograms.req(self),
        }


class TrainingSessionMass(BaseGAN, BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)

    def requires(self):
        self.config_general["epochs"] = self.epochs
        return {
            "summaries": CreateMassGANSummary.req(self),
            "histograms": CreateMassHistogram.req(self),
        }


class GridSearchWGAN(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)

    def requires(self):
        param_dict_crit = {
            "layers": [3],
            "L2": [0.0],
            "lbn": [True, False],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],  # ["E", "px", "py", "pz", "m", "phi", "pt", "eta", "pair_cos"],
            "lbn_particles": [3],
            "nodes": [280],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "learning_rate": [1e-4],
            "optimizer": ["ADAM"],
            "callbacks": [["feature_statistics_layer"], ["feature_statistics_layer", "reset_optimizer"],],
            "dense_net": [{"layers": 2, "nodes": 280, "blocks": 2, "activation": "leaky_relu"}],
        }

        param_dict_gen = {
            "layers": [5],
            "nodes": [200],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [100],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "optimizer": ["ADAM"],
            "callbacks": [["random_rotation"], ["random_rotation", "reset_optimizer"]],
        }

        param_dict_general = {
            "batch_size": [256],
            "GP": [10.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": ["Z"],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1, 2, 3, 4]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class DetectorAcceptanceTraining(law.WrapperTask):
    epochs = luigi.IntParameter(default=2)
    version = luigi.Parameter()

    particle_process = "Zmumu"
    htcondor_gpu_ram = luigi.IntParameter(default=6000)

    notify = law.slack.NotifySlackParameter()

    def requires(self):
        param_dict_crit = {
            "layers": [3],
            "L2": [0.0],
            "lbn": [True],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],
            "lbn_particles": [4],
            "nodes": [280],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "optimizer": ["ADAM"],
            "callbacks": [["reset_optimizer", "lr_decay"]],
            "dense_net": [{"layers": 3, "nodes": 280, "blocks": 2, "activation": "leaky_relu"}],
        }

        param_dict_gen = {
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [{"mass_kernel": "constant", "mass_init": [0.105658, 0.105658], "rotation_kernel": None, "boost_kernel": None,},],
            "param_network": [{"layers": 6, "nodes": 100, "blocks": 1, "activation": "selu"},],
            "callbacks": [["pdu", "reset_optimizer", "lr_decay", "detector_acceptance_only"],],
        }

        param_dict_general = {
            "batch_size": [512],
            "GP": [2.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": ["Zmumu"],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
            "seed": [102, 103, 104, 105, 106],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PDUDebugging(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)
    htcondor_gpu_ram = luigi.IntParameter(default=7000)

    def requires(self):
        param_dict_crit = {
            "layers": [2],
            "L2": [0.0],
            "lbn": [True],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],
            "lbn_particles": [3],
            "nodes": [150],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "learning_rate": [1e-4],
            "optimizer": ["ADAM"],
            # "callbacks": [["reset_optimizer", "lr_decay", "feature_statistics"]],
            "callbacks": [["reset_optimizer", "lr_decay", "feature_statistics"]],
            "dense_net": [{"layers": 2, "nodes": 150, "blocks": 2, "activation": "leaky_relu"}],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [2],
            "nodes": [100],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105, 0.105],
                    "rotation_kernel": {"nodes": 100, "layers": 2, "blocks": 2, "activation": "selu", "mass_activation": "zero",},
                    "boost_kernel": {"nodes": 100, "layers": 2, "blocks": 2, "activation": "selu", "mass_activation": "zero",},
                },
            ],
            "param_network": [{"layers": 3, "blocks": 1, "nodes": 100}],
            "feature_network": [{"layers": 3, "nodes": 100, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu", "reset_optimizer", "lr_decay"]],
        }

        param_dict_general = {
            "batch_size": [512],
            "seed": [102],
            "GP": [1.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": ["Zmumu"],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class MassGANTesting(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=20)
    particle_process = "Z_mass"
    htcondor_gpu_ram = luigi.IntParameter(default=1000)

    def requires(self):
        param_dict_crit = {
            "L2": [1e-4],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "layers": [5],
            "nodes": [300],
            "lbn": [False],
            "lbn_features": [[None]],
            "lbn_particles": [0],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "optimizer": ["ADAM"],
            "callbacks": [["lr_decay"],],
        }

        param_dict_gen = {
            "layers": [4, 5, 6],
            "nodes": [400, 600],
            "activation": ["relu", "selu", "tanh"],
            "batch_norm": [False],
            "L2": [1e-4],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "optimizer": ["ADAM"],
            "callbacks": [["lr_decay"]],
        }

        param_dict_general = {
            "batch_size": [1024],
            "seed": [207],
            "decay_steps": [1],
            "GP": [10.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": [self.particle_process],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1, 2]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionMass.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PDUTesting(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)
    particle_process = "Zmumu"
    htcondor_gpu_ram = luigi.IntParameter(default=1000)

    def requires(self):
        param_dict_crit = {
            "L2": [0.0],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "layers": [2],
            "lbn": [False],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],
            "lbn_particles": [3],
            "learning_rate": [1e-4],
            "lr_decay": ["cosine", 1e-2],
            "nodes": [200],
            "optimizer": ["ADAM"],
            "callbacks": [["lr_decay", "feature_statistics_layer", "no_gradients"],],
            "dense_net": [{"layers": 2, "nodes": 200, "blocks": 3, "activation": "leaky_relu"},],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [2],
            "nodes": [100],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": ["cosine", 1e-2],
            "MMD_loss": [None],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105, 0.105],
                    "rotation_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 0.003,
                    },
                },
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105, 0.105],
                    "rotation_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 0.003,
                    },
                    "boost_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 0.003,
                    },
                },
            ],
            "param_network": [None],
            "feature_network": [{"layers": 3, "nodes": 100, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu", "lr_decay"], ["pdu", "lr_decay", "shortcut"]],
        }

        param_dict_general = {
            "batch_size": [1024],
            "seed": [207, 208],
            "decay_steps": [5, 10],
            "GP": [2.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": [self.particle_process],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1, 2]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PDUPretrained(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)
    particle_process = "Zmumu"
    htcondor_gpu_ram = luigi.IntParameter(default=5000)

    def requires(self):
        param_dict_crit = {
            "L2": [1e-4],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "layers": [2],
            "lbn": [False],
            "lbn_features": [None],
            "lbn_particles": [3],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "nodes": [200],
            "optimizer": ["ADAM"],
            "callbacks": [["lr_decay", "no_gradients"],],
            "dense_net": [{"layers": 2, "nodes": 200, "blocks": 3, "activation": "leaky_relu", "kernel_regularizer": 1e-3,},],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [2],
            "nodes": [100],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "MMD_loss": [None],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {"mass_kernel": "constant", "mass_init": [0.105, 0.105],},
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105, 0.105],
                    "rotation_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105, 0.105],
                    "rotation_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                    "boost_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
            ],
            "param_network": [None],
            "feature_network": [{"layers": 2, "nodes": 100, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu", "lr_decay", "trained_mass"], ["pdu", "lr_decay"]],
        }

        param_dict_general = {
            "batch_size": [1024],
            "seed": [222, 223],
            "decay_steps": [1],
            "GP": [10.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": [self.particle_process],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1, 2]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PDUFinal(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)
    particle_process = "Zee_sorted"
    htcondor_gpu_ram = luigi.IntParameter(default=3000)

    def requires(self):
        param_dict_crit = {
            "L2": [1e-4],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "layers": [2],
            "lbn": [False],
            "lbn_features": [None],
            "lbn_particles": [3],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "nodes": [200],
            "optimizer": ["ADAM"],
            "callbacks": [["lr_decay", "no_gradients"],],
            "dense_net": [{"layers": 2, "nodes": 300, "blocks": 3, "activation": "leaky_relu", "kernel_regularizer": 1e-3,},],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [2],
            "nodes": [100],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "MMD_loss": [None],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.000511, 0.000511],
                    "rotation_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                    "boost_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.000511, 0.000511],
                    "boost_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.000511, 0.000511],
                    "rotation_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
            ],
            "param_network": [None],
            "feature_network": [{"layers": 2, "nodes": 100, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu", "lr_decay"],],
        }

        param_dict_general = {
            "batch_size": [1024],
            "seed": [222, 223],
            "decay_steps": [1],
            "GP": [10.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": [self.particle_process],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1, 2]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PDUFinalMuMu(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)
    particle_process = "Zmumu_sorted"
    htcondor_gpu_ram = luigi.IntParameter(default=3000)

    def requires(self):
        param_dict_crit = {
            "L2": [1e-4],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "layers": [2],
            "lbn": [False],
            "lbn_features": [None],
            "lbn_particles": [3],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "nodes": [200],
            "optimizer": ["ADAM"],
            "callbacks": [["lr_decay", "no_gradients"],],
            "dense_net": [{"layers": 2, "nodes": 300, "blocks": 3, "activation": "leaky_relu", "kernel_regularizer": 1e-3,},],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [2],
            "nodes": [100],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "MMD_loss": [None],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105658, 0.105658],
                    "rotation_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                    "boost_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105658, 0.105658],
                    "boost_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105658, 0.105658],
                    "rotation_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
            ],
            "param_network": [None],
            "feature_network": [{"layers": 2, "nodes": 100, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu", "lr_decay"],],
        }

        param_dict_general = {
            "batch_size": [1024],
            "seed": [222, 223],
            "decay_steps": [1],
            "GP": [10.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": [self.particle_process],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1, 2]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PDUFinalzbb(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)
    particle_process = "zbb_gen_01"
    htcondor_gpu_ram = luigi.IntParameter(default=3000)

    def requires(self):
        param_dict_crit = {
            "L2": [1e-4],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "layers": [2],
            "lbn": [False],
            "lbn_features": [None],
            "lbn_particles": [3],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "nodes": [200],
            "optimizer": ["ADAM"],
            "callbacks": [["lr_decay", "no_gradients"],],
            "dense_net": [{"layers": 2, "nodes": 300, "blocks": 3, "activation": "leaky_relu", "kernel_regularizer": 1e-3,},],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [2],
            "nodes": [100],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "MMD_loss": [None],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {
                    "mass_kernel": "constant",
                    "mass_init": [4.7, 4.7],
                    "rotation_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                    "boost_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
                {
                    "mass_kernel": "constant",
                    "mass_init": [4.7, 4.7],
                    "boost_kernel": {
                        "nodes": 200,
                        "layers": 3,
                        "blocks": 2,
                        "activation": "relu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
                
            ],
            "param_network": [None],
            "feature_network": [{"layers": 2, "nodes": 100, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu", "lr_decay"],],
        }

        param_dict_general = {
            "batch_size": [1024],
            "seed": [222],
            "decay_steps": [1],
            "GP": [1e-1],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": [self.particle_process],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1, 2]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class ML4JetsRunPDU(law.WrapperTask):
    epochs = luigi.IntParameter(default=2)
    version = luigi.Parameter()

    htcondor_gpu_ram = luigi.IntParameter(default=12000)

    notify = law.slack.NotifySlackParameter()

    def requires(self):
        param_dict_crit = {
            "layers": [3],
            "L2": [0.0],
            "lbn": [False],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],
            "lbn_particles": [4],
            "nodes": [280],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "optimizer": ["ADAM"],
            "callbacks": [["reset_optimizer", "lr_decay"]],
            "dense_net": [{"layers": 3, "nodes": 280, "blocks": 2, "activation": "leaky_relu"}],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [3],
            "nodes": [300],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [1e-3],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105, 0.105],
                    "rotation_kernel": {"nodes": 200, "layers": 3, "blocks": 2, "activation": "selu", "mass_activation": "zero",},
                    "boost_kernel": {"nodes": 200, "layers": 3, "blocks": 2, "activation": "selu", "mass_activation": "zero",},
                    "noise": {"size": 30},
                },
            ],
            "param_network": [{"layers": 6, "nodes": 100, "blocks": 1, "activation": "selu"},],
            "feature_network": [{"layers": 3, "nodes": 200, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu", "cauchy_shape", "reset_optimizer", "lr_decay"], ["pdu", "reset_optimizer", "lr_decay"],],
        }

        param_dict_general = {
            "batch_size": [512, 5000],
            "GP": [2.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": ["Zmumu"],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
            "seed": [102, 103, 104, 105, 106],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class ML4JetsRunNoPDU(law.WrapperTask):
    epochs = luigi.IntParameter(default=2)
    version = luigi.Parameter()
    particle_process = "Z"
    htcondor_gpu_ram = luigi.IntParameter(default=1000)
    notify = law.slack.NotifySlackParameter()

    def requires(self):
        from analysis.tasks_summaries import CreateWGANSummary, CreateHistograms

        param_dict_crit = {
            "L2": [0.0],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "layers": [3],
            "lbn": [True],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],
            "lbn_particles": [4],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "nodes": [280],
            "optimizer": ["ADAM"],
            "callbacks": [["reset_optimizer", "lr_decay"],],
            "dense_net": [{"layers": 3, "nodes": 280, "blocks": 2, "activation": "leaky_relu"},],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [3],
            "nodes": [600],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [1e-3],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [None],
            "param_network": [None],
            "feature_network": [None],
            "callbacks": [["reset_optimizer", "lr_decay", "random_rotation"],],
        }

        param_dict_general = {
            "batch_size": [512],
            "GP": [10.0],
            "n_crit": [10],
            "n_gen": [1],
            "seed": [102],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": ["Z"],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [CreateHistograms.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class ML4JetsRunPDUMMD(law.WrapperTask):
    epochs = luigi.IntParameter(default=2)
    version = luigi.Parameter()

    notify = law.slack.NotifySlackParameter()

    def requires(self):
        param_dict_crit = {
            "layers": [2],
            "L2": [0.0],
            "lbn": [True],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],
            "lbn_particles": [3],
            "nodes": [280],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "learning_rate": [1e-4],
            "optimizer": ["ADAM"],
            "callbacks": [["reset_optimizer", "lr_decay"]],
            "dense_net": [{"layers": 3, "nodes": 280, "blocks": 2, "activation": "leaky_relu"}],
        }

        param_dict_gen = {
            "layers": [5],
            "nodes": [200],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "hinge_loss": [None],
            "MMD_loss": [None, 1e-2, 1e-1, 1.0, 1e2, 1e3],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {
                    "mass_kernel": "constant",
                    "mass_init": [0.105, 0.105],
                    "rotation_kernel": {"layers": 3, "blocks": 2, "activation": "tanh", "mass_activation": "zero",},
                    "noise": {"size": 30},
                },
            ],
            "param_network": [{"layers": 5, "nodes": 40, "blocks": 1, "activation": "selu"},],
            "feature_network": [{"layers": 3, "nodes": 100, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu", "reset_optimizer", "lr_decay"], ["pdu", "reset_optimizer", "lr_decay", "cauchy_shape"],],
        }

        param_dict_general = {
            "batch_size": [512],
            "GP": [10.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": ["Zmumu"],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
            "seed": [102],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PDUZbb(law.WrapperTask):
    epochs = luigi.IntParameter(default=2)
    version = luigi.Parameter()
    htcondor_gpu_ram = luigi.IntParameter(default=7900)

    notify = law.slack.NotifySlackParameter()

    def requires(self):
        param_dict_crit = {
            "layers": [3],
            "L2": [0.0],
            "lbn": [True],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],
            "lbn_particles": [4],
            "nodes": [280],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "optimizer": ["ADAM"],
            "callbacks": [["lr_decay"]],
            "dense_net": [{"layers": 2, "nodes": 280, "blocks": 2, "activation": "leaky_relu"}],
        }

        param_dict_gen = {
            "layers": [2],
            "blocks": [2],
            "nodes": [200],
            "activation": ["selu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "lr_decay": [1e-2],
            "hinge_loss": [0.0],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {
                    "mass_kernel": {"layers": 4, "nodes": 200, "activation": "selu"},
                    "rotation_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 0.0,
                    },
                    "boost_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 0.0,
                    },
                },
                {
                    "mass_kernel": {"layers": 4, "nodes": 200, "activation": "selu"},
                    "rotation_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-2,
                    },
                    "boost_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-2,
                    },
                    "noise": {"size": 30},
                },
                {
                    "mass_kernel": {"layers": 4, "nodes": 200, "activation": "selu"},
                    "rotation_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-3,
                    },
                    "boost_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-3,
                    },
                },
                {
                    "mass_kernel": {"layers": 4, "nodes": 200, "activation": "selu"},
                    "rotation_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                    "boost_kernel": {
                        "nodes": 100,
                        "layers": 2,
                        "blocks": 2,
                        "activation": "selu",
                        "mass_activation": "zero",
                        "kernel_regularizer": 1e-4,
                    },
                },
            ],
            "param_network": [{"layers": 2, "nodes": 100, "blocks": 2, "activation": "selu"},],
            # "param_network": [None],
            "feature_network": [{"layers": 3, "nodes": 200, "blocks": 2, "activation": "selu"},],
            "callbacks": [["pdu" "lr_decay"],],
        }

        param_dict_general = {
            "batch_size": [512],
            "GP": [2.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": ["Zbb"],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[0, 1]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
            "seed": [102],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PDUTesting_ttH(BaseData, BaseTask, law.WrapperTask):
    epochs = luigi.IntParameter(default=60)

    def requires(self):
        param_dict_crit = {
            "layers": [1],
            "L2": [0.0],
            "lbn": [True, False],
            "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],  # ["E", "px", "py", "pz", "m", "phi", "pt", "eta", "pair_cos"],
            "nodes": [280],
            "activation": ["leaky_relu"],
            "batch_norm": [False],
            "learning_rate": [1e-4],
            "optimizer": ["ADAM"],
            "callbacks": [[""]],
            "dense_net": [{"layers": 3, "nodes": 280, "blocks": 3, "activation": "leaky_relu"}],
        }

        param_dict_gen = {
            "layers": [5, 6],
            "nodes": [200],
            "activation": ["relu"],
            "batch_norm": [False],
            "L2": [0.0],
            "latent_dim": [30],
            "latent_shape": ["normal"],
            "learning_rate": [1e-4],
            "optimizer": ["ADAM"],
            "trainable_axis": [False],
            "pdu": [
                {"mass_kernel": "constant", "noise": {"size": 30}},
                {"mass_kernel": "constant_trainable", "noise": {"size": 30}},
                {
                    "mass_kernel": "constant_trainable",
                    "rotation_kernel": {"layers": 3, "blocks": 1, "activation": "tanh", "mass_activation": "zero",},
                    "noise": {"size": 30},
                },
                {
                    "mass_kernel": "constant",
                    "rotation_kernel": {"layers": 3, "blocks": 1, "activation": "tanh", "mass_activation": "zero",},
                    "noise": {"size": 30},
                },
                {"mass_kernel": {"layers": 4, "nodes": 50, "activation": "selu",}, "noise": {"size": 30},},
                {
                    "mass_kernel": {"layers": 4, "nodes": 50, "activation": "selu",},
                    "rotation_kernel": {"layers": 3, "blocks": 1, "activation": "tanh", "mass_activation": "zero",},
                    "noise": {"size": 30},
                },
            ],
            "callbacks": [["pdu", "reset_optimizer"], ["pdu"]],
        }

        param_dict_general = {
            "batch_size": [512],
            "GP": [10.0],
            "n_crit": [10],
            "n_gen": [1],
            "epochs": [self.epochs],
        }

        param_dict_data = {
            "dataset": ["best"],
            "particle_process": ["ttH"],
            "dataorigin": ["gen"],
            "datapolicy": ["train"],
            "particle_indexes": [[4, 5]],  # ttH H -> bb : [4, 5]
            "k_split_folds": [2],
            "k_split_number": [0],
        }
        # Dict parameter
        dicts_crit = [{"architecture_config_crit": dict(zip(param_dict_crit.keys(), vals))} for vals in product(*param_dict_crit.values())]
        # Dict parameter
        dicts_gen = [{"architecture_config_gen": dict(zip(param_dict_gen.keys(), vals))} for vals in product(*param_dict_gen.values())]
        # Dict parameter
        dicts_general = [{"config_general": dict(zip(param_dict_general.keys(), vals))} for vals in product(*param_dict_general.values())]
        # regular parameters
        dicts_data = [dict(zip(param_dict_data.keys(), vals)) for vals in product(*param_dict_data.values())]
        total = (dicts_crit, dicts_gen, dicts_general, dicts_data)
        return {
            "trainings": [TrainingSessionWGAN.req(self, **ChainMap(*configs)) for configs in product(*total)],
        }


class PlotAllDistributions(BaseTask, law.WrapperTask):
    """
    Plots all distributions available
    """

    def requires(self):
        # dists = ["Zee", "Zmumu", "Zll", "ttH", "ttbb", "Zee_no_detector", "Zmumu_no_detector", "Zll_no_detector"]
        dists = ["Zmumu", "Zll", "Zee"]
        return [CreateDataSummary.req(self, particle_process=dist) for dist in dists]
