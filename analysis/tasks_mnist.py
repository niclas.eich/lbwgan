import os
from itertools import product

import luigi
import law
import numpy as np
import matplotlib.pyplot as plt

from analysis.tasks_base import (
    BaseTask,
    BaseData,
    BaseGAN,
    BaseNetwork,
    GLOBAL_SEED,
    HTCondorBaseTask,
)
from utils.processing.dataset import preprocessing

law.contrib.load("numpy")


def plot_images(images, figsize=(32, 32), fname=None):
    """ Plot some images """
    n_examples = len(images)
    dim = np.ceil(np.sqrt(n_examples))
    plt.figure(figsize=figsize)
    for i in range(n_examples):
        plt.subplot(dim, dim, i + 1)
        img = np.squeeze(images[i])
        plt.imshow(img, cmap=plt.cm.Greys)
        plt.axis("off")
    plt.tight_layout()
    if fname is not None:
        plt.savefig(fname)
    plt.close()


class MNISTData(law.ExternalTask):
    def output(self):
        return {
            "train": {
                "x": law.LocalFileTarget(
                    os.path.join(
                        os.getenv("MNIST_DATA_PATH"), "train-images-idx3-ubyte.gz"
                    )
                ),
                "y": law.LocalFileTarget(
                    os.path.join(
                        os.getenv("MNIST_DATA_PATH"), "train-labels-idx1-ubyte.gz"
                    )
                ),
            },
            "test": {
                "x": law.LocalFileTarget(
                    os.path.join(
                        os.getenv("MNIST_DATA_PATH"), "t10k-images-idx3-ubyte.gz"
                    )
                ),
                "y": law.LocalFileTarget(
                    os.path.join(
                        os.getenv("MNIST_DATA_PATH"), "t10k-labels-idx1-ubyte.gz"
                    )
                ),
            },
        }


class TrainMNISTWGAN(
    BaseNetwork, BaseGAN, BaseTask, HTCondorBaseTask, law.LocalWorkflow
):
    lbn = False
    input_dim = [28, 28, 1]
    output_dim = [28, 28, 1]

    def create_branch_map(self):
        """
        this is a docstring
        """
        return [0]

    def requires(self):
        return {"data": MNISTData.req(self)}

    def output(self):
        return law.TargetCollection(
            {
                "training_history": self.local_target("training_history.pkl"),
                "summary_dir": law.LocalDirectoryTarget(self.local_path("summary")),
                "checkpoint_dir": law.LocalDirectoryTarget(
                    self.local_path("checkpoints")
                ),
                "config": self.local_target("config.json"),
            }
        )

    def run(self):
        import tensorflow as tf
        import numpy as np
        import gzip
        import json
        import os
        from model.mnist.models import WGAN, WGANTrainer
        from model.logger import Logger
        from model.evaluater import WGANGenerator

        self.config = {
            "layers": self.layers,
            "nodes": self.nodes,
            "optimizer": self.optimizer,
            "activation": self.activation,
            "LBN": self.lbn,
            "input_dim": self.input_dim,
            "LBN_particles": self.lbn_particles,
            "LBN_features": self.lbn_features,
            "epochs": self.epochs,
            "batch_size": self.batch_size,
            "learning_rate": self.learning_rate,
            "L2": self.L2,
            "batch_norm": self.batch_norm,
            "GP": self.GP,
            "n_crit": self.n_crit,
            "n_gen": self.n_gen,
            "normal_dist": False,
            "reduce_n_crit": self.reduce_n_crit,
            "latent_dim": self.latent_dim,
            "output_dim": self.output_dim,
            "summary_dir": self.local_path("summary"),
            "checkpoint_dir": self.local_path("checkpoints"),
        }
        """
        Load Data and shuffle
        """

        def _read32(bytestream):
            dt = np.dtype(np.uint32).newbyteorder(">")
            return np.frombuffer(bytestream.read(4), dtype=dt)[0]

        def _extract_images(fname):
            with gzip.GzipFile(fileobj=open(fname, "rb")) as bytestream:
                _read32(bytestream)
                num_images = _read32(bytestream)
                rows = _read32(bytestream)
                cols = _read32(bytestream)
                buf = bytestream.read(rows * cols * num_images)
                data = np.frombuffer(buf, dtype=np.uint8)
                return data.reshape(num_images, rows, cols)

        def _extract_labels(fname):
            with gzip.GzipFile(fileobj=open(fname, "rb")) as bytestream:
                _read32(bytestream)
                num_items = _read32(bytestream)
                buf = bytestream.read(num_items)
                return np.frombuffer(buf, dtype=np.uint8)

        train_data = _extract_images(self.input()["data"]["train"]["x"].path)
        train_label = _extract_labels(self.input()["data"]["train"]["y"].path)

        """
        pre-shuffle data
        """
        permutation = np.random.permutation(len(train_data))
        """
        scale images
        """
        train_data = train_data - np.mean(train_data)
        scaling_factor = np.max(np.abs(train_data))
        train_data = np.array(train_data, dtype=np.float32) / scaling_factor
        train_data = np.expand_dims(train_data, axis=-1)
        self.config["scaling_factor"] = float(scaling_factor)

        val_split = int(len(train_data) * 0.9)

        data = {
            "x_train": train_data[permutation][0:val_split],
            "y_train": train_label[permutation][0:val_split],
            "x_val": train_data[permutation][val_split:],
            "y_val": train_label[permutation][val_split:],
        }

        config = tf.ConfigProto()
        # dynamically grow the memory used on the GPU
        # config.gpu_options.allow_growth = True
        config.gpu_options.per_process_gpu_memory_fraction = 0.45
        # to log device placement (on which device the operation ran)
        with tf.Session(config=config) as sess:
            # create model instance
            WGAN = WGAN(self.config)
            # create tensorboard logger
            logger = Logger(sess, self.config)
            # create trainer
            trainer = WGANTrainer(sess, WGAN, data, self.config, logger)
            # load model if exists
            WGAN.load(sess)
            # train model
            trainer.train()
            training_history = trainer.get_training_history()
            self.output()["training_history"].dump(training_history)
        self.output()["config"].dump(self.config, indent=4)
        return


class CreateMNISTWGANSummary(BaseGAN, BaseNetwork, BaseTask):
    metrics = ["c_loss", "c_loss_combined", "GP", "g_loss"]

    def requires(self):
        from analysis.tasks_main import TrainMNISTWGAN

        return TrainMNISTWGAN.req(self)

    def output(self):
        ret = {m: self.local_target("training_{}.png".format(m)) for m in self.metrics}
        ret["total"] = self.local_target("training_total.png")
        return ret

    def run(self):
        from utils.plotting.training import plot_training_history, plot_metric

        self.config = self.input()["collection"].targets[0].targets["config"].load()

        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        """
        Plot TrainingHistory
        """
        history = (
            self.input()["collection"].targets[0].targets["training_history"].load()
        )
        tot_metrics = {}
        for metric in self.metrics:
            if metric in history.keys():
                if "val_" + metric in history.keys():
                    plot_metric(
                        {
                            metric: history[metric],
                            "val_" + metric: history["val_" + metric],
                        },
                        self.output()[metric].path,
                    )
                else:
                    plot_metric({metric: history[metric]}, self.output()[metric].path)

                tot_metrics[metric] = history[metric]
        plot_metric(tot_metrics, self.output()["total"].path)
        """
        compute accuracy on Test-set
        """

    def store_parts(self):
        return super(CreateMNISTWGANSummary, self).store_parts() + ()


class GeneratedMNISTData(
    BaseGAN, BaseNetwork, HTCondorBaseTask, law.LocalWorkflow, BaseTask
):
    """ DOCSTRING  """

    n_generated = 200

    def create_branch_map(self):
        return [0]

    def requires(self):
        return {"model": TrainMNISTWGAN.req(self)}

    def output(self):
        return {
            "train": {"x": self.local_target("x_gen.npy")},
            "plots": [
                self.local_target("plot_mnist_{0:02d}".format(i)) for i in range(8)
            ],
        }

    def run(self):
        import tensorflow as tf
        import numpy as np
        from model.mnist.models import WGAN
        from model.evaluater import WGANGenerator

        # This is the config reagarding the tensorflow graph
        self.config = self.input()["model"]["config"].load()

        """
        create needed directories
        """
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = 0.45
        """
        Generate Noise for sampling
        and additionally empty "TrainingData", because the tensor needs some (poor guy)
        """
        tf.reset_default_graph()
        with tf.Session(config=config) as sess:
            WGAN = WGAN(self.config)
            generator = WGANGenerator(sess, WGAN, self.config)
            WGAN.load(sess)
            generated_data = generator.evaluate(self.n_generated)
            generated_data = np.reshape(
                generated_data,
                (np.prod(generated_data.shape[0:2]), *generated_data.shape[2:]),
            )
            print("TYPE: ", type(generated_data))
            print("Shape: ", generated_data.shape)
        self.output()["train"]["x"].dump(generated_data)
        for i, plot_targets in enumerate(self.output()["plots"]):
            plot_images(
                generated_data[i * 25 : (i + 1) * 25],
                figsize=(32, 32),
                fname=plot_targets.path,
            )
        return

    def store_parts(self):
        return super(GeneratedMNISTData, self).store_parts() + ()


class TrainingSessionMNISTWGAN(BaseGAN, BaseNetwork, BaseTask, law.WrapperTask):
    """DOCSTRIN"""

    input_dim = [28, 28, 1]
    output_dim = [28, 28, 1]
    lbn = False

    def requires(self):
        param_dict = {
            "layers": [0, 2],
            "nodes": [200],
            "activation": ["leaky_relu"],
            "GP": [10.0, 100.0],
            "batch_norm": [True, False],
            "n_crit": [5, 3],
            "n_gen": [1],
            "optimizer": ["ADAM"],
        }
        # reduce_n_crit = [False]
        # particle_process = ["ttH"]
        # layers = [6]
        # L2 = [1e-5]
        # lbn = [True]
        # nodes = [288, 120]
        # activations = ["relu"]
        # GP = [10.]
        # input_dim = [[32]]
        # batch_norm = [True]
        # n_crit = [3]
        # optimizer = ["ADAM", "SGD"]

        return {
            "summaries": [
                CreateMNISTWGANSummary.req(self, **dict(zip(param_dict.keys(), vals)))
                for vals in product(*param_dict.values())
            ],
            "examples": [
                GeneratedMNISTData.req(self, **dict(zip(param_dict.keys(), vals)))
                for vals in product(*param_dict.values())
            ],
        }
