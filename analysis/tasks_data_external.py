import os
import law
import luigi
from analysis.tasks_base import BaseData


class SimulatedData(law.ExternalTask, BaseData):
    """Docstring for SimulatedData. """

    def output(self):
        import os

        if self.particle_process == "ttH" or self.particle_process == "ttbb":
            return {
                case: {
                    "{0}".format(xy): law.LocalFileTarget(
                        os.path.join(os.getenv("DATA_PATH_ttHbb"), "low__{0}/{1}_{2}.npy".format(self.dataset, xy, case),)
                    )
                    for xy in ["x", "y"]
                }
                for case in ["train", "test"]
            }
        elif (
            self.particle_process == "Zll"
            or self.particle_process == "Zee"
            or self.particle_process == "Zmumu"
            or self.particle_process == "Z"
            or self.particle_process == "Z_mass"
        ):
            return {"train": {"x": law.LocalFileTarget(os.path.join(os.getenv("DATA_PATH_Zll"), "data.npy"))}}
        elif self.particle_process == "Zbb":
            return {"train": {"x": law.LocalFileTarget(os.path.join(os.getenv("DATA_PATH_Zbb"), "data.npy"))}}
        elif (
            self.particle_process == "Zll_no_detector"
            or self.particle_process == "Zee_no_detector"
            or self.particle_process == "Zmumu_no_detector"
            or self.particle_process == "Z_no_detector"
        ):
            return {"train": {"x": law.LocalFileTarget(os.path.join(os.getenv("DATA_PATH_Zll_no_detector"), "data.npy"))}}
        elif self.particle_process == "zee_01" or self.particle_process == "zmumu_01":
            return {"train": {"x": law.LocalFileTarget(os.path.join(os.getenv("DATA_PATH_Zll_01"), "data_lhe.npy"))}}
        elif self.particle_process == "zbb_gen_01":
            return {"train": {"x": law.LocalFileTarget(os.path.join(os.getenv("DATA_PATH_Zbb_gen_01"), "data_lhe.npy"))}}
        elif self.particle_process == "Zmumu_sorted" or self.particle_process == "Zee_sorted":
            return {"train": {"x": law.LocalFileTarget(os.path.join(os.getenv("DATA_PATH_Zll_sorted"), "data_delphes.npy"))}}
