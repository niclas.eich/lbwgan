import os
from itertools import product

import law
import luigi
import numpy as np
from analysis.tasks_base import BaseTask, BaseData, BaseGAN, BaseNetwork, GLOBAL_SEED
from analysis.tasks_data import GeneratedData, GeneratedMass
from analysis.tasks_data_external import SimulatedData
from analysis.tasks_main import TrainWGAN, TrainClassifierSimSim, TrainMass
from analysis.framework import HTCondorWorkflow, HTCWCPU, HTCWGPU
from utils.processing.dataset import Dataset
from utils.processing.sanitize import sanitize_input
from utils.processing.physics_observables import feature_collection, eta, pt
from model.base_model import SAVING_SCHEDULE

law.contrib.load("numpy")


class CreateClassifierSummary(BaseData, BaseNetwork, law.LocalWorkflow, BaseTask):
    """
    Summary of traninig
    """

    def create_branch_map(self):
        return {i: i for i in range(self.k_split_folds)}

    def requires(self):
        return TrainClassifierSimSim.req(self)

    def output(self):
        return {m: self.local_target("training_{}.png".format(m)) for m in ["acc", "auc", "loss"]}

    def run(self):
        from utils.plotting.training import plot_training_history, plot_metric
        from model.models import BinaryClassifier

        self.config = self.input()["config"].load()
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        """
        Plot TrainingHistory
        """
        # history = self.input()["collection"].targets[0].targets["training_history"].load()
        history = self.input()["training_history"].load()
        for metric in ["acc", "auc", "loss"]:
            if metric in history.keys():
                plot_metric(
                    {metric: history[metric], "val_" + metric: history["val_" + metric],}, self.output()[metric].path,
                )
        """
        compute accuracy on Test-set
        """

    def store_parts(self):
        return super(CreateClassifierSummary, self).store_parts() + ()


class CreateWGANSummary(BaseData, BaseGAN):
    metrics = ["c_loss", "c_loss_combined", "GP", "g_loss"]

    def requires(self):
        return TrainWGAN.req(self)

    def output(self):
        ret = {m: self.local_target("training_{}.png".format(m)) for m in self.metrics}
        ret["total"] = self.local_target("training_total.png")
        return ret

    def run(self):
        from utils.plotting.training import plot_training_history, plot_metric
        from model.models import BinaryClassifier

        self.config = self.input()["collection"].targets[0].targets["config"].load()

        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        """
        Plot TrainingHistory
        """
        history = self.input()["collection"].targets[0].targets["training_history"].load()
        tot_metrics = {}
        for metric in self.metrics:
            if metric in history.keys():
                if "val_" + metric in history.keys():
                    plot_metric(
                        {metric: history[metric], "val_" + metric: history["val_" + metric],}, self.output()[metric].path, log=True,
                    )
                else:
                    plot_metric({metric: history[metric]}, self.output()[metric].path, log=True)

                tot_metrics[metric] = history[metric]
        plot_metric(tot_metrics, self.output()["total"].path)
        """
        compute accuracy on Test-set
        """

    def store_parts(self):
        return super(CreateWGANSummary, self).store_parts() + ()


class CreateHistograms(BaseData, BaseGAN, HTCWCPU, law.LocalWorkflow):
    """
    Function to create histograms and compare two datasets
    """

    def create_branch_map(self):
        return [0]

    def requires(self):
        self.dataset = "best"
        return {
            "generated": GeneratedData.req(self),
            "simulated": SimulatedData.req(self),
        }

    def output(self):
        ret = {
            "comparison": {"postprocessed": {}, "vanilla": {}},
            "histograms": {"postprocessed": {}, "vanilla": {}},
            "config": self.local_target("config.json")
        }
        for ep in np.unique(np.sort(np.append(SAVING_SCHEDULE, [self.config_general["epochs"]]))):
            if ep > self.config_general["epochs"]:
                break
            else:
                ret["comparison"]["postprocessed"][ep] = (
                    law.LocalDirectoryTarget(self.local_path("{}/postprocessed/comparison").format(ep)),
                )
                ret["histograms"]["postprocessed"][ep] = (
                    law.LocalDirectoryTarget(self.local_path("{}/postprocessed/histograms").format(ep)),
                )
                # ret["comparison"]["vanilla"][ep] = (law.LocalDirectoryTarget(self.local_path("{}/vanilla/comparison").format(ep)),)
                # ret["histograms"]["vanilla"][ep] = (law.LocalDirectoryTarget(self.local_path("{}/vanilla/histograms").format(ep)),)
        return ret

    def run(self):
        from utils.plotting.physics import (
            plot_distribution,
            compare_distributions,
            plot_isotropy,
        )
        from utils.processing.physics_observables import reshape_to_particles
        from model.evaluater import pdu_tensor_names

        dataset_sim = Dataset(
            self.input()["simulated"], self, data_split=True, folds=self.k_split_folds, stage=self.k_split_number, seed=GLOBAL_SEED,
        )
        sim_data = dataset_sim.data["x_train"]
        if "ml4jets" in self.version.lower():
            tag = "Eich ML4JETS2020"
        elif "final" in self.version.lower():
            tag = ""
        else:
            tag = self.version
        tag = ""

        # for proc in ["postprocessed", "vanilla"]:
        for proc in ["postprocessed"]:
            for ep in np.unique(np.sort(np.append(SAVING_SCHEDULE, [self.config_general["epochs"]]))):
                if ep > self.config_general["epochs"]:
                    break
                self.output()["histograms"][proc][ep][0].touch()
                self.output()["comparison"][proc][ep][0].touch()

                gen_out = self.input()["generated"]["train"]["x"][ep].load()
                try:
                    gen_data = gen_out["data"]
                except KeyError as e:
                    print("Error occoured:")
                    print(e)
                    print("Breaking")
                    break
                sim_data = reshape_to_particles(sim_data)
                gen_data = reshape_to_particles(gen_data)

                if proc == "postprocessed" and not (self.particle_process == "Z"):
                    """
                    do eta-cut
                    """
                    if self.particle_process in ["Zmumu", "Zmumu_sorted"]:
                        ETA_CUT = 2.4
                        PT_CUT = 10.0
                    elif self.particle_process in ["Zee", "Zee_sorted"]:
                        ETA_CUT = 2.5
                        PT_CUT = 10.0
                    elif self.particle_process in ["zbb_gen_01"]:
                        ETA_CUT = 10.
                        PT_CUT = 0.0
                    else:
                        raise ValueError("Particle-process was {}".format(self.particle_process))
                    gen_eta = eta(gen_data)[..., 0:2, :]
                    """
                    Delete under eta cut
                    """
                    idx_delete = np.where(np.abs(gen_eta) > ETA_CUT)[0]
                    print("Epoch: {0}".format(ep))
                    print(
                        "ETA-CUT:deleting {0} elements of {1}.\n{2:1.3f}\n".format(
                            len(idx_delete), len(gen_data), float(len(idx_delete) / len(gen_data)),
                        )
                    )
                    gen_data = np.delete(gen_data, idx_delete, axis=0)
                    """
                    Delete under pt cut
                    """
                    gen_pt = pt(gen_data)[..., 0:2, :]
                    idx_delete = np.where(np.abs(gen_pt) < PT_CUT)[0]
                    print(
                        "PT-CUT:deleting {0} elements of {1}.\n{2:1.3f}\n".format(
                            len(idx_delete), len(gen_data), float(len(idx_delete) / len(gen_data)),
                        )
                    )
                    gen_data = np.delete(gen_data, idx_delete, axis=0)
                assert sim_data.shape[-1] == gen_data.shape[-1]

                # combined_dict = {}
                combined_dict = {
                    "combined_idx": self.config_data["combined_particles"]["idx"],
                    "combined_names": self.config_data["combined_particles"]["names"],
                    "combined_latex": self.config_data["combined_particles"]["latex"],
                }
                if proc == "vanilla":
                    gen_pass = gen_out
                else:
                    gen_pass = None
                print("Plotting generated distribution")
                # skip this for now to save memory
                if False:
                    plot_distribution(
                        gen_data,
                        self.output()["histograms"][proc][ep][0].path,
                        data_name=self.particle_process,
                        tag=tag,
                        particle_names=self.config_data["particle_names"]["names"],
                        particle_latex=self.config_data["particle_names"]["latex"],
                        file_ext="png",
                        inrestframe=True,
                        **combined_dict,
                    )
                if not (self.particle_process == "Z"):
                    for key, name, no_combine in zip(
                        ["particles_rest_0", "particles_rest_1", "data"], ["rest_0", "rest_1", "boosted"], [False, False, True],
                    ):
                        if not (name == "data"):
                            pnames = self.config_data["particle_names"]["names"][0:2]
                            platex = self.config_data["particle_names"]["latex"][0:2]
                        else:
                            pnames = self.config_data["particle_names"]["names"]
                            platex = self.config_data["particle_names"]["latex"]
                        plot_isotropy(
                            gen_out[key],
                            self.output()["histograms"][proc][ep][0].path,
                            data_name=name,
                            tag=tag,
                            particle_names=pnames,
                            particle_latex=platex,
                            no_combine=no_combine,
                            **combined_dict
                        )
                print("Plotting comparison of generated distribution and simulation")
                compare_distributions(
                    gen_data,
                    sim_data,
                    "Gen",
                    "Sim",
                    self.output()["comparison"][proc][ep][0].path,
                    tag=tag,
                    particle_names=self.config_data["particle_names"]["names"],
                    particle_latex=self.config_data["particle_names"]["latex"],
                    file_ext="png",
                    inrestframe=True,
                    **combined_dict
                )
        self.config = {"general": self.config_general,
                       "data": self.config_data,
                       "generator": self.architecture_config_gen,
                       "critic": self.architecture_config_crit,
                       }
        self.config = sanitize_input(self.config)
        self.output()["config"].dump(self.config, indent=4)

    def store_parts(self):
        return super(CreateHistograms, self).store_parts() + ()


class CreateDataSummary(BaseData):
    """
    plots histograms for a datadistribution
    """

    def requires(self):
        self.dataset = "best"
        return {"simulated": SimulatedData.req(self)}

    def output(self):
        return law.LocalDirectoryTarget(self.local_path("histograms"))

    def run(self):
        from utils.plotting.physics import plot_distribution
        from utils.processing.physics_observables import reshape_to_particles

        if not os.path.isdir(self.output().path):
            os.makedirs(self.output().path)

        dataset_sim = Dataset(
            self.input()["simulated"], self, data_split=True, folds=self.k_split_folds, stage=self.k_split_number, seed=GLOBAL_SEED,
        )

        sim_data = dataset_sim.data["x_train"]
        sim_data = reshape_to_particles(sim_data)
        combined_dict = {
            "combined_idx": self.config_data["combined_particles"]["idx"],
            "combined_names": self.config_data["combined_particles"]["names"],
            "combined_latex": self.config_data["combined_particles"]["latex"],
        }

        plot_distribution(
            sim_data,
            self.output().path,
            data_name=self.particle_process,
            tag="Sim-Data",
            particle_names=self.config_data["particle_names"]["names"],
            particle_latex=self.config_data["particle_names"]["latex"],
            **combined_dict,
            colour="orange",
            inrestframe=True,
        )


class SimilarityDivergence(BaseData, BaseGAN, HTCondorWorkflow, law.LocalWorkflow):
    def create_branch_map(self):
        return [0]

    def output(self):
        return self.local_target("scores.pkl")

    def requires(self):
        self.dataset = "best"
        return {
            "generated": GeneratedData.req(self),
            "simulated": SimulatedData.req(self),
        }

    def run(self):
        from utils.processing.compare_datasets import similarity_score

        dataset_sim = Dataset(
            self.input()["simulated"], self, data_split=True, folds=self.k_split_folds, stage=self.k_split_number, seed=GLOBAL_SEED,
        )
        sim_data = dataset_sim.data["x_train"]
        gen_data = self.input()["generated"]["train"]["x"][self.config_general["epochs"]].load()
        if not (gen_data == np.array([False])).all():
            scores = similarity_score(gen_data, sim_data, self.config_data["combined_particles"]["idx"])
        else:
            scores = np.array([1e6 for i in range(3)])
        self.output().parent.touch()
        self.output().dump(scores)


class CreateMassHistogram(CreateHistograms):
    def requires(self):
        self.dataset = "best"
        return {
            "generated": GeneratedMass.req(self),
            "simulated": SimulatedData.req(self),
        }

    def output(self):
        ret = {}
        for ep in np.unique(np.sort(np.append(SAVING_SCHEDULE, [self.config_general["epochs"]]))):
            if ep > self.config_general["epochs"]:
                break
            else:
                ret[ep] = (law.LocalDirectoryTarget(self.local_path("{}").format(ep)),)
        return ret

    def run(self):
        from utils.plotting.physics import plot_hist_ratio

        dataset_sim = Dataset(
            self.input()["simulated"], self, data_split=True, folds=self.k_split_folds, stage=self.k_split_number, seed=GLOBAL_SEED,
        )
        sim_data = dataset_sim.data["x_train"]
        if "final" in self.version.lower():
            tag = ""
        else:
            tag = self.version

        for ep in np.unique(np.sort(np.append(SAVING_SCHEDULE, [self.config_general["epochs"]]))):
            if ep > self.config_general["epochs"]:
                break
            self.output()[ep][0].touch()

            gen_out = self.input()["generated"]["train"]["x"][ep].load()
            try:
                gen_data = gen_out["data"]
            except KeyError as e:
                print("Error occoured:")
                print(e)
                print("Breaking")
                break

            p_dicts = [
                {"feature_name": "m",
                    "feature_latex": "$m$",
                    "plot_name": "m",
                    "plot_dict": {"config": "symmetric", "legend_loc": 1, "n_bins": 600},},
                {
                    "feature_name": "m",
                    "plot_name": "m2",
                    "feature_latex": "$m$",
                    "plot_dict": {"config": "long_mass", "legend_loc": 1, "ratio_ylim": 2.0, "n_bins": 600,},
                },
            ]

            for p_dict in p_dicts:
                p_dict["tag"] = tag
                p_dict["histtype"] = "step"
                fname = os.path.join(self.output()[ep][0].path, p_dict["plot_name"])
                plot_hist_ratio([gen_data, sim_data], ["Gen", "Sim"], "m", fname, **p_dict, **p_dict["plot_dict"])


class CreateMassGANSummary(CreateWGANSummary):
    def requires(self):
        return TrainMass.req(self)
