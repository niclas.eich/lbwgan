import luigi
import law
import numpy as np
from analysis.tasks_summaries import CreateHistograms, CreateWGANSummary
from utils.processing.hyperparameters import draw_random_gan_params
from utils.processing.sanitize import sanitize_input
from analysis.tasks_base import BaseTask
from analysis.tasks_summaries import SimilarityDivergence


class RandomHyperSearch(BaseTask):

    n_iteration = luigi.IntParameter(default=10)
    n_parallel = luigi.IntParameter(default=2)
    epochs = luigi.IntParameter(default=10)

    def requires(self):
        if self.n_iteration == 0:
            return None
        else:
            return RandomHyperSearch.req(self, n_iteration=self.n_iteration - 1)

    def output(self):
        return self.local_target("runs_{}.pkl".format(self.n_iteration))

    def run(self):
        print("Running jobs with index {}".format(self.n_iteration))
        # redoing it with 124 instead of 123
        param_dicts = draw_random_gan_params(
            self.n_parallel, self.epochs, seed=int(125 + self.n_iteration)
        )
        assert len(param_dicts) == self.n_parallel
        out = yield [
            SimilarityDivergence.req(self, **param_dict) for param_dict in param_dicts
        ]
        rets = {}
        for i in range(self.n_parallel):
            rets[i] = {
                "config": param_dicts[i],
                "scores": out[i]["collection"][0].load(),
            }
        self.output().parent.touch()
        self.output().dump(rets)


class PlotHyperSearch(BaseTask):

    n_iteration = luigi.IntParameter(default=10)
    n_parallel = luigi.IntParameter(default=2)
    epochs = luigi.IntParameter(default=10)

    n_best = luigi.IntParameter(default=10)

    def requires(self):
        return [
            RandomHyperSearch.req(self, n_iteration=i) for i in range(self.n_iteration)
        ]

    def run(self):
        scores = []
        configs = []

        failed_trainings = 0
        for i in range(self.n_iteration):
            inp = self.input()[i].load()
            for j in range(len(inp)):
                sc = np.array(inp[j]["scores"])
                if len(sc) == 3:
                    sc = np.ones(19) * 1e6
                    failed_trainings += 1
                scores.append(sc)
            for j in range(len(inp)):
                configs.append(inp[j]["config"])

        print(
            "In total {} of {} trainings failed due to NaNs".format(
                failed_trainings, len(scores)
            )
        )
        scores = np.array(scores)
        configs = np.array(configs)
        means = np.squeeze(np.mean(scores, axis=-1))
        stds = np.squeeze(np.mean(scores, axis=-1))
        if self.n_parallel * self.n_iteration <= self.n_best:
            best = np.argsort(means)
        else:
            best = np.argsort(means)[: self.n_best]
        print("Best configs had index:\n", best)
        configs = configs[best]
        _ = yield [CreateHistograms.req(self, **config) for config in configs]
        _ = yield [CreateWGANSummary.req(self, **config) for config in configs]


class RunWithoutDetAcceptance(BaseTask):
    epochs = luigi.IntParameter(default=10)
    n_best = luigi.IntParameter(default=10)
    n_iteration = 13

    def requires(self):
        return [
            RandomHyperSearch.req(
                self, version="ml4jets_big_random_search_full", n_iteration=i
            )
            for i in range(0, 13)
        ]

    def run(self):
        scores = []
        configs = []

        failed_trainings = 0
        for i in range(self.n_iteration):
            inp = self.input()[i].load()
            for j in range(len(inp)):
                sc = np.array(inp[j]["scores"])
                if len(sc) == 3:
                    sc = np.ones(19) * 1e6
                    failed_trainings += 1
                scores.append(sc)
            for j in range(len(inp)):
                configs.append(inp[j]["config"])

        print(
            "In total {} of {} trainings failed due to NaNs".format(
                failed_trainings, len(scores)
            )
        )
        scores = np.array(scores)
        configs = np.array(configs)
        means = np.squeeze(np.mean(scores, axis=-1))
        stds = np.squeeze(np.mean(scores, axis=-1))
        if len(configs) <= self.n_best:
            best = np.argsort(means)
        else:
            best = np.argsort(means)[: self.n_best]
        print("Best configs had index:\n", best)
        configs = configs[best]
        # change configs
        for config in configs:
            config["architecture_config_gen"]["param_network"] = None
            print("\n" * 6)
            print("config:")
            print(config)
        # submit
        _ = yield [CreateHistograms.req(self, **config) for config in configs]


class RunZee(BaseTask):
    epochs = luigi.IntParameter(default=2000)
    n_best = luigi.IntParameter(default=10)
    n_iteration = 13

    def requires(self):
        return [
            RandomHyperSearch.req(
                self, version="ml4jets_big_random_search_full", n_iteration=i
            )
            for i in range(0, 13)
        ]

    def run(self):
        scores = []
        configs = []

        failed_trainings = 0
        for i in range(self.n_iteration):
            inp = self.input()[i].load()
            for j in range(len(inp)):
                sc = np.array(inp[j]["scores"])
                if len(sc) == 3:
                    sc = np.ones(19) * 1e6
                    failed_trainings += 1
                scores.append(sc)
            for j in range(len(inp)):
                configs.append(inp[j]["config"])

        print(
            "In total {} of {} trainings failed due to NaNs".format(
                failed_trainings, len(scores)
            )
        )
        scores = np.array(scores)
        configs = np.array(configs)
        means = np.squeeze(np.mean(scores, axis=-1))
        stds = np.squeeze(np.mean(scores, axis=-1))
        if len(configs) <= self.n_best:
            best = np.argsort(means)
        else:
            best = np.argsort(means)[: self.n_best]
        print("Best configs had index:\n", best)
        configs = configs[best]
        # change configs
        for config in configs:
            config["particle_process"] = "Zee"
            config["architecture_config_gen"]["pdu"]["mass_init"] = [0.000511, 0.000511]
            print("\n" * 6)
            print("config:")
            print(config)
        # submit
        _ = yield [CreateHistograms.req(self, **config) for config in configs]


class RunMoreEpochs(BaseTask):
    n_best = luigi.IntParameter(default=10)
    n_iteration = 13

    def requires(self):
        return [
            RandomHyperSearch.req(
                self, version="ml4jets_big_random_search_full", n_iteration=i
            )
            for i in range(0, 13)
        ]

    def run(self):
        scores = []
        configs = []

        failed_trainings = 0
        for i in range(self.n_iteration):
            inp = self.input()[i].load()
            for j in range(len(inp)):
                sc = np.array(inp[j]["scores"])
                if len(sc) == 3:
                    sc = np.ones(19) * 1e6
                    failed_trainings += 1
                scores.append(sc)
            for j in range(len(inp)):
                configs.append(inp[j]["config"])

        print(
            "In total {} of {} trainings failed due to NaNs".format(
                failed_trainings, len(scores)
            )
        )
        scores = np.array(scores)
        configs = np.array(configs)
        means = np.squeeze(np.mean(scores, axis=-1))
        stds = np.squeeze(np.mean(scores, axis=-1))
        if len(configs) <= self.n_best:
            best = np.argsort(means)
        else:
            best = np.argsort(means)[: self.n_best]
        print("Best configs had index:\n", best)
        configs = configs[best]
        # change configs
        for config in configs:
            config["config_general"]["epochs"] = 4000
            print(config)
        # submit
        _ = yield [CreateHistograms.req(self, **config) for config in configs]
