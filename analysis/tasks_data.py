import os
import law
import luigi
import numpy as np
from tensorflow import set_random_seed
from analysis.tasks_main import TrainWGAN, TrainMass
from analysis.tasks_data_external import SimulatedData
from analysis.tasks_base import (
    BaseTask,
    BaseData,
    BaseGAN,
    BaseNetwork,
    GLOBAL_SEED,
    HTCondorBaseTask,
)
from analysis.framework import HTCWGPU
from model.base_model import SAVING_SCHEDULE

law.contrib.load("numpy")


class GeneratedData(BaseData, BaseGAN, HTCWGPU, law.LocalWorkflow):
    n_generated = luigi.IntParameter(default=450000, significant=False)
    k_split_folds = luigi.IntParameter(default=2)
    k_split_number = luigi.IntParameter(default=0)

    def create_branch_map(self):
        return {i: i for i in range(self.k_split_folds)}

    def requires(self):
        ret = {"model": TrainWGAN.req(self)}
        if "detector_acceptance_only" in self.architecture_config_gen["callbacks"]:
            ret["data"] = SimulatedData.req(self)
        return ret

    def output(self):
        ret = {"train": {"x": {}, "y": {}}}
        # generate for all saved models
        for ep in np.unique(np.sort(np.append(SAVING_SCHEDULE, [self.config_general["epochs"]]))):
            if ep <= self.config_general["epochs"]:
                ret["train"]["x"][ep] = self.local_target("ep_{}/x_gen.npz".format(ep))
                if self.particle_process == "ttH" or self.particle_process == "ttbb":
                    ret["train"]["y"] = self.local_target("ep_{}/y_gen.npz")
            else:
                break
        return ret

    def run(self):
        import tensorflow as tf
        from model.models import WGAN, DetectorAcceptanceWGAN
        from model.evaluater import (
            WGANGenerator,
            pdu_tensor_names,
            DetectorAcceptanceEvaluater,
        )

        set_random_seed(self.seed)
        # This is the config reagarding the tensorflow graph
        self.config = self.input()["model"]["config"].load()
        model_succeeded = self.input()["model"]["success"].load()["status"]
        """
        create needed directories
        """
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        # config.gpu_options.per_process_gpu_memory_fraction = 0.45
        if "detector_acceptance_only" in self.architecture_config_gen["callbacks"]:
            from utils.processing.dataset import Dataset

            dataset = Dataset(
                self.input()["data"],
                self,
                preprocessing=True,
                validation_split=0.1,
                data_split=True,
                folds=self.k_split_folds,
                stage=self.k_split_number,
                seed=GLOBAL_SEED,
            )
            self.config_data["scaling_factor"] = float(dataset.scaling_factor)
            data = dataset.data
        """
        Generate Noise for sampling
        and additionally empty "TrainingData", because the tensor needs some (poor guy)
        """
        for ep in np.unique(np.sort(np.append(SAVING_SCHEDULE, [self.config_general["epochs"]]))):
            if model_succeeded is True:
                if ep > self.config_general["epochs"]:
                    break
                tf.reset_default_graph()
                with tf.Session(config=config) as sess:
                    if "detector_acceptance_only" in self.architecture_config_gen["callbacks"]:
                        wgan = DetectorAcceptanceWGAN(self.config, load=True, load_epoch=ep)
                        generator = DetectorAcceptanceEvaluater(sess, wgan, self.config, data)
                    else:
                        wgan = WGAN(self.config, load=True, load_epoch=ep)
                        generator = WGANGenerator(sess, wgan, self.config)
                    # WGAN.load(sess)
                    generated_data, pdu_distrs = generator.evaluate(self.n_generated)
                    # reshape data into (n, 32)
                    generated_data = np.reshape(generated_data, (-1, generated_data.shape[-1]))
                    print("Generated_data:")
                    print("MEAN: {0:1.2f}".format(np.mean(generated_data)))
                    print("STD: {0:1.2f}".format(np.std(generated_data)))
                    print("Rescaling data by: {}".format(self.config["data"]["scaling_factor"]))
                    generated_data = generated_data * self.config["data"]["scaling_factor"]
                    print("Scaled Generated_data:")
                    print("MEAN: {0:1.2f}".format(np.mean(generated_data)))
                    print("STD: {0:1.2f}".format(np.std(generated_data)))
                out = {"data": generated_data}
                if pdu_distrs is not None:
                    for i, pt in enumerate(pdu_tensor_names):
                        if pdu_distrs[i] == np.array([]):
                            out[pt] = None
                        else:
                            if len(pdu_distrs[i].shape) == 3:
                                out[pt] = np.reshape(pdu_distrs[i], (-1, pdu_distrs[i].shape[-1])) * self.config["data"]["scaling_factor"]

                            elif len(pdu_distrs[i].shape) == 4:
                                out[pt] = (
                                    np.reshape(pdu_distrs[i], (-1, pdu_distrs[i].shape[-2], pdu_distrs[i].shape[-1],),)
                                    * self.config["data"]["scaling_factor"]
                                )
                else:
                    for pt in pdu_tensor_names:
                        out[pt] = None
                self.output()["train"]["x"][ep].parent.touch()
                self.output()["train"]["x"][ep].dump(**out)
                if self.particle_process == "ttH":
                    generated_label = np.ones(len(generated_data))
                    self.output()["train"]["y"][ep].dump(generated_label)
                elif self.particle_process == "ttbb":
                    generated_label = np.zeros(len(generated_data))
                    self.output()["train"]["y"][ep].dump(generated_label)
            else:
                if ep > self.config_general["epochs"]:
                    break
                print("Model failed during Training. Saving dummy file")
                self.output()["train"]["x"][ep].parent.touch()
                self.output()["train"]["x"][ep].dump(np.array([False]))
        return

    def store_parts(self):
        return super(GeneratedData, self).store_parts() + ()


class GeneratedMass(GeneratedData):
    def requires(self):
        ret = {"model": TrainMass.req(self)}
        return ret

    def run(self):
        import tensorflow as tf
        from model.models import MassWGAN
        from model.evaluater import WGANGenerator

        set_random_seed(self.seed)
        # This is the config reagarding the tensorflow graph
        self.config = self.input()["model"]["config"].load()
        model_succeeded = self.input()["model"]["success"].load()["status"]
        """
        create needed directories
        """
        if not (os.path.isdir(self.local_path())):
            try:
                os.makedirs(self.local_path())
            except OSError:
                pass
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        # config.gpu_options.per_process_gpu_memory_fraction = 0.45
        if "detector_acceptance_only" in self.architecture_config_gen["callbacks"]:
            from utils.processing.dataset import Dataset

            dataset = Dataset(
                self.input()["data"],
                self,
                preprocessing=True,
                validation_split=0.1,
                data_split=True,
                folds=self.k_split_folds,
                stage=self.k_split_number,
                seed=GLOBAL_SEED,
            )
            self.config_data["scaling_factor"] = float(dataset.scaling_factor)
            data = dataset.data
        """
        Generate Noise for sampling
        and additionally empty "TrainingData", because the tensor needs some (poor guy)
        """
        for ep in np.unique(np.sort(np.append(SAVING_SCHEDULE, [self.config_general["epochs"]]))):
            if model_succeeded is True:
                if ep > self.config_general["epochs"]:
                    break
                tf.reset_default_graph()
                with tf.Session(config=config) as sess:
                    wgan = MassWGAN(self.config, load=True, load_epoch=ep)
                    generator = WGANGenerator(sess, wgan, self.config)
                    # WGAN.load(sess)
                    generated_data, pdu_distrs = generator.evaluate(self.n_generated)
                    # reshape data into (n, 32)
                    generated_data = np.reshape(generated_data, (-1, generated_data.shape[-1]))
                    print("Generated_data:")
                    print("MEAN: {0:1.2f}".format(np.mean(generated_data)))
                    print("STD: {0:1.2f}".format(np.std(generated_data)))
                    print("Rescaling data by: {}".format(self.config["data"]["scaling_factor"]))
                    generated_data = generated_data * self.config["data"]["scaling_factor"]
                    print("Scaled Generated_data:")
                    print("MEAN: {0:1.2f}".format(np.mean(generated_data)))
                    print("STD: {0:1.2f}".format(np.std(generated_data)))
                out = {"data": generated_data}
                self.output()["train"]["x"][ep].parent.touch()
                self.output()["train"]["x"][ep].dump(**out)
            else:
                if ep > self.config_general["epochs"]:
                    break
                print("Model failed during Training. Saving dummy file")
                self.output()["train"]["x"][ep].parent.touch()
                self.output()["train"]["x"][ep].dump(np.array([False]))
        return


class AnyData(BaseData, BaseGAN, BaseNetwork, BaseTask):
    """
    General Data-Task, that gives the same objects for 
    simulated/generated,
    ttH/ttbb,
    low/high/mixed,
    reco/best
    """

    def requires(self):
        if self.dataorigin == "sim":
            return SimulatedData.req(self, dataset=self.dataset, particle_process=self.particle_process)
        elif self.dataorigin == "gen":
            return (
                GeneratedData.req(
                    self,
                    layers=self.layers,
                    lbn=self.lbn,
                    nodes=self.nodes,
                    activation=self.activation,
                    input_dim=self.input_dim,
                    lbn_features=self.lbn_features,
                    epochs=self.epochs,
                    particle_process=self.particle_process,
                    dataset=self.dataset,
                    dataorigin="sim",
                    datapolicy=self.datapolicy,
                    optimizer=self.optimizer,
                    normal_dist=self.normal_dist,
                    GP=self.GP,
                    n_crit=self.n_crit,
                    n_gen=self.n_gen,
                    branch=self.k_split_number,
                ),
            )

    def output(self):
        if self.dataorigin == "sim":
            return self.input()[self.datapolicy]
        elif self.dataorigin == "gen":
            return self.input()["collection"].targets[self.k_split_number][self.datapolicy]
