import luigi
import law
import os
from analysis.framework import HTCondorWorkflow
from collections import OrderedDict
import numpy as np

law.contrib.load("slack")

GLOBAL_SEED = 30372
_all_particle_processes = {
    "ttH": {
        "names": ["bhad", "blep", "lj1", "lj2", "bj1", "bj2", "lep1", "nu1"],
        "latex": ["$b_{had}$", "$b_{lep}$", "$lj_{1}$", "$lj_{2}$", "$bj_{1}$", "$bj_{2}$", "$lep_{1}$", "$\\nu_{1}$",],
        "combined_particles": [[1, 6, 7], [4, 5], [0, 2, 3], [6, 7], [2, 3]],
        "combined_names": ["top_lep", "Higgs", "top_had", "W-", "W+"],
        "combined_latex": ["$\\bar{t}_{lep}$", "$H$", "$t_{had}$", "$W^-$", "$W^+$"],
    },
    "ttbb": {
        "names": ["bhad", "blep", "lj1", "lj2", "bj1", "bj2", "lep1", "nu1"],
        "latex": ["$b_{had}$", "$b_{lep}$", "$lj_{1}$", "$lj_{2}$", "$bj_{1}$", "$bj_{2}$", "$lep_{1}$", "$\\nu_{1}$",],
        "combined_particles": [[1, 6, 7], [4, 5], [0, 2, 3], [6, 7], [2, 3]],
        "combined_names": ["top_lep", "gluon", "top_had", "W-", "W+"],
        "combined_latex": ["$\\bar{t}_{lep}$", "$g$", "$t_{had}$", "$W^-$", "$W^+$"],
    },
    "Zee": {
        "names": ["e1", "e2", "Z"],
        "latex": ["$e_1$", "$e_2$", "$Z$"],
        "combined_particles": [],
        "combined_names": [],
        "combined_latex": [],
    },
    "Zmumu": {
        "names": ["mu1", "mu2", "Z"],
        "latex": ["$\mu_1$", "$\mu_2$", "$Z$"],
        "combined_particles": [],
        "combined_names": [],
        "combined_latex": [],
        # "combined_particles": [[0, 1]],
        # "combined_names": ["Z"],
        # "combined_latex": ["$Z$"],
    },
    "Zbb": {
        "names": ["b1", "b2"],
        "latex": ["$b_1$", "$b_2$"],
        "combined_particles": [[0, 1]],
        "combined_names": ["Z"],
        "combined_latex": ["$Z$"],
    },
    "Zll": {
        "names": ["l1", "l2"],
        "latex": ["$lep_1$", "$lep_2$"],
        "combined_particles": [[0, 1]],
        "combined_names": ["Z"],
        "combined_latex": ["$Z$"],
    },
    "Z": {"names": ["Z"], "latex": ["$Z$"], "combined_particles": [], "combined_names": [], "combined_latex": [],},
    "Z_mass": {"names": ["m"], "latex": ["$m$"], "combined_particles": [], "combined_names": [], "combined_latex": [],},
    "Zee_no_detector": {
        "names": ["e1", "e2"],
        "latex": ["$e_1$", "$e_2$"],
        "combined_particles": [[0, 1]],
        "combined_names": ["Z"],
        "combined_latex": ["$Z$"],
    },
    "Zmumu_no_detector": {
        "names": ["mu1", "mu2"],
        "latex": ["$\mu_1$", "$\mu_2$"],
        "combined_particles": [[0, 1]],
        "combined_names": ["Z"],
        "combined_latex": ["$Z$"],
    },
    "Zll_no_detector": {
        "names": ["l1", "l2"],
        "latex": ["$lep_1$", "$lep_2$"],
        "combined_particles": [[0, 1]],
        "combined_names": ["Z"],
        "combined_latex": ["$Z$"],
    },
    "Z_no_detector": {"names": ["Z"], "latex": ["$Z$"], "combined_particles": [], "combined_names": [], "combined_latex": [],},
    "zee_01": {
        "names": ["e_p", "e_m", "Z"],
        "latex": ["$e^+$", "$e^-$", "$Z$"],
        "combined_particles": [],
        "combined_names": [],
        "combined_latex": [],
    },
    "zmumu_01": {
        "names": ["mu_p", "mu_m", "Z"],
        "latex": ["$\mu^+$", "$\mu^-$", "$Z$"],
        "combined_particles": [],
        "combined_names": [],
        "combined_latex": [],
    },
    "Zmumu_sorted": {
        "names": ["mu_m", "mu_p", "Z"],
        "latex": ["$\mu^-$", "$\mu^+$", "$Z$"],
        "combined_particles": [],
        "combined_names": [],
        "combined_latex": [],
    },
    "Zee_sorted": {
        "names": ["mu_m", "mu_p", "Z"],
        "latex": ["$e^-$", "$e^+$", "$Z$"],
        "combined_particles": [],
        "combined_names": [],
        "combined_latex": [],
    },
    "zbb_gen_01": {
        "names": ["b", "b_", "Z"],
        "latex": ["$b$", "$\\bar{b}$", "$Z$"],
        "combined_particles": [],
        "combined_names": [],
        "combined_latex": [],
    },
}


def cut_string(s, l=3):
    s = str(s)
    if len(s) <= l:
        return s
    else:
        return s[0:l]


def parameter_path(param_dict, name=""):
    ret = []
    for key, value in sorted(param_dict.items()):
        if "dir" in key:
            continue
        elif isinstance(value, list):
            tag = "_".join(str(x) for x in value).replace(" ", "_")
        elif isinstance(value, tuple):
            tag = "_".join(str(x) for x in value).replace(" ", "_")
        elif isinstance(value, int):
            tag = "{}".format(value)
        elif isinstance(value, float):
            if value == 0.0:
                tag = "0.0"
            elif abs(value) > 1e-3:
                tag = "{0:1.2f}".format(value)
            elif abs(value) > 1e-5:
                tag = "{0:1.5f}".format(value)
        elif isinstance(value, bool):
            tag = "{}".format("on" if value else "off")
        elif isinstance(value, str):
            tag = "{}".format(value)
        elif isinstance(value, dict):
            # recursion, yay
            tag = "_".join(str(x) for x in parameter_path(value))
        elif isinstance(value, luigi.freezing.FrozenOrderedDict):
            # recursion, yay
            tag = "_".join(str(x) for x in parameter_path(value))
        elif value is None:
            tag = "None"
        else:
            raise NotImplementedError("Item {} of type {} not supported.".format(key, type(value)))
        ret += ["{}{}_{}".format(name, cut_string(key), tag)]
    return ret


class BaseTask(law.Task):
    """
    Base task, from which every subtask inherits its basic functionality
    """

    # task_namespace = 'lbwgan'
    version = luigi.Parameter()
    seed = luigi.IntParameter(default=GLOBAL_SEED)
    test = luigi.BoolParameter()
    verbose = luigi.BoolParameter(default=False, significant=False)
    notify = law.slack.NotifySlackParameter(default=False)

    def store_parts(self):
        return (self.__class__.__name__, self.version, "seed_{}".format(self.seed))

    def local_path(self, *path):
        parts = (os.getenv("OUT_PATH"),) + self.store_parts() + path
        return os.path.join(*parts)

    def local_target(self, *path):
        return law.LocalFileTarget(self.local_path(*path))


class BaseData(BaseTask):
    """
    Just important for the dataset sorting Generator vs p_t
    """

    dataset = luigi.ChoiceParameter(choices=["reco", "best"], default="best")
    dataorigin = luigi.ChoiceParameter(choices=["sim", "gen"], default="sim")
    datapolicy = luigi.ChoiceParameter(choices=["train", "test"], default="train")
    particle_process = luigi.ChoiceParameter(
        choices=[
            "ttH",
            "ttbb",
            "Zee",
            "Zmumu",
            "Zll",
            "Z_mass",
            "Z",
            "Zbb",
            "Zee_no_detector",
            "Zmumu_no_detector",
            "Zee_sorted",
            "Zmumu_sorted",
            "Zll_no_detector",
            "Z_no_detector",
            "zee_01",
            "zmumu_01",
            "zbb_gen_01",
        ]
    )
    particle_indexes = law.CSVParameter(cls=luigi.IntParameter, default=[0, 1, 2, 3, 4, 5, 6, 7])
    k_split_folds = luigi.IntParameter(default=2)
    k_split_number = luigi.IntParameter(default=0)

    def __init__(self, **kwargs):
        super(BaseData, self).__init__(**kwargs)
        if len(self.particle_indexes) > len(_all_particle_processes[self.particle_process]["names"]):
            self.particle_indexes = [i for i in range(len(_all_particle_processes[self.particle_process]["names"]))]

        comb_idx = []
        comb_names = []
        comb_latex = []
        for i, idx in enumerate(_all_particle_processes[self.particle_process]["combined_particles"]):
            if set(idx) <= set(self.particle_indexes):
                comb_idx.append([self.particle_indexes.index(ip) for ip in idx])
                comb_names.append(_all_particle_processes[self.particle_process]["combined_names"][i])
                comb_latex.append(_all_particle_processes[self.particle_process]["combined_latex"][i])

        names = [_all_particle_processes[self.particle_process]["names"][i] for i in self.particle_indexes]
        latex = [_all_particle_processes[self.particle_process]["latex"][i] for i in self.particle_indexes]
        self.config_data = OrderedDict(
            {
                "particle_process": self.particle_process,
                "input_dim": [(len(self.particle_indexes)) * 4],
                "particle_names": {"names": names, "latex": latex},
                "combined_particles": {"idx": comb_idx, "names": comb_names, "latex": comb_latex,},
            }
        )

    def store_parts(self):
        return super(BaseData, self).store_parts() + (
            "{}_dataset_{}_split_{}_{}".format(self.datapolicy, self.dataset, self.k_split_number, self.k_split_folds),
            "particle_process_{}".format(self.particle_process),
            "particles_{}".format("_".join(map(str, self.particle_process))),
        )


class BaseGAN(BaseTask):
    """
    GAN specific parameters
    """

    defaults_architecture_crit = {
        "layers": 6,
        "nodes": 288,
        "activation": "leaky_relu",
        "batch_norm": False,
        "L2": 0.0,
        "learning_rate": 1e-4,
        "optimizer": "ADAM",
        "lbn": False,
        "lbn_particles": 8,
        "lbn_features": ["E", "px", "py", "pz", "m", "pt"],
        "callbacks": [],
    }

    defaults_architecture_gen = {
        "layers": 6,
        "nodes": 288,
        "activation": "relu",
        "batch_norm": False,
        "L2": 0.0,
        "latent_dim": 30,
        "latent_shape": "normal",
        "learning_rate": 1e-4,
        "optimizer": "ADAM",
        "callbacks": ["random_rotation"],
    }

    defaults_general = {
        "epochs": 1,
        "batch_size": 512,
        "GP": 10,
        "n_crit": 25,
        "n_gen": 1,
    }

    architecture_config_crit = luigi.DictParameter(
        default=defaults_architecture_crit, description="Dictionary that defines the Architecture of a network",
    )
    architecture_config_gen = luigi.DictParameter(
        default=defaults_architecture_gen, description="Dictionary that defines the Architecture of a network",
    )
    config_general = luigi.DictParameter(default=defaults_general, description="Dictionary that defines the Training parametes",)

    def __init__(self, **kwargs):
        """
        if a config is given, the default-dictionary will be overriden
        to solve this, we add all keys that are not present to the specified config 
        """
        super(BaseGAN, self).__init__(**kwargs)
        self.architecture_config_gen = OrderedDict(self.defaults_architecture_gen, **self.architecture_config_gen)
        self.architecture_config_crit = OrderedDict(self.defaults_architecture_crit, **self.architecture_config_crit)
        self.config_general = OrderedDict(self.defaults_general, **self.config_general)

    def store_parts(self):
        ret_gen = parameter_path(self.architecture_config_gen, "gen_")
        ret_crit = parameter_path(self.architecture_config_crit, "crit_")
        ret_general = parameter_path(self.config_general)
        rets = [item for sublist in [ret_gen, ret_crit, ret_general] for item in sublist]
        return super(BaseGAN, self).store_parts() + tuple(rets)


class BaseNetwork(BaseTask):
    """description for BaseNetwork

    activations:
        relu, elu, selu, leaky_relu
    optimizers:
        RMS, ADAM, SGD, ADADELTA
    """

    defaults_architecture = {
        "layers": 6,
        "nodes": 288,
        "activation": "relu",
        "batch_norm": False,
        "L2": 0.0,
        "learning_rate": 1e-4,
        "optimizer": "ADAM",
        "lbn": False,
        "lbn_particles": 8,
        "lbn_features": ["E", "px", "py", "pz", "m", "phi", "pt", "eta", "pair_cos"],
        "callbacks": [],
    }

    defaults_general = {
        "epochs": 50,
        "batch_size": 256,
    }

    architecture_config = luigi.DictParameter(
        default=defaults_architecture, description="Dictionary that defines the Architecture of a network",
    )
    general_config = luigi.DictParameter(default=defaults_general, description="Dictionary that defines the Training parametes",)

    def __init__(self, **kwargs):
        """
        if a config is given, the default-dictionary will be overriden
        to solve this, we add all keys that are not present to the specified config 
        """
        super(BaseNetwork, self).__init__(**kwargs)
        self.architecture_config = dict(self.defaults_architecture, **self.architecture_config)
        self.config_general = dict(self.defaults_general, **self.config_general)

    def store_parts(self):
        return super(BaseNetwork, self).store_parts() + tuple(map(parameter_path, [self.architecture_config, self.config_general]))


class HTCondorBaseTask(BaseTask, HTCondorWorkflow):
    def __init__(self, **kwargs):
        if self.test is True:
            self.htcondor_test = True
        super(HTCondorBaseTask, self).__init__(**kwargs)
