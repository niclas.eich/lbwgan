#!/usr/bin/env bash

action() {
    # determine the directory of this file
    if [ ! -z "$ZSH_VERSION" ]; then
        local this_file="${(%):-%x}"
    else
        local this_file="${BASH_SOURCE[0]}"
    fi
    local base="$( cd "$( dirname "$this_file" )" && pwd )"
    local law_base="$( dirname "$( dirname "$base" )" )"


    export LBWGAN_BASE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && /bin/pwd )"
    export MNIST_DATA_PATH="/net/scratch/deeplearning/MNIST/"

    export PYTHONPATH="$base:$PYTHONPATH"

    export LAW_HOME="$base/.law"
    export LAW_CONFIG_FILE="$base/law.cfg"
    source $base/data_setup.sh

    source "$( law completion )"
      # print a warning when no luigi scheduler host is set
    export LBWGAN_SCHEDULER_HOST=134.61.19.115
    if [ -z "$LBWGAN_SCHEDULER_HOST" ]; then
        2>&1 echo "NOTE: LBWGAN_SCHEDULER_HOST is not set, use '--local-scheduler' in your tasks!"
    fi
}
action


