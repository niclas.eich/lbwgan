import os
import numpy as np
from utils.processing.physics_observables import (
    feature_collection,
    reshape_to_particles,
    add_combined_particles,
)
from utils.plotting.histograms import (
    plot_hist,
    plot_hist_ratio,
    plot_hist2d,
    plot_hist_map,
)
from utils.plotting.sphere import plot_sky_map
from model.evaluater import pdu_tensor_names

hist1d_features = [
    {"feature_name": "E", "feature_latex": "$E$", "plot_dict": {"config": "positive", "legend_loc": 1, "n_bins": 400},},
    {"feature_name": "px", "feature_latex": "$p_x$", "plot_dict": {"config": "symmetric", "legend_loc": 8, "n_bins": 400},},
    {"feature_name": "py", "feature_latex": "$p_y$", "plot_dict": {"config": "symmetric", "legend_loc": 8, "n_bins": 400},},
    {"feature_name": "pz", "feature_latex": "$p_z$", "plot_dict": {"config": "symmetric", "legend_loc": 1, "n_bins": 400},},
    {"feature_name": "m", "feature_latex": "$m$", "plot_dict": {"config": "symmetric", "legend_loc": 1, "n_bins": 600},},
    {
        "feature_name": "m",
        "plot_name": "m2",
        "feature_latex": "$m$",
        "plot_dict": {"config": "long_mass", "legend_loc": 1, "ratio_ylim": 2.0, "n_bins": 600,},
    },
    {"feature_name": "eta", "feature_latex": "$eta$", "plot_dict": {"config": "symmetric", "legend_loc": 8, "x_unit": "", "n_bins": 200,},},
    {
        "feature_name": "phi",
        "feature_latex": "$\phi$",
        "plot_dict": {"config": "clipped", "legend_loc": 4, "x_axis_pi": True, "x_unit": "", "n_bins": 200,},
    },
    {
        "feature_name": "theta",
        "feature_latex": "$\\theta$",
        "plot_dict": {"config": "clipped", "legend_loc": 4, "x_axis_pi": True, "x_unit": "", "n_bins": 200,},
    },
    {"feature_name": "beta", "feature_latex": "$beta$", "plot_dict": {"config": "clipped", "legend_loc": 2, "x_unit": "", "n_bins": 150,},},
    {
        "feature_name": "cosine_theta_star",
        "feature_latex": "$cos(\\theta^{\\ast})$",
        "plot_dict": {"config": "clipped", "legend_loc": 2, "x_unit": "", "n_bins": 150,},
    },
    {
        "feature_name": "gamma",
        "feature_latex": "$gamma$",
        "plot_dict": {"config": "positive", "legend_loc": 1, "x_unit": "", "n_bins": 150,},
    },
    {"feature_name": "pt", "feature_latex": "$pt$", "plot_dict": {"config": "positive", "legend_loc": 1, "n_bins": 400},},
]

hist2d_features = [
    {"feature_name": "eta", "feature_latex": "$eta$", "plot_dict": {"cmap": "jet"},},
    {"feature_name": "phi", "feature_latex": "$phi$", "plot_dict": {"x_axis_pi": True, "y_axis_pi": True, "cmap": "jet"},},
]

hist1d_features_event = [
    {
        "feature_name": "px_conservation",
        "feature_latex": "$\\frac{\sum p_x}{\sum |p_x|}$",
        "plot_dict": {"config": "symmetric", "x_unit": ""},
    },
    {
        "feature_name": "py_conservation",
        "feature_latex": "$\\frac{\sum p_y}{\sum |p_y|}$",
        "plot_dict": {"config": "symmetric", "x_unit": ""},
    },
]

skymaps = ({"feature_name": "pvec", "feature_latex": "$p$", "plot_dict": {},},)


def compare_distributions(events_a, events_b, name_a, name_b, out_dir="./plots/", tag="", **kwargs):
    """
    compares two distributions of 4-vector particle evetns
    """
    events_a = reshape_to_particles(events_a)
    events_a = add_combined_particles(events_a, kwargs.get("combined_idx", None))

    events_b = reshape_to_particles(events_b)
    events_b = add_combined_particles(events_b, kwargs.get("combined_idx", None))

    particle_names = kwargs.get("particle_names", np.arange(0, events_a.shape[1]))
    particle_latex = kwargs.get("particle_latex", particle_names)
    if kwargs.get("combined_names", None) is not None:
        if len(particle_names) < events_a.shape[1]:
            particle_names += kwargs["combined_names"]
            assert len(particle_names) == events_a.shape[1]
            particle_latex += kwargs.pop("combined_latex", kwargs["combined_names"])
    """
    start plotting interesting metrics
    """
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    data_features_a = feature_collection(events_a)
    data_features_b = feature_collection(events_b)
    """
    in case a pdu is in the model, also take the pdu-distributions
    """
    pdu_dists = {}
    if kwargs.get("gen_out", None) is not None:
        for i, ptn in enumerate(pdu_tensor_names):
            if kwargs["gen_out"][ptn] is not None and not ptn == "masses":
                events = reshape_to_particles(kwargs["gen_out"][ptn])
                events = add_combined_particles(events, kwargs.get("combined_idx", None))
                pdu_dists[ptn] = feature_collection(events)
            elif kwargs["gen_out"][ptn] is not None and ptn == "masses":
                pdu_dists[ptn] = kwargs["gen_out"]["masses"]
            else:
                raise NotImplementedError
    """
    first, plot all features that are defined for every particle
    """
    print("plotting particle features")
    for i_particle, (p_name, p_latex) in enumerate(zip(particle_names, particle_latex)):
        for feature_dict in hist1d_features:
            feature_name = feature_dict["feature_name"]
            if feature_dict.get("plot_name", None) is not None:
                fname = os.path.join(out_dir, "hist1d_{}_{}.{}".format(p_name, feature_dict["plot_name"], kwargs.get("file_ext", "png"),))
            else:
                fname = os.path.join(out_dir, "hist1d_{}_{}.{}".format(p_name, feature_name, kwargs.get("file_ext", "png")))
            if feature_dict["feature_name"] == "cosine_theta_star" and i_particle == 2:
                continue
            data_a = data_features_a[feature_name][:, i_particle]
            data_b = data_features_b[feature_name][:, i_particle]
            p_dict = feature_dict["plot_dict"]
            p_dict["tag"] = tag
            p_dict["histtype"] = "step"
            var_name = p_latex + " " + feature_dict["feature_latex"]
            plot_list = [data_a, data_b]
            plot_names = [name_a, name_b]
            for i, (key, val) in enumerate(pdu_dists.items()):
                if key == "masses":
                    continue
                plot_list.append(val[feature_name][:, i_particle])
                plot_names.append(key)
            if not pdu_dists == {}:
                if feature_name == "m" and i_particle < 2:
                    plot_list.append(pdu_dists["masses"][..., i_particle])
                    plot_names.append("pdu_masses")
            # for p, n in zip(plot_list, plot_names):
            # print("Name: {}\t{}".format(n, p.shape))
            plot_hist_ratio(plot_list, plot_names, var_name, fname, **p_dict)
    print("Finished plotting particle features")
    """
    second, plot all features, that are defined for the whole event
    """
    # print("plotting 1d features")
    # for feature_dict in hist1d_features_event:
    # feature_name = feature_dict["feature_name"]
    # fname = os.path.join(out_dir, "hist1d_{}.{}".format(feature_name, kwargs.get("file_ext", "png")))
    # data_a = data_features_a[feature_name]
    # data_b = data_features_b[feature_name]
    # p_dict = feature_dict["plot_dict"]
    # p_dict["tag"] = tag
    # p_dict["histtype"] = "step"
    # var_name = feature_dict["feature_latex"]
    # if kwargs.get("inrestframe", True):
    # print("DEBUG: in rest!\t{}".format(var_name))
    # else:
    # print("DEBUG: no rest!\t{}".format(var_name))
    # plot_hist([data_a, data_b], [name_a, name_b], var_name, fname, **p_dict)
    # print("Finished plotting 1d features")

    if kwargs.get("inrestframe", False) is True:
        print("start plotting Rest Frame")
        out_dir = os.path.join(out_dir, "in_restframe")
        os.makedirs(out_dir, exist_ok=True)
        from utils.processing.physics_observables import boost

        resonance_a = np.expand_dims(events_a[:, 0] + events_a[:, 1], axis=1)
        mask = np.where(resonance_a[..., 3] < 0)[0]
        resonance_a[mask, ..., 1:] *= -1

        events_a_copy = np.copy(events_a)
        events_b_copy = np.copy(events_b)

        events_a_copy[mask, ..., 1:] *= -1
        boosted_events_a = boost(events_a_copy, resonance_a)

        resonance_b = np.expand_dims(events_b_copy[:, 0] + events_b_copy[:, 1], axis=1)
        mask = np.where(resonance_b[..., 3] < 0)[0]
        resonance_b[mask, ..., 1:] *= -1
        events_b_copy[mask, ..., 1:] *= -1
        boosted_events_b = boost(events_b_copy, resonance_b)
        new_kwargs = {
            "combined_names": None,
            "combined_idx": None,
            "inrestframe": False,
        }
        kwargs.update(new_kwargs)
        print("Plotting comparison in rest frame")
        compare_distributions(boosted_events_a, boosted_events_b, name_a, name_b, out_dir=out_dir, **kwargs)
        print("Finished comparison in rest frame")


def plot_distribution(events, out_dir, data_name="", tag="", **kwargs):
    """
    plots many histograms of 4-vector particle events
    """
    events = reshape_to_particles(events)
    if data_name in ["Zee_sorted", "Zmumu_sorted", "Zee", "Zmumu", "Zbb"]:
        pass
    else:
        events = add_combined_particles(events, kwargs.get("combined_idx", None))
    particle_names = kwargs.get("particle_names", np.arange(0, events.shape[1]))
    particle_latex = kwargs.get("particle_latex", particle_names)
    if kwargs.get("combined_names", None) is not None:
        if len(particle_names) < events.shape[1]:
            particle_names += kwargs["combined_names"]
            assert len(particle_names) == events.shape[1]
            particle_latex += kwargs.pop("combined_latex", kwargs["combined_names"])
    """
    start plotting interesting metrics
    """
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    data_features = feature_collection(events, resonance_last=True)
    """
    first, plot all features that are defined for every particle
    """
    print("Plotting particle features")
    for i_particle, (p_name, p_latex) in enumerate(zip(particle_names, particle_latex)):
        for feature_dict in hist1d_features:
            if feature_dict["feature_name"] == "cosine_theta_star" and i_particle == 2:
                continue
            feature_name = feature_dict["feature_name"]
            fname = os.path.join(out_dir, "hist1d_{}_{}.{}".format(p_name, feature_name, kwargs.get("file_ext", "png")))
            data = data_features[feature_name][:, i_particle]
            p_dict = feature_dict["plot_dict"]
            p_dict["tag"] = tag
            p_dict["histtype"] = "stepfilled"
            p_dict["colour"] = kwargs.get("colour", None)
            var_name = p_latex + " " + feature_dict["feature_latex"]
            plot_hist(data, data_name, var_name, fname, **p_dict)
    """
    second, plot all features, that are defined for the whole event
    """
    # print("Plotting 1d features")
    # for feature_dict in hist1d_features_event:
    # feature_name = feature_dict["feature_name"]
    # fname = os.path.join(out_dir, "hist1d_{}.{}".format(feature_name, kwargs.get("file_ext", "png")))
    # data = data_features[feature_name]
    # p_dict = feature_dict["plot_dict"]
    # p_dict["tag"] = tag
    # p_dict["histtype"] = "stepfilled"
    # p_dict["colour"] = kwargs.get("colour", None)
    # var_name = feature_dict["feature_latex"]
    # if kwargs.get("inrestframe", True):
    # print("DEBUG: in rest!\t{}".format(var_name))
    # else:
    # print("DEBUG: no rest!\t{}".format(var_name))
    # plot_hist(data, data_name, var_name, fname, **p_dict)
    # print("Finished Plotting 1d features")
    """
    third, plot 2d histograms of correlations of combined particles
    """
    if kwargs.get("combined_idx", None) is not None:
        for idx in kwargs["combined_idx"]:
            if len(idx) == 2:
                for feature_dict in hist2d_features:
                    feature_name = feature_dict["feature_name"]
                    fname = os.path.join(
                        out_dir,
                        "hist2d_{}_{}_{}.{}".format(
                            particle_names[idx[0]], particle_names[idx[1]], feature_name, kwargs.get("file_ext", "png")
                        ),
                    )
                    data_a = np.squeeze(data_features[feature_name][:, idx[0]])
                    data_b = np.squeeze(data_features[feature_name][:, idx[1]])
                    var_a = "{} {}".format(particle_latex[idx[0]], feature_name)
                    var_b = "{} {}".format(particle_latex[idx[1]], feature_name)
                    p_dict = feature_dict["plot_dict"]
                    p_dict["tag"] = tag
                    var_name = feature_dict["feature_latex"]
                    plot_hist2d(data_a, data_b, var_a, var_b, fname, **p_dict)
    """
    fourth, plot 2d correlations for all particles
    """
    if events.shape[1] > 1:
        for feature_dict in hist2d_features:
            feature_name = feature_dict["feature_name"]
            fname = os.path.join(out_dir, "multi_hist_{}.".format(feature_name, kwargs.get("file_ext", "png")))
            data = data_features[feature_name]
            p_dict = feature_dict["plot_dict"]
            p_dict["tag"] = tag
            p_dict["suptitle"] = "{} - 2d histograms".format(feature_name)
            p_dict["var_name"] = feature_name
            p_dict["colorbar"] = False
            if feature_name == "phi":
                p_dict["global_colorbar"] = False
                p_dict["norm"] = "half-log"
                p_dict["colorbar"] = True
            elif feature_name == "eta":
                p_dict["global_colorbar"] = True
                p_dict["norm"] = "log"
                p_dict["colorbar"] = False
            plot_hist_map(data, particle_latex, fname, **p_dict)
    print("Finished Plotting 2d features")

    if kwargs.get("inrestframe", False) is True:
        print("Plotting Rest Frame features")
        out_dir = os.path.join(out_dir, "in_restframe")
        os.makedirs(out_dir, exist_ok=True)

        from utils.processing.physics_observables import (
            boost,
            forward_backward_asymmetry_from_angles,
            cosine_theta_star,
            cosine_theta_star_Collins_Soper,
        )

        resonance = np.expand_dims(events[:, 0] + events[:, 1], axis=1)
        mask = np.where(resonance[..., 3] > 0)[0]
        # events_copy = np.copy(events[mask, :])
        events_copy = np.copy(events)
        events_copy[mask, ..., 1:] *= -1
        resonance[mask, ..., 1:] *= -1

        # resonance = np.expand_dims(events[:, 0] + events[:, 1], axis=1)
        # mask = np.where(resonance[..., 3] < 0)[0]
        # resonance[mask, ..., 1:] *= -1
        # events_copy = np.copy(events)
        # events_copy[mask, ..., 1:] *= -1

        boosted_particle = boost(events_copy, resonance)
        # angles = cosine_theta_star(np.expand_dims(boosted_particle[:, 0,], axis=1), resonance)
        angles = cosine_theta_star_Collins_Soper(events_copy, resonance)
        angles  = np.expand_dims(angles, axis=-1)
        # angles = np.squeeze(angles, axis=-1)
        angles *= np.abs(resonance[..., 3]) / resonance[..., 3]

        print("Forward-Backward-Asymmetry:")
        print("Total:\t{0:1.2f}".format(forward_backward_asymmetry_from_angles(angles)))

        import matplotlib.pyplot as plt
        from scipy.stats import binned_statistic

        bins = np.array([50., 60., 70.0, 80.0, 82.5, 85.0, 87.5, 90.0, 92.5, 95.0, 97.5, 100.0, 110.0, 140.0])
        # bins = np.arange(60, 120, 10)
        values, bin_edges, bin_number = binned_statistic(
            boosted_particle[:, 2, 0], np.squeeze(angles), forward_backward_asymmetry_from_angles, bins=bins
        )
        points = (bin_edges[1:] + bin_edges[:-1]) / 2

        x_err_up = np.insert(np.diff(points), 0, 0.0, axis=0) / 2
        x_err_down = np.append(np.diff(points), 0.0) / 2

        fig, ax = plt.subplots(1, 1)
        ax.errorbar(points, values, yerr=None, xerr=[x_err_up, x_err_down], marker="o", linestyle="none")
        ax.grid()
        ax.axvline(x=90, color="black", linestyle="dashed")
        ax.set_xlabel("$\sqrt{s} [GeV]$")
        ax.set_ylabel("$A_{FB}$")
        ax.axhline(y=0, linestyle="dashed", color="black")
        fig.savefig(os.path.join(out_dir, "a_fb"))

        new_kwargs = {
            "combined_names": None,
            "combined_idx": None,
            "inrestframe": False,
        }
        kwargs.update(new_kwargs)
        plot_distribution(boosted_particle, out_dir, data_name=data_name, **kwargs)
        plot_isotropy(boosted_particle, out_dir, data_name=data_name, **kwargs)


def plot_isotropy(events, out_dir, data_name="", tag="", **kwargs):
    """
    plots many histograms of 4-vector particle events
    """
    events = reshape_to_particles(events)
    if kwargs.get("no_combine", False) is True:
        events = add_combined_particles(events, kwargs.get("combined_idx", None))
    particle_names = kwargs.get("particle_names", np.arange(0, events.shape[1]))
    particle_latex = kwargs.get("particle_latex", particle_names)
    if kwargs.get("no_combine", True) is False:
        if kwargs.get("combined_names", None) is not None:
            if len(particle_names) < events.shape[1]:
                particle_names += kwargs["combined_names"]
                assert len(particle_names) == events.shape[1]
                particle_latex += kwargs.pop("combined_latex", kwargs["combined_names"])
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    data_features = feature_collection(events)
    """
    fifth, skymaps 
    """
    for i_particle, (p_name, p_latex) in enumerate(zip(particle_names, particle_latex)):
        for feature_dict in skymaps:
            feature_name = feature_dict["feature_name"]
            fname = os.path.join(out_dir, "skymap_{}_{}_{}.{}".format(data_name, p_name, feature_name, kwargs.get("file_ext", "png")))
            data = data_features[feature_name][:, i_particle]
            p_dict = feature_dict.get("plot_dict", {})
            var_name = p_latex + " " + feature_dict.get("feature_latex", "")
            plot_sky_map(data, fname)
