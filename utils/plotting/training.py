import numpy as np
import pandas as pd
import matplotlib as mpl

mpl.use("agg")
import matplotlib.pyplot as plt
import seaborn as sns

N_MAX_POINTS = 200


def plot_training_history(histories, fname, history_labels=[""], log=False):
    if isinstance(histories, list):
        if len(histories) == 0:
            raise ValueError("You passed an empty history list!")
    else:
        histories = [histories]
        history_labels = [history_labels]
    if len(histories) != len(history_labels):
        raise ValueError("There must be  as many names as histories!")

    fig, ax = plt.subplots(2, 1)
    for results, labels in zip(histories, history_labels):
        n_acc_train = results["acc"]
        n_loss_train = results["loss"]
        n_acc_val = results["val_acc"]
        n_loss_val = results["val_loss"]
        # n_loss_min = np.argmin(n_loss_val)
        # n_acc_max = np.argmax(n_acc_val)
        n_range = range(0, len(n_acc_train))
        ax[0].errorbar(
            n_range, n_acc_val, label="acc-Validation_{}".format(labels), marker="."
        )
        ax[0].errorbar(
            n_range, n_acc_train, label="acc-Training_{}".format(labels), marker="."
        )
        ax[0].set_ylabel("Accuracy", fontsize=25)
        ax[0].set_ylim(0, 1.05)
        # ax[0].axvline(x=n_acc_max, linestyle='dashed', color='black')
        ax[0].grid()
        ax[0].legend(loc="upper right")

        ax[1].errorbar(
            n_range, n_loss_val, label="loss-Validation_{}".format(labels), marker="."
        )
        ax[1].errorbar(
            n_range, n_loss_train, label="loss-Training_{}".format(labels), marker="."
        )
        ax[1].set_ylabel("Loss", fontsize=25)
        ax[1].set_xlabel("n Training-Epoch", fontsize=25)
        # ax[1].axvline(x=n_loss_min, linestyle='dashed', color='black')
        ax[1].grid()
        ax[1].legend(loc="upper right")

        if log:
            ax[0].set_yscale("log")
            ax[1].set_yscale("log")

    fig.savefig(fname)
    plt.close(fig)


def plot_metric(history, fname, log=False):
    keys = history.keys()
    fig, ax = plt.subplots(1, 1)
    for key in keys:
        arr = history[key]
        arr, n_el = mean_to_n_values(arr, N_MAX_POINTS)
        if log is True:
            arr = arr[int(len(arr) * 0.15) :]
            arr = arr + np.min(arr) + 1e-6
        n_range = range(0, len(arr) * n_el, n_el)
        ax.errorbar(n_range, arr, label="{}".format(key), marker=".", linestyle=None)
        # ax[0].axvline(x=n_acc_max, linestyle='dashed', color='black')
        ax.grid()
        ax.legend(loc=3)

    ax.legend(loc="upper right")

    if log:
        ax.set_yscale("symlog")

    fig.savefig(fname)
    plt.close(fig)


def plot_comparison(data, metric, category, fname, swarm=True):
    """
    Plots scatterplots and violinplots of datasitributions, divided into categories

    dict:
        dictonary, contaning the metric to plot and the category
    metric:
        key for the metric to plot
    category:
        category to divide into
    fname:
        filename for saving
    swarm:
        If True, additionally draw scatterplot

    """
    large = 22
    med = 16
    # small = 12
    params = {
        "axes.titlesize": large,
        "legend.fontsize": med,
        "figure.figsize": (16, 10),
        "axes.labelsize": med,
        "xtick.labelsize": med,
        "ytick.labelsize": med,
        "figure.titlesize": large,
    }
    plt.rcParams.update(params)
    plt.style.use("seaborn-whitegrid")
    # sns.set_style("white")
    sns.set(style="whitegrid")
    # cast to pandas dataframe
    df = pd.DataFrame.from_dict(data)
    fig, ax = plt.subplots(1, 1)
    ax = sns.violinplot(x=category, y=metric, data=df, ax=ax, inner=None, saturation=1)

    if swarm is True:
        ax = sns.swarmplot(
            x=category, y=metric, data=df, color="black", edgecolor="gray"
        )

    fig.savefig(fname)
    plt.close(fig)


def mean_to_n_values(arr, n_total):
    """
    Function that computes the mean every n elements to reduce
    the array size to n_total (maximum)
    """
    n_el = 1
    ret = np.array([])
    i = 0
    if len(arr) > n_total:
        n_el = len(arr) // n_total
        while len(ret) + n_el < n_total:
            ret = np.append(ret, np.mean(arr[i * n_el : (i + 1) * n_el]))
            i += 1
        ret = np.append(ret, np.mean(arr[i * n_el :]))
    return arr, n_el
