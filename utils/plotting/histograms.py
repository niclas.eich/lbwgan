import os
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from utils.plotting.tools import set_ticks_to_pi
from utils.processing.physics_observables import reshape_to_particles
from matplotlib.ticker import AutoMinorLocator
from matplotlib.colors import LogNorm


mpl.use("agg")
mpl.rcParams.update({"font.size": 13})
default_colour = ["blue", "orange", "green", "purple", "red", "black", "cyan"]

larger = 36
large = 30
med = 16
# small = 12
_params = {
    "axes.titlesize": larger,
    "legend.fontsize": med,
    "figure.figsize": (16, 10),
    "axes.labelsize": larger,
    "xtick.labelsize": large,
    "ytick.labelsize": large,
    "figure.titlesize": large,
    "xtick.bottom": True,
    "xtick.direction": "in",
    "ytick.direction": "in",
    "xtick.major.size": 12,
    "ytick.major.size": 12,
    "xtick.minor.size": 8,
    "ytick.minor.size": 8,
    "ytick.left": True,
}

_params_big_multiplot = {
    "figure.figsize": (16, 10),
    "figure.dpi": 300,
    "xtick.direction": "out",
    "ytick.direction": "out",
    "xtick.labelsize": 24,
    "ytick.labelsize": 24,
    "xtick.major.size": 6,
    "ytick.major.size": 6,
    "xtick.minor.size": 2,
    "ytick.minor.size": 2,
}

plt.rcParams.update(_params)
cmap_bot = plt.cm.YlGn
cmap_top = plt.cm.OrRd
cmap_top.set_under("w", 1)
cmap_bot.set_under("w", 1)

_defaults_scale = {
    "symmetric": {
        "x_min_func": lambda x: np.mean(x) - 3.0 * np.std(x),
        "x_max_func": lambda x: np.mean(x) + 3.0 * np.std(x),
    },
    "positive": {
        "x_min_func": lambda x: 0.0,
        "x_max_func": lambda x: np.mean(x) + 3 * np.std(x),
    },
    "clipped": {"x_min_func": lambda x: np.min(x), "x_max_func": lambda x: np.max(x),},
    "pi": {"x_min_func": lambda x: -np.pi, "x_max_func": lambda x: np.pi,},
    "long_mass": {"x_min_funx": lambda x: 0, "x_max_func": lambda x: 160.0},
    "default": {"x_min_func": None, "x_max_func": None,},
}


def _set_styles(**kwargs):
    if kwargs.pop("night_mode", False) is True:
        plt.style.use("dark_background")
        if kwargs.get("night_cmap", None) is None:
            cmap = plt.cm.jet
            cmap.set_under("black", 1)
            kwargs["cmap"] = cmap
        else:
            kwargs["cmap"] = kwargs.get("night_cmap", None)
    else:
        plt.style.use(kwargs.pop("plt_style", "seaborn-white"))
        sns.set_style(style=kwargs.pop("sns_style", "white"))
        sns.set_style("ticks")
    plt.rcParams.update(_params)
    return kwargs


def plot_hist_ratio(datasets, names, var_name, fname, **kwargs):
    """
    Plots histogram and adds a ratio plot
    """
    kwargs = _set_styles(**kwargs)
    fig, ax = plt.subplots(2, 1, gridspec_kw={"height_ratios": [3, 1]}, sharex="all")
    sns.set_style("ticks")
    ax[0], ax[1] = _histogram_ratio(ax, datasets, names, var_name, **kwargs)
    fig.savefig(fname, bbox_inches="tight")
    plt.close()


def plot_hist(datasets, names, var_name, fname, **kwargs):
    """
    Plots histogram
    """
    kwargs = _set_styles(**kwargs)
    fig, ax = plt.subplots(1, 1)
    sns.set_style("ticks")
    ax = _histogram(ax, datasets, names, var_name, **kwargs)
    fig.savefig(fname, bbox_inches="tight")
    plt.close()


def plot_hist2d(x_data, y_data, var_x, var_y, fname, **kwargs):
    """
    Plots a 2d histogram
    """
    kwargs = _set_styles(**kwargs)
    fig, ax = plt.subplots(1, 1)
    ax = _histogram2d(ax, x_data, y_data, var_x, var_y, **kwargs)
    fig.suptitle(kwargs.pop("suptitle", ""))
    fig.savefig(fname, bbox_inches="tight")
    plt.close()


def plot_hist_map(data, particle_names, fname, **kwargs):
    """
    optional arguments:
        titles: f_diag, f_top, f_bot
    """
    n_particles = len(particle_names)
    graph_dict = {}
    graph_dict["width"] = n_particles - 1
    graph_dict["height"] = n_particles - 1
    graph_dict["particle_names"] = particle_names
    tag = kwargs.pop("tag", "")
    triu = np.triu_indices(len(particle_names), 1)
    triu = np.transpose(np.array([triu[0], triu[1]]))
    cmap = kwargs.get("cmap", plt.cm.OrRd),
    if len(particle_names) > 5:
        plt.rcParams.update(_params_big_multiplot)
    plt.rcParams.update(_params_big_multiplot)
    for i in range(n_particles):
        for j in range(n_particles):
            plot_dict = {}
            if any(np.equal(triu, [i, j]).all(1)):
                plot_dict["dim"] = 2
                plot_dict["x_data"] = np.squeeze(data[:, j])
                plot_dict["y_data"] = np.squeeze(data[:, i])
                plot_dict["var_x"] = particle_names[j]
                plot_dict["var_y"] = particle_names[i]
                if kwargs.get("norm", None) == "half-log":
                    if i == 0 and j == 1:
                        plot_dict["norm"] = "log"
                    else:
                        plot_dict["norm"] = None
                    _=kwargs.pop("norm", None)
                if len(particle_names) > 5:
                    if i == j - 1:
                        plot_dict["xlabel"] = False
                        plot_dict["ylabel"] = True
                        if i == 0:
                            plot_dict["title"] = particle_names[j]
                        if i == len(particle_names) - 1:
                            plot_dict["y_ticks"] = False
                            plot_dict["x_ticks"] = True
                        else:
                            plot_dict["y_ticks"] = False
                            plot_dict["x_ticks"] = False
                    elif i == 0 and j > 1:
                        plot_dict["title"] = particle_names[j]
                        plot_dict["y_ticks"] = False
                        plot_dict["x_ticks"] = False
                        plot_dict["xlabel"] = False
                        plot_dict["ylabel"] = False
                    else:
                        plot_dict["xlabel"] = False
                        plot_dict["ylabel"] = False
                        plot_dict["x_ticks"] = False
                        plot_dict["y_ticks"] = False
                plot_dict.update(kwargs)
            else:
                graph_dict[(i, j - 1)] = None
            if graph_dict.get((i, j - 1), True) is not None:
                graph_dict[(i, j - 1)] = plot_dict
    fig, ax, images = _hist_map(graph_dict)
    plt.figtext(
        1.0,
        1.01,
        tag,
        verticalalignment="bottom",
        horizontalalignment="right",
        color="black",
        fontsize=15,
    )
    if kwargs.get("global_colorbar", False) is True:
        plt.colorbar(images[0], ax=ax, cmap=cmap)
    # plt.colorbar(ax=ax)
    fig.suptitle(kwargs.pop("suptitle", ""))
    # plt.tight_layout(rect=[0, 0, 1., 0.90])
    # plt.tight_layout(w_pad=0.1)
    fig.savefig(fname)
    plt.close()


def _hist_map(graph_dict, **kwargs):
    """
    graph_dict defines a dictionary of dictionaries, that build up a multiplot graphic
    keys:
    
    width: int
    height: int
    suptitle: string(optional)
    particle_names: list(optional)
    (i, j): dict (optional)
            dim: 1/2 (Dimension of hist)
            x_data: array
            y_data: array (optional)
            x_label: string
            y_label: string(optional)
            x_ticks: boolean(optional)
            y_ticks: boolean(optional)
            title: string(optional)
            n_bins: int(optional)
            cmap: matplotlib colormap(optional)
    """
    width = graph_dict["width"]
    height = graph_dict["height"]
    particles_used = graph_dict.get(
        "particle_names", ["particle {0:2d}".format(i + 1) for i in range(width)]
    )
    fig_kwargs = {}
    if width >= 5:
        fig_kwargs["sharex"] = True
        fig_kwargs["sharey"] = True
    fig, ax = plt.subplots(width, height)
    # determine vmax-value
    mappables = []
    for i in range(width):
        for j in range(height):
            plot_dict = graph_dict.get((i, j), None)
            if plot_dict is not None:
                if plot_dict["dim"] == 2:
                    H, xedges, yedges = np.histogram2d(
                        plot_dict.get("x_data", None),
                        plot_dict.get("y_data", None),
                        bins=kwargs.get("n_bins", 40),
                    )
                    mappables.append(H)
    if kwargs.get("global_colorbar", False) is True:
        vmax = np.max(mappables)
    else:
        vmax = None
    images = []
    for i in range(width):
        for j in range(height):
            plot_dict = graph_dict.pop((i, j), None)
            if plot_dict is not None:
                plot_dict["vmax"] = vmax
                if plot_dict["dim"] == 2:
                    ylab = False
                    xlab = False
                    if i == j:
                        xlab = True
                        ylab = True
                    plot_dict["xlabel"] = xlab
                    plot_dict["ylabel"] = ylab
                    axx, im =_histogram2d(
                        ax[i][j],
                        plot_dict.pop("x_data", None),
                        plot_dict.pop("y_data", None),
                        **plot_dict
                    )
                    images.append(im)
                elif plot_dict["dim"] == 1:
                    _histogram(
                        ax[i][j],
                        plot_dict["x_data"],
                        plot_dict["x_label"],
                        plot_dict["x_label"],
                        **plot_dict
                    )
                else:
                    raise NotImplementedError(
                        "Currently only 1D Histograms and 2D Histograms are implemented"
                    )
            else:
                ax[i][j].axis("off")
    return fig, ax, images


def _histogram(
    ax_object,
    datasets,
    names,
    var_name,
    config=_defaults_scale["default"],
    y_scale="linear",
    n_bins="auto",
    same_int=True,
    **kwargs
):
    """
    Plotting function that plots one or multiple histograms
    kwargs that can be given are:
    histtype:
        step, stepfilled
    errorbars:
        True/False
    tag:
        String
    x_axis_pi:
        True/False
    y_axis_pi:
        True/False
    """
    if not isinstance(datasets, list):
        datasets = [datasets]
    if not isinstance(names, list):
        names = [names]
    if isinstance(config, str):
        if config in _defaults_scale.keys():
            config = _defaults_scale[config]
        else:
            raise KeyError("Config-name: {} not known!".format(config))

    no_nan_sets = []
    for data in datasets:
        nan_array = np.isnan(data)
        no_nans = ~ nan_array
        no_nan_sets.append(data[no_nans])
    datasets = no_nan_sets
    _, bin_edges = np.histogram(
        np.concatenate([np.squeeze(data) for data in datasets]), bins=n_bins
    )
    n_hists = len(datasets)
    # sort datasets if they are not normalized
    if same_int is False:
        datasets, names = zip(
            *sorted(zip(datasets, names), key=lambda x: len(x[0]), reverse=True)
        )

    for i_hist, (dataset, name) in enumerate(zip(datasets, names)):
        if np.prod(dataset.shape) != np.squeeze(dataset).shape[0]:
            raise ValueError(
                "Histograms only accept 1 Dimensional data, but got dimension {} of dataset {}".format(
                    dataset.shape, name
                )
            )
        else:
            dataset = np.squeeze(dataset)
        std = np.std(dataset)
        mu = np.mean(dataset)
        if same_int is True:
            weights = np.ones(dataset.shape) / dataset.shape[0]
        else:
            weights = None
        counts, _ = np.histogram(dataset, bins=bin_edges)
        label = "{0}: $\mu=${1:1.3f}, $\sigma=${2:1.3f}".format(name, mu, std)
        hist_type = kwargs.get("histtype", "step")
        alpha = 1 if hist_type == "step" else (n_hists - i_hist + 3) / (n_hists + 3)
        y, bin_edges, p = ax_object.hist(
            dataset,
            histtype=hist_type,
            bins=bin_edges,
            weights=weights,
            lw=2,
            label=label,
            alpha=alpha,
            zorder=0,
            color=kwargs.get("colour", default_colour[i_hist]),
        )
        if kwargs.get("errorbars", False) is True:
            bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
            ax_object.errorbar(
                bin_centers,
                y,
                yerr=counts ** 0.5 / dataset.shape[0],
                marker=".",
                drawstyle="steps-mid",
                capsize=2,
                alpha=0.6,
                fmt="none",
                color=p[0].get_edgecolor(),
            )
    # ax_object.grid(b=kwargs.pop("grid", False))
    tag = kwargs.pop("tag", "") + " #bins:{0:d}".format(len(bin_edges) - 1)
    ax_object.xaxis.set_minor_locator(AutoMinorLocator())
    ax_object.yaxis.set_minor_locator(AutoMinorLocator())
    ax_object.text(
        1.0,
        1.01,
        tag,
        verticalalignment="bottom",
        horizontalalignment="right",
        transform=ax_object.transAxes,
        color="black",
        fontsize=15,
    )

    ax_object.set_xlabel("{0} {1}".format(var_name, kwargs.pop("x_unit", "[GeV]")))
    if same_int is False:
        ax_object.set_ylabel("N-events")
    else:
        ax_object.set_ylabel("fraction of data")
    if config.get("x_min_func", None) is not None:
        x_max = np.max(
            [config.get("x_max_func")(np.squeeze(data)) for data in datasets]
        )
        x_min = np.min(
            [config.get("x_min_func")(np.squeeze(data)) for data in datasets]
        )
        ax_object.set_xlim(x_min, x_max)
    if kwargs.pop("x_axis_pi", False) is True:
        set_ticks_to_pi(ax_object, axis="x")
    if kwargs.pop("y_axis_pi", False) is True:
        set_ticks_to_pi(ax_object, axis="y")
    ax_object.legend(loc=kwargs.pop("legend_loc", "best"))

    if kwargs.get("axvline", None) is not None:
        if not isinstance(kwargs["axvline"], list):
            vlines = [kwargs["axvline"]]
        else:
            vlines = kwargs["axvline"]
        for vline in vlines:
            ax_object.axvline(vline, linestyle="dashed", color="red")

    if y_scale == "log":
        ax_object.yscale("symlog")
    elif y_scale == "linear":
        pass
    else:
        raise ValueError("Unkown scaling given {}.".format(y_scale))
    return ax_object


def _histogram_ratio(
    ax_object,
    datasets,
    names,
    var_name,
    config=_defaults_scale["default"],
    y_scale="linear",
    n_bins="auto",
    same_int=True,
    **kwargs
):
    """
    Plotting function that plots one or multiple histograms
    kwargs that can be given are:
    histtype:
        step, stepfilled
    errorbars:
        True/False
    tag:
        String
    x_axis_pi:
        True/False
    y_axis_pi:
        True/False
    """
    if not isinstance(datasets, list):
        datasets = [datasets]
    if not isinstance(names, list):
        names = [names]
    if isinstance(config, str):
        if config in _defaults_scale.keys():
            config = _defaults_scale[config]
        else:
            raise KeyError("Config-name: {} not known!".format(config))

    _, bin_edges = np.histogram(
        np.concatenate([np.squeeze(data) for data in datasets]), bins=n_bins
    )
    n_hists = len(datasets)
    # sort datasets if they are not normalized
    if same_int is False:
        datasets, names = zip(
            *sorted(zip(datasets, names), key=lambda x: len(x[0]), reverse=True)
        )

    hists = []
    hists_names = []

    for i_hist, (dataset, name) in enumerate(zip(datasets, names)):
        if np.prod(dataset.shape) != np.squeeze(dataset).shape[0]:
            raise ValueError(
                "Histograms only accept 1 Dimensional data, but got dimension {} of dataset {}".format(
                    dataset.shape, name
                )
            )
        else:
            dataset = np.squeeze(dataset)
        std = np.std(dataset)
        mu = np.mean(dataset)
        if same_int is True:
            weights = np.ones(dataset.shape) / dataset.shape[0]
        else:
            weights = None
        counts, _ = np.histogram(dataset, bins=bin_edges)
        label = "{0}: $\mu=${1:1.3f}, $\sigma=${2:1.3f}".format(name, mu, std)
        hist_type = kwargs.get("histtype", "step")
        alpha = 1 if hist_type == "step" else (n_hists - i_hist + 3) / (n_hists + 3)
        y, bin_edges, p = ax_object[0].hist(
            dataset,
            histtype=hist_type,
            bins=bin_edges,
            weights=weights,
            lw=2,
            label=label,
            alpha=alpha,
            zorder=0,
            color=kwargs.get("colour", default_colour[i_hist]),
        )
        hists.append(counts)
        hists_names.append(name)
        bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
        if kwargs.get("errorbars", False) is True:
            ax_object[0].errorbar(
                bin_centers,
                y,
                yerr=counts ** 0.5 / dataset.shape[0],
                marker=".",
                drawstyle="steps-mid",
                capsize=2,
                alpha=0.6,
                fmt="none",
                color=p[0].get_edgecolor(),
            )
    # ax_object[0].grid(b=kwargs.pop("grid", False))
    tag = kwargs.pop("tag", "") + " #bins:{0:d}".format(len(bin_edges) - 1)
    ax_object[0].xaxis.set_minor_locator(AutoMinorLocator())
    ax_object[0].yaxis.set_minor_locator(AutoMinorLocator())
    ax_object[0].text(
        1.0,
        1.01,
        tag,
        verticalalignment="bottom",
        horizontalalignment="right",
        transform=ax_object[0].transAxes,
        color="black",
        fontsize=15,
    )

    if same_int is False:
        ax_object[0].set_ylabel("N-events")
    else:
        ax_object[0].set_ylabel("fraction of data")
    if config.get("x_min_func", None) is not None:
        x_max = np.max(
            [config.get("x_max_func")(np.squeeze(data)) for data in datasets]
        )
        x_min = np.min(
            [config.get("x_min_func")(np.squeeze(data)) for data in datasets]
        )
        ax_object[0].set_xlim(x_min, x_max)
    if kwargs.pop("x_axis_pi", False) is True:
        set_ticks_to_pi(ax_object[0], axis="x")
    if kwargs.pop("y_axis_pi", False) is True:
        set_ticks_to_pi(ax_object[0], axis="y")
    ax_object[0].legend(loc=kwargs.pop("legend_loc", "best"))

    if kwargs.get("axvline", None) is not None:
        if not isinstance(kwargs["axvline"], list):
            vlines = [kwargs["axvline"]]
        else:
            vlines = kwargs["axvline"]
        for vline in vlines:
            ax_object[0].axvline(vline, linestyle="dashed", color="red")

    if y_scale == "log":
        ax_object[0].yscale("symlog")
    elif y_scale == "linear":
        pass
    else:
        raise ValueError("Unkown scaling given {}.".format(y_scale))
    """
    Ratio plot
    """
    r_a = hists[0] / np.sum(hists[0])
    r_b = hists[1] / np.sum(hists[1])
    ratios = (r_a - r_b) / (r_b + 1e-7)
    bin_widths = np.diff(bin_centers)
    bin_widths = np.append(bin_widths, bin_widths[-1]) / 2.0

    ax_object[1].errorbar(
        bin_centers,
        ratios,
        xerr=bin_widths,
        color="black",
        linestyle="None",
        marker=None,
    )
    ax_object[1].axhline(y=0.0, linestyle="dashed", color="grey", alpha=0.5)
    ax_object[1].set_ylabel(
        "$\\frac{{{0}-{1}}}{{{1}}}$".format(hists_names[0], hists_names[1])
    )
    ax_object[1].set_xlabel("{0} {1}".format(var_name, kwargs.pop("x_unit", "[GeV]")))
    ax_object[1].set_ylim(
        -1 * kwargs.get("ratio_ylim", 0.5), 1.0 * kwargs.get("ratio_ylim", 0.5)
    )

    return ax_object


def _histogram2d(ax_object, x_data, y_data, var_x, var_y, **kwargs):
    assert (
        np.squeeze(x_data).shape == np.squeeze(y_data).shape
    ), "For 2d histograms, the arrays must have the same size! {} vs {}".format(
        x_data.shape, y_data.shape
    )
    # ax_object.tick_params(axis=u'both', which=u'both', direction='out')
    if kwargs.get("norm", None) == "log":
        norm = LogNorm()
    else:
        norm = None
    counts, xedges, yedges, im = ax_object.hist2d(
        x_data,
        y_data,
        vmin=1,
        vmax=kwargs.pop("vmax", None),
        bins=kwargs.pop("n_bins", 40),
        cmap=kwargs.pop("cmap", plt.cm.OrRd),
        norm=norm,
        zorder=0,
    )
    if kwargs.pop("xlabel", True) is True:
        ax_object.set_xlabel(var_x)
    if kwargs.pop("ylabel", True) is True:
        ax_object.set_ylabel(var_y)
    ax_object.xaxis.set_minor_locator(AutoMinorLocator())
    ax_object.yaxis.set_minor_locator(AutoMinorLocator())
    if kwargs.pop("x_axis_pi", False) is True:
        set_ticks_to_pi(ax_object, axis="x")
    if kwargs.pop("y_axis_pi", False) is True:
        set_ticks_to_pi(ax_object, axis="y")
    if kwargs.get("title", None) is not None:
        ax_object.set_title(kwargs.get("title", ""))
    tag = kwargs.pop("tag", "")
    if kwargs.pop("x_ticks", True) is False:
        ax_object.set_xticklabels([])
    if kwargs.pop("y_ticks", True) is False:
        ax_object.set_yticklabels([])
    ax_object.text(
        1.0,
        1.01,
        tag,
        verticalalignment="bottom",
        horizontalalignment="right",
        transform=ax_object.transAxes,
        color="black",
        fontsize=15,
    )
    if kwargs.get("colorbar", True) is True:
        plt.colorbar(im, ax=ax_object)
    return ax_object, im


# def create_histogram(list_datasets, names_datasets, variable, out_path,
# mode='png', scale='both', n_bins='auto', sigma_lim=4):
# print("\nutils.plotting.histograms.create_histogram is deprecated, use utils.plotting.histograms.histogram")
# if scale == 'both':
# create_histogram(list_datasets, names_datasets, variable, out_path,
# mode, scale='linear')
# create_histogram(list_datasets, names_datasets, variable,
# out_path + '__log', mode, scale='log')
# return
# if mode not in out_path:
# out_path += '.png'
# mpl.rcParams.update({'font.size': 13})
# if not isinstance(list_datasets, list):
# list_datasets = [list_datasets]
# if not isinstance(names_datasets, list):
# names_datasets = [names_datasets]
# all_data = np.empty((0))
# for label, dataset in zip(names_datasets, list_datasets):
# if not np.isfinite(dataset).all():
# print('There are some non-finite values in the dataset %s!'
# % label)
# if np.isnan(dataset).any():
# print('There are even NAN values in the dataset!')
# dataset[~np.isnan(dataset)]
# try:
# all_data = np.concatenate([all_data, dataset])
# except:
# print(all_data.shape)
# print(dataset.shape)
# raise
# try:
# _, bin_edges = np.histogram(all_data, bins=n_bins)
# except ValueError as error:
# if not np.isfinite(all_data).all():
# print('There are some non-finite values in the data!')
# if np.isnan(all_data).any():
# print('There are some NAN values in the data!')
# print('Something seems to be wrong with the data:\n' + str(all_data))
# raise error
# plt.figure()
# plt.grid()
# y_min = np.inf
# std_max = 0
# for dataset, name in zip(list_datasets, names_datasets):
# std = np.std(dataset)
# std_max = np.max([std, std_max])
# mu = np.average(dataset)
# label = name + r': $\mu=$%f, $\sigma=$%f' % (mu, std)
# weights = np.ones(dataset.shape) / dataset.shape[0]
# counts, _ = np.histogram(dataset, bins=bin_edges)
# y, bin_edges, p = plt.hist(dataset, histtype='step', bins=bin_edges,
# weights=weights, lw=2, label=label)
# bin_centers = 0.5 * (bin_edges[1:] + bin_edges[:-1])
# plt.errorbar(
# bin_centers,
# y,
# yerr=counts**0.5 / dataset.shape[0],
# marker='.',
# drawstyle='steps-mid',
# capsize=2,
# alpha=0.6,
# fmt='none',
# color=p[0].get_edgecolor()
# )
# y_min = min(y_min, np.min(y[y != 0.0]))
# # plt.title('Datapoints: #{0:4d}k'.format(int(len(dataset)*1e-3)))
# if "E" in variable:
# plt.xlim(-0.2*std_max, sigma_lim*std_max)
# else:
# plt.xlim(-sigma_lim*std_max, sigma_lim*std_max)
# plt.xlabel(variable + ', #bins:%d' % (len(bin_edges) - 1))
# plt.ylabel('fraction of data')
# legend = plt.legend(bbox_to_anchor=(1.04, 0.5), loc='center left')
# if scale == 'log':
# plt.yscale('symlog', linthreshy=y_min, linscaley=0.1)
# elif scale == 'linear':
# pass
# else:
# raise ValueError('create_histogram has no scale %s.' % scale)
# plt.savefig(out_path, bbox_extra_artists=(legend,), bbox_inches='tight')
# plt.close()


# def create_histogram2d(x, y, x_label, y_label, title, fname):
# large = 22
# med = 16
# # small = 12
# params = {'axes.titlesize': large,
# 'legend.fontsize': med,
# 'figure.figsize': (16, 10),
# 'axes.labelsize': med,
# 'xtick.labelsize': med,
# 'ytick.labelsize': med,
# 'figure.titlesize': large}
# plt.rcParams.update(params)
# plt.style.use('seaborn-whitegrid')
# # sns.set_style("white")
# sns.set(style="whitegrid")
# # df = pd.DataFrame.from_dict({'x': x, 'y': y})
# # print(df.shape)
# cmap = plt.cm.YlGn
# cmap.set_under('w', 1)

# fig, ax = plt.subplots(1, 1)
# # cmap = sns.cubehelix_palette(as_cmap=True, dark=0, light=1, reverse=False)
# # ax = sns.hexplplot(df.x, df.y, cmap=cmap, ax=ax, n_levels=10, shade=False)
# ax.set_xlabel(x_label)
# ax.set_ylabel(y_label)
# ax.set_title(title)
# ax =  plt.hist2d(np.squeeze(x), np.squeeze(y), vmin=1, bins=80, cmap=cmap)
# ax = plt.colorbar()
# fig.savefig(fname)


# def create_histogram2d_map(graph_dict, fname, n_bins=40):
# """
# graph_dict defines a dictionary of dictionaries, that build up a multiplot graphic
# keys:

# width: int
# height: int
# particle_names: list(optional)
# (i, j): dict (optional)
# dim: 1/2 (Dimension of hist)
# x_data: array
# y_data: array (optional)
# x_label: string(optional)
# y_label: string(optional)
# x_ticks: boolean(optional)
# y_ticks: boolean(optional)
# title: string(optional)
# n_bins: int(optional)
# cmap: matplotlib colormap(optional)
# """

# width = graph_dict["width"]
# height = graph_dict["height"]
# particles_used = graph_dict.get("particle_names", ["particle {0:2d}".format(i+1) for i in range(width)])

# fig, ax = plt.subplots(width, height)
# for i in range(width):
# for j in range(height):
# plot_dict = graph_dict.pop((i, j), None)
# cmap = graph_dict.pop("cmap", cmap_bot if i > j else cmap_top)

# if plot_dict is not None:
# if plot_dict.pop("x_ticks", False) is False:
# ax[i][j].set_xticklabels([])
# if plot_dict.pop("y_ticks", False) is False:
# ax[i][j].set_yticklabels([])
# if i == 0:
# ax[i][j].set_title(particles_used[j])
# if j == 0:
# ax[i][j].set_ylabel(particles_used[i], rotation=0)

# bins = plot_dict.pop("n_bins", n_bins)
# if plot_dict["dim"] == 2:
# y_data = plot_dict["y_data"]
# x_data = plot_dict["x_data"]
# ax[i][j].hist2d(x_data, y_data, vmin=1, bins=bins, cmap=cmap)
# elif plot_dict["dim"] == 1:
# x_data = plot_dict["x_data"]
# std = np.std(x_data)
# mu = np.average(x_data)
# label = plot_dict.pop("x_label", "")
# label += r': $\mu=$%f, $\sigma=$%f' % (mu, std)
# weights = np.ones(x_data.shape) / x_data.shape[0]
# counts, bin_edges = np.histogram(x_data, bins=bins)
# y, bin_edges, p = ax[i][j].hist(x_data, histtype='stepfilled',
# bins=bin_edges,
# weights=weights, lw=2, label=label)
# if plot_dict.get("title") is not None:
# ax[i][j].set_title(plot_dict.pop("title"))
# else:
# raise NotImplementedError("Currently only 1D Histograms and 2D Histograms are implemented")
# fig.savefig(fname)
# plt.tight_layout()
# plt.close()


# def plot_correlations(features, f_diag, f_bot, f_top, particle_names, fname, **kwargs):
# """
# optional arguments:
# titles: f_diag, f_top, f_bot
# """
# n_particles = len(particle_names)
# graph_dict = {}
# graph_dict["width"] = n_particles
# graph_dict["height"] = n_particles
# graph_dict["particle_names"] = particle_names
# triu  = np.triu_indices(len(particle_names), 1)
# triu = np.transpose(np.array([triu[0], triu[1]]))
# triu_bot = np.flip(triu, axis=1)
# for i in range(n_particles):
# for j in range(n_particles):
# plot_dict = {}
# if any(np.equal(triu, [i, j]).all(1)):
# if f_top is not None:
# plot_dict["dim"] = 2
# plot_dict["x_data"] = np.squeeze(features[f_top][:, i])
# plot_dict["y_data"] = np.squeeze(features[f_top][:, j])
# plot_dict["title"] = kwargs.get("titles", "")[1]
# else:
# graph_dict[(i, j)] = None
# elif any(np.equal(triu_bot, [i, j]).all(1)):
# if f_bot is not None:
# plot_dict["dim"] = 2
# plot_dict["x_data"] = np.squeeze(features[f_bot][:, i])
# plot_dict["y_data"] = np.squeeze(features[f_bot][:, j])
# plot_dict["title"] = kwargs.get("titles", "")[2]
# else:
# graph_dict[(i, j)] = None
# elif i == j:
# if f_diag is not None:
# if kwargs.get("diag_corr", False) is True:
# plot_dict["dim"] = 2
# else:
# plot_dict["dim"] = 1
# plot_dict["x_data"] = np.squeeze(features[f_diag][:, i])
# plot_dict["title"] = kwargs.get("titles", "")[0]
# else:
# graph_dict[(i, j)] = None
# if graph_dict.get((i, j), True) is not None:
# plot_dict["x_ticks"] = kwargs.get("x_ticks", False)
# plot_dict["y_ticks"] = kwargs.get("y_ticks", False)
# graph_dict[(i, j)] = plot_dict
# create_histogram2d_map(graph_dict, fname)
