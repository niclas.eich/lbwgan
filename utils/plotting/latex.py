import numpy as np

"""
adapted from:
yanlend/print_latex_table.py
"""


def print_to_table(
    contents,
    values,
    content_keys,
    value_keys,
    highlight_best=True,
    file=None,
    col_orientation="c",
):
    """
    Print data into a LateX table
    The names of the coloumns are given in keys

    highlight_best: best values are printed in bold face, boolean

    max_is_best: whether largest values are best (True) or smallest values, boolean

    file: file to which the output should be written, string or None
          If file is None, table is printed to stdout
    """

    # Header
    print_str = (
        "\\begin{tabular}{| l"
        + (" " + col_orientation) * len(content_keys)
        + "||"
        + (col_orientation + "|") * len(value_keys)
        + "} \hline\n"
    )
    print_str += " # &"
    for ck in content_keys:
        print_str += ck + "&"
    for i, vk in enumerate(value_keys):
        print_str += vk
        if not (i == len(value_keys) - 1):
            print_str += "&"
    print_str += " \\\\ \n \hline\hline\n"

    # Content
    for m in range(len(contents)):
        if m > 1 and m % 5 == 0:
            print_str += "\hline\hline\n"
        print_str += "{0:2d} &".format(int(m))
        for ck in content_keys:
            if ck in contents[m].keys():
                print_str += str(contents[m][ck])
            else:
                print_str += ""
            print_str += " & "
        for i, vk in enumerate(value_keys):
            if vk in values[m].keys():
                print_str += "${0:1.3f}$".format(float(values[m][vk]))
            else:
                print_str += ""
            if i < len(value_keys) - 1:
                print_str += " & "
        print_str += "\\\\ \n"
        print_str = print_str.replace("!", "\\phantom{0}")

    # Footer
    print_str += "\n \hline \n \end{tabular}"

    # Write output
    if file is None:
        print(print_str)
    else:
        with open(file, "w") as f:
            f.write(print_str)
