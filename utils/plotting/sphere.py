import matplotlib.pyplot as plt
import matplotlib as mpl

mpl.use("agg")
from matplotlib import cm, colors
from mpl_toolkits.mplot3d import Axes3D
from astrotools import skymap, coord, healpytools as hpt
from utils.processing.physics_observables import reshape_to_particles
import numpy as np


def plot_points_on_sphere(points, fname):
    """
    plots points on a sphere...

    """
    # Create a sphere
    r = 1
    pi = np.pi
    cos = np.cos
    sin = np.sin
    phi, theta = np.mgrid[0.0:pi:100j, 0.0 : 2.0 * pi : 100j]
    x = r * sin(phi) * cos(theta)
    y = r * sin(phi) * sin(theta)
    z = r * cos(phi)

    # Create cubic bounding box to simulate equal aspect ratio
    # max_range = np.array([x.max()-x.min(), y.max()-y.min(), z.max()-z.min()]).max()
    # Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(x.max()+x.min())
    # Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(y.max()+y.min())
    # Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(z.max()+z.min())

    # normalize points
    points /= np.linalg.norm(points, axis=-1, keepdims=True)
    if len(points.shape) == 2:
        points = np.expand_dims(points, axis=1)

    # Set colours and render
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_surface(x, y, z, rstride=1, cstride=1, color="c", alpha=0.3, linewidth=0)
    for i in range(points.shape[1]):
        ax.scatter(
            points[..., i, 0],
            points[..., i, 1],
            points[..., i, 2],
            s=20,
            label="Particle {}".format(i),
        )
    ax.legend()
    ax.set_xlim([-1, 1])
    ax.set_ylim([-1, 1])
    ax.set_zlim([-1, 1])
    # Build up a bounding box, to make the sphere really spherical
    # for xb, yb, zb in zip(Xb, Yb, Zb):
    # ax.plot([xb], [yb], [zb], 'w')

    plt.tight_layout()
    fig.savefig(fname, dpi=300)
    plt.close()


def plot_sky_map(points, fname):
    points = points.T
    pixel = hpt.vec2pix(
        nside=64, v=points
    )  # nside is resolution: e.g. nside=64 for 49152 pixel cells and nside=16 for ~3000 pixel cells on the sphere
    _map = np.bincount(pixel, minlength=hpt.nside2npix(64))
    skymap.heatmap(_map, opath=fname)
