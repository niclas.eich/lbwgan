import tfmpl
import numpy as np
import matplotlib as mpl

mpl.use("agg")
import matplotlib.pyplot as plt


@tfmpl.figure_tensor
def plot_lbn(
    lbn_part_weights,
    lbn_rest_weights,
    figsize=(5, 5),
    particle_indexes=np.arange(0, 8),
    particle_labels=None,
):
    """ Plot LBN matrices"""
    # one figure for the wights and one for the rest-frames
    if particle_labels is None:
        particle_labels = [
            "particle {0:2d}".format(i + 1) for i in range(lbn_rest_weights.shape[0])
        ]
    figures = tfmpl.create_figures(2, figsize=figsize)
    for i, fig in enumerate(figures):
        ax = fig.gca()
        if i == 0:
            plot_weights(
                lbn_part_weights,
                ax,
                particle_labels,
                name="Particle-{}".format(""),
                cmap="OrRd",
            )
        elif i == 1:
            plot_weights(
                lbn_rest_weights,
                ax,
                particle_labels,
                name="Restframe-{}".format(""),
                cmap="YlGn",
            )
    return figures


def plot_weights(
    weights,
    ax,
    particle_labels,
    name,
    cmap="OrRd",
    particle_indexes=np.arange(0, 8),
    **fig_kwargs
):
    # normalize weight tensor to a sum of 100 per row
    weights_normalized = (
        weights / np.sum(weights, axis=0).reshape((1, weights.shape[1])) * 100
    )
    # create and style the plot
    ax.imshow(weights_normalized, cmap=cmap, vmin=0, vmax=100)
    ax.set_title("{} weights".format(name), fontdict={"fontsize": 12})

    ax.set_xlabel("LBN particle number")
    ax.set_xticks(list(range(weights_normalized.shape[1])))

    ax.set_ylabel("Input particle")
    ax.set_yticks(list(range(weights_normalized.shape[0])))
    ax.set_yticklabels(particle_labels)

    # write weights into each bin
    for (i, j), val in np.ndenumerate(weights_normalized):
        ax.text(
            j,
            i,
            "{0:2d}%\n{1:4.2f}".format(
                int(round(weights_normalized[i, j])), weights[i, j]
            ),
            fontsize=8,
            ha="center",
            va="center",
            color="k",
        )

    # lines to separate decay products of top quarks and the Higgs boson
    for height in [2.5, 4.5]:
        ax.plot(
            (-0.5, weights.shape[1] - 0.5), (height, height), color="k", linewidth=0.5
        )
