import numpy as np
import matplotlib.pyplot as plt

"""
https://stackoverflow.com/questions/40642061/how-to-set-axis-ticks-in-multiples-of-pi-python-matplotlib
"""


def multiple_formatter(denominator=2, number=np.pi, latex="\pi"):
    def gcd(a, b):
        while b:
            a, b = b, a % b
        return a

    def _multiple_formatter(x, pos):
        den = denominator
        num = np.int(np.rint(den * x / number))
        com = gcd(num, den)
        (num, den) = (int(num / com), int(den / com))
        if den == 1:
            if num == 0:
                return r"$0$"
            if num == 1:
                return r"$%s$" % latex
            elif num == -1:
                return r"$-%s$" % latex
            else:
                return r"$%s%s$" % (num, latex)
        else:
            if num == 1:
                return r"$\frac{%s}{%s}$" % (latex, den)
            elif num == -1:
                return r"$\frac{-%s}{%s}$" % (latex, den)
            else:
                return r"$\frac{%s%s}{%s}$" % (num, latex, den)

    return _multiple_formatter


class Multiple:
    def __init__(self, denominator=2, number=np.pi, latex="\pi"):
        self.denominator = denominator
        self.number = number
        self.latex = latex

    def locator(self):
        return plt.MultipleLocator(self.number / self.denominator)

    def formatter(self):
        return plt.FuncFormatter(
            multiple_formatter(self.denominator, self.number, self.latex)
        )


def example():
    tau = np.pi * 2
    den = 60
    major = Multiple(den, tau, r"\tau")
    minor = Multiple(den * 4, tau, r"\tau")
    x = np.linspace(-tau / 60, tau * 8 / 60, 500)
    plt.plot(x, np.exp(-x) * np.cos(60 * x))
    plt.title(r"Multiples of $\tau$")
    ax = plt.gca()
    ax.grid(True)
    ax.axhline(0, color="black", lw=2)
    ax.axvline(0, color="black", lw=2)
    ax.xaxis.set_major_locator(major.locator())
    ax.xaxis.set_minor_locator(minor.locator())
    ax.xaxis.set_major_formatter(major.formatter())
    plt.show()


def set_ticks_to_pi(ax, denominator=4, axis="x"):
    xmajor = Multiple(denominator, np.pi, r"\pi")
    xminor = Multiple(denominator * 4, np.pi, r"\pi")

    ymajor = Multiple(denominator//2, np.pi, r"\pi")
    yminor = Multiple(denominator//2 * 4, np.pi, r"\pi")
    
    if axis == "x":
        ax.xaxis.set_major_locator(xmajor.locator())
        ax.xaxis.set_minor_locator(xminor.locator())
        ax.xaxis.set_major_formatter(xmajor.formatter())
    if axis == "y":
        ax.yaxis.set_major_locator(ymajor.locator())
        ax.yaxis.set_minor_locator(yminor.locator())
        ax.yaxis.set_major_formatter(ymajor.formatter())
