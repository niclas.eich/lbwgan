import tensorflow as tf
import numpy as np
from utils.processing.physics_observables import m as compute_mass


def k_fold_split(k, arr, n, seed):
    """
    Function that returns the indices of the desired splits
    because it is randomly sampled, the dataset needs no shuffling
    k: number of folds
    arr: array to split or length of array given as int
    n: number of fold to return
    seed: seed to initialize

    return:
        returns list of indices
    """
    np.random.seed(seed)
    if type(arr) is int:
        length = arr
    else:
        length = len(arr)
    all_idx = np.arange(0, int(length))
    idx_list = []
    for i in range(k):
        idx_list.append(np.random.choice(all_idx, len(all_idx) // (k - i), replace=False))
        all_idx = np.delete(all_idx, idx_list[i])
        if i == n:
            break
    return idx_list[n]


def preprocessing(data, verbose=True, **kwargs):
    """
    Is this a nice Docstring?

    returns the scaled dataset and the scaling-factor
    """
    if kwargs.pop("save", False) is False:
        std = np.std(data)
    else:
        std = 1.0
    assert not std == 0
    if verbose:
        print("Preprocessing data")
        print("Scaling data with factor: {0:4.1f}".format(std))
    return data / std, std


def tf_non_zero(t, epsilon=1e-12):
    """
    Ensures that all zeros in a tensor *t* are replaced by *epsilon*.
    """
    # use combination of abs and sign instead of a where op
    return t + (1 - tf.abs(tf.sign(t))) * epsilon


class Dataset(object):
    """
    The dataset class helps to load different targets, so the law tasks can have a homogeneous
    apperance, while case handling is done in here

    datasets are:
        ttbb
        ttH

    """

    data_dict = {}

    def __init__(self, target, task, **kwargs):
        labels = None
        self.n_particles = len(task.particle_indexes)
        if "train" in target.keys():
            train_data = target["train"]["x"].load()
            if "y" in target["train"].keys():
                train_label = target["train"]["y"].load()
            if task.particle_process == "ttbb":
                label = 0
                target_idxs = np.where(train_label == label)
                print("exited here: 1")
                print(task.particle_process)
            elif task.particle_process == "ttH":
                label = 1
                target_idxs = np.where(train_label == label)
                print("exited here: 2")
                print(task.particle_indexes)
                print(task.particle_process)
            elif task.particle_process in ["Zee", "Zee_no_detector"]:
                target_idxs = np.arange(0, 255757)
            elif task.particle_process in ["Zmumu", "Z", "Z_mass"]:
                target_idxs = np.arange(255757, len(train_data))
                if task.particle_process in ["Z", "Z_mass"]:
                    task.particle_indexes = [0, 1]
            elif task.particle_process == "Zll":
                target_idxs = np.arange(0, len(train_data))
            elif task.particle_process in ["Zmumu_no_detector", "Z_no_detector"]:
                target_idxs = np.arange(255757, len(train_data))
                if task.particle_process == "Z_no_detector":
                    task.particle_indexes = [0, 1]
            elif task.particle_process == "Zll_no_detector":
                target_idxs = np.arange(0, len(train_data))
            elif task.particle_process in ["Zbb"]:
                target_idxs = np.arange(0, len(train_data), 1)
            elif task.particle_process in ["zbb_gen_01"]:
                target_idxs = np.arange(0, len(train_data), 1)
            elif task.particle_process == "zee_01":
                target_idxs = np.arange(0, 493859)
            elif task.particle_process == "zmumu_01":
                target_idxs = np.arange(493859, 493859 + 494460)
            elif task.particle_process == "Zee_sorted":
                target_idxs = np.arange(0, 256130)
            elif task.particle_process == "Zmumu_sorted":
                target_idxs = np.arange(256130, 256130 + 380019)
            else:
                raise NotImplementedError
            # case between (N, n_p*4) and (N, n_p, 4) shape
            if len(np.shape(train_data)) <= 2:
                train_data = np.reshape(train_data, (len(train_data), train_data.shape[-1] // 4, 4))
            if len(task.particle_indexes) <= train_data.shape[1]:
                for idx in task.particle_indexes:
                    if idx > train_data.shape[1]:
                        break
                train_data = train_data[:, task.particle_indexes]
            if task.particle_process in ["Z", "Z_no_detector", "Z_mass"]:
                train_data = np.expand_dims(np.sum(train_data, axis=-2), axis=1)
            if task.particle_process in ["Zmumu", "Zee", "Zll", "Zbb", "Zee_sorted", "Zmumu_sorted", "zee_01", "zmumu_01", "zbb_gen_01"]:
                # maybe do not shuffle
                # for event in train_data:
                    # np.random.shuffle(event)
                train_data = np.append(train_data, np.expand_dims(np.sum(train_data, axis=-2), axis=-2), axis=-2,)
            if task.particle_process in ["Z_mass"]:
                train_data = compute_mass(train_data)
            if len(np.shape(train_data)) > 2 and not(task.particle_process in ["Z_mass"]):
                train_data = np.reshape(train_data, (len(train_data), train_data.shape[1] * 4))
            if task.particle_process in ["Z_mass"]:
                train_data = np.expand_dims(np.squeeze(train_data), axis=-1)
            self.data_dict["x_train"] = train_data[target_idxs]
            if "y" in target["train"].keys():
                self.data_dict["y_train"] = train_label[target_idxs]

        if "test" in target.keys():
            test_data = target["test"]["x"].load()
            if "y" in target["test"].keys():
                test_label = target["test"]["y"].load()
            if task.particle_process == "ttbb":
                label = 0
            elif task.particle_process == "ttH":
                label = 1
            else:
                raise NotImplementedError
            # case between (N, n_p*4) and (N, n_p, 4) shape
            if len(np.shape(test_data)) <= 2:
                test_data = np.reshape(test_data, (len(test_data), test_data.shape[-1] // 4, 4))
                test_data = test_data[:, task.particle_indexes]
                test_data = np.reshape(test_data, (len(test_data), len(task.particle_indexes) * 4))
            self.data_dict["x_test"] = test_data[np.where(test_label == label)]
            self.data_dict["y_test"] = test_label[np.where(test_label == label)]

        if kwargs.pop("preprocessing", False) is True:
            self.scaling_factor = self._preprocess_data()
        else:
            self.scaling_factor = 1.0
        if kwargs.pop("data_split", False) is True:
            self._split_data(kwargs["folds"], kwargs["stage"], kwargs["seed"])
        if kwargs.get("validation_split", None) is not None:
            self._validation_split(kwargs["validation_split"])

        if kwargs.get("test", False) is True:
            try:
                self.data_dict["x_train"] = self.data_dict["x_train"][0:10000]
                if self.data_dict.get("y_train", None) is not None:
                    self.data_dict["y_train"] = self.data_dict["y_train"][0:10000]
            except IndexError:
                pass

    def _split_data(self, folds, stage, seed):
        split_idx = k_fold_split(folds, self.data_dict["x_train"], stage, seed)
        self.data_dict["x_train"] = self.data_dict["x_train"][split_idx]

    def _preprocess_data(self):
        self.data_dict["x_train"], scaling_factor = preprocessing(self.data_dict["x_train"])
        if self.data_dict.get("x_test", None) is not None:
            self.data_dict["x_test"] /= scaling_factor
        return scaling_factor

    def _validation_split(self, split):
        val_split = int(len(self.data_dict["x_train"]) * (1.0 - split))
        self.data_dict["x_train"] = self.data_dict["x_train"][:val_split]
        if self.data_dict.get("y_train", None) is not None:
            self.data_dict["y_train"] = self.data_dict["y_train"][:val_split]
            self.data_dict["y_val"] = self.data_dict["y_train"][val_split:]
        self.data_dict["x_val"] = self.data_dict["x_train"][val_split:]

    @property
    def rv_histograms(self):
        from scipy.stats import rv_histogram

        rv_histograms = []
        for i_p in range(self.n_particles):
            for i_comp in range(1, 4):
                rv_histograms.append(rv_histogram(np.histogram(self.data_dict["x_train"][:, (i_p * 4 + i_comp)])))
        return np.array(rv_histograms)

    @property
    def data(self):
        """
        returns a data_dict that potentially has these keys:
            x_train
            y_train
            x_val
            y_val
            x_test
            y_test
        """
        return self.data_dict
