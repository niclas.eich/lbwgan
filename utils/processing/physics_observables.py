import numpy as np

EPSILON = 1e-5


def n_particles(vec):
    """
    n_particles
    """
    if len(vec.shape) == 2:
        assert vec.shape[-1] == 4
        return vec.shape[0]
    elif len(vec.shape) == 1:
        return 1
    elif len(vec.shape) == 3:
        assert vec.shape[-1] == 4
        return vec.shape[1]
    else:
        raise ValueError


def E(vec):
    """
    Energy.
    """
    return np.expand_dims(vec[..., 0], -1)


def px(vec):
    """ 
    Momentum component x.
    """
    return np.expand_dims(vec[..., 1], -1)


def py(vec):
    """
    Momentum component y.
    """
    return np.expand_dims(vec[..., 2], -1)


def pz(vec):
    """
    Momentum component z.
    """
    return np.expand_dims(vec[..., 3], -1)


def _pvec(vec):
    """
    Momentum vector. Hidden.
    """
    return vec[..., 1:]


def _p2(vec):
    """
    Squared absolute momentum. Hidden.
    """
    return np.expand_dims(np.maximum(np.sum(_pvec(vec) ** 2, axis=-1), EPSILON ** 2), -1)


def p(vec):
    """
    Absolute momentum.
    """
    return np.sqrt(_p2(vec))


def pt(vec):
    """
    Scalar, transverse momentum.
    """
    return np.maximum(_p2(vec) - pz(vec) ** 2, EPSILON) ** 0.5


def eta(vec):
    """
    Pseudorapidity.
    """
    return np.arctanh(np.clip(pz(vec) / p(vec), EPSILON - 1, 1 - EPSILON))


def phi(vec):
    """
    Azimuth.
    """
    return np.arctan2(np_non_zero(py(vec), EPSILON), px(vec))


def theta(vec):
    """
    Polar
    """
    return np.arccos(pz(vec) / np_non_zero(p(vec), EPSILON))


def m(vec):
    """
    Mass.
    """
    return np.maximum(E(vec) ** 2 - _p2(vec), EPSILON ** 2) ** 0.5


def beta(vec):
    """
    Relativistic speed, v/c or p/E.
    """
    return p(vec) / np.maximum(E(vec), EPSILON)


def beta_vec(vec):
    """
    Relativistic speed-vector, v/c or p/E.
    """
    return _pvec(vec) / np.maximum(E(vec), EPSILON)


def gamma(vec):
    """
    Relativistic gamma factor, 1 / sqrt(1-beta**2) or E / m.
    """
    return E(vec) / m(vec)


def pair_dr(vec):
    """
    Distance between all pairs of particles in the eta-phi plane.
    """
    # eta difference on lower triangle elements
    d_eta = np.reshape(eta(vec), (-1, n_particles(vec), 1)) - np.reshape(eta(vec), (-1, 1, n_particles(vec)))
    d_eta = np.reshape(d_eta, (-1, n_particles(vec) ** 2))[:, triu_indices(vec)]

    # phi difference on lower triangle elements, handle boundaries
    d_phi = np.reshape(phi(vec), (-1, n_particles(vec), 1)) - np.reshape(phi(vec), (-1, 1, n_particles(vec)))
    d_phi = np.reshape(d_phi, (-1, n_particles(vec) ** 2))[:, triu_indices(vec)]
    d_phi = np.abs(d_phi)
    d_phi = np.minimum(d_phi, 2.0 * np.math.pi - d_phi)

    return (d_eta ** 2 + d_phi ** 2) ** 0.5


def _pvec_norm(vec):
    """
    Normalized momentum vector. Hidden.
    """
    return _pvec(vec) / p(vec)


def _pvec_norm_T(vec):
    """
    Normalized, transposed momentum vector. Hidden.
    """
    return np.transpose(_pvec_norm(vec), axes=(0, -1, -2))[:, [0, 2, 1]]


def pair_cos(vec):
    """
    Cosine of the angle between all pairs of particles.
    """
    # cos = (p1 x p2) / (|p1| x |p2|) = (p1 / |p1|) x (p2 / |p2|)
    all_pair_cos = np.matmul(_pvec_norm(vec), _pvec_norm_T(vec))

    # return only upper triangle without diagonal
    return np.reshape(all_pair_cos, [-1, n_particles(vec) ** 2])[:, triu_indices(vec)]


def pair_ds(vec):
    """
    Sign-conserving Minkowski space distance between all pairs of particles.
    """
    # (dE**2 - dpx**2 - dpy**2 - dpz**2)**0.5
    # first, determine all 4-vector differences
    pvm = np.expand_dims(vec, axis=-2)
    pvm_T = np.transpose(pvm, axes=(0, 2, 1, 3))
    all_diffs = pvm - pvm_T

    # extract elements of the upper triangle w/o diagonal and calculate their norm
    diffs = np.reshape(all_diffs, [-1, n_particles(vec) ** 2, 4])[:, triu_indices(vec)]
    diffs_E = diffs[..., 0]
    diffs_p2 = np.sum(diffs[..., 1:] ** 2, axis=-1)

    ds = diffs_E ** 2 - diffs_p2
    return np.sign(ds) * np.abs(ds) ** 0.5


def pair_dy(vec):
    """
    Rapidity difference between all pairs of particles.
    """
    # dy = y1 - y2 = atanh(beta1) - atanh(beta2)
    beta_1 = np.expand_dims(np.clip(beta(vec), EPSILON, 1 - EPSILON), axis=-2)
    beta_2 = np.transpose(beta_1, axes=(0, 2, 1, 3))
    dy = np.arctanh(beta_1) - np.arctanh(beta_2)

    # return only upper triangle without diagonal
    return np.reshape(dy, [-1, n_particles(vec) ** 2])[:, triu_indices(vec)]


def forward_backward_asymmetry(events, axis):
    """
    computes the forward-backward assymetry of events
    with respect to the event-axis
    """
    angles = np.matmul(_pvec_norm(events), _pvec_norm_T(axis))
    forward = np.where(angles >= 0.0)[0]
    backward = np.where(angles < 0.0)[0]

    a_fb = (len(forward) - len(backward)) / (len(forward) + len(backward))
    return a_fb


def forward_backward_asymmetry_from_angles(angles):
    """
    computes the forward-backward assymetry of events
    from angles
    """
    forward = np.where(angles >= 0.0)[0]
    backward = np.where(angles < 0.0)[0]

    a_fb = (len(forward) - len(backward)) / (len(forward) + len(backward))
    return a_fb


def cosine_theta_star(events, axis):
    angles = np.matmul(_pvec_norm(events), _pvec_norm_T(axis))
    return angles


def cosine_theta_star_Collins_Soper(events, axis):
    numerator = 2 * (
        (events[:, 0, 0] + events[:, 0, 3]) / np.sqrt(2) * (events[:, 1, 0] - events[:, 1, 3]) / np.sqrt(2)
        - (events[:, 0, 0] - events[:, 0, 3]) / np.sqrt(2) * (events[:, 1, 0] + events[:, 1, 3]) / np.sqrt(2)
    )
    denominator = np.squeeze(np.sqrt(np.sum(axis ** 2, axis=-1) * (np.sum(axis ** 2, axis=-1) + np.sum(pt(axis) ** 2, axis=-1))))
    return numerator/denominator


def forward_backward_asymmetry_vector(events):
    """
    computes the forward-backward assymetry of events
    with respect to the event-axis
    """
    angles = np.matmul(_pvec_norm(events[:, 0]), _pvec_norm_T(events[:, 1]))
    forward = np.where(angles >= 0.0)[0]
    backward = np.where(angles < 0.0)[0]

    a_fb = (len(forward) - len(backward)) / (len(forward) + len(backward))
    return a_fb


def beta_scalar(particle):
    """
    input a vec4 of E, px, py, pz
    returns the scalar beta of a vector
    """
    vec4 = reshape_to_particles(particle)
    momenta = vec4[..., 1:]
    energies = np.expand_dims(vec4[..., 0], -1)

    # beta = tf.divide(tf.sqrt(tf.reduce_sum(tf.square(momenta), axis=-1)), tf.squeeze(energies, axis=-1) + EPSILON, name="beta")
    beta = np.divide(np.sqrt(np.sum(np.square(momenta), axis=-1)), np.squeeze(energies, axis=-1),)
    return np.expand_dims(beta, axis=-1)


def gamma_scalar(vec4):
    """
    input a vec4 of E, px, py, pz
    returns the scalar gamma of a vector
    """
    vec4 = reshape_to_particles(vec4)
    momenta = vec4[..., 1:]
    energies = np.expand_dims(vec4[..., 0], -1)
    beta = beta_scalar(vec4)
    # gamma = np.divide(1., np.sqrt(1. - np.square(beta)), name="gamma")
    gamma = np.divide(1.0, np.sqrt(1.0 - np.square(beta - EPSILON)))
    return gamma


def boost(particle, restframe):
    n_restframes = n_particles(restframe)

    beta_vec3 = beta_vec(restframe)
    beta = beta_scalar(restframe)
    gamma = gamma_scalar(restframe)
    energies = reshape_to_particles(restframe)[..., 0]
    e_vector = np.expand_dims(np.concatenate([np.expand_dims(np.ones_like(energies), axis=-1), -beta_vec3 / beta], axis=-1,), axis=-1,)
    e_vector_T = np.transpose(e_vector, axes=[0, 1, 3, 2])

    identity = np.identity(4)
    U_matrix = np.array([[-1, 0, 0, 0]] + 3 * [[0, -1, -1, -1]])

    identity = np.reshape(np.tile(identity, [n_restframes, 1]), [n_restframes, 4, 4])
    U_matrix = np.reshape(np.tile(U_matrix, [n_restframes, 1]), [n_restframes, 4, 4])

    beta = np.expand_dims(beta, axis=-1)
    gamma = np.expand_dims(gamma, axis=-1)
    """
    (1, 0) + ((-1+g)*b-1, g*b)
    (0, 1)   (g*b, (-1+g)*b-1)
    """
    Lambda = np.add(identity, (U_matrix + gamma) * ((U_matrix + 1) * beta - U_matrix) * np.matmul(e_vector, e_vector_T),)
    particle = reshape_to_particles(particle)
    particle = np.expand_dims(particle, -1)
    boosted_particle = np.squeeze(np.matmul(Lambda, particle), axis=-1)
    return boosted_particle


def np_non_zero(a, epsilon):
    """
    Ensures that all zeros in a array *a* are replaced by *epsilon*.
    """
    # use combination of abs and sign instead of a where op
    return a + (1 - np.abs(np.sign(a))) * epsilon


def tril_range(n, k=-1):
    """
    Returns a 1D numpy array containing all lower triangle indices of a square matrix with size *n*.
    *k* is the offset from the diagonal.
    """
    tril_indices = np.tril_indices(n, k)
    return np.arange(n ** 2).reshape(n, n)[tril_indices]


def triu_range(n, k=1):
    """
    Returns a 1D numpy array containing all upper triangle indices of a square matrix with size *n*.
    *k* is the offset from the diagonal.
    """
    triu_indices = np.triu_indices(n, k)
    return np.arange(n ** 2).reshape(n, n)[triu_indices]


def triu_indices(vec):
    """
    Triu indices of vector
    """
    return triu_range(n_particles(vec))


def momentum_conservation(vec, axis=1):
    ret = np.sum(vec[..., axis], axis=-1) / np.sum(np.abs(vec[..., axis]) + 1e-4, axis=-1)
    return ret


def reshape_to_particles(vec):
    """
    returns a 4-vector reshaped to (Batch-size, n_particles, 4)
    """
    if len(vec.shape) == 2:
        if vec.shape[-1] % 4 == 0:
            return np.reshape(vec, (vec.shape[0], vec.shape[-1] // 4, 4))
        else:
            raise ValueError("Error in reshaping.\nShape {} does not fit to size of a 4-vector!".format(vec.shape))
    elif len(vec.shape) == 3:
        if vec.shape[-1] == 4:
            return vec
        else:
            raise ValueError("Error in reshaping.\nShape {} does not fit to size of a 4-vector!".format(vec.shape))
    else:
        raise ValueError("Error in reshaping.\nShape must have length 2 or 3 but had lenght: {}!".format(len(vec.shape)))


def add_combined_particles(vec, combined_idx):
    """
    combines 4-vectors to particles (by simple addition)
    combined_idx needs to be a list of lists!
    """
    if combined_idx is None or combined_idx == []:
        return vec
    vec = reshape_to_particles(vec)
    comb = []
    if not isinstance(combined_idx[0], list):
        combined_idx = [combined_idx]
    # this line just adds the 4-vectors of the combined_idx and puts them in shape (batch, n_particles, 4)
    comb = np.concatenate([np.expand_dims(np.sum(vec[:, cidx, :], axis=-2), axis=1) for cidx in combined_idx], axis=-2,)
    return np.concatenate((vec, comb), axis=1)


def feature_collection(events, **kwargs):
    """
    computes all features of the given events, event by event
    Data should be given in the shape (N_events, N_particles, 4)
    Else they are reshaped from (N_events, N_particles * 4) to the 

    Returns a dict with all features
    """
    feature_dict = {}
    if len(events.shape) == 2:
        events = np.reshape(events, (events.shape[0], events.shape[1] // 4, 4))
    if kwargs.get("resonance_last", False) is False:
        sys_momentum = np.expand_dims( np.sum(events, axis=-2), axis=1)
    else:
        sys_momentum = np.expand_dims(events[:, -1], axis=-2)
    feature_dict["E"] = E(events)
    feature_dict["pvec"] = _pvec(events)
    feature_dict["px"] = px(events)
    feature_dict["py"] = py(events)
    feature_dict["pz"] = pz(events)
    feature_dict["p"] = p(events)
    feature_dict["pt"] = pt(events)
    feature_dict["eta"] = eta(events)
    feature_dict["phi"] = phi(events)
    feature_dict["theta"] = theta(events)
    feature_dict["m"] = m(events)
    feature_dict["beta"] = beta(events)
    feature_dict["gamma"] = gamma(events)
    feature_dict["pair_dr"] = pair_dr(events)
    feature_dict["pair_ds"] = pair_ds(events)
    feature_dict["pair_dy"] = pair_dy(events)
    feature_dict["pair_cos"] = pair_cos(events)
    feature_dict["px_conservation"] = momentum_conservation(events, axis=1)
    feature_dict["py_conservation"] = momentum_conservation(events, axis=2)
    feature_dict["cosine_theta_star"] = cosine_theta_star(events[:, :-1], sys_momentum)
    return feature_dict
