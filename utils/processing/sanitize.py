import luigi
import numpy as np


def sanitize_input(inp):
    if isinstance(inp, luigi.freezing.FrozenOrderedDict):
        inp = dict(inp)
    if isinstance(inp, list):
        ret_list = []
        for item in inp:
            ret_list.append(sanitize_input(item))
        return ret_list
    elif isinstance(inp, dict):
        ret_dict = {}
        for key, value in inp.items():
            ret_dict[key] = sanitize_input(value)
        return ret_dict
    elif isinstance(inp, (np.int32, np.int64)):
        inp = int(inp)
    elif isinstance(inp, (np.float32, np.float64)):
        inp = float(inp)
    elif isinstance(inp, (np.bool_)):
        inp = bool(inp)
    return inp
