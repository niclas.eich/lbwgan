import numpy as np
from scipy.spatial.distance import jensenshannon
from utils.processing.physics_observables import (
    reshape_to_particles,
    add_combined_particles,
    feature_collection,
)


def similarity_score(
    dataset_a,
    dataset_b,
    comb_idx=None,
    compare_keys=["px", "pz", "pz", "E", "m", "eta", "phi"],
):
    dataset_a = reshape_to_particles(dataset_a)
    dataset_b = reshape_to_particles(dataset_b)

    dataset_a = add_combined_particles(dataset_a, comb_idx)
    dataset_b = add_combined_particles(dataset_b, comb_idx)

    features_a = feature_collection(dataset_a)
    features_b = feature_collection(dataset_b)

    metrics = []
    for metric in compare_keys:
        for i_p in range(len(features_a[metric][0])):
            if i_p < 2 and metric == "m":
                continue
            hist_a, edges = np.histogram(features_a[metric][:, i_p], bins=200)
            hist_b, _ = np.histogram(features_b[metric][:, i_p], bins=edges)
            metrics.append(jensenshannon(hist_a, hist_b))
    metrics = np.array(metrics)
    return metrics
