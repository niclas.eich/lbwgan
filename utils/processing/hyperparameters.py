import numpy as np
from utils.processing.sanitize import sanitize_input

CRIT_RANGES = {
    "layers": np.arange(2, 7, 1),
    "L2": np.append(np.logspace(-2, -4, 4), [0.0]),
    "lbn": [True],
    "lbn_features": [["E", "px", "py", "pz", "m", "pt"]],
    "lbn_particles": np.arange(3, 7, 1),
    "nodes": np.arange(100, 700, 7),
    "activation": ["leaky_relu", "relu", "selu"],
    "batch_norm": [False],
    "learning_rate": np.logspace(-2, -6, 9),
    "optimizer": ["ADAM"],
    "callbacks": [["reset_optimizer", "lr_decay"]],
    "dense_net": {
        "layers": np.arange(1, 4, 1),
        "nodes": np.arange(100, 700, 20),
        "blocks": np.arange(1, 5, 1),
        "activation": ["leaky_relu", "relu", "selu"],
    },
}

GEN_RANGES = {
    "layers": np.arange(2, 7, 1),
    "nodes": np.arange(200, 700, 7),
    "activation": ["relu", "selu"],
    "batch_norm": [False, True],
    "L2": np.append(np.logspace(-2, -4, 4), [0.0]),
    "latent_dim": np.arange(10, 50, 5),
    "latent_shape": ["normal"],
    "learning_rate": np.logspace(-2, -6, 9),
    "hinge_loss": [0.0],
    "optimizer": ["ADAM"],
    "trainable_axis": [False],
    "callbacks": [
        ["pdu", "reset_optimizer", "lr_decay"],
        ["pdu", "reset_optimizer", "lr_decay"],
    ],
    "pdu": {
        "mass_kernel": ["constant"],
        "mass_init": [[0.105, 0.105]],
        "rotation_kernel": {
            "nodes": np.arange(20, 500, 20),
            "layers": np.arange(2, 7, 1),
            "blocks": np.arange(1, 4, 1),
            "kernel_regularizer": np.append(np.logspace(-2, -4, 4), [0.0]),
            "activation": ["relu", "selu", "tanh"],
            "mass_activation": ["zero"],
        },
        "boost_kernel": [None],
    },
    "param_network": {
        "nodes": np.arange(20, 500, 20),
        "layers": np.arange(2, 7, 1),
        "blocks": np.arange(1, 4, 1),
        "activation": ["relu", "selu", "tanh"],
    },
    "feature_network": {
        "nodes": np.arange(100, 700, 20),
        "layers": np.arange(3, 7, 1),
        "blocks": np.arange(1, 4, 1),
        "activation": ["relu", "selu", "tanh"],
    },
}

# "boost_kernel": {"nodes": np.arange(20, 500, 20),
# "layers": np.arange(2, 7, 1),
# "blocks": np.arange(1, 4, 1),
# "kernel_regularizer": np.append(np.logspace(-2, -4, 4), [0.]),
# "activation": ["relu", "selu", "tanh"],
# "mass_activation": ["zero"]}},

GENERAL_RANGES = {
    "batch_size": np.arange(256, 2000, 128),
    "GP": np.logspace(0.0, 2.0, 10),
    "n_crit": np.arange(5, 20, 1),
    "n_gen": [1],
}
"""
Don't forget to fill in epochs manually!
"""


def draw_dicts(ranges, n_random, process="Zmumu"):
    ret_dicts = {}
    for key, value in ranges.items():
        if key == "mass_init":
            if process == "Zmumu":
                ret_dicts[key] = np.random.choice([[0.105, 0.105]], n_random)
            elif process == "Zee":
                ret_dicts[key] = np.random.choice([[0.000511, 0.000511]], n_random)
        elif isinstance(value, (list, np.ndarray)):
            if isinstance(value[0], (list, np.ndarray)):
                mask = np.random.choice(np.arange(0, len(value), 1), n_random)
                ret_dicts[key] = list(map(list, np.array(value)[mask]))
            else:
                ret_dicts[key] = np.random.choice(value, n_random)
        elif isinstance(value, dict):
            ret_dicts[key] = draw_dicts(value, n_random)
        else:
            raise ValueError("Some Case not Handled!")
    return ret_dicts


def dl_to_ld(d, n_random):
    ret_list = []
    for idx in range(n_random):
        ret_dict = {}
        for key, value in d.items():
            if isinstance(value, (list, np.ndarray)):
                ret_dict[key] = value[idx]
            elif isinstance(value, dict):
                ret_dict[key] = dl_to_ld(value, n_random)[idx]
        ret_list.append(ret_dict)
    return ret_list


def draw_random_gan_params(n=4, epochs=10, seed=123):
    np.random.seed(seed)
    param_dict_crit = dl_to_ld(draw_dicts(CRIT_RANGES, n), n)

    param_dict_gen = dl_to_ld(draw_dicts(GEN_RANGES, n), n)

    param_dict_general = dl_to_ld(draw_dicts(GENERAL_RANGES, n), n)
    if epochs is not None:
        for pdg in param_dict_general:
            pdg["epochs"] = epochs

    param_dict_data = {
        "dataset": "best",
        "particle_process": "Zmumu",
        "dataorigin": "gen",
        "datapolicy": "train",
        "particle_indexes": [0, 1],
        "k_split_folds": 2,
        "k_split_number": 0,
        "seed": 102,
    }
    param_dict_data = [param_dict_data for _ in range(n)]
    dicts = [
        {
            "architecture_config_crit": d_crit,
            "architecture_config_gen": d_gen,
            "config_general": general,
            **d_data,
        }
        for d_crit, d_gen, general, d_data in zip(
            param_dict_crit, param_dict_gen, param_dict_general, param_dict_data
        )
    ]
    return sanitize_input(dicts)
