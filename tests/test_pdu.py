import unittest
import numpy as np
import tensorflow as tf
from model.modules import mass, mass_to_energy_vec4, energy_to_mass_vec4, boost


def relative_vector_difference(array_a, array_b):
    rel_diffs = np.abs(array_a - array_b) / np.abs(array_a)
    return rel_diffs.flatten()


class UtilsTestCase(unittest.TestCase):
    """
    Unit tests for all utiliry functions
    """

    n_particles = 2
    batch_size = 8096
    epsilon = 2.0 * 1e-5
    # how much percent may differ
    grace_range = 1e-3

    def setUp(self):
        self.particle_placeholder = tf.placeholder(
            tf.float32, shape=(self.batch_size, self.n_particles, 4)
        )
        self.sess = tf.Session()

        self.vec4 = np.zeros((self.batch_size, self.n_particles, 4))
        self.masses = np.ones((self.batch_size, self.n_particles))
        self.masses *= np.random.uniform(1e-5, 1e4, (self.batch_size, self.n_particles))

        self.momenta = np.random.normal(0, 1e3, (self.batch_size, self.n_particles, 3))
        self.vec4[:, :, 1:] = self.momenta
        self.vec4[:, :, 0] = np.sqrt(
            np.square(self.masses) + (np.sum(np.square(self.momenta), axis=-1))
        )

    def tearDown(self):
        tf.reset_default_graph()

    def assertRelDiffThreshold(self, array_a, array_b, threshold=grace_range):
        rel_diffs = relative_vector_difference(array_a, array_b)
        self.assertTrue(
            len(
                np.where((np.greater(rel_diffs, self.epsilon).flatten()))[0]
                / (self.n_particles * self.batch_size)
                < self.grace_range
            )
        )

    def assertGreaterArray(self, array_a, eps=0):
        self.assertTrue(any(np.greater(array_a.flatten(), eps)))

    def test_mass_unit(self):
        print("Running Test for mass-unit")
        self.y = mass(self.particle_placeholder)
        masses = np.squeeze(
            self.sess.run(self.y, feed_dict={self.particle_placeholder: self.vec4})
        )
        self.assertGreaterArray(masses, 0.0)
        rel_diffs = relative_vector_difference(masses, self.masses)
        self.assertRelDiffThreshold(masses, self.masses)

    def test_conversion_unit(self):
        print("Running Test for conversion-unit")
        self.y = mass_to_energy_vec4(self.particle_placeholder)
        self.z = energy_to_mass_vec4(self.y)
        vecs = np.squeeze(
            self.sess.run(self.z, feed_dict={self.particle_placeholder: self.vec4})
        )
        rel_diffs = relative_vector_difference(self.vec4, vecs)
        self.assertFalse(any(np.greater(rel_diffs, self.epsilon)))

    def test_boost(self):
        print("Running Test for boost-unit")
        self.y = boost(self.particle_placeholder, self.particle_placeholder)
        vecs = self.sess.run(self.y, feed_dict={self.particle_placeholder: self.vec4})
        self.assertRelDiffThreshold(np.mean(vecs, axis=0), 0)
        self.assertRelDiffThreshold(np.std(vecs, axis=0), 0)
