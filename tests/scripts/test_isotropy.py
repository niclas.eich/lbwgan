import os
import tensorflow as tf
import numpy as np
from utils.plotting.sphere import plot_sky_map
from utils.plotting.histograms import plot_hist
from utils.processing.physics_observables import phi, reshape_to_particles
from model.layers import particle_rotation_layer_2d, particle_rotation_layer_3d


TAG = "repaired_random_angles"

NBATCH = 1024
PLOT_DIR = "tests/scripts/plots/rotations"
sess = tf.Session()

os.makedirs(PLOT_DIR, exist_ok=True)


def random_4_vecs(n):
    xs = np.random.uniform(0.2, 1.0, (n, 2))
    ys = np.zeros((n, 2))
    zs = np.cos(np.random.uniform(0.0, 1.0, (n, 2)) * np.pi)
    E = np.sqrt(xs ** 2 + ys ** 2 + zs ** 2)

    # xs[: , 1] = xs[:, 0]* (-1)
    # ys[: , 1] = ys[:, 0]* (-1)
    # zs[: , 1] = zs[:, 0]* (-1)
    return np.stack([E, xs, ys, zs], axis=-1)


inp = tf.keras.Input((2, 4), batch_size=NBATCH)
# rot = particle_rotation_layer_2d(random=True, init_spherical_angles=[0.0, 0.0], global_angle=True, trainable_axis=False)
rot = particle_rotation_layer_3d(random=True, random_axis=True, global_axis=True)

rotated, ax = rot(inp, ret_all=True)


with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    vecs = random_4_vecs(NBATCH)
    out = sess.run(rotated, feed_dict={inp: vecs})
    for i in range(100):
        if i % 10 == 0:
            print("{} %".format(i + 10))
        vecs = random_4_vecs(NBATCH)
        tmpout = reshape_to_particles(sess.run(rotated, feed_dict={inp: vecs}))
        out = np.append(out, tmpout, axis=0)
    # from IPython import embed;embed()
for i in range(out.shape[1]):
    print("# Events: {}".format(len(out[..., i])))
    plot_sky_map(out[..., i, 1:], fname="{}/{}_skymap_{}.png".format(PLOT_DIR, TAG, i))
    ph = phi(out[:, i])
    plot_hist(ph, "Sample", "$\phi$", "{}/{}_phi_hist_{}.png".format(PLOT_DIR, TAG, i))
