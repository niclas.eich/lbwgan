import os
import numpy as np
import tensorflow as tf
from utils.processing.physics_observables import feature_collection, eta
from utils.plotting.histograms import plot_hist
from model.layers import particle_decay_unit

BATCH_SIZE = 40000
NSAMPLES = 2

data = np.load("/Users/niclaseich/data/Zll/data.npy")

plot_dir = "tests/plots/z_eta/"
os.makedirs(plot_dir, exist_ok=True)

muons = data[255757:]

z = np.expand_dims(np.sum(muons, axis=-2), axis=-2)

cuts = np.array([0.5, 0.0, 0.5, 0])
cuts = np.ones((BATCH_SIZE, 4)) * cuts

f_z = feature_collection(z)
f_muon = feature_collection(muons)
eta_cut_pos = np.max(f_muon["eta"])
eta_cut_neg = np.min(f_muon["eta"])

sess = tf.Session()
x = tf.placeholder(tf.float32, (BATCH_SIZE, 1, 4))
x_cuts = tf.placeholder(tf.float32, (BATCH_SIZE, 4))
pdu = particle_decay_unit(
    mass_init=np.array([0.105, 0.105]), mass_kernel="constant", batch_size=BATCH_SIZE
)
y = pdu([x, x_cuts])
sess.run(tf.global_variables_initializer())

print("eta_cut+:{0:1.3f}".format(eta_cut_pos))
print("eta_cut-:{0:1.3f}".format(eta_cut_neg))
print("Start sampling for different Z")

hist_boundaries = {
    "x_min_func": lambda x: eta_cut_neg * 1.3,
    "x_max_func": lambda x: eta_cut_pos * 1.3,
}

for i in range(NSAMPLES):
    print("... sample {0:2d}".format(i))
    sample_idx = np.random.choice(np.arange(0, len(z)), 1)[0]
    input_data = np.ones((BATCH_SIZE, 1, 4)) * z[sample_idx]
    print(
        "\tZ: ({0:2.1f}, {1:2.1f}, {2:2.1f}, {3:2.1f})".format(
            z[sample_idx, 0, 0],
            z[sample_idx, 0, 1],
            z[sample_idx, 0, 2],
            z[sample_idx, 0, 3],
        )
    )
    print("\tZ-pt:  {0:2.1f}".format(f_z["pt"][sample_idx, 0, 0]))
    print("\tZ-eta: {0:2.1f}".format(f_z["eta"][sample_idx, 0, 0]))
    res = sess.run(y, feed_dict={x: input_data, x_cuts: cuts})
    eta_res = eta(res)
    fname_1 = os.path.join(plot_dir, "eta_sample_m1_{0:02d}.png".format(i))
    fname_2 = os.path.join(plot_dir, "eta_sample_m2_{0:02d}.png".format(i))
    kwargs = {
        "axvline": [eta_cut_pos, eta_cut_neg],
        "config": hist_boundaries,
        "legend_loc": 9,
        "histtype": "stepfilled",
    }
    plot_hist(
        eta_res[:, 0, :], "Sample {}".format(i), "$\eta_{\mu_1}$", fname_1, **kwargs
    )
    plot_hist(
        eta_res[:, 1, :], "Sample {}".format(i), "$\eta_{\mu_2}$", fname_2, **kwargs
    )
