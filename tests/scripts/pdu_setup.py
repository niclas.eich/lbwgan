import tensorflow as tf
from model.layers import particle_decay_unit, mass_block
from utils.processing.physics_observables import feature_collection
import numpy as np

BATCH_SIZE = 100

softmax_masses = mass_block(nodes=50, layers=3, activation="tanh")
x = tf.placeholder(tf.float32, (BATCH_SIZE, 1, 4))
pdu = particle_decay_unit(
    mass_kernel="constant", mass_init=[0.105, 0.210], batch_size=BATCH_SIZE
)

sseq = tf.keras.Sequential()
sseq.add(pdu)

y = sseq(x)

data = np.zeros((100, 1, 4))
data[..., 0] += np.random.uniform(1e-4, 1.0, (100, 1))
data[..., 1:] = 1e-5
data = np.clip(data, 0.0, None)
sess = tf.Session()

sess.run(tf.global_variables_initializer())

res, res_unboosted = sess.run(
    [y, sseq.layers[0].rotated_particles], feed_dict={x: data}
)

res_added = np.expand_dims(np.sum(res, axis=-2), axis=1)

diff_vec4 = data - res_added

f_data = feature_collection(data)
f_res_added = feature_collection(res_added)
f_res = feature_collection(res)
