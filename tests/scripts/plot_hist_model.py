import os
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import tensorflow.keras.backend as K
from IPython import embed
from model.layers import (
    combine_to_resonance,
    dense_block,
    feature_statistics_layer,
    fully_connected_block,
    particle_decay_unit,
    particle_features_layer,
    particle_layer,
    particle_rotation_layer_2d,
    residual_layer,
    residual_network,
)
from utils.processing.physics_observables import m

c_o = {
    "particle_layer": particle_layer,
    "particle_rotation_layer_2d": particle_rotation_layer_2d,
    "residual_network": residual_network,
}
# pretrained_z_block = tf.keras.models.load_model(str(os.getenv("PRETRAINED_MODEL")), custom_objects=c_o)
pretrained_z_block = tf.keras.models.load_model(
    str(os.getenv("PRETRAINED_TEST")), custom_objects=c_o
)
sess = tf.Session()
data = []
sess.run(tf.global_variables_initializer())
pretrained_z_block.load_weights(str(os.getenv("PRETRAINED_TEST")))
print("Weight:")
print(pretrained_z_block.layers[1].get_weights()[0][0, 0:4])

noise = tf.random.normal((512, 30), 0.0, 1.0)

vecs = pretrained_z_block(noise)

n_it = 4000

for i in range(n_it):
    out = sess.run(vecs)
    # out = sess.run(pretrained_z_block.output, feed_dict={pretrained_z_block.input: np.random.normal(0.0, 1.0, (512, 30))})
    data.append(out)
data = np.reshape(np.array(data), (n_it * 512, 1, 4))
data = data * 107.0
mass = m(data).flatten()

print("#events:\t", len(mass))
print("Mean:\t", np.mean(mass), "+", np.std(mass))
print("Weight:")
print(pretrained_z_block.layers[1].get_weights()[0][0, 0:4])
# for l in pretrained_z_block.layers:
# print(l.name)
plt.hist(mass, bins=50)
plt.show()
