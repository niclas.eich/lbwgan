import os
import tensorflow as tf
import numpy as np
from model.models import WGAN
from model.logger import Logger
from model.trainer import WGANTrainer

WGAN_config = {
    "layers": 3,
    "nodes": 20,
    "optimizer": "ADAM",
    "activation": "relu",
    "LBN": True,
    "input_dim": [32],
    "LBN_particles": 8,
    "LBN_features": None,
    "epochs": 1,
    "batch_size": 256,
    "learning_rate": 0.0001,
    "L2": 0.000001,
    "batch_norm": True,
    "GP": 10.0,
    "n_crit": 3,
    "n_gen": 10,
    "normal_dist": False,
    "reduce_n_crit": False,
    "latent_dim": 10,
    "output_dim": [32],
    "summary_dir": "/tmp/tests/summary/",
    "checkpoint_dir": "/tmp/tests/checkpoints/",
}

test_saving = False
test_loading = True

if __name__ == "__main__":
    if not (os.path.isdir(WGAN_config["summary_dir"])):
        os.makedirs(WGAN_config["summary_dir"])
    if not (os.path.isdir(WGAN_config["checkpoint_dir"])):
        os.makedirs(WGAN_config["checkpoint_dir"])

    config = tf.ConfigProto(device_count={"GPU": 0})
    if test_saving is True:
        with tf.Session(config=config) as sess:
            WGAN = WGAN(WGAN_config)
            trainer = WGANTrainer(sess, WGAN, None, WGAN_config, None)
            WGAN.save(sess)
            vars = [v for v in tf.trainable_variables()]
            print(vars[0])
            print(sess.run(vars[0][0, 0:6]))
    elif test_loading is True:
        with tf.Session(config=config) as sess:
            WGAN = WGAN(WGAN_config)
            WGAN.load(sess)
            vars = [v for v in tf.trainable_variables()]
            print(vars[0])
            print(sess.run(vars[0][0, 0:6]))
