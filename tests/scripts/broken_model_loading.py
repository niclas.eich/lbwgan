print("Please enter path of model to load:")
path = input()
import os
import json
import time
import numpy as np
import tensorflow as tf
from model.evaluater import WGANGenerator
from model.logger import Logger
from model.models import WGAN, DetectorAcceptanceWGAN
from model.trainer import WGANTrainer
from model.layers import (
    combine_to_resonance,
    dense_block,
    feature_statistics_layer,
    fully_connected_block,
    particle_decay_unit,
    particle_features_layer,
    particle_layer,
    particle_rotation_layer_2d,
    residual_layer,
    residual_network,
)
from IPython import embed
from model.lbn import LBNLayer
from tensorflow.python import debug as tf_debug

tf.debugging.set_log_device_placement(True)
gpus = tf.config.experimental.list_physical_devices("GPU")


def load_data():
    muon_path = os.path.join(os.getenv("DATA_PATH_Zll"), "data.npy")
    muons = np.load(muon_path)[255757:]
    muons = muons / np.std(muons)

    for event in muons:
        np.random.shuffle(event)
    muons = np.reshape(muons, (-1, 8))
    return muons


def load_model(path, tag="fail"):
    c_path = os.path.join(path, "critic_{}.h5".format(tag))
    g_path = os.path.join(path, "generator_{}.h5".format(tag))
    assert os.path.isfile(c_path)
    assert os.path.isfile(g_path)

    custom_obj_crit = {
        "leaky_relu": tf.nn.leaky_relu,
        "particle_layer": particle_layer,
        "feature_statistics_layer": feature_statistics_layer,
        "dense_block": dense_block,
        "residual_network": residual_network,
        "particle_features_layer": particle_features_layer,
        "fully_connected_block": fully_connected_block,
    }
    custom_obj_crit["LBNLayer"] = LBNLayer
    custom_obj_gen = {
        "particle_layer": particle_layer,
        "particle_rotation_layer_2d": particle_rotation_layer_2d,
        "particle_features_layer": particle_features_layer,
        "residual_network": residual_network,
        "dense_block": dense_block,
        "fully_connected_block": fully_connected_block,
        "particle_decay_unit": particle_decay_unit,
        "residual_layer": residual_layer,
        "combine_to_resonance": combine_to_resonance,
    }
    generator = tf.keras.models.load_model(g_path, custom_objects=custom_obj_gen)
    critic = tf.keras.models.load_model(c_path, custom_objects=custom_obj_crit)

    return critic, generator


if __name__ == "__main__":
    muons = load_data()
    data = {"x_train": muons}

    assert os.path.isdir(path)
    config_path = os.path.join(path, "config.json")
    with open(config_path) as json_file:
        config = json.load(json_file)

    print("Seeing {} GPUs".format(len(gpus)))
    sess_config = tf.ConfigProto()
    sess = tf.Session(config=sess_config)
    sess = tf_debug.LocalCLIDebugWrapperSession(sess)
    sess.add_tensor_filter("has_inf_or_nan", tf_debug.has_inf_or_nan)
    with sess:
        options = tf.RunOptions(output_partition_graphs=True)
        metadata = tf.RunMetadata()
        wgan = WGAN(config)
        logger = Logger(sess, config)
        trainer = WGANTrainer(sess, wgan, data, config, logger, verbose=False)
        wgan.load(wgan.total_config, epoch="secure", weights_only=True)
        wgan.load_meta_graph(sess)
        trainer.config_general["epochs"] = sess.run(wgan.cur_epoch_tensor) + 5
        print("running until epoch: {}".format(trainer.config_general["epochs"]))
        sess_run_kwargs = {"options": options, "run_metadata": metadata}
        t_start = time.time()
        success = trainer.train(
            secure_safe=False, no_save=True, sess_run_kwargs=sess_run_kwargs
        )
        t_end = time.time()
        print("trained in {}".format(t_end - t_start))
