import os
import numpy as np
from utils.processing.dataset import Dataset
from utils.processing.physics_observables import feature_collection
from utils.plotting.histograms import create_histogram2d, create_histogram2d_map


def plot_correlations(features, f_diag, f_top, f_bot, idx, fname):
    graph_dict = {}
    graph_dict["width"] = len(idx)
    graph_dict["height"] = len(idx)
    for i in range(len(idx)):
        for j in range(len(idx)):
            plot_dict = {}
            if any(np.equal(triu, [i, j]).all(1)):
                plot_dict["dim"] = 2
                plot_dict["x_data"] = np.squeeze(features[f_top][:, i])
                plot_dict["y_data"] = np.squeeze(features[f_top][:, j])
            elif any(np.equal(triu_bot, [i, j]).all(1)):
                plot_dict["dim"] = 2
                plot_dict["x_data"] = np.squeeze(features[f_bot][:, i])
                plot_dict["y_data"] = np.squeeze(features[f_bot][:, j])
            elif i == j:
                plot_dict["dim"] = 1
                plot_dict["x_data"] = np.squeeze(features[f_diag][:, i])
            graph_dict[(i, j)] = plot_dict
    create_histogram2d_map(graph_dict, fname)


if __name__ == "__main__":

    x = np.load(os.path.join(os.getenv("DATA_PATH"), "low__best/x_train.npy"))
    x = np.reshape(x, (x.shape[0], x.shape[-1] // 4, 4))

    triu = np.triu_indices(8, 1)
    triu = np.transpose(np.array([triu[0], triu[1]]))
    triu_bot = np.flip(triu, axis=1)

    PLOT_DIR = "tests/plots/data/metrics/"
    os.makedirs(PLOT_DIR, exist_ok=True)

    features = [["m", "eta", "phi"], ["E", "beta", "gamma"], ["pt", "px", "pz"]]
    for name, idx in zip(["full", "bb"], [list(range(0, 8)), [4, 5]]):
        f_dict = feature_collection(x[..., idx, :])
        for feature in features:
            f_name = os.path.join(PLOT_DIR, name + "_".join(feature) + ".png")
            plot_correlations(f_dict, feature[0], feature[1], feature[2], idx, f_name)
