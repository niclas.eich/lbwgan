import os
import numpy as np
from utils.processing.physics_observables import feature_collection
from utils.plotting.histograms import create_histogram2d_map, create_histogram


x = np.load(os.path.join(os.getenv("DATA_PATH"), "low__best/x_train.npy"))
x = x

x = np.reshape(x, (x.shape[0], x.shape[-1] // 4, 4))

x_map = []
y_map = []
x_labels = []

triu = np.triu_indices(8, 1)
triu = np.transpose(np.array([triu[0], triu[1]]))

triu_bot = np.flip(triu, axis=1)

f_dict = feature_collection(x)
plot_dir = "tests/plots/"

for i in range(8):
    x_row = []
    y_row = []
    x_label_rpw = []
    for j in range(8):
        if any(np.equal(triu, [i, j]).all(1)):
            x_row.append(np.squeeze(f_dict["eta"][:, i]))
            y_row.append(np.squeeze(f_dict["eta"][:, j]))
            x_label_rpw.append("")
        # if any(np.equal(triu, [i, j]).all(1)):
        # x_row.append(f_dict["pair_cos"][np.where(np.equal(triu, [i,j]).all(1))[0][0]])
        # x_label_rpw.append("")
        # y_row.append(None)
        # create_histogram(f_dict["pair_cos"][np.where(np.equal(triu, [i,j]).all(1))[0][0]], ["X_Train"], "cos_particle_{}_{}".format(i, j), os.path.join(plot_dir, "cos_particle_{}_{}".format(i, j)), scale="linear", n_bins=40)
        elif i == j:
            x_row.append(f_dict["E"][:, i])
            y_row.append(None)
            x_label_rpw.append("")
            create_histogram(
                np.squeeze(f_dict["E"][:, i]),
                ["X_Train"],
                "energy_{}".format(i),
                os.path.join(plot_dir, "energy_{}".format(i)),
                scale="linear",
            )
        elif any(np.equal(triu_bot, [i, j]).all(1)):
            x_row.append(np.squeeze(f_dict["phi"][:, i]))
            y_row.append(np.squeeze(f_dict["phi"][:, j]))
            x_label_rpw.append("")
        else:
            x_row.append(None)
            y_row.append(None)
            x_label_rpw.append(None)
    x_map.append(x_row)
    y_map.append(y_row)
    x_labels.append(x_label_rpw)
fname = os.path.join(plot_dir, "test_2dhist_01.png")

create_histogram2d_map(x_map, y_map, x_labels, None, fname, n_bins=40)
