import os
import tensorflow as tf
import numpy as np
import scipy.stats as scs
from model.utils.processing import get_update_ops
from utils.plotting.histograms import create_histogram
from model.layers import momentum_shape_transformation_layer

n_bins = 5000
batch_size = 256

normal_width = 0.1

plot_dir = "tests/plots/transformations/layer"
if os.path.isdir(plot_dir):
    print("Directory already exists!")
else:
    os.makedirs(plot_dir)

x = np.load(os.path.join(os.getenv("DATA_PATH"), "low__best/x_train.npy"))
x = np.reshape(x, (x.shape[0], x.shape[-1] // 4, 4))
scaling = np.std(x)
x /= scaling

rv_histograms = []
print(x.shape)

for i_p in range(8):
    for i in range(1, 4):
        rv_histograms.append(scs.rv_histogram(np.histogram(x[:, i_p, i], n_bins)))

tf.keras.backend.clear_session()
sseq = tf.keras.Sequential()
# sseq.add(tf.keras.layers.Dense(32, input_shape=(8, 4)))
sseq.add(tf.keras.layers.BatchNormalization(input_shape=(8, 4), trainable=True))
sseq.add(
    momentum_shape_transformation_layer(
        n_particles=8, rv_hists=rv_histograms, input_shape=(32,)
    )
)
sseq.build(input_shape=(None,) + (8, 4))

sseq.save("/tmp/test_layer.h5")

sseq_loaded = tf.keras.models.load_model(
    "/tmp/test_layer.h5",
    custom_objects={
        "momentum_shape_transformation_layer": momentum_shape_transformation_layer
    },
)

x0 = tf.keras.layers.Input(
    tensor=tf.random.normal([batch_size, 8, 4], mean=0.0, stddev=normal_width)
)
print("\n\n\n", "#" * 30, "BEFORE OUTPUT WAS FED")
print("UpdateOPs BatchNorm")
for op in get_update_ops(sseq_loaded):
    print(op)
print("Done")
out = sseq_loaded(x0)
print("\n\n\n", "#" * 30, "AFTER OUTPUT WAS FED")
print("\n\nUpdateOPs BatchNorm\n")
for op in get_update_ops(sseq_loaded):
    print(op)
print("Done")
with tf.control_dependencies(get_update_ops(sseq_loaded)):
    out_controlled = tf.multiply(1.0, out, name="output")


transformed_data = []
var_init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(var_init)
    print("Training-Phase")
    for _ in range(5000):
        sess.run(out_controlled, feed_dict={tf.keras.backend.learning_phase(): 1})
    print("Inference-Phase")
    for _ in range(400):
        transformed_data.append(
            sess.run(out_controlled, feed_dict={tf.keras.backend.learning_phase(): 0})
        )
transformed_data = np.reshape(np.array(transformed_data), (400 * batch_size, 8, 4))

for i in range(8):
    print("Particle {}".format(i))
    for mom, j in zip(["px", "py", "pz"], range(1, 4)):
        data = [transformed_data[:, i, j], x[:, i, j]]
        names = ["transformed", "source"]
        create_histogram(
            data,
            names,
            "Particle {0}, {1}, width: {2:1.1f}".format(i, mom, normal_width),
            os.path.join(plot_dir, "{0}_{1}_{2:1.1f}.png".format(i, mom, normal_width)),
            scale="linear",
        )
        if j > 0:
            print("comp-{}".format(j))
            print(
                "MIN: {} vs {}".format(
                    np.min(x[:, i, j]), np.min(transformed_data[:, i, j])
                )
            )
            print(
                "MAX: {} vs {}".format(
                    np.max(x[:, i, j]), np.max(transformed_data[:, i, j])
                )
            )
