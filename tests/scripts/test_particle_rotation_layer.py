import os
import tensorflow as tf
import numpy as np
from model.layers import particle_rotation_layer_3d, particle_rotation_layer_2d
import matplotlib
import matplotlib.pyplot as plt
import tensorflow_graphics.geometry.transformation as tfg_transformations
from utils.processing.dataset import tf_non_zero
from utils.processing.physics_observables import feature_collection
from utils.plotting.histograms import create_histogram
from utils.plotting.sphere import plot_points_on_sphere

BATCH_SIZE = 512
N_IT = 100
N_EPOCHS = 10
N_DATASET = int(BATCH_SIZE * 1e3)
N_BATCHES = N_DATASET // BATCH_SIZE
N_PARTICLES = 2

# ORIENTATIONS = np.random.uniform(-1., 1., (N_PARTICLES, 4))
ORIENTATIONS = np.zeros((N_PARTICLES, 4))
ORIENTATIONS[..., 0, 1] = 1
ORIENTATIONS[..., 1, 1] = -1

PLOT_DIR = "tests/scripts/plots/transformations/rotation/new/"
os.makedirs(PLOT_DIR, exist_ok=True)


def phi(vec4):
    px = vec4[..., 1]
    py = tf_non_zero(vec4[..., 2])
    return tf.atan2(py, px)


def test_random_orientation():
    """
    This functions turns some steady rotated vectors into random configurations
    """
    # Building Sequential model
    sseq = tf.keras.Sequential()
    x0 = tf.placeholder(tf.float32, shape=(BATCH_SIZE, N_PARTICLES, 4))
    sseq.add(particle_rotation_layer_2d(random=True, init_spherical_angles=[0, 0]))
    out = sseq(x0)
    # Generate Input-Data
    a = np.ones((BATCH_SIZE, N_PARTICLES, 4))
    # accquire metrics of input-data
    f = feature_collection(a)
    print("Constant input-Vector:")
    print("Phi:\t", np.mean(f["phi"]), " +- ", np.std(f["phi"]))
    # do rotations
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        rot_a, angles = sess.run([out, sseq.layers[0].angles], feed_dict={x0: a})
        rot_a = np.reshape(rot_a, (BATCH_SIZE, N_PARTICLES, 4))
        for i in range(N_IT):
            vec, ang = sess.run([out, sseq.layers[0].angles], feed_dict={x0: a})
            vec = np.reshape(vec, (BATCH_SIZE, N_PARTICLES, 4))
            rot_a = np.concatenate((rot_a, vec))
            angles = np.concatenate((angles, ang))
    # accquire metrics of rotated vectors
    g = feature_collection(rot_a)
    print("Rotatetd input-Vector:")
    print("Phi:\t", np.mean(g["phi"]), " +- ", np.std(g["phi"]))
    print("angles:\t", np.mean(angles), " +- ", np.std(angles))
    # plot histograms
    create_histogram(
        [np.reshape(angles, [np.prod(angles.shape)])],
        "rotated-set",
        "phi",
        os.path.join(PLOT_DIR + "random_angles.png"),
        scale="linear",
    )
    create_histogram(
        [np.reshape(g["phi"], [BATCH_SIZE * (N_IT + 1) * N_PARTICLES])],
        "rotated-set",
        "phi",
        os.path.join(PLOT_DIR + "random_orientation.png"),
        scale="linear",
    )


def test_learned_rotation(noise_level, train_axis=True):
    print("Noise: {}".format(noise_level))
    if train_axis is True:
        phi_random = np.random.uniform(0, 2 * np.pi)
        theta_random = np.random.uniform(0, np.pi)
    else:
        phi_random = 0.0
        theta_random = 0.0
    print(
        "random start-axis: theta: {0:1.1f} phi: {1:1.1f}".format(
            theta_random, phi_random
        )
    )
    tf.reset_default_graph()
    sseq = tf.keras.Sequential()
    x0 = tf.placeholder(tf.float32, shape=(BATCH_SIZE, N_PARTICLES * 4))
    sseq.add(
        particle_rotation_layer_2d(
            trainable=True,
            trainable_axis=train_axis,
            init_spherical_angles=[phi_random, theta_random],
        )
    )
    out = sseq(x0)

    y0 = tf.placeholder(tf.float32, shape=(BATCH_SIZE, N_PARTICLES * 4))

    mse = tf.reduce_mean(tf.square(out - y0))
    mse_along_axis = tf.reduce_mean(
        tf.reduce_mean(
            tf.reshape(tf.square(out - y0), (BATCH_SIZE, N_PARTICLES, 4)), axis=2
        ),
        axis=0,
    )

    optimizer = tf.train.AdamOptimizer(learning_rate=1e-4, beta1=0.5, beta2=0.9)
    vars = sseq.trainable_variables
    print("vars:")
    print(vars)
    train = optimizer.minimize(mse, var_list=vars)
    """
    generate Datasets
    """
    x_random = np.ones((N_DATASET, N_PARTICLES, 4))
    x_random[..., 1:3] *= np.random.uniform(0, 1.0, (N_DATASET, N_PARTICLES, 2))
    x_random /= np.expand_dims(np.linalg.norm(x_random[..., 1:], axis=-1), -1)

    axis = np.zeros((N_DATASET, N_PARTICLES, 3))
    axis[..., -1] = 1
    hidden_angles = np.random.uniform(-np.pi, np.pi, N_PARTICLES)
    angles = np.ones((N_DATASET, N_PARTICLES)) * hidden_angles
    angle_noise = np.random.normal(0.0, noise_level, (N_DATASET, N_PARTICLES))
    angles += angle_noise
    angles = np.expand_dims(angles, axis=-1)
    print("Hidden rotations:")
    print(angles[0:2])
    print("Axis:")
    print(axis[0:2])
    x_rotation = tfg_transformations.axis_angle.rotate(x_random[..., 1:], axis, angles)
    phis = []
    deltas_mean = []
    deltas_std = []
    phi_spherical = []
    theta_spherical = []
    losses = []
    features_random = feature_collection(x_random)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        print("Start-Axis:")
        print(sess.run(sseq.layers[0].axis[0]))
        x_rotated = np.copy(x_random)
        x_rotated[..., 1:] = sess.run(x_rotation)
        features_rotated = feature_collection(x_rotated)
        delta_phi = features_rotated["phi"] - features_random["phi"]
        print("Delta Phis:\n")
        print(delta_phi[0:2])
        x_random = np.reshape(x_random, (N_DATASET, N_PARTICLES * 4))
        x_rotated = np.reshape(x_rotated, (N_DATASET, N_PARTICLES * 4))
        for n_epochs in range(N_EPOCHS):
            shuffle_idx = np.random.permutation(N_DATASET)
            x_random = x_random[shuffle_idx]
            x_rotated = x_rotated[shuffle_idx]
            features_random = feature_collection(x_random)
            print("EPOCH #{0:1d}".format(n_epochs))
            n_log = 100
            for i in range(N_DATASET // BATCH_SIZE):
                # print("Iteration: {0:3d}".format(i))
                _, loss, output, phi_s, theta_s = sess.run(
                    [
                        train,
                        mse_along_axis,
                        out,
                        sseq.layers[0].phis,
                        sseq.layers[0].thetas,
                    ],
                    feed_dict={
                        x0: x_random[i * BATCH_SIZE : (i + 1) * BATCH_SIZE],
                        y0: x_rotated[i * BATCH_SIZE : (i + 1) * BATCH_SIZE],
                    },
                )
                output = np.reshape(output, (BATCH_SIZE, N_PARTICLES, 4))
                if i % n_log == 0:
                    losses.append(loss)
                    phi_spherical.append(phi_s)
                    theta_spherical.append(theta_s)
                    features = feature_collection(output)
                    delta_phi = (
                        features["phi"]
                        - features_random["phi"][i * BATCH_SIZE : (i + 1) * BATCH_SIZE]
                    )
                    delta_phi = np.squeeze(delta_phi)
                    delta_phi[np.where(delta_phi > np.pi)] -= 2 * np.pi
                    delta_phi[np.where(delta_phi < -np.pi)] += 2 * np.pi
                    deltas_mean.append(np.squeeze(np.mean(delta_phi, axis=0)))
                    deltas_std.append(np.squeeze(np.std(delta_phi, axis=0)))
        print("Axis[0]\n", sess.run(sseq.layers[0].axis[0]))
    theta_spherical = np.array(theta_spherical)
    phi_spherical = np.array(phi_spherical)
    deltas_std = np.array(deltas_mean)
    deltas_mean = np.array(deltas_std)
    # deltas_mean = np.array(deltas_mean)
    # deltas_std = np.array(deltas_std)
    losses = np.array(losses)
    n_range = np.arange(0, len(deltas_mean))
    n_plots = 4
    if train_axis is False:
        n_plots = 2
    fig, ax = plt.subplots(
        N_PARTICLES, n_plots, figsize=(15, 10), sharex="all", sharey="col"
    )
    fig.suptitle(
        "Noise: {0:1.2f} {1}".format(
            noise_level, "learn_axis" if train_axis else "fixed_axis"
        )
    )
    # yrange_long = matplotlib.ticker.FixedLocator([-np.pi, -np.pi*0.5, 0., np.pi*0.5, np.pi])
    # yrange_mid = matplotlib.ticker.FixedLocator([0., np.pi*0.5, np.pi, np.pi*1.5, 2*np.pi])
    # yrange_low = matplotlib.ticker.FixedLocator([0., np.pi*0.25, np.pi*0.5, np.pi])
    for n_p in range(N_PARTICLES):
        for i in range(n_plots):
            if i == 0:
                # ax[n_p, i].yaxis.set_major_formatter(matplotlib.ticker.FormatStrFormatter('%g $\pi$'))
                # ax[n_p, i].yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(base=2.))
                # ax[n_p, i].yaxis.set_major_locator(yrange_long)
                ax[n_p, i].set_ylabel("$\\theta [rad]$")
                ax[n_p, i].errorbar(n_range, deltas_mean[..., n_p], marker=".")
                ax[n_p, i].axhline(
                    y=np.pi, color="black", alpha=0.1, linestyle="dashed"
                )
                ax[n_p, i].axhline(
                    y=0.5 * np.pi, color="black", alpha=0.1, linestyle="dashed"
                )
                ax[n_p, i].axhline(y=0, color="black", alpha=0.1, linestyle="dashed")
                ax[n_p, i].axhline(
                    y=-0.5 * np.pi, color="black", alpha=0.1, linestyle="dashed"
                )
                ax[n_p, i].axhline(
                    y=-np.pi, color="black", alpha=0.1, linestyle="dashed"
                )
                ax[n_p, i].set_ylim(-1.5 * np.pi, 1.5 * np.pi)
                ax[n_p, i].axhline(y=hidden_angles[n_p], color="red")
                ax[n_p, i].axhline(y=2 * np.pi + hidden_angles[n_p], color="red")
                ax[n_p, i].axhline(y=hidden_angles[n_p] - 2 * np.pi, color="red")
                if n_p == 0:
                    ax[n_p, i].set_title("Learned Rotation")
                if train_axis is True:
                    ax[n_p, i].axhline(
                        y=-hidden_angles[n_p], linestyle="dashed", color="green"
                    )
                    ax[n_p, i].axhline(
                        y=2 * np.pi - hidden_angles[n_p],
                        linestyle="dashed",
                        color="green",
                    )
            if train_axis is True:
                if i == 1:
                    if n_p == 0:
                        ax[n_p, i].set_title("Learned $\phi_{{spherical}}$")
                    ax[n_p, i].errorbar(n_range, phi_spherical[..., n_p], marker=".")
                    ax[n_p, i].set_ylim(0, 2.0 * np.pi)
                elif i == 2:
                    if n_p == 0:
                        ax[n_p, i].set_title("Learned $\\theta_{{spherical}}$")
                    ax[n_p, i].set_ylim(0, np.pi)
                    ax[n_p, i].errorbar(n_range, theta_spherical[..., n_p], marker=".")
                elif i == 3:
                    if n_p == 0:
                        ax[n_p, i].set_title("Loss")
                    ax[n_p, i].errorbar(
                        n_range, losses[..., n_p], marker=".", color="red"
                    )
                    ax[n_p, i].grid()
            else:
                if i == 1:
                    if n_p == 0:
                        ax[n_p, i].set_title("Loss")
                    ax[n_p, i].errorbar(
                        n_range, losses[..., n_p], marker=".", color="red"
                    )
                    ax[n_p, i].grid()

            if n_p == N_PARTICLES - 1:
                ax[n_p, i].set_xlabel("Epochs")
    fig.savefig(
        os.path.join(
            PLOT_DIR
            + "noise_{0:1.2f}_{1}.png".format(
                noise_level, "learn_axis" if train_axis else "fixed_axis"
            )
        )
    )
    plt.close()


def test_random_sphere_point(random_ax=True, global_ax=True):
    """
    This functions turns some steady rotated vectors into random configurations
    """
    # Building Sequential model

    sseq = tf.keras.Sequential()
    x0 = tf.placeholder(tf.float32, shape=(BATCH_SIZE, N_PARTICLES, 4))
    if random_ax is True:
        sseq.add(
            particle_rotation_layer_3d(
                random=True,
                random_axis=random_ax,
                global_axis=global_ax,
                global_angle=True,
            )
        )
    else:
        sseq.add(
            particle_rotation_layer_2d(
                random=True,
                random_axis=random_ax,
                global_axis=global_ax,
                global_angle=True,
            )
        )
    out = sseq(x0)
    # Generate Input-Data
    a = np.ones((BATCH_SIZE, N_PARTICLES, 4)) * ORIENTATIONS
    # accquire metrics of input-data
    # f = feature_collection(a)
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        rot_a, ax, rel_ax = sess.run(
            [out, sseq.layers[0].axis, sseq.layers[0].rel_axis], feed_dict={x0: a}
        )
        rot_a = np.reshape(rot_a, (BATCH_SIZE, N_PARTICLES, 4))
    # accquire metrics of rotated vectors
    g = feature_collection(rot_a)
    plot_points_on_sphere(
        rot_a[..., 1:],
        os.path.join(
            PLOT_DIR,
            "on_sphere_part_random_ax_{0:d}_global_ax_{1}_n_p_{2}.png".format(
                int(random_ax), int(global_ax), int(N_PARTICLES)
            ),
        ),
    )
    plot_points_on_sphere(
        ax,
        os.path.join(
            PLOT_DIR,
            "on_sphere_part_random_ax_{0:d}_global_ax_{1}_n_p_{2}_ax.png".format(
                int(random_ax), int(global_ax), int(N_PARTICLES)
            ),
        ),
    )
    plot_points_on_sphere(
        rel_ax,
        os.path.join(
            PLOT_DIR,
            "on_sphere_part_random_ax_{0:d}_global_ax_{1}_n_p_{2}_rel_ax.png".format(
                int(random_ax), int(global_ax), int(N_PARTICLES)
            ),
        ),
    )


if __name__ == "__main__":
    # if os.path.isdir(PLOT_DIR):
    # print("Directory already exists!")
    # else:
    # os.makedirs(PLOT_DIR)
    # test_random_orientation()
    # for n in [0.1, 0.3, 0.9, 2.3]:
    # for ta in [True, False]:
    # test_learned_rotation(n, ta)
    for ax in [True, False]:
        for g_ax in [True, False]:
            test_random_sphere_point(random_ax=ax, global_ax=g_ax)
