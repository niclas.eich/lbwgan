import os
import numpy as np
import scipy.stats as scs
import matplotlib.pyplot as plt
from utils.plotting.histograms import create_histogram
import matplotlib.pyplot as plt

SEED = 1235


def random_dist(n_points):
    dist = np.random.normal(-1.5, 0.7, n_points)
    dist = np.concatenate((dist, np.random.normal(1.5, 1.0, n_points)))
    dist = np.concatenate((dist, np.random.normal(2.5, 0.3, n_points)))
    # dist = np.concatenate((dist, np.random.normal(-0.5, 0.7, n_points)))
    return dist


plot_dir = "tests/plots/transformations/s_{}".format(SEED)
plot_dir_data = "tests/plots/transformations/data"


def transformation(x, slopes, bias, buckets):
    x = scs.norm.cdf(x)
    idx = np.digitize(x, buckets)
    dist = bias[idx - 1] + (x - buckets[idx - 1]) * slopes[idx - 1]
    return dist


def sig(a):
    return 1.0 / (1 + np.exp(-a))


if __name__ == "__main__":
    if os.path.isdir(plot_dir):
        print("Directory already exists!")
    else:
        os.makedirs(plot_dir)
    N = 400000
    x = random_dist(N)
    create_histogram(
        x, "test_dist", "x", os.path.join(plot_dir, "test_dist.png"), scale="linear"
    )

    n_bin_counts = [50, 100, 500, 1000, 10000]
    icdfs = []
    ibucketss = []

    slopess = []
    biass = []

    for n_bins in n_bin_counts:
        hist = np.histogram(x, n_bins)
        hist_dist = scs.rv_histogram(hist)

        ibuckets = np.linspace(0.0, 1.0, n_bins)
        # ibuckets = sig(ibuckets)
        # ibuckets = (ibuckets-np.min(ibuckets))/(np.max(ibuckets-np.min(ibuckets)))
        # ibuckets = np.delete(ibuckets, np.where(ibuckets < 0)[0])
        # ibuckets = np.delete(ibuckets, np.where(ibuckets > 1)[0])
        buckets = np.linspace(-10, 10, 2 * n_bins)

        icdf = hist_dist.ppf(ibuckets)
        icdfs.append(icdf)
        cdf = hist_dist.cdf(buckets)
        ibucketss.append(ibuckets)
        """
        Plot Results
        """
        # fig, ax = plt.subplots(1, 2, figsize=(16, 10))
        # fig.suptitle('N_bins: {}'.format(n_bins), fontsize=16)
        # # CDF
        # ax[0].set_title("CDF")
        # ax[0].plot(buckets, cdf, marker="o", linestyle=None)
        # # iCDF
        # ax[1].plot(ibuckets, icdf, marker="o", linestyle="None")
        # ax[1].set_title("inverse CDF")
        # fig.savefig(os.path.join(plot_dir, "test_cdf_{}.png".format(n_bins)))
        """
        interpolations
        """
        slopes = np.diff(icdf) / np.diff(ibuckets)
        slopes = np.concatenate((slopes, np.zeros(1)))
        slopess.append(slopes)
        bias = icdf
        biass.append(bias)
        create_histogram(
            ibuckets,
            "bins: {}".format(n_bins),
            "buckets",
            os.path.join(plot_dir, "test_granularity_{}.png".format(n_bins)),
            scale="linear",
        )

    fig, ax = plt.subplots(1, len(n_bin_counts), figsize=(16, 10))
    fig.suptitle("Comparison for different bin_counts:", fontsize=16)
    for i in range(len(n_bin_counts)):
        # iiCDF
        ax[i].plot(ibucketss[i], icdfs[i], marker="None")
        ax[i].set_title("N_bins: {}".format(n_bin_counts[i]))
    fig.savefig(os.path.join(plot_dir, "test_inverse_CDF_comparison.png"))

    dists = []
    for i in range(len(n_bin_counts)):
        y = np.random.normal(0.0, 1.0, N)
        dist = transformation(y, slopess[i], biass[i], ibucketss[i])
        create_histogram(
            dist,
            "test_transformed_dist",
            "n_bins: {}".format(n_bin_counts[i]),
            os.path.join(
                plot_dir, "test_transformed_dist{}.png".format(n_bin_counts[i])
            ),
            scale="linear",
        )

    print("Plotting Data")
    x = np.load(os.path.join(os.getenv("DATA_PATH"), "low__best/x_train.npy"))
    x = np.reshape(x, (x.shape[0], x.shape[-1] // 4, 4))

    n_bins = 5000
    for i_particle in range(np.shape(x)[1]):
        if os.path.isdir(os.path.join(plot_dir_data, "particle_{}".format(i_particle))):
            print("Path exists")
        else:
            os.makedirs(os.path.join(plot_dir_data, "particle_{}".format(i_particle)))

        ibucketss = []
        icdfs = []
        print("#" * 30)
        print("\tParticle {}".format(i_particle))
        print("#" * 30)
        for j, comp in enumerate(["E", "px", "py", "pz"]):
            create_histogram(
                x[:, i_particle, j],
                "Data Part {}, {}".format(j, comp),
                comp,
                os.path.join(
                    os.path.join(
                        plot_dir_data,
                        "particle_{}/".format(i_particle),
                        "{}_data.png".format(comp),
                    )
                ),
                scale="linear",
            )
            hist = np.histogram(x[:, i_particle, j], n_bins)
            hist_dist = scs.rv_histogram(hist)
            ibuckets = np.linspace(0.0, 1.0, n_bins)
            icdf = hist_dist.ppf(ibuckets)
            ibucketss.append(ibuckets)
            icdfs.append(icdf)
            """
            interpolations
            """
            slopes = np.diff(icdf) / np.diff(ibuckets)
            slopes = np.concatenate((slopes, np.zeros(1)))
            slopess.append(slopes)
            bias = icdf
            biass.append(bias)
            y = np.random.normal(0.0, 1.0, len(x[:, i, 0]))
            dist = transformation(y, slopes, bias, ibuckets)
            create_histogram(
                dist,
                "Transformed Part {}, {}".format(j, comp),
                comp,
                os.path.join(
                    os.path.join(
                        plot_dir_data,
                        "particle_{}/".format(i_particle),
                        "{}_transformed.png".format(comp),
                    )
                ),
                scale="linear",
            )
            print("- - - - - - - - - - - - - - - - -")
            print("Information for Particle {}, {}:".format(i_particle, comp))
            print(
                "Means:\t|{0:3.2f}|{1:3.2f}|".format(
                    np.mean(x[:, i_particle, j]), np.mean(dist)
                )
            )
            print(
                "STD:\t|{0:3.2f}|{1:3.2f}|".format(
                    np.std(x[:, i_particle, j]), np.std(dist)
                )
            )
            print(
                "MAX:\t|{0:3.2f}|{1:3.2f}|".format(
                    np.max(x[:, i_particle, j]), np.max(dist)
                )
            )
            print(
                "MIN:\t|{0:3.2f}|{1:3.2f}|".format(
                    np.min(x[:, i_particle, j]), np.min(dist)
                )
            )
            print("- - - - - - - - - - - - - - - - -")

        plt.close()
        print("Plotting CDFs")
        fig_data, ax_01 = plt.subplots(1, len(ibucketss), figsize=(16, 10))
        fig_data.suptitle("Comparison for $E, p_x, p_y, p_z$ :", fontsize=16)
        for j, comp in enumerate(["E", "px", "py", "pz"]):
            print("icdf for {}".format(comp))
            ax_01[j].plot(ibucketss[j], icdfs[j], marker="None")
            ax_01[j].set_title(": {}".format(comp))
        fig_data.savefig(
            os.path.join(
                plot_dir_data, "particle_{}/".format(i_particle), "inverse_CDFs.png"
            )
        )
