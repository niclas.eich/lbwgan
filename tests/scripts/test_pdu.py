import tensorflow as tf
import numpy as np
from model.layers import mass

x = tf.placeholder(tf.float32, shape=(10, 2, 4))
y = mass(x)
init = tf.global_variables_initializer()
sess = tf.Session()
vec4 = np.zeros((10, 2, 4))
masses = np.ones((10, 2))
masses[:, 1] = 2
masses[..., 0] *= np.arange(0, 10, 1)
masses[..., 1] *= np.arange(0, 10, 1)
momenta = np.random.normal(0, 4, (10, 2, 3))
vec4[:, :, 1:] = momenta
vec4[:, :, 0] = np.sqrt(np.square(masses) + (np.sum(np.square(momenta), axis=-1)))

sess.run(init)
res = sess.run(y, feed_dict={x: vec4})
