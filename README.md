# Branch-INFO:

# LBWGAN

This is a master thesis project by Niclas Eich. The goal is to create a WGAN that can produce meaningful physics simulation with the help of internally boosting particles into their restframe and autonomously picking features for their processing.

# Usage:

The source script is `setup.sh`. Using a virtualenv is highly advised. 
Additionally one has to create a script called `data_setup.sh`, where the datalocation is set together with the outputdirectory. This would look on the vispa-cluster like this:


    export OUT_PATH="/net/scratch/NiclasEich/lbwgan_output"
    export DATA_PATH="/net/scratch/cms/data/ttHbb/
