import time
import tensorflow as tf
from abc import ABC, abstractmethod

# from tensorflow.keras.backend import manual_variable_initialization; manual_variable_initialization(True)


class BaseTrain(ABC):
    def __init__(self, sess, model, data, config, logger):
        self.model = model
        self.logger = logger
        self.config_general = config["general"]
        self.sess = sess
        self.data = data
        self.init = tf.group(
            tf.global_variables_initializer(), tf.local_variables_initializer()
        )
        self.sess.run(self.init)
        # uninitialized_vars = []
        # for var in tf.all_variables():
        # try:
        # self.sess.run(var)
        # except tf.errors.FailedPreconditionError:
        # uninitialized_vars.append(var)
        # init_new_vars_op = tf.initialize_variables(uninitialized_vars)
        # self.sess.run(init_new_vars_op)

    def train(self, secure_safe=True, no_save=False, sess_run_kwargs={}):
        success = True
        start_epoch = self.model.cur_epoch_tensor.eval(self.sess)
        tot_time = 0.0
        tot_fails = 0
        cur_epoch = start_epoch
        # for cur_epoch in range(start_epoch, self.config_general["epochs"] + 1):
        while (
            cur_epoch <= self.config_general["epochs"]
            and self.config_general["epochs"] > 0
        ):
            try:
                if cur_epoch == 10000:
                    if "pretrained" in self.config_generator["callbacks"]:
                        print("Making  pretrained model trainable again")
                        self.model.pretrained_z_block.trainable = True
                        g_vars = self.model.generator.trainable_variables
                        self.model.gen_loss_grad = self.model.gen_optimizer.compute_gradients(
                            self.model.g_loss, var_list=g_vars
                        )
                if secure_safe is True and no_save is False and cur_epoch % 10 == 0:
                    self.model.save(self.sess, name_tag="secure")
                s_time = time.time()
                self.train_epoch(sess_run_kwargs=sess_run_kwargs)
                self.sess.run(self.model.increment_cur_epoch_tensor)
                cur_epoch = self.sess.run(self.model.cur_epoch_tensor)
                if no_save is False:
                    self.model.save(self.sess)
            except tf.errors.InvalidArgumentError as e:
                # print("\n"*2)
                print("An Exception got caught during tranining:")
                # print(e)
                print("Trying to restart training.")
                tot_fails += 1
                self.model.load(
                    self.model.total_config, epoch="secure", weights_only=True
                )
                self.model.load_meta_graph(self.sess)
                # print("Training failed. This was probably due to NaNs in some Tensors")
                # print("trying to gracefully end the Training")
                # self.model.save(self.sess, name_tag="fail")
                # success = False
                # break
            e_time = time.time()
            t_tot = e_time - s_time
            print("Time for epoch {}".format(t_tot))
            tot_time += t_tot
        self.model.save(self.sess)
        print("\nTotal Time needed: {}".format(tot_time))
        print("Training failed a total of {} times".format(tot_fails))
        return success

    @abstractmethod
    def train_epoch(self):
        """
        implement the logic of epoch:
        -loop over the number of iterations in the config and call the train step
        -add any summaries you want using the summary
        """
        pass


class BaseEvaluater(ABC):
    def __init__(self, sess, model, data, config):
        self.model = model
        self.config_general = config["general"]
        self.config = config
        self.sess = sess
        self.data = data
        # self.init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
        # self.sess.run(self.init)

    @abstractmethod
    def evaluate(self):
        """
        implement the logic of epoch:
        -loop over the number of iterations in the config and call the train step
        -add any summaries you want using the summary
        """
        pass
