import time
import tensorflow as tf
from tensorflow.keras import backend as K
import numpy as np
from model.base_trainer import BaseEvaluater
from model.layers import (
    mass_to_energy_vec4,
    boost_p,
    particle_rotation_layer_3d_eta_cut,
)

pdu_tensor_names = [
    "particles_0",
    "particles_1",
    "particles_rest_0",
    "particles_rest_1",
    "masses",
]


class BinaryClassifierEvaluater(BaseEvaluater):

    """Docstring for BinaryClassifierEvaluater. """

    def __init__(self, sess, model, data, config):
        super(BinaryClassifierEvaluater, self).__init__(sess, model, data, config)

    def evaluate(self):
        losses = []
        y_iters = []
        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        self.sess.run(
            [self.model.iterator_init, reset_metrics_op],
            feed_dict={
                self.model.x: self.data["x_test"],
                self.model.y: self.data["y_test"],
            },
        )
        while True:
            try:
                loss, auc, y_iter, acc = self.sess.run(
                    [
                        self.model.loss,
                        self.model.auc,
                        self.model.y_iter,
                        self.model.acc,
                    ],
                    feed_dict={self.model.is_training: False},
                )
                y_iters.append(np.mean(y_iter))
                losses.append(loss)
            except tf.errors.OutOfRangeError:
                feed_dict = {
                    self.model.x: self.data["x_test"],
                    self.model.y: self.data["y_test"],
                }
                self.sess.run(self.model.iterator_init, feed_dict=feed_dict)
                break

        y_iter = np.mean(y_iters)
        loss = np.mean(losses)
        # Use newest value for acc and auc
        summaries_dict = {
            "acc": acc,
            "loss": loss,
            "auc": auc,
            "y_iter": y_iter,
        }
        return summaries_dict


class WGANGenerator(BaseEvaluater):
    """Docstring for MyClass. """

    def __init__(self, sess, model, config):
        super(WGANGenerator, self).__init__(sess, model, None, config)

    def evaluate(self, n_generated):
        generated_data = []
        pdu_tensors = []
        if self.config["generator"].get("pdu", None) is not None:
            pdu_distrs = [[] for n in pdu_tensor_names]
            pdu_tensors = [
                self.model.pdu_particles_base,
                self.model.pdu_particles_refined,
                self.model.pdu_particles_rest_0,
                self.model.pdu_particles_rest_1,
                self.model.pdu_masses,
            ]
        else:
            pdu_distrs = None
            pdu_tensors = None
        # First a dry run, to let batch norm burn in
        for _ in range(10000):
            try:
                self.sess.run(
                    self.model.g_samples,
                    feed_dict={self.model.is_training: False, K.learning_phase(): 1},
                )
            except tf.errors.OutOfRangeError:
                print("Exception occoured!")
                self.sess.run(self.model.iterator_init)
        i = 0
        times = []
        while len(generated_data) * self.config_general["batch_size"] < n_generated:
            try:
                s_time = time.time()
                if self.config["generator"].get("pdu", None) is not None:
                    gen_batch, pdu_distr = self.sess.run(
                        [self.model.g_samples, pdu_tensors],
                        feed_dict={
                            self.model.is_training: False,
                            K.learning_phase(): 0,
                        },
                    )
                else:
                    gen_batch = self.sess.run(
                        [self.model.g_samples],
                        feed_dict={
                            self.model.is_training: False,
                            K.learning_phase(): 0,
                        },
                    )
                e_time = time.time()
                times.append(e_time - s_time)
                generated_data.append(gen_batch)
                if self.config["generator"].get("pdu", None) is not None:
                    for i, l in enumerate(pdu_distrs):
                        if pdu_distr[i] is not None:
                            l.append(pdu_distr[i])
                if i % 100 == 0:
                    print(
                        "Generated #{} events".format(
                            i * self.config_general["batch_size"]
                        )
                    )
                i += 1
            except tf.errors.OutOfRangeError:
                print("Exception occoured!")
                self.sess.run(self.model.iterator_init)
        if self.config["generator"].get("pdu", None) is not None:
            pdu_distrs = [np.array(l) for l in pdu_distrs]
        times = np.array(times)
        print("\n")
        print("Some Time statistics  about Generation:")
        print(
            "Generated Events at rate {}# events".format(
                self.config_general["batch_size"]
            )
        )
        print("Mean: {0}".format(np.mean(times)))
        print("Std: {0}".format(np.std(times)))
        print(
            "Total: {} in {}".format(
                len(generated_data) * self.config_general["batch_size"], np.sum(times)
            )
        )
        print("1e6 events:")
        print(
            1e6
            / (len(generated_data) * self.config_general["batch_size"])
            * np.sum(times)
        )
        print("Returning...")
        generated_data = np.array(generated_data)
        return generated_data, pdu_distrs

class DetectorAcceptanceEvaluater(WGANGenerator):
    def __init__(self, sess, model, config, data):
        super(DetectorAcceptanceEvaluater, self).__init__(sess, model, config)
        self.model.data = data["x_train"]

    def evaluate(self, n_generated):
        self.sess.run([self.model.iterator_init])
        return super(DetectorAcceptanceEvaluater, self).evaluate(n_generated)
