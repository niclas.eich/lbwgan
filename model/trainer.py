import os
import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K
from model.base_trainer import BaseTrain
from model.layers import particle_layer, particle_rotation_layer_2d, residual_network
from utils.plotting.lbn import plot_lbn

from tensorflow.keras.backend import manual_variable_initialization

manual_variable_initialization(True)


class BinaryClassifierTrainer(BaseTrain):
    training_history = {}
    """Docstring for BinaryClassifierTrainer. """
    metrics = ["acc", "loss", "auc", "val_acc", "val_loss", "val_auc"]

    def __init__(self, sess, model, data, config, logger):
        super(BinaryClassifierTrainer, self).__init__(sess, model, data, config, logger)
        for m in self.metrics:
            self.training_history[m] = []

    def train_epoch(self):
        losses = []
        y_iters = []
        acc = 0
        auc = 0
        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        self.sess.run(
            [self.model.iterator_init, reset_metrics_op],
            feed_dict={
                self.model.x: self.data["x_train"],
                self.model.y: self.data["y_train"],
            },
        )
        while True:
            try:
                _, loss, auc, y_iter, acc = self.sess.run(
                    [
                        self.model.train,
                        self.model.loss,
                        self.model.auc,
                        self.model.y_iter,
                        self.model.acc,
                    ],
                    feed_dict={self.model.is_training: True},
                )
                y_iters.append(np.mean(y_iter))
                losses.append(loss)

            except tf.errors.OutOfRangeError:
                feed_dict = {
                    self.model.x: self.data["x_train"],
                    self.model.y: self.data["y_train"],
                }
                self.sess.run(self.model.iterator_init, feed_dict=feed_dict)
                break

        y_iter = np.mean(y_iters)
        loss = np.mean(losses)
        """
        filll numpy training history
        """
        self.training_history["acc"].append(acc)
        self.training_history["loss"].append(loss)
        self.training_history["auc"].append(auc)

        cur_it = self.model.cur_epoch_tensor.eval(self.sess)
        print("- - " * 10)
        print("Epoch: {0:3d}".format(cur_it))
        print("Training:")
        print("loss: {0:1.2f}\tauc: {1:1.3f}\tacc {2:1.3f}".format(loss, auc, acc))
        summaries_dict = {
            "tr_acc": {"type": "scalar", "value": acc},
            "tr_loss": {"type": "scalar", "value": loss},
            "tr_auc": {"type": "scalar", "value": auc,},
            "tr_mean_y_out": {"type": "scalar", "value": y_iter,},
        }
        self.logger.summarize(cur_it, summaries_dict=summaries_dict)
        self.validation()
        self.model.save(self.sess)

    def validation(self):
        losses = []
        y_iters = []
        acc = 0
        auc = 0
        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        self.sess.run(
            [self.model.iterator_init, reset_metrics_op],
            feed_dict={
                self.model.x: self.data["x_val"],
                self.model.y: self.data["y_val"],
            },
        )
        while True:
            try:
                # loss, auc, y_iter, acc = self.sess.run([self.model.loss, self.model.auc, self.model.y_iter, self.model.acc],
                # feed_dict={self.model.is_training: False})
                loss, auc, y_iter, acc = self.sess.run(
                    [
                        self.model.loss,
                        self.model.update_val_auc,
                        self.model.y_iter,
                        self.model.update_val_acc,
                    ],
                    feed_dict={self.model.is_training: False},
                )
                y_iters.append(np.mean(y_iter))
                losses.append(loss)
            except tf.errors.OutOfRangeError:
                feed_dict = {
                    self.model.x: self.data["x_val"],
                    self.model.y: self.data["y_val"],
                }
                self.sess.run(self.model.iterator_init, feed_dict=feed_dict)
                break
        y_iter = np.mean(y_iters)
        loss = np.mean(losses)

        self.training_history["val_acc"].append(acc)
        self.training_history["val_loss"].append(loss)
        self.training_history["val_auc"].append(auc)

        cur_it = self.model.cur_epoch_tensor.eval(self.sess)
        print("Validation:")
        print("loss: {0:1.2f}\tauc: {1:1.3f}\tacc {2:1.3f}".format(loss, auc, acc))
        summaries_dict = {
            "val_acc": acc,
            "val_loss": loss,
            "val_auc": auc,
            "val_mean_y_out": y_iter,
        }
        self.logger.summarize(cur_it, summarizer="val", summaries_dict=summaries_dict)

    def get_training_history(self):
        """
        Getter Method for Training History
        """
        for key in self.training_history.keys():
            self.training_history[key] = np.array(self.training_history[key])
        return self.training_history


class WGANTrainer(BaseTrain):
    """Docstring for WGANTrainer """

    training_history = {}
    metrics = ["c_loss_combined", "c_loss", "GP", "g_loss"]

    def __init__(self, sess, model, data, config, logger, verbose=True):
        super(WGANTrainer, self).__init__(sess, model, data, config, logger)
        self.verbose = verbose
        self.model.data = data["x_train"]
        self.config_critic = config["critic"]
        self.config_generator = config["generator"]
        self.config_general = config["general"]
        self.config_data = config["data"]
        if "hinge_loss" in self.config_generator["callbacks"]:
            self.metrics.append("hinge_loss")
        for m in self.metrics:
            self.training_history[m] = []

        if "pretrained" in self.config_generator["callbacks"]:
            self.model.pretrained_z_block.load_weights(
                str(os.getenv("PRETRAINED_MODEL"))
            )
        if "trained_mass" in self.config_generator["callbacks"]:
            self.model.z_mass.load_weights(
                str(os.getenv("PRETRAINED_MASS"))
            )

    def train_epoch(self, sess_run_kwargs={}):
        cur_it = self.model.cur_epoch_tensor.eval(self.sess)
        ep_c_losses = []
        ep_c_losses_combined = []
        ep_gradient_penalties = []
        ep_g_losses = []
        """
        append hinge_loss, if given in callbacks
        """
        if "hinge_loss" in self.config_generator["callbacks"]:
            hinge = True
        else:
            hinge = False

        particles = self.config_data["particle_names"]["names"]
        vec_components = ["E", "p_x", "p_y", "p_z"]
        var_names = [
            "Hist_{}_{}".format(part, v_comp)
            for part in particles
            for v_comp in vec_components
        ]
        """
        build feature_names
        """
        if self.config_critic["lbn"] is True:
            feature_names = []
            for f_name in self.config_critic["lbn_features"]:
                if "pair" in f_name:
                    indices = np.triu_indices(self.config_critic["lbn_particles"], 1)
                    for i, j in zip(indices[0], indices[1]):
                        feature_names.append(
                            "LBN_feature_{0}_particle_{1:02d}_{2:02d}".format(f_name, i, j)
                        )
                else:
                    for i_particle in range(self.config_critic["lbn_particles"]):
                        feature_names.append(
                            "LBN_feature_{0}_particle_{1:02d}".format(f_name, i_particle)
                        )

        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        self.sess.run([self.model.iterator_init, reset_metrics_op])
        # for i, layer in enumerate(pretrained_z_block.layers):
        # self.model.generator.layers[i].set_weights(layer.get_weights())
        """
        Training-Loop
        """
        n_crit = self.config_general["n_crit"]
        n_gen = self.config_general["n_gen"]
        # define empty lists for histograms
        real_data = []
        g_samples = []
        # for histograms of critic-output
        c_out_r = []
        c_out_g = []
        c_out_i = []
        # for histograms of lbn features
        feature_r = []
        feature_g = []
        feature_i = []
        #
        if "reset_optimizer" in self.config_critic["callbacks"]:
            self.sess.run(self.model.reset_crit_optimizer)
        if "reset_optimizer" in self.config_generator["callbacks"]:
            self.sess.run(self.model.reset_gen_optimizer)
        while True:
            c_L2_losses = []
            c_losses = []
            c_losses_combined = []
            gradient_penalties = []
            g_L2_losses = []
            g_losses = []
            gps_lbn = []
            if hinge is True:
                hinge_losses = []
            try:
                global_s = self.sess.run(self.model.global_step_tensor)
                for n_c in range(n_crit):
                    global_s = self.sess.run(
                        [
                            self.model.increment_global_step_tensor,
                            self.model.global_step_tensor,
                        ]
                    )
                    # print("\tstep: ", global_s)
                    """
                    Critit-Loop
                    """
                    if self.config_critic["lbn"] is True and not (
                        "resonance_only" in self.config_critic["callbacks"]
                    ):
                        # print("Critic {}".format(global_s))
                        (
                            _,
                            c_loss,
                            c_loss_combined,
                            gradient_penalty,
                            c_L2,
                            # features_real,
                            # features_gen,
                            # features_interp,
                            c_out_gen,
                            c_out_real,
                            c_out_interp,
                            # gp_lbn,
                        ) = self.sess.run(
                            [
                                self.model.crit_optim,
                                self.model.c_loss,
                                self.model.c_loss_combined,
                                self.model.gradient_penalty,
                                self.model.c_regularizer,
                                # self.model.features_real,
                                # self.model.features_gen,
                                # self.model.features_interp,
                                self.model.c_logit_fake,
                                self.model.c_logit_real,
                                self.model.c_logit_interp,
                                # self.model.lbn_gradient_penalty,
                            ],
                            feed_dict={
                                self.model.is_training: True,
                                K.learning_phase(): 1,
                            },
                            **sess_run_kwargs,
                        )
                        # print("loss: ", c_loss)
                    else:
                        (
                            _,
                            c_loss,
                            c_loss_combined,
                            gradient_penalty,
                            c_L2,
                            c_out_gen,
                            c_out_real,
                            c_out_interp,
                        ) = self.sess.run(
                            [
                                self.model.crit_optim,
                                self.model.c_loss,
                                self.model.c_loss_combined,
                                self.model.gradient_penalty,
                                self.model.c_regularizer,
                                self.model.c_logit_fake,
                                self.model.c_logit_real,
                                self.model.c_logit_interp,
                            ],
                            feed_dict={self.model.is_training: True},
                            **sess_run_kwargs,
                        )
                    c_L2_losses.append(c_L2)
                    c_losses.append(c_loss)
                    c_losses_combined.append(np.mean(c_loss_combined))
                    gradient_penalties.append(gradient_penalty)
                    ep_c_losses.append(c_loss)
                    ep_c_losses_combined.append(np.mean(c_loss_combined))
                    ep_gradient_penalties.append(gradient_penalty)
                    c_out_g.append(c_out_gen)
                    c_out_r.append(c_out_real)
                    c_out_i.append(c_out_interp)
                    # if self.config_critic["lbn"] is True and not("resonance_only" in self.config_critic["callbacks"]):
                    # gps_lbn.append(gp_lbn)
                    # feature_r.append(features_real)
                    # feature_g.append(features_gen)
                    # feature_i.append(features_interp)
                weights = self.sess.run(
                    [self.model.critic_weights],
                    feed_dict={self.model.is_training: False, K.learning_phase(): 0},
                )
                for n_g in range(n_gen):
                    global_s = self.sess.run(
                        [
                            self.model.increment_global_step_tensor,
                            self.model.global_step_tensor,
                        ]
                    )
                    # print("\tstep: ", global_s)
                    """
                    Generator-Loop
                    """
                    # print("Generator")
                    (
                        _,
                        g_loss,
                        g_sample,
                        g_L2,
                        latent_vector,
                        data_points,
                    ) = self.sess.run(
                        [
                            self.model.gen_optim,
                            self.model.g_loss,
                            self.model.g_samples,
                            self.model.g_regularizer,
                            self.model.x,
                            self.model.input_iter,
                        ],
                        feed_dict={self.model.is_training: True, K.learning_phase(): 1},
                    )
                    g_L2_losses.append(g_L2)
                    ep_g_losses.append(g_loss)
                    g_losses.append(g_loss)
                    g_samples.append(g_sample)
                    real_data.append(data_points)
                    if hinge is True:
                        hinge_losses.append(
                            self.sess.run(
                                [self.model.hinge_loss],
                                feed_dict={
                                    self.model.is_training: True,
                                    K.learning_phase(): 1,
                                },
                            )
                        )

                """
                Log for each batch
                """
                g_loss = np.mean(g_losses)
                c_L2_loss = np.mean(c_L2_losses)
                g_L2_loss = np.mean(g_L2_losses)
                c_loss = np.mean(c_losses)
                c_loss_combined = np.mean(c_losses_combined)
                gradient_penalty = np.mean(gradient_penalties)
                if hinge is True:
                    hinge_loss = np.mean(hinge_losses)
                    self.training_history["hinge_loss"].append(hinge_loss)

                self.training_history["c_loss"].append(c_loss)
                self.training_history["GP"].append(gradient_penalty)
                self.training_history["c_loss_combined"].append(
                    np.mean(c_loss_combined)
                )
                self.training_history["g_loss"].append(g_loss)

                summaries_dict = {
                    "c_L2_loss": {"type": "scalar", "value": c_L2_loss},
                    "c_loss": {"type": "scalar", "value": c_loss},
                    "c_loss_tot": {"type": "scalar", "value": c_loss_combined,},
                    "GP": {"type": "scalar", "value": gradient_penalty},
                    "g_loss": {"type": "scalar", "value": g_loss},
                    "g_L2_loss": {"type": "scalar", "value": g_L2_loss},
                }
                for iw, w in enumerate(weights[0]):
                    summaries_dict["dense_{0:02d}_mean".format(iw)] = {
                        "type": "scalar",
                        "value": np.mean(np.abs(w)),
                    }
                    summaries_dict["dense_{0:02d}_std".format(iw)] = {
                        "type": "scalar",
                        "value": np.std(np.abs(w)),
                    }
                # if self.config_critic["lbn"] is True and not("resonance_only" in self.config_critic["callbacks"]):
                # summaries_dict["GP_LBN"] = {
                # "type": "scalar",
                # "value": np.mean(np.array(gps_lbn)),
                # }

                global_step = self.model.global_step_tensor.eval(self.sess)
                """
                Add histograms every 50 iterations
                """
                tb_histograms = False
                if tb_histograms is True:
                    if (
                        global_step % int(50 * self.config_general["n_crit"]) == 0
                        and global_step > 299
                    ):
                        g_samples = np.array(g_samples)
                        real_data = np.array(real_data)
                        if self.config_critic["lbn"] is True and not (
                            "resonance_only" in self.config_critic["callbacks"]
                        ):
                            feature_r = np.array(feature_r)
                            feature_g = np.array(feature_g)
                            feature_i = np.array(feature_i)
                        """
                        Histograms of Kinematic observables
                        """
                        for i, v_name in enumerate(var_names):
                            target_vectors = g_samples[..., i]
                            real_vectors = real_data[..., i]
                            # print("{0}\t MEAN: {1:2.2f}\tSTD:{2:2.2f}".format(v_name, np.mean(target_vectors), np.std(target_vectors)))
                            if len(target_vectors.flatten()) > 0:
                                summaries_dict[v_name] = {
                                    "type": "hist",
                                    "value": target_vectors,
                                }
                                summaries_dict["REAL_" + v_name] = {
                                    "type": "hist",
                                    "value": real_vectors,
                                }
                        """
                        Histograms of critic-outputs
    y                    """
                        summaries_dict["c_out_real"] = {
                            "type": "hist",
                            "value": np.array(c_out_r),
                        }
                        summaries_dict["c_out_gen"] = {
                            "type": "hist",
                            "value": np.array(c_out_g),
                        }
                        summaries_dict["c_out_interp"] = {
                            "type": "hist",
                            "value": np.array(c_out_i),
                        }
                        """
                        Histograms of LBN Features
                        """
                        if self.config_critic["lbn"] is True and not (
                            "resonance_only" in self.config_critic["callbacks"]
                        ):
                            """
                            plot the feature-distributions after the LBN
                            """
                            for target_name, targets in zip(
                                ["real", "gen", "interp"],
                                [feature_r, feature_g, feature_i],
                            ):
                                for i, f_name in enumerate(feature_names):
                                    target_vectors = targets[..., i]
                                    # we have to delete nans
                                    target_vectors = target_vectors[
                                        ~np.isnan(target_vectors)
                                    ]
                                    if len(target_vectors.flatten()) > 0:
                                        summaries_dict[target_name + f_name] = {}
                                        summaries_dict[target_name + f_name][
                                            "value"
                                        ] = target_vectors
                                        summaries_dict[target_name + f_name][
                                            "type"
                                        ] = "hist"
                            """
                            plot  the LBN weights
                            """
                            lbn_weight_plot = plot_lbn(
                                tf.abs(self.model.lbn.particle_weights),
                                tf.abs(self.model.lbn.restframe_weights),
                                particle_labels=self.config_data["particle_names"][
                                    "latex"
                                ],
                            )
                            summaries_dict["LBN-weight-matrices"] = {
                                "type": "image",
                                "value": lbn_weight_plot,
                            }
                            # Reset lists for next histograms
                            feature_r = []
                            feature_g = []
                            feature_i = []
                    real_data = []
                    g_samples = []
                    c_out_r = []
                    c_out_g = []
                    c_out_i = []

                if self.verbose is True:
                    self.logger.summarize(global_step, summaries_dict=summaries_dict)
            except tf.errors.OutOfRangeError:
                self.sess.run(self.model.iterator_init, feed_dict=None)
                break
        ep_c_loss = np.mean(ep_c_losses)
        ep_c_loss_combined = np.mean(ep_c_losses_combined)
        ep_gradient_penalty = np.mean(ep_gradient_penalties)
        ep_g_loss = np.mean(ep_g_losses)

        if self.config_generator.get("lr_decay", None) is not None:
            lr_g = self.sess.run(self.model.lr_gen)
        else:
            lr_g = self.model.lr_gen
        if self.config_critic.get("lr_decay", None) is not None:
            lr_c = self.sess.run(self.model.lr_crit)
        else:
            lr_c = self.model.lr_crit
        print("- - " * 14)
        print("Epoch: {0:3d},\tstep: {1:5d}".format(cur_it, global_step))
        print("Critic:")
        print("LR: {0:1.6f}".format(lr_c))
        print(
            "Loss: {0:1.2f}\tGP: {1:1.2f}\tComb: {2:1.2f}".format(
                ep_c_loss, ep_gradient_penalty, ep_c_loss_combined
            )
        )
        print("Generator:")
        print("LR: {0:1.6f}".format(lr_g))
        print("Loss: {0:1.2f}".format(ep_g_loss))
        if hinge is True:
            print("Hinge-Loss: {0:1.2f}".format(hinge_loss))
        # self.validation()

    def validation(self):
        c_losses = []
        c_losses_combined = []
        gradient_penalties = []
        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        self.sess.run([self.model.iterator_init, reset_metrics_op])
        # self.sess.run([self.model.iterator_init, reset_metrics_op], feed_dict={self.model.x: self.data["noise_val"], self.model.y: self.data["x_val"]})
        """
        Training-Loop
        """
        while True:
            try:
                """
                Critic-Loop
                """
                c_loss, c_loss_combined, gradient_penalty = self.sess.run(
                    [
                        self.model.c_loss,
                        self.model.c_loss_combined,
                        self.model.gradient_penalty,
                    ],
                    feed_dict={self.model.is_training: True},
                )
                c_losses.append(c_loss)
                c_losses_combined.append(c_loss_combined)
                gradient_penalties.append(gradient_penalty)
            except tf.errors.OutOfRangeError:
                break

        c_loss = np.mean(c_losses)
        c_loss_combined = np.mean(c_loss_combined)
        gradient_penalty = np.mean(gradient_penalties)

        self.training_history["val_c_loss"].append(c_loss)
        self.training_history["val_GP"].append(gradient_penalty)
        self.training_history["val_c_loss_combined"].append(c_loss_combined)

        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        # self.sess.run([self.model.iterator_init, reset_metrics_op], feed_dict={self.model.x: self.data["noise_val"], self.model.y: self.data["x_val"]})
        self.sess.run(
            [self.model.iterator_init, reset_metrics_op],
            feed_dict={self.model.y: self.data["x_val"], K.learning_phase(): 0},
        )

        print("Validation")
        print("Critic:")
        print(
            "Loss: {0:1.2f}\tGP: {1:1.2f}\tComb: {2:1.2f}".format(
                c_loss, gradient_penalty, c_loss_combined
            )
        )
        cur_it = self.model.cur_epoch_tensor.eval(self.sess)
        summaries_dict = {
            "val_c_loss": {"type": "scalar", "value": c_loss},
            "val_c_loss_tot": {"type": "scalar", "value": c_loss_combined},
            "val_GP": {"type": "scalar", "value": gradient_penalty},
        }
        self.logger.summarize(cur_it, summarizer="val", summaries_dict=summaries_dict)

    def get_training_history(self):
        """
        Getter Method for Training History
        """
        for key in self.training_history.keys():
            self.training_history[key] = np.array(self.training_history[key])
        return self.training_history
