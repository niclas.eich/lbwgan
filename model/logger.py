import tensorflow as tf
import os


class Logger:
    def __init__(self, sess, config):
        self.sess = sess
        self.config_general = config["general"]
        self.summary_placeholders = {}
        self.summary_ops = {}
        self.train_summary_writer = tf.summary.FileWriter(
            os.path.join(self.config_general["summary_dir"], "train"), self.sess.graph
        )
        self.val_summary_writer = tf.summary.FileWriter(
            os.path.join(self.config_general["summary_dir"], "val")
        )

    # it can summarize scalars and images.
    def summarize(self, step, summarizer="train", scope="", summaries_dict=None):
        """
        :param step: the step of the summary
        :param summarizer: use the train summary writer or the val one
        :param scope: variable scope
        :param summaries_dict: the dict of the summaries values (tag,value)
        :return:
        """
        if summarizer == "train":
            summary_writer = self.train_summary_writer
        elif summarizer == "val":
            summary_writer = self.val_summary_writer
        else:
            raise NotImplementedError
        with tf.variable_scope(scope):

            if summaries_dict is not None:
                summary_list = []
                for tag, value in summaries_dict.items():
                    if tag not in self.summary_ops:
                        if value["type"] == "scalar":
                            self.summary_placeholders[tag] = tf.placeholder(
                                "float32", value["value"].shape
                            )
                        elif value["type"] == "hist":
                            self.summary_placeholders[tag] = tf.placeholder(
                                "float32", [None] + list(value["value"].shape[1:])
                            )

                        if value["type"] == "scalar":
                            self.summary_ops[tag] = tf.summary.scalar(
                                tag, self.summary_placeholders[tag]
                            )
                        elif value["type"] == "hist":
                            self.summary_ops[tag] = tf.summary.histogram(
                                tag, self.summary_placeholders[tag]
                            )
                        elif value["type"] == "image":
                            self.summary_ops[tag] = tf.summary.image(
                                tag, value["value"]
                            )

                    if value["type"] == "image":
                        summary_list.append(self.sess.run(self.summary_ops[tag]))
                    else:
                        summary_list.append(
                            self.sess.run(
                                self.summary_ops[tag],
                                {self.summary_placeholders[tag]: value["value"]},
                            )
                        )

                for summary in summary_list:
                    summary_writer.add_summary(summary, step)
                summary_writer.flush()
