import tensorflow as tf
import tensorflow_graphics.geometry.transformation as tfg_transformation
import tensorflow_probability as tfp
from tensorflow_graphics.geometry.transformation.rotation_matrix_3d import from_euler
from tensorflow_graphics.math import vector
from model.modules import (
    boost,
    reshape_to_particles,
    mass,
    n_particles,
    mass_to_energy_vec4,
    EPSILON,
    beta_scalar,
    beta_vec,
    gamma_scalar,
    two_body_decay,
    recompute_energy,
    eta,
    phi,
    reshape_to_flat,
)
from model.utils.processing import function_from_string
import numpy as np
import luigi


tfd = tfp.distributions


def rotate_vec_to_vec(v, x0):
    if len(x0.shape) < 3:
        x0 = tf.math.l2_normalize(tf.expand_dims(x0, axis=0))
    comp_vec = tf.expand_dims(v[..., 0, :], axis=1)

    # uvector = vector.cross(x0, tf.math.l2_normalize(comp_vec))
    uvector = vector.cross(x0, comp_vec, axis=-1)
    uvector = tf.math.l2_normalize(uvector, axis=-1)

    angles = (-1.0) * tf.acos(
        tf.reduce_sum(
            tf.multiply(
                tf.math.l2_normalize(comp_vec, axis=-1),
                tf.math.l2_normalize(x0, axis=-1),
            ),
            axis=-1,
            keepdims=True,
        )
    )
    if not uvector.shape[-2] == v.shape[-2]:
        uvector = tf.tile(uvector, [1, v.shape[-2], 1])
        angles = tf.tile(angles, [1, v.shape[-2], 1])
    rotated = tfg_transformation.axis_angle.rotate(v, uvector, angles)
    """
    additionaly rotate vecs along axis of new point
    """
    ph = tf.random.uniform(
        tf.expand_dims(tf.expand_dims(rotated[:, 1, 1], axis=1), axis=1).shape,
        0.0,
        np.pi * 2.0,
    )
    second_rotated = tfg_transformation.axis_angle.rotate(
        tf.expand_dims(rotated[:, 1, :], axis=1),
        tf.expand_dims(rotated[:, 0, :], axis=1),
        ph,
    )
    return tf.concat((tf.expand_dims(rotated[:, 0], axis=1), second_rotated), axis=1)
    # return rotated


class constant_mul_layer(tf.keras.layers.Layer):
    """
    custom keras layer, that adds mean and stds to features
    """

    def __init__(self, const=1.0, **kwargs):
        self.const = const
        super(constant_mul_layer, self).__init__(**kwargs)

    def build(self, input_shape):
        super(constant_mul_layer, self).build(input_shape)

    def call(self, input):
        ret = input * self.const
        return ret

    def get_config(self):
        config = {"const": self.const}
        base_config = super(constant_mul_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class residual_layer(tf.keras.layers.Layer):
    """
    Residual layer, that adds the output of a dense layer onto 
    the input

    input
        - layer 1 - nodes
        - layer 2 - nodes
        - ...
        - layer n - nodes
        - layer X - input-shape
    input + layer X
    """

    def __init__(self, nodes=50, layers=3, activation="selu", **kwargs):
        self.nodes = nodes
        self.n_layers = layers
        assert self.n_layers >= 1, "Number of hidden layers must be greater equal 1!"
        self.activation = activation
        # mixed activation for masses?
        self.mass_activation = kwargs.pop("mass_activation", None)
        self.momentum_activation = kwargs.pop("momentum_activation", None)
        self.kernel_regularizer = kwargs.pop("kernel_regularizer", None)
        if isinstance(
            self.kernel_regularizer,
            (int, float, np.int32, np.int64, np.float32, np.float64),
        ):
            self.kernel_regularizer = tf.keras.regularizers.l2(self.kernel_regularizer)
        super(residual_layer, self).__init__(**kwargs)

    def build(self, input_shape):
        """
        First we need to check if the input is a list of tensors or a single tensor

        The first tensor is always the tensor which is used for the last addition.
        The remaining tensors are just concatenated for feature-building.
        """
        if not isinstance(input_shape, list):
            input_shape = [input_shape]
        self.out_shape = input_shape[0]

        self.layers = []
        self.ret_as_flat = False
        if self.activation == "selu":
            kernel_initializer = "lecun_normal"
        else:
            kernel_initializer = "glorot_normal"
        if len(self.out_shape) == 2:
            self.ret_as_flat = True
        for i in range(self.n_layers):
            self.layers.append(
                tf.keras.layers.Dense(
                    self.nodes,
                    function_from_string(self.activation),
                    kernel_initializer=kernel_initializer,
                    kernel_regularizer=self.kernel_regularizer,
                )
            )
        self.layer_X = tf.keras.layers.Dense(np.prod(self.out_shape[1:]).value,)
        if self.mass_activation == "zero":
            self.activation_X = mass_momentum_activation_layer(
                mass_activation=None, momentum_activation=self.momentum_activation
            )
            self.keep_activation = mass_momentum_activation_layer(
                mass_activation="zero", momentum_activation=None
            )
        else:
            self.activation_X = mass_momentum_activation_layer(
                mass_activation=self.mass_activation,
                momentum_activation=self.momentum_activation,
            )
        super(residual_layer, self).build(input_shape)

    def call(self, input, tuning_par=None):
        inputs = input
        if not isinstance(input, list):
            inputs = [input]
        main_in = inputs[0]
        if len(inputs) > 1:
            inputs = tf.keras.layers.Concatenate(axis=-1)(
                [tf.keras.layers.Flatten()(inp) for inp in inputs]
            )
        else:
            inputs = inputs[0]
        all_inputs_flat = tf.keras.layers.Flatten()(inputs)
        layer = self.layers[0](all_inputs_flat)
        for next_layer in self.layers[1:]:
            layer = next_layer(layer)
        layer = tf.keras.layers.Flatten()(self.layer_X(layer))
        if self.mass_activation == "zero":
            layer = self.keep_activation(layer)
        if tuning_par is not None:
            layer = layer * tuning_par
        main_in_flat = tf.keras.layers.Flatten()(main_in)
        out = tf.keras.layers.Add()([main_in_flat, layer])
        out_act = self.activation_X(out)
        if self.ret_as_flat is True:
            out_act = tf.keras.layers.Flatten()(out_act)
        return out_act

    def get_config(self):
        config = {
            "nodes": self.nodes,
            "layers": self.n_layers,
            "activation": self.activation,
            "kernel_regularizer": self.kernel_regularizer,
            "mass_activation": self.mass_activation,
            "momentum_activation": self.momentum_activation,
        }
        base_config = super(residual_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        if type(input_shape) is not list:
            return input_shape
        else:
            return input_shape[0]


class fully_connected_block(tf.keras.layers.Layer):
    """
    simple fully connected, that has number of nodes in
    """

    def __init__(self, nodes=50, layers=3, activation="selu", **kwargs):
        self.nodes = nodes
        self.n_layers = layers
        assert self.n_layers >= 1, "Number of hidden layers must be greater equal 1!"
        self.activation = activation
        self.kernel_regularizer = kwargs.pop("kernel_regularizer", None)
        self.batch_norm = kwargs.pop("batch_norm", False)
        # mixed activation for masses?
        super(fully_connected_block, self).__init__(**kwargs)

    def build(self, input_shape):
        """
        First we need to check if the input is a list of tensors or a single tensor

        The first tensor is always the tensor which is used for the last addition.
        The remaining tensors are just concatenated for feature-building.
        """
        if not isinstance(input_shape, list):
            input_shape = [input_shape]
        self.out_shape = input_shape[0]
        self.ret_as_flat = False
        if self.activation == tf.nn.selu:
            kernel_initializer = "lecun_normal"
        else:
            kernel_initializer = "glorot_normal"
        self.flatten_layer = tf.keras.layers.Flatten()
        self.concat_layer = tf.keras.layers.Concatenate(axis=-1)

        if len(self.out_shape) == 2:
            self.ret_as_flat = True
        self.layers = []
        for i in range(self.n_layers):
            if i == 0:
                self.layers.append(
                    tf.keras.layers.Dense(
                        self.nodes,
                        activation=function_from_string(self.activation),
                        kernel_initializer=kernel_initializer,
                        kernel_regularizer=self.kernel_regularizer,
                        input_shape=input_shape,
                    )
                )
            else:
                self.layers.append(
                    tf.keras.layers.Dense(
                        self.nodes,
                        activation=function_from_string(self.activation),
                        kernel_initializer=kernel_initializer,
                        kernel_regularizer=self.kernel_regularizer,
                    )
                )
            if self.batch_norm is True:
                self.layers.append(tf.keras.layers.BatchNormalization())
        super(fully_connected_block, self).build(input_shape)

    def call(self, input):
        inputs = input
        if not isinstance(input, list):
            inputs = [input]
        if len(inputs) > 1:
            inputs = self.concat_layer([self.flatten_layer(inp) for inp in inputs])
        else:
            inputs = inputs[0]
        layers = self.flatten_layer(inputs)
        for layer in self.layers:
            layers = layer(layers)
        return layers

    def get_config(self):
        config = {
            "nodes": self.nodes,
            "layers": self.n_layers,
            "kernel_regularizer": self.kernel_regularizer,
            "batch_norm": self.batch_norm,
            "activation": self.activation,
        }
        base_config = super(fully_connected_block, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return (self.nodes,)


class dense_block(tf.keras.layers.Layer):
    """
    simple Dense-block, that has number of nodes in
    """

    def __init__(self, nodes=50, layers=3, blocks=1, activation="selu", **kwargs):
        self.nodes = nodes
        self.n_layers = layers
        self.n_blocks = int(blocks)
        assert self.n_layers >= 1, "Number of hidden layers must be greater equal 1!"
        self.activation = activation
        self.kernel_regularizer = kwargs.pop("kernel_regularizer", None)
        self.out_nodes = kwargs.pop("out_nodes", None)
        # mixed activation for masses?
        super(dense_block, self).__init__(**kwargs)

    def build(self, input_shape):
        """
        First we need to check if the input is a list of tensors or a single tensor

        The first tensor is always the tensor which is used for the last addition.
        The remaining tensors are just concatenated for feature-building.
        """
        if not isinstance(input_shape, list):
            input_shape = [input_shape]
        self.out_shape = input_shape[0]

        self.ret_as_flat = False
        if self.activation == tf.nn.selu:
            kernel_initializer = "lecun_normal"
        else:
            kernel_initializer = "glorot_normal"
        self.flatten_layer = tf.keras.layers.Flatten()
        self.concat_layer = tf.keras.layers.Concatenate(axis=-1)

        if len(self.out_shape) == 2:
            self.ret_as_flat = True
        self.blocks = []
        for i in range(self.n_blocks):
            layers = []
            for i in range(self.n_layers):
                layers.append(
                    tf.keras.layers.Dense(
                        self.nodes,
                        activation=function_from_string(self.activation),
                        kernel_initializer=kernel_initializer,
                        kernel_regularizer=self.kernel_regularizer,
                    )
                )
            layers.append(tf.keras.layers.Concatenate())
            self.blocks.append(layers)
        if self.out_nodes is not None:
            self.last_layer = tf.keras.layers.Dense(self.out_nodes)
        super(dense_block, self).build(input_shape)

    def call(self, input):
        inputs = input
        if not isinstance(input, list):
            inputs = [input]
        if len(inputs) > 1:
            inputs = self.concat_layer([self.flatten_layer(inp) for inp in inputs])
        else:
            inputs = inputs[0]
        all_layers = self.flatten_layer(inputs)
        for layers in self.blocks:
            concat_layer = all_layers
            for layer in layers[:-1]:
                all_layers = layer(all_layers)
            all_layers = layers[-1]([all_layers, concat_layer])
        if self.out_nodes is not None:
            all_layers = self.last_layer(all_layers)
        return all_layers

    def get_config(self):
        config = {
            "nodes": self.nodes,
            "blocks": self.n_blocks,
            "layers": self.n_layers,
            "kernel_regularizer": self.kernel_regularizer,
            "activation": self.activation,
            "out_nodes": self.out_nodes,
        }
        base_config = super(dense_block, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return (self.nodes,)


def compute_higher_moment(tensor, m):
    m = int(m)
    batch_size = tensor.shape[0]
    tensor = tf.keras.layers.Flatten()(tensor)
    mean = tf.reduce_mean(tensor, axis=0, keepdims=True)
    moment = tf.reduce_mean((tensor - mean) ** m, axis=0, keepdims=True)
    moment = tf.tile(moment, [batch_size, 1])
    return moment


class feature_statistics_layer(tf.keras.layers.Layer):
    """
    custom keras layer, that adds mean and stds to features
    """

    def __init__(self, **kwargs):
        super(feature_statistics_layer, self).__init__(**kwargs)

    def build(self, input_shape):
        if len(input_shape) > 2:
            self.flatten_layer = tf.keras.layers.Flatten()
        else:
            self.flatten_layer = None
        self.concat_layer = tf.keras.layers.Concatenate(axis=-1)

    def call(self, input):
        if self.flatten_layer is not None:
            input = self.flatten_layer(input)
        batch_size = input.shape[0].value
        means = tf.math.reduce_mean(input, axis=-2)
        means = tf.tile(tf.expand_dims(means, axis=0), [batch_size, 1])
        stds = tf.math.reduce_std(input, axis=-2)
        stds = tf.tile(tf.expand_dims(stds, axis=0), [batch_size, 1])
        # skewness = compute_higher_moment(input, 3)
        # kurtosis = compute_higher_moment(input, 4)

        # ret = self.concat_layer([input, means, stds, skewness, kurtosis])
        ret = self.concat_layer([input, means, stds])
        return ret

    def get_config(self):
        config = {}
        base_config = super(feature_statistics_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class mass_momentum_activation_layer(tf.keras.layers.Layer):
    """
    mixed activation for masses/energies and momenta of a four-vector
    """

    def __init__(self, mass_activation="", momentum_activation=None, **kwargs):
        self.mass_activation = mass_activation
        self.momentum_activation = momentum_activation
        self._shape = None
        self.mass_guard = kwargs.pop("mass_guard", 0.0)
        super(mass_momentum_activation_layer, self).__init__(**kwargs)

    def build(self, input_shape):
        self.ret_as_flat = False
        n_p = n_particles(input_shape)
        if len(input_shape) == 3:
            self._shape = input_shape

        elif len(input_shape) == 2:
            self._shape = (input_shape[1].value // 4, 4)
            self.ret_as_flat = True
        elif len(input_shape) == 1:
            raise ValueError("Batch Dimension is missing!")
        else:
            raise ValueError("Shape not understood")
        self.in_reshape = tf.keras.layers.Reshape(self._shape)
        if self.mass_activation == "zero":
            self.mass_activation_layer = tf.zeros(
                [input_shape[0].value, n_p, 1], tf.float32
            )
        else:
            self.mass_activation_layer = tf.keras.layers.Activation(
                function_from_string(self.mass_activation)
            )
        self.momentum_activation_layer = tf.keras.layers.Activation(
            function_from_string(self.momentum_activation)
        )
        self.concat = tf.keras.layers.Concatenate(axis=-1)

        super(mass_momentum_activation_layer, self).build(input_shape)

    def call(self, input):
        reshaped = self.in_reshape(input)
        if self.mass_activation == "zero":
            masses = self.mass_activation_layer
        else:
            masses = tf.expand_dims(
                self.mass_activation_layer(reshaped[..., 0]), axis=-1
            )
        masses = tf.add(masses, self.mass_guard)
        momenta = self.momentum_activation_layer(reshaped[..., 1:])
        concat = self.concat([masses, momenta])
        if self.ret_as_flat is True:
            return tf.keras.layers.Flatten()(concat)
        else:
            return concat

    def get_config(self):
        config = {
            "mass_activation": self.mass_activation,
            "momentum_activation": self.momentum_activation,
            "mass_guard": self.mass_guard,
        }
        base_config = super(mass_momentum_activation_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return input_shape

class particle_features_layer(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        self.feature_mass = kwargs.pop("mass", False)
        self.feature_phi = kwargs.pop("phi", False)
        self.feature_eta = kwargs.pop("eta", False)
        self.flatten_layer = None
        self.concat_layer = None
        super(particle_features_layer, self).__init__(**kwargs)

    def build(self, input_shape):
        self.flatten_layer = tf.keras.layers.Flatten()
        self.concat_layer = tf.keras.layers.Concatenate()
        super(particle_features_layer, self).build(input_shape)

    def call(self, input):
        input = reshape_to_particles(input)
        features = []
        if self.feature_mass is True:
            features.append(mass(input))
        if self.feature_phi is True:
            features.append(phi(input))
        if self.feature_eta is True:
            features.append(eta(input))
        if len(features) > 1:
            out_tensor = self.concat_layer(
                [self.flatten_layer(inp) for inp in features]
            )
        else:
            out_tensor = self.flatten_layer(features[0])
        return out_tensor

    def get_config(self):
        config = {
            "mass": self.feature_mass,
            "eta": self.feature_eta,
            "phi": self.feature_phi,
        }
        base_config = super(particle_features_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class boost_layer(tf.keras.layers.Layer):
    """
    layer that takes a list of inputs and boostes the first input into the
    restframe of the second
    """

    def __init__(self, boost_mode="STANDARD", **kwargs):
        self.boost_mode = boost_mode
        super(boost_layer, self).__init__(**kwargs)

    def build(self, input_shape):
        if not isinstance(input_shape, list):
            raise ValueError("Input must be a list of tensors!")
        self.n_restframes = input_shape[1][1]
        self.identity = tf.constant(np.identity(4), tf.float32)
        self.U_matrix = tf.constant([[-1, 0, 0, 0]] + 3 * [[0, -1, -1, -1]], tf.float32)

        self.identity = tf.reshape(
            tf.tile(self.identity, [self.n_restframes, 1]), [self.n_restframes, 4, 4]
        )
        self.U_matrix = tf.reshape(
            tf.tile(self.U_matrix, [self.n_restframes, 1]), [self.n_restframes, 4, 4]
        )
        super(boost_layer, self).build(input_shape)

    def call(self, input):
        particle = input[0]
        restframe = input[1]
        n_p = n_particles(particle)
        beta_vec3 = beta_vec(restframe)
        beta = beta_scalar(restframe)
        gamma = gamma_scalar(restframe)
        energies = reshape_to_particles(restframe)[..., 0]
        e_vector = tf.expand_dims(
            tf.concat(
                [tf.expand_dims(tf.ones_like(energies), axis=-1), -beta_vec3 / beta],
                axis=-1,
            ),
            axis=-1,
        )
        e_vector_T = tf.transpose(e_vector, perm=[0, 1, 3, 2])

        self.beta = tf.expand_dims(beta, axis=-1)
        self.gamma = tf.expand_dims(gamma, axis=-1)
        Lambda = tf.add(
            self.identity,
            (self.U_matrix + self.gamma)
            * ((self.U_matrix + 1) * self.beta - self.U_matrix)
            * tf.matmul(e_vector, e_vector_T),
            name="lambda_matrix",
        )
        particle = reshape_to_particles(particle)
        particle = tf.expand_dims(particle, -1)
        if self.boost_mode == "STANDARD":
            pass
        elif self.boost_mode == "COMBINATIONS":
            l_indices = np.tile(np.arange(self.n_restframes.value), n_p.value)
            p_indices = np.repeat(np.arange(n_p.value), self.n_restframes.value)
            Lambda = tf.gather(Lambda, l_indices, axis=1)
            particle = tf.gather(particle, p_indices, axis=1)
        boosted_particle = tf.squeeze(
            tf.matmul(Lambda, particle), axis=-1, name="boosted_particles"
        )
        return boosted_particle

    def get_config(self):
        config = {
            "boost_mode": self.boost_mode,
        }
        base_config = super(boost_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class particle_layer(tf.keras.layers.Layer):
    """
    Custom Layer, that has 4vectores as output, consisting of energy and momenta
    E^2 = p_x^2 + p_y^2 + p_z^2 + m^2
    """

    def __init__(
        self,
        n_particles,
        energy_activation="relu",
        momentum_activation=None,
        zero_pt=False,
        *args,
        **kwargs
    ):
        assert n_particles >= 1, "Number of particles must be greater equal 1!"
        self.n_particles = n_particles
        self.energy_activation = energy_activation
        self.momentum_activation = momentum_activation
        self.zero_pt = zero_pt
        self.mass_init = kwargs.pop("mass_init", None)
        self.cauchy_shape = kwargs.pop("cauchy_shape", False)
        self.pt_repr = kwargs.pop("pt_repr", False)
        if self.pt_repr is True:
            self.n_momenta = 2
        else:
            self.n_momenta = 3
        if self.cauchy_shape is True:
            self.energy_activation = "None"
        super(particle_layer, self).__init__(**kwargs)

    def build(self, input_shape):
        """
        Build function, that is called to build the model
        """
        if self.mass_init is not None and self.cauchy_shape is False:
            init = tf.constant_initializer(self.mass_init)
        else:
            init = "zeros"
        if self.cauchy_shape is True:
            self.cauchy_layuer = cauchy_trafo_layer(mass_init=self.mass_init)
            self.normal_mapping = tfd.Normal(loc=0.0, scale=1.0)
        self.out_m = tf.keras.layers.Dense(
            units=self.n_particles,
            input_shape=input_shape,
            activation=function_from_string(self.energy_activation),
            bias_initializer=init,
            name="out_masses",
        )
        """
        Check if vector repr is (E, pt, pz) = (E, pt, 0, pz)
        or (E, px, py, pz)
        """
        self.out_momenta = tf.keras.layers.Dense(
            units=self.n_particles * self.n_momenta,
            input_shape=input_shape,
            activation=function_from_string(self.momentum_activation),
            name="out_momenta",
        )
        super(particle_layer, self).build(input_shape)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.n_particles * 4)

    def call(self, input):
        out_m = self.out_m(input)
        if self.cauchy_shape is True:
            out_m = self.normal_mapping.cdf(out_m)
            out_m = self.cauchy_layuer(out_m)
        out_momenta = self.out_momenta(input)

        out_momenta = tf.reshape(
            out_momenta, [tf.shape(out_momenta)[0], self.n_particles, self.n_momenta]
        )
        out_energy = tf.sqrt(
            tf.add(
                tf.square(out_m),
                tf.reduce_sum(tf.square(out_momenta), axis=-1) + EPSILON,
            ),
            name="out_energies",
        )
        """
        if pt representation is chosen, a 0 vector for py has to be added
        """
        if self.pt_repr is True:
            py = tf.zeros_like(tf.expand_dims(out_momenta[..., 0], axis=-1))
            out_momenta = tf.concat(
                (
                    tf.expand_dims(out_momenta[..., 0], axis=-1),
                    py,
                    tf.expand_dims(out_momenta[..., 1], axis=-1),
                ),
                axis=2,
            )
        if self.zero_pt is True:
            px = tf.zeros_like(tf.expand_dims(out_momenta[..., 0], axis=-1))
            py = tf.zeros_like(tf.expand_dims(out_momenta[..., 0], axis=-1))
            out_momenta = tf.concat(
                (
                    px,
                    py,
                    tf.expand_dims(out_momenta[..., 1], axis=-1),
                ),
                axis=2,
            )
        # Reshaping to [None, self.n_particles, 1]
        out_energy = tf.expand_dims(out_energy, axis=2)
        output = tf.concat((out_energy, out_momenta), axis=2)
        # Flatten Tensor again from [None, self.n_particles, 4] to [None, self.n_particles*4]
        output = tf.reshape(
            output,
            [tf.shape(output)[0], self.n_particles * 4],
            name="particle_layer_output",
        )
        return output

    def get_config(self):
        config = {
            "n_particles": self.n_particles,
            "energy_activation": self.energy_activation,
            "momentum_activation": self.momentum_activation,
            "pt_repr": self.pt_repr,
            "cauchy_shape": self.cauchy_shape,
            "mass_init": self.mass_init,
            "zero_pt": self.zero_pt,
        }
        base_config = super(particle_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

class trained_mass_to_particle(tf.keras.layers.Layer):
    """
    Custom Layer, that has 4vectores as output, consisting of energy and momenta
    E^2 = p_x^2 + p_y^2 + p_z^2 + m^2
    """

    def __init__(
        self,
        *args,
        **kwargs
    ):
        super(trained_mass_to_particle, self).__init__(**kwargs)

    def build(self, input_shape):
        self.out_momenta = tf.keras.layers.Dense(
            units=3,
            input_shape=input_shape[1],
            activation=None,
            name="out_momenta",
        )
        super(trained_mass_to_particle, self).build(input_shape)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], 4)

    def call(self, inputs):
        mass = inputs[0]
        net = inputs[1]
        out_momenta = self.out_momenta(net)

        out_energy = tf.sqrt(
            tf.add(
                tf.square(mass),
                tf.expand_dims(tf.reduce_sum(tf.square(out_momenta), axis=-1) + EPSILON, axis=-1)
            ),
            name="out_energies",
        )
        output = tf.concat((out_energy, out_momenta), axis=-1)
        # Flatten Tensor again from [None, self.n_particles, 4] to [None, self.n_particles*4]
        output = tf.reshape(
            output,
            [tf.shape(output)[0], 4],
            name="trained_mass_to_particle_output",
        )
        return output

    def get_config(self):
        config = {
        }
        base_config = super(trained_mass_to_particle, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class cauchy_trafo_layer(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        self.mass_init = kwargs.pop("mass_init", 90.0)
        super(cauchy_trafo_layer, self).__init__(**kwargs)

    def build(self, input_shape):
        shift_init = tf.constant_initializer((self.mass_init))
        self.shift = self.add_weight(
            name="shift", shape=(1, 1), initializer=shift_init, trainable=True
        )
        g_init = tf.random_uniform_initializer(1 * 1e-2, 2 * 1e-2)
        self.Gamma = self.add_weight(
            name="Gamma", shape=(1, 1), initializer=g_init, trainable=True
        )

    def call(self, input):
        """
        input must be in (0, 1)
        """
        cauchy = self.Gamma * tf.tan(
            np.pi * (input - tf.constant(np.ones(input.shape) * 0.5, dtype=tf.float32))
        )
        shifted = tf.add(self.shift, cauchy)
        shifted_clipped = tf.nn.relu(shifted) + 1e-9
        return shifted_clipped

    def get_config(self):
        config = {
            "mass_init":
            self.mass_init,
        }
        base_config = super(cauchy_trafo_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class particle_rotation_layer_2d(tf.keras.layers.Layer):
    """
    Custom Layer, that gets 4vectores as input and rotates them in a plane.
    Can be either concatenated to a feature layer to determine the rotations
    or use static rotations.
    E^2 = p_x^2 + p_y^2 + p_z^2 + m^2


    init_spherical_angles = [0., 0.] corresponds to a vector [0., 0., 1]
    """

    def __init__(self, **kwargs):
        self.random_rot = kwargs.pop("random", False)
        self.global_angle = kwargs.pop("global_angle", False)
        self.global_axis = kwargs.pop("global_axis", False)
        if self.random_rot is True:
            self.trainable_w = False
        else:
            self.trainable_w = kwargs.pop("trainable", False)
        self.random_axis = kwargs.pop("random_axis", False)
        if self.random_axis is True:
            self.trainable_axis = False
        else:
            self.trainable_axis = kwargs.pop("trainable_axis", False)
        self.init_spherical_angles = kwargs.pop("init_spherical_angles", None)
        if self.init_spherical_angles is not None:
            self.init_spherical_angles = np.array(self.init_spherical_angles)
        self.axis = None
        super(particle_rotation_layer_2d, self).__init__(**kwargs)

    def build(self, input_shape):
        self.n_particles = n_particles(input_shape)
        self.batch_size = input_shape[0].value
        if self.batch_size is None:
            raise ValueError(
                "At build-time, the batch_size of this layuer must be known!!!\nyou can specify the batch_size for tf.keras.Input explicitly"
            )
        assert self.n_particles >= 1, "Number of particles must be greater equal 1!"

        if self.random_rot is True:
            if self.global_angle is True:
                self.bias = (
                    tf.random.uniform(shape=(self.batch_size, 1, 1), minval=0, maxval=1)
                    * 2.0
                    * np.pi
                )
            else:
                self.bias = (
                    tf.random.uniform(
                        shape=(self.batch_size, self.n_particles, 1), minval=0, maxval=1
                    )
                    * 2
                    * np.pi
                )
        else:
            if self.global_angle is True:
                self.bias = self.add_weight(
                    name="bias",
                    shape=(1, 1),
                    initializer="zeros",
                    trainable=self.trainable_w,
                )
            else:
                self.bias = self.add_weight(
                    name="bias",
                    shape=(self.n_particles, 1),
                    initializer="zeros",
                    trainable=self.trainable_w,
                )
        if self.random_axis is True:
            if self.global_axis is True:
                self.phis = tf.random.uniform((self.batch_size, 1), -np.pi, np.pi)
                self.thetas = tf.acos(
                    2 * tf.random.uniform((self.batch_size, 1), 0.0, 1.0) - 1
                )
            else:
                self.phis = tf.random.uniform(
                    (self.batch_size, self.n_particles), -np.pi, np.pi
                )
                self.thetas = tf.acos(
                    2 * tf.random.uniform((self.batch_size, self.n_particles), 0.0, 1.0)
                    - 1
                )
        else:
            if self.global_axis is True:
                self.spherical_angles = self.add_weight(
                    name="axis_angles",
                    shape=(1, 2),
                    initializer="ones",
                    trainable=self.trainable_axis,
                )
            else:
                self.spherical_angles = self.add_weight(
                    name="axis_angles",
                    shape=(self.n_particles, 2),
                    initializer="ones",
                    trainable=self.trainable_axis,
                )
            # orientate the axis along the given one (same for each particle)
            if self.init_spherical_angles is not None:
                s_angles = self.spherical_angles * self.init_spherical_angles
            else:
                if self.global_axis is True:
                    init_theta = np.expand_dims(
                        np.arccos(2 * np.random.uniform(0, 1, (1)) - 1), axis=-1
                    )
                    init_phi = np.expand_dims(
                        np.random.uniform(-np.pi, np.pi, (1)), axis=-1
                    )
                else:
                    init_theta = np.expand_dims(
                        np.arccos(2 * np.random.uniform(0, 1, (self.n_particles)) - 1),
                        axis=-1,
                    )
                    init_phi = np.expand_dims(
                        np.random.uniform(-np.pi, np.pi, (self.n_particles)), axis=-1
                    )
                s_angles = self.spherical_angles * np.concatenate(
                    (init_phi, init_theta), axis=-1
                )
            # the axis should be clipped
            self.phis = tf.clip_by_value(s_angles[..., 0], 0, 2 * np.pi)
            self.thetas = tf.clip_by_value(s_angles[..., 1], 0, 1 * np.pi)

        # build axis
        x_spherical = tf.multiply(tf.sin(self.thetas), tf.cos(self.phis))
        y_spherical = tf.multiply(tf.sin(self.thetas), tf.sin(self.phis))
        z_spherical = tf.cos(self.thetas)
        self.axis = tf.stack([x_spherical, y_spherical, z_spherical], axis=-1)

    def call(self, input):
        """ 
        in the call function the functionality of the layer is built
        """
        # if not isinstance(input, list):
        # input = [input]
        if self.random_rot is True:
            self.angles = self.bias
        else:
            # assert len(input) >= 1
            # if len(input) == 1:
            # # no additional input was given
            self.angles = tf.expand_dims(
                tf.multiply(tf.nn.sigmoid(self.bias), 2 * np.pi), 0
            )
            # else:
            # # additional input
            # self.angles = tf.multiply(tf.nn.tanh(tf.add(tf.matmul(input[1], self.kernel), self.bias)), np.pi)

        particles = reshape_to_particles(input)
        energies = particles[..., 0]
        momenta = particles[..., 1:]

        rotated_momenta = tfg_transformation.axis_angle.rotate(
            momenta, self.axis, self.angles
        )

        output = tf.concat((tf.expand_dims(energies, axis=-1), rotated_momenta), axis=2)
        output = tf.reshape(
            output, [-1, self.n_particles * 4], name="particle_rotation_layer_2d_output"
        )
        return output

    def get_config(self):
        config = {
            "random": self.random_rot,
            "random_axis": self.random_axis,
            "global_angle": self.global_angle,
            "global_axis": self.global_axis,
            "trainable": self.trainable_w,
            "trainable_axis": self.trainable_axis,
            "init_spherical_angles": self.init_spherical_angles,
        }
        base_config = super(particle_rotation_layer_2d, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.n_particles * 4)


class particle_rotation_layer_3d(tf.keras.layers.Layer):
    """
    Custom Layer, that gets 4vectores as input and rotates them in a plane.
    Can be either concatenated to a feature layer to determine the rotations
    or use static rotations.
    E^2 = p_x^2 + p_y^2 + p_z^2 + m^2


    init_spherical_angles = [0., 0.] corresponds to a vector [0., 0., 1]
    """

    def __init__(self, **kwargs):
        self.random_rot = kwargs.pop("random", False)
        self.global_axis = kwargs.pop("global_axis", False)
        if self.random_rot is True:
            self.trainable_w = False
        else:
            self.trainable_w = kwargs.pop("trainable", False)
        self.random_axis = kwargs.pop("random_axis", False)
        if self.random_axis is True:
            self.trainable_axis = False
        else:
            self.trainable_axis = kwargs.pop("trainable_axis", False)
        self.init_spherical_angles = kwargs.pop("init_spherical_angles", None)
        if self.init_spherical_angles is not None:
            self.init_spherical_angles = np.array(self.init_spherical_angles)
        self.axis = None
        super(particle_rotation_layer_3d, self).__init__(**kwargs)

    def build(self, input_shape):
        self.n_particles = n_particles(input_shape)
        self.batch_size = input_shape[0].value
        if self.batch_size is None:
            raise ValueError(
                "At build-time, the batch_size of this layuer must be known!!!\nyou can specify the batch_size for tf.keras.Input explicitly"
            )
        assert self.n_particles >= 1, "Number of particles must be greater equal 1!"
        """
        Random orientation on sphere 
        """
        self.quaternions = tf.random.normal((self.batch_size, 1, 4), 0.0, 1.0)

        if self.random_axis is True:
            if self.global_axis is True:
                self.phis = tf.random.uniform((self.batch_size, 1), -np.pi, np.pi)
                self.thetas = tf.acos(
                    2 * tf.random.uniform((self.batch_size, 1), 0.0, 1.0) - 1
                )
            else:
                self.phis = tf.random.uniform(
                    (self.batch_size, self.n_particles), -np.pi, np.pi
                )
                self.thetas = tf.acos(
                    2 * tf.random.uniform((self.batch_size, self.n_particles), 0.0, 1.0)
                    - 1
                )

        else:
            if self.global_axis is True:
                self.spherical_angles = self.add_weight(
                    name="axis_angles",
                    shape=(1, 2),
                    initializer="ones",
                    trainable=self.trainable_axis,
                )
            else:
                self.spherical_angles = self.add_weight(
                    name="axis_angles",
                    shape=(self.n_particles, 2),
                    initializer="ones",
                    trainable=self.trainable_axis,
                )
            # orientate the axis along the given one (same for each particle)
            if self.init_spherical_angles is not None:
                s_angles = self.spherical_angles * self.init_spherical_angles
            else:
                if self.global_axis is True:
                    init_theta = np.expand_dims(
                        np.arccos(2 * np.random.uniform(0, 1, (1)) - 1), axis=-1
                    )
                    init_phi = np.expand_dims(
                        np.random.uniform(-np.pi, np.pi, (1)), axis=-1
                    )
                else:
                    init_theta = np.expand_dims(
                        np.arccos(2 * np.random.uniform(0, 1, (self.n_particles)) - 1),
                        axis=-1,
                    )
                    init_phi = np.expand_dims(
                        np.random.uniform(-np.pi, np.pi, (self.n_particles)), axis=-1
                    )
                s_angles = self.spherical_angles * np.concatenate(
                    (init_phi, init_theta), axis=-1
                )
            # the axis should be clipped
            self.phis = tf.clip_by_value(s_angles[..., 0], -np.pi, np.pi)
            self.thetas = tf.clip_by_value(s_angles[..., 1], 0, np.pi)

    def call(self, input, ret_all=False):
        """ 
        in the call function the functionality of the layer is built
        """
        # build axis
        x_spherical = tf.multiply(tf.sin(self.thetas), tf.cos(self.phis))
        y_spherical = tf.multiply(tf.sin(self.thetas), tf.sin(self.phis))
        z_spherical = tf.cos(self.thetas)

        axis = tf.stack([x_spherical, y_spherical, z_spherical], axis=-1)

        particles = reshape_to_particles(input)
        energies = particles[..., 0]
        momenta = particles[..., 1:]

        quaternions = tf.math.l2_normalize(self.quaternions, axis=-1)
        random_3d_matrix = tfg_transformation.rotation_matrix_3d.from_quaternion(
            quaternions, name="rotation_matrix_3d"
        )
        rotated_momenta = tf.linalg.matvec(random_3d_matrix, momenta)
        # rotated_momenta = rotate_vec_to_vec(momenta, axis)

        output = tf.concat(
            (tf.expand_dims(energies, axis=-1), rotated_momenta),
            axis=2,
            name="particle_rotation_layer_3d_output",
        )
        if ret_all is True:
            return output, quaternions
        else:
            return output

    def get_config(self):
        config = {
            "random": self.random_rot,
            "random_axis": self.random_axis,
            "global_axis": self.global_axis,
            "trainable": self.trainable_w,
            "trainable_axis": self.trainable_axis,
            "init_spherical_angles": self.init_spherical_angles,
        }
        base_config = super(particle_rotation_layer_3d, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.n_particles * 4)


class particle_rotation_layer_3d_eta_cut(tf.keras.layers.Layer):
    """
    Custom Layer, that gets 4vectores as input and rotates them in a plane.
    Can be either concatenated to a feature layer to determine the rotations
    or use static rotations.
    E^2 = p_x^2 + p_y^2 + p_z^2 + m^2


    init_spherical_angles = [0., 0.] corresponds to a vector [0., 0., 1]
    """

    def __init__(self, **kwargs):
        self.init_spherical_angles = kwargs.pop("init_spherical_angles", None)
        if self.init_spherical_angles is not None:
            self.init_spherical_angles = np.array(self.init_spherical_angles)
        self.axis = None
        super(particle_rotation_layer_3d_eta_cut, self).__init__(**kwargs)

    def build(self, input_shape):
        if not isinstance(input_shape, list):
            raise ValueError("Tensor input must be a list.")
        self.n_particles = n_particles(input_shape[0])
        self.batch_size = input_shape[0][0].value
        if self.batch_size is None:
            raise ValueError(
                "At build-time, the batch_size of this layuer must be known!!!\nyou can specify the batch_size for tf.keras.Input explicitly"
            )
        assert self.n_particles >= 1, "Number of particles must be greater equal 1!"
        """
        Random orientation on sphere 
        """
        self.phis = tf.random.uniform((self.batch_size, 1), -np.pi, np.pi)
        self.thetas = tf.random.uniform((self.batch_size, 1), 0.0, 1.0)

    def call(self, input):
        """ 
        in the call function the functionality of the layer is built
        """
        if not isinstance(input, list):
            raise ValueError("Tensor input must be a list.")
        particles = input[0]
        params = input[1]
        shifted_thetas = tf.acos(
            tf.clip_by_value(
                2
                * tf.add(
                    tf.expand_dims(params[..., 0], axis=-1) * self.thetas,
                    tf.expand_dims(params[..., 1], axis=-1),
                )
                - 1,
                -1,
                1,
            )
        )
        shifted_phis = tf.add(
            tf.expand_dims(params[..., 2], axis=-1) * self.phis,
            tf.expand_dims(params[..., 3], axis=-1),
        )
        # build axis
        x_spherical = tf.multiply(tf.sin(shifted_thetas), tf.cos(shifted_phis))
        y_spherical = tf.multiply(tf.sin(shifted_thetas), tf.sin(shifted_phis))
        z_spherical = tf.cos(shifted_thetas)

        axis = tf.stack([x_spherical, y_spherical, z_spherical], axis=-1)

        particles = reshape_to_particles(particles)
        energies = particles[..., 0]
        momenta = particles[..., 1:]

        rotated_momenta = rotate_vec_to_vec(momenta, axis)

        output = tf.concat(
            (tf.expand_dims(energies, axis=-1), rotated_momenta),
            axis=2,
            name="particle_rotation_layer_3d_output",
        )
        return output

    def get_config(self):
        config = {
            "init_spherical_angles": self.init_spherical_angles,
        }
        base_config = super(particle_rotation_layer_3d_eta_cut, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return (input_shape[0][0].value, self.n_particles * 4)


class residual_network(tf.keras.layers.Layer):
    def __init__(self, nodes=100, layers=2, blocks=2, **kwargs):
        self.nodes = nodes
        self.layers = layers
        self.n_blocks = blocks
        self.activation = kwargs.pop("activation", tf.nn.relu)
        self.mass_activation = kwargs.pop("mass_activation", None)
        self.momentum_activation = kwargs.pop("momentum_activation", None)
        self.additional_inputs = kwargs.pop("additional_inputs", None)
        self.kernel_regularizer = kwargs.pop("kernel_regularizer", None)
        if isinstance(
            self.kernel_regularizer,
            (int, float, np.int32, np.int64, np.float32, np.float64),
        ):
            self.kernel_regularizer = tf.keras.regularizers.l2(self.kernel_regularizer)

        assert self.n_blocks >= 1
        super(residual_network, self).__init__(**kwargs)

    def build(self, input_shape):
        self.blocks = []
        for _ in range(self.n_blocks):
            self.blocks.append(
                residual_layer(
                    self.nodes,
                    self.layers,
                    activation=self.activation,
                    mass_activation=self.mass_activation,
                    momentum_activation=self.momentum_activation,
                    kernel_regularizer=self.kernel_regularizer,
                )
            )
        super(residual_network, self).build(input_shape)

    def call(self, input, tuning_par=None):
        if not isinstance(input, list):
            input = [input]
        layer = self.blocks[0](input, tuning_par=tuning_par)
        for i, next_layer in enumerate(self.blocks[1:]):
            layer = next_layer(layer, tuning_par=tuning_par)
        return layer

    def get_config(self):
        config = {
            "nodes": self.nodes,
            "layers": self.layers,
            "blocks": self.n_blocks,
            "activation": self.activation,
            "kernel_regularizer": self.kernel_regularizer,
            "mass_activation": self.mass_activation,
            "momentum_activation": self.momentum_activation,
        }
        base_config = super(residual_network, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return input_shape


class mass_block(tf.keras.layers.Layer):
    def __init__(self, nodes, layers, activation=None, **kwargs):
        self.n_mass_nodes = nodes
        self.n_mass_layers = layers
        self.mass_activation = activation
        super(mass_block, self).__init__(**kwargs)

    def build(self, input_shape):
        if self.mass_activation == tf.nn.selu:
            kernel_initializer = "lecun_normal"
        else:
            kernel_initializer = "glorot_normal"
        self.mass_layers = []
        for i in range(self.n_mass_layers):
            self.mass_layers.append(
                tf.keras.layers.Dense(
                    self.n_mass_nodes,
                    activation=function_from_string(self.mass_activation),
                    kernel_initializer=kernel_initializer,
                )
            )
        self.mass_layers.append(tf.keras.layers.Dense(3, activation=None))
        super(mass_block, self).build(input_shape)

    def call(self, input):
        if not isinstance(input, list):
            input = [input]
        if len(input) > 1:
            flat_in = tf.keras.layers.Concatenate(axis=-1)(
                [tf.keras.layers.Flatten()(inp) for inp in input]
            )
        else:
            flat_in = input[0]
        m_layers = self.mass_layers[0](flat_in)
        for layer in self.mass_layers[1:]:
            m_layers = layer(m_layers)
        masses = tf.multiply(
            tf.keras.layers.Flatten()(tf.nn.softmax(m_layers)[..., :2]),
            tf.squeeze(input[0], axis=-1),
        )
        return masses

    def get_config(self):
        config = {
            "nodes": self.n_mass_nodes,
            "layers": self.n_mass_layers,
            "activation": self.mass_activation,
        }
        base_config = super(mass_block, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        if isinstance(input_shape, list):
            return (input_shape[0][0].value, 2)
        else:
            return (input_shape[0].value, 2)


class particle_decay_unit(tf.keras.layers.Layer):
    """
    pdu
    The particle decay unit takes a particle and constructs 2 new particles
    by the physical rules of a two-body decay
    """

    def __init__(
        self,
        mass_kernel=None,
        rotation_kernel=None,
        boost_kernel=None,
        batch_size=None,
        parametrized_rot=False,
        *args,
        **kwargs
    ):
        mass_init = kwargs.pop("mass_init", None)
        super(particle_decay_unit, self).__init__(**kwargs)
        self.mass_kernel = mass_kernel
        if self.mass_kernel is None:
            self.mass_kernel = "constant_trainable"
        self.mass_init = mass_init
        self.rotation_kernel = rotation_kernel
        self.boost_kernel = boost_kernel
        self.batch_size = batch_size
        self.parametrized_rot = parametrized_rot
        if isinstance(self.mass_kernel, str):
            self.mass_kernel_config = self.mass_kernel
        else:
            self.mass_kernel_config = None
        super(particle_decay_unit, self).__init__(**kwargs)

    def build(self, input_shape):
        """
        builds the necessary layers.
        If the keywords "constant" or "constant_trainable" are given for
        the mass_kernel, a weight is built.
        """
        if self.parametrized_rot is True:
            self.random_3d_rotation = particle_rotation_layer_3d_eta_cut()
        else:
            self.random_3d_rotation = particle_rotation_layer_3d(
                random=True, random_axis=True, global_axis=True
            )

        self.boost = boost_layer()

        if isinstance(self.mass_kernel, dict):
            self.mass_kernel = mass_block.from_config(self.mass_kernel)
        elif self.mass_kernel == "constant" or self.mass_kernel == "constant_trainable":
            if self.mass_init is not None:
                init = tf.constant_initializer((self.mass_init[0], self.mass_init[1]))
            else:
                init = tf.random_uniform_initializer(1e-5, 10.0)
            self.mass_const = self.add_weight(
                shape=(2,),
                initializer=init,
                trainable=True if "trainable" in self.mass_kernel else False,
            )
            # assert mass is positive and has a minimum
            self.mass = tf.math.add(tf.math.abs(self.mass_const), 1e-6)
            self.mass = tf.tile(self.mass, [self.batch_size])
            self.mass = tf.reshape(self.mass, (self.batch_size, 1, 2))

            def kernel(inp):
                if not isinstance(inp, list):
                    inp = [inp]
                total_mass = tf.expand_dims(tf.reduce_sum(self.mass, axis=-1), axis=-1)
                frac = self.mass / total_mass
                return frac * tf.expand_dims(
                    tf.math.reduce_min(
                        tf.concat(
                            (total_mass, tf.clip_by_value(inp[0] - 1e-5, 1e-6, inp[0])),
                            axis=1,
                        ),
                        axis=-2,
                    ),
                    axis=-1,
                )

            self.mass_kernel = kernel
        elif isinstance(self.mass_kernel, str):
            raise ValueError(
                'mass_kernel {} not understood. Please give a callable tensor or one of the keywords "constant" or "constant_trainable"'.format(
                    self.mass_kernel
                )
            )
        """
        if no Kernel is given, just pass the input
        """

        if isinstance(self.rotation_kernel, dict):
            self.rotation_kernel = residual_network.from_config(self.rotation_kernel)
        elif self.rotation_kernel is None:
            pass

        if isinstance(self.boost_kernel, dict):
            self.boost_kernel = residual_network.from_config(self.boost_kernel)
        if self.boost_kernel is None:
            pass
        super(particle_decay_unit, self).build(input_shape)

    def call(self, inputs, tuning_par=1.0):
        if not isinstance(inputs, list):
            inputs = [inputs]
        particle_in = inputs[0]
        mother_particle = reshape_to_particles(particle_in)
        mother_mass = mass(mother_particle)
        masses = self.mass_kernel([mother_mass] + inputs[1:])
        particles = two_body_decay(mother_mass, masses)
        if self.rotation_kernel is not None:
            with tf.name_scope("rotation_kernel"):
                particles_refined = reshape_to_particles(
                    self.rotation_kernel(
                        [particles] + inputs[1:], tuning_par=tuning_par
                    )
                )
        else:
            particles_refined = particles
        if self.parametrized_rot is True:
            rotated_particles = self.random_3d_rotation([particles_refined, inputs[1]])
        else:
            rotated_particles = self.random_3d_rotation(particles_refined)
        if self.boost_kernel is not None:
            with tf.name_scope("boost_kernel"):
                rotated_particles_refined = reshape_to_particles(
                    self.boost_kernel(
                        [rotated_particles] + inputs[1:], tuning_par=tuning_par
                    )
                )
        else:
            rotated_particles_refined = rotated_particles
        rotated_particles_refined_E = mass_to_energy_vec4(rotated_particles_refined)
        boost_frame = tf.multiply(
            mother_particle, tf.constant([1, -1, -1, -1], dtype=tf.float32)
        )
        boosted_particles = self.boost([rotated_particles_refined_E, boost_frame])
        boosted_particles = recompute_energy(particles, boosted_particles)
        """
        also return intermediate particles for logging
        """
        if self.parametrized_rot is True:
            particle_basic = self.random_3d_rotation([particles, inputs[1]])
        else:
            particle_basic = self.random_3d_rotation(particles)
        boosted_particles_base = self.boost(
            [mass_to_energy_vec4(particle_basic), boost_frame]
        )
        boosted_particles_ref = recompute_energy(particles, boosted_particles_base)

        boosted_particle_ref = self.boost(
            [mass_to_energy_vec4(rotated_particles), boost_frame]
        )
        boosted_particles_ref = recompute_energy(particles, boosted_particles_ref)
        return [
            tf.squeeze(boosted_particles),
            tf.squeeze(boosted_particles_base),
            tf.squeeze(boosted_particle_ref),
            tf.squeeze(particle_basic),
            tf.squeeze(rotated_particles),
            tf.squeeze(masses),
        ]

    def get_config(self):
        config = {
            "mass_kernel": self.mass_kernel.get_config()
            if self.mass_kernel_config is None
            else self.mass_kernel_config,
            "mass_init": self.mass_init,
            "rotation_kernel": self.rotation_kernel.get_config()
            if self.rotation_kernel is not None
            else None,
            "boost_kernel": self.boost_kernel.get_config()
            if self.boost_kernel is not None
            else None,
            "batch_size": self.batch_size,
            "parametrized_rot": self.parametrized_rot,
        }
        base_config = super(particle_decay_unit, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
        # return dict(list(config.items()))

    def compute_output_shape(self, input_shape):
        return [
            (input_shape[0], 2, 4),
            (input_shape[0], 2, 4),
            (input_shape[0], 2, 4),
            (input_shape[0], 2),
        ]


class combine_to_resonance(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        self.trainable = False
        self.no_pt_res = kwargs.pop("no_pt_res", False)
        kwargs.pop("trainable", False)
        super(combine_to_resonance, self).__init__(self, **kwargs)

    def build(self, input_shape):
        if self.no_pt_res is True:
            self.momentum_x_y = tf.zeros(
                [input_shape[0].value, 2], tf.float32
            )

    def call(self, inputs):
        inputs = reshape_to_particles(inputs)
        res = tf.add(inputs[..., 0, :], inputs[..., 1, :])
        if self.no_pt_res is True:
            res = tf.concat((tf.expand_dims(res[..., 0], axis=-1), self.momentum_x_y, tf.expand_dims(res[..., 3], axis=-1)), axis=-1)
        return res

    def get_config(self):
        config = {"trainable": False, "no_pt_res": self.no_pt_res}
        base_config = super(combine_to_resonance, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


def boost_p(frame, vec4):
    # vec4 = reshape_to_flat(vec4)
    # frame = reshape_to_flat(frame)
    boost_frame = tf.multiply(frame, tf.constant([1, -1, -1, -1], dtype=tf.float32))
    boosted_particles = boost_layer()([vec4, boost_frame])
    return reshape_to_particles(boosted_particles)


def build_pdu_from_dicts(input_p, config_general, config_generator, **kwargs):
    """
    builds a PDU from dicts and returns the pdu
    """
    scaling_factor = kwargs.get("scaling_factor", 1.0)
    batch_size = config_general["batch_size"]
    pdu_dict = config_generator["pdu"]
    mass_k = pdu_dict.get("mass_kernel", "constant_trainable")
    pdu_kwargs = {}
    pdu_kwargs["mass_init"] = pdu_dict.get("mass_init", None)
    # "constant" and "constant_trainable" are known to pdu
    if "const" not in mass_k:
        if isinstance(mass_k, dict) or isinstance(
            mass_k, luigi.freezing.FrozenOrderedDict
        ):
            m_block = mass_block(
                layers=mass_k.get("layers", 3),
                nodes=mass_k.get("nodes", 50),
                activation=mass_k.get("activation", "selu"),
            )
            mass_k = m_block
    if pdu_dict.get("rotation_kernel", None) is not None:
        rotation_kernel = residual_network(
            nodes=pdu_dict["rotation_kernel"].get("nodes", 50),
            layers=pdu_dict["rotation_kernel"].get("layers", 3),
            blocks=pdu_dict["rotation_kernel"].get("blocks", 1),
            kernel_regularizer=pdu_dict["rotation_kernel"].get(
                "kernel_regularizer", None
            ),
            activation=pdu_dict["rotation_kernel"].get("activation", "relu"),
            mass_activation=pdu_dict["rotation_kernel"].get(
                "mass_activation", "softplus"
            ),
            momentum_activation=pdu_dict["rotation_kernel"].get(
                "momentum_activation", None
            ),
        )
    else:
        rotation_kernel = None
    if pdu_dict.get("boost_kernel", None) is not None:
        boost_kernel = residual_network(
            nodes=pdu_dict["boost_kernel"].get("nodes", 50),
            layers=pdu_dict["boost_kernel"].get("layers", 3),
            blocks=pdu_dict["boost_kernel"].get("blocks", 1),
            kernel_regularizer=pdu_dict["boost_kernel"].get("kernel_regularizer", None),
            activation=pdu_dict["boost_kernel"].get("activation", "relu"),
            mass_activation=pdu_dict["boost_kernel"].get("mass_activation", "softplus"),
            momentum_activation=pdu_dict["boost_kernel"].get(
                "momentum_activation", None
            ),
        )
    else:
        boost_kernel = None
    mass_init = pdu_dict.get("mass_init", None)
    if mass_init is not None:
        mass_init = np.array(mass_init) / scaling_factor

    w_param_rot = False
    pdu = particle_decay_unit(
        mass_kernel=mass_k,
        boost_kernel=boost_kernel,
        rotation_kernel=rotation_kernel,
        batch_size=batch_size,
        mass_init=mass_init,
        parametrized_rot=w_param_rot,
    )
    return pdu
