import numpy as np
import tensorflow as tf
import tfmpl
import matplotlib.pyplot as plt
from abc import ABC, abstractmethod
from model.base_model import BaseModel
from model.lbn import LBN
from model.utils.processing import get_activation_init, function_from_string

from model.base_trainer import BaseTrain

BUFFER_SIZE = 5000


class BaseMNIST(BaseModel):
    """
    BaseWGAN adapted from:
    https://github.com/ChengBinJin/WGAN-TensorFlow/blob/master/src/wgan.py
    https://github.com/tensorpack/tensorpack/blob/master/examples/GAN/Improved-WGAN.py
    """

    def __init__(self, config):
        super(BaseMNIST, self).__init__(config)

    def build_model(self):

        with tf.name_scope("input") as scope:
            self.is_training = tf.placeholder(tf.bool, name="is_training")
            # self.x = tf.placeholder(tf.float32, shape=[None] + [(self.config["latent_dim"])], name="latent_placeholde")
            self.x = tf.random.normal(
                shape=[self.config["batch_size"]] + [self.config["latent_dim"]],
                mean=0.0,
                stddev=1.0,
                name="latent_vector",
            )
            self.y = tf.placeholder(
                tf.float32,
                shape=[None] + list(self.config["output_dim"]),
                name="generator_output",
            )
            self._gen_train_ops, self._crit_train_ops = [], []
            self._gen_update_ops, self._crit_update_ops = [], []

            dataset = (
                tf.data.Dataset.from_tensor_slices((self.y))
                .shuffle(buffer_size=BUFFER_SIZE)
                .batch(self.config["batch_size"], drop_remainder=True)
            )
            iterator = tf.data.Iterator.from_structure(
                dataset.output_types, dataset.output_shapes
            )
            self.iterator_init = iterator.make_initializer(dataset)
            self.y_iter = iterator.get_next()
        """
        Build Network Graph
        """
        self.g_samples = self.generator(self.x)
        self.c_logit_real = self.critic(self.y_iter)
        self.c_logit_fake = self.critic(self.g_samples, is_reuse=True)

        with tf.name_scope("interpolation") as scope:
            """
            interpolate between the vectors
            """
            alpha = tf.random_uniform(
                shape=[self.config["batch_size"]] + list(self.config["output_dim"]),
                minval=0.0,
                maxval=1.0,
                name="alpha",
            )
            interp = self.y_iter + alpha * (self.y_iter - self.g_samples)
            """
            Now feed into Critic
            """
            self.c_logit_interp = self.critic(interp, is_reuse=True)

        # Gradient Penalty Loss
        with tf.name_scope("gradient_penalty") as scope:
            gradients = tf.gradients(self.c_logit_interp, [interp])[0]
            gradients = tf.sqrt(
                1e-12 + tf.reduce_sum(tf.square(gradients), reduction_indices=[1, 2])
            )
            # self.gradient_penalty = self.config["GP"] * tf.reduce_mean(tf.square(tf.maximum(0., gradients - 1.)), name='gradient_penalty')
            self.gradient_penalty = self.config["GP"] * tf.reduce_mean(
                tf.square(gradients - 1.0), name="gradient_penalty"
            )

        with tf.name_scope("losses") as scope:
            c_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="critic")
            g_vars = tf.get_collection(
                tf.GraphKeys.TRAINABLE_VARIABLES, scope="generator"
            )

            self.l2_loss = tf.losses.get_regularization_loss()
            self.c_loss = tf.reduce_mean(
                self.c_logit_fake - self.c_logit_real, name="c_loss"
            )
            self.g_loss = tf.negative(tf.reduce_mean(self.c_logit_fake), name="g_loss")

            self.c_loss_combined = tf.add(self.c_loss, self.gradient_penalty)
            self.c_loss_combined = tf.add(self.c_loss_combined, self.l2_loss)
            self.g_loss = tf.add(self.g_loss, self.l2_loss)
        """
        Define Minimization Operations
        """
        with tf.name_scope("optimizers") as scope:
            if self.config["optimizer"] == "RMS":
                self.crit_optimizer = tf.train.RMSPropOptimizer(
                    learning_rate=self.config["learning_rate"]
                )
                self.gen_optimizer = tf.train.RMSPropOptimizer(
                    learning_rate=self.config["learning_rate"]
                )
            elif self.config["optimizer"] == "ADAM":
                self.crit_optimizer = tf.train.AdamOptimizer(
                    learning_rate=self.config["learning_rate"]
                )
                self.gen_optimizer = tf.train.AdamOptimizer(
                    learning_rate=self.config["learning_rate"]
                )
            elif self.config["optimizer"] == "ADADELTA":
                self.crit_optimizer = tf.train.AdadeltaOptimizer(
                    learning_rate=self.config["learning_rate"]
                )
                self.gen_optimizer = tf.train.AdadeltaOptimizer(
                    learning_rate=self.config["learning_rate"]
                )
            elif self.config["optimizer"] == "SGD":
                self.crit_optimizer = tf.train.GradientDescentOptimizer(
                    learning_rate=self.config["learning_rate"]
                )
                self.gen_optimizer = tf.train.GradientDescentOptimizer(
                    learning_rate=self.config["learning_rate"]
                )

            self.reset_crit_optimizer = tf.variables_initializer(
                self.crit_optimizer.variables()
            )
            self.crit_train = self.crit_optimizer.minimize(
                self.c_loss_combined, var_list=c_vars
            )
            self._crit_update_ops += tf.compat.v1.get_collection(
                tf.GraphKeys.UPDATE_OPS, scope="critic"
            )
            crit_ops = [self.crit_train] + self._crit_train_ops

            self.reset_gen_optimizer = tf.variables_initializer(
                self.gen_optimizer.variables()
            )
            self.gen_train = self.gen_optimizer.minimize(self.g_loss, var_list=g_vars)
            self._gen_update_ops += tf.compat.v1.get_collection(
                tf.GraphKeys.UPDATE_OPS, scope="generator"
            )
            gen_ops = [self.gen_train] + self._gen_train_ops

            # numeric_check = [tf.check_numerics(test_tensor, "Tensor {} is broken!".format(t_name)) for t_name, test_tensor in zip(["G_Loss", "G_output", "Features_GEN", "Features_Interp", "Features_Real", "C_Loss"], [self.g_loss, self.g_samples, self.features_gen, self.features_interp, self.features_real, self.c_loss_combined])]
            numeric_check = [
                tf.check_numerics(test_tensor, "Tensor {} is broken!".format(t_name))
                for t_name, test_tensor in zip(
                    ["G_Loss", "C_Loss"], [self.g_loss, self.c_loss_combined]
                )
            ]
            with tf.control_dependencies(numeric_check):
                with tf.control_dependencies(self._crit_update_ops):
                    self.crit_optim = tf.group(*crit_ops)
                with tf.control_dependencies(self._gen_update_ops):
                    self.gen_optim = tf.group(*gen_ops)

    @abstractmethod
    def generator(self, input_tensor, name="generator", is_reuse=False):
        """
        Generator definition
        """

    @abstractmethod
    def critic(self, input_tensor, name="critic", is_reuse=False):
        """
        Critic definition
        """


class WGAN(BaseMNIST):

    """Docstring for GAN. """

    def __init__(self, config):
        super(WGAN, self).__init__(config)

    def generator(self, input_tensor, name="generator", is_reuse=False):
        with tf.variable_scope(name) as scope:
            if is_reuse is True:
                scope.reuse_variables()
            hidden = input_tensor
            for i in range(self.config["layers"]):
                hidden = tf.layers.dense(
                    hidden,
                    units=self.config["nodes"],
                    activation=function_from_string(self.config["activation"]),
                    kernel_regularizer=tf.contrib.layers.l2_regularizer(
                        scale=self.config["L2"]
                    ),
                    name="layer_{0:02d}".format(i),
                )
                if self.config["batch_norm"] is True:
                    hidden = tf.layers.batch_normalization(
                        hidden,
                        training=self.is_training,
                        name="layer_{0:02d}_batch_norm".format(i),
                    )
            hidden = tf.layers.dense(
                hidden,
                units=4096,
                activation=function_from_string(self.config["activation"]),
            )
            hidden = tf.reshape(hidden, [-1, 4, 4, 256])
            if self.config["batch_norm"] is True:
                hidden = tf.layers.batch_normalization(
                    hidden, training=self.is_training
                )

            hidden = tf.contrib.layers.conv2d_transpose(
                hidden, num_outputs=128, kernel_size=5, stride=2
            )
            if self.config["batch_norm"] is True:
                hidden = tf.layers.batch_normalization(
                    hidden, training=self.is_training
                )
            hidden = tf.contrib.layers.conv2d_transpose(
                hidden, num_outputs=64, kernel_size=5, stride=2
            )
            if self.config["batch_norm"] is True:
                hidden = tf.layers.batch_normalization(
                    hidden, training=self.is_training
                )
            hidden = tf.contrib.layers.conv2d_transpose(
                hidden, num_outputs=1, kernel_size=5, stride=2, activation_fn=tf.nn.tanh
            )
            return hidden[:, 2:-2, 2:-2, :]

    def critic(
        self, input_tensor, name="critic", is_reuse=False, return_lbn_features=False
    ):
        """
        Critic definition
        """
        with tf.variable_scope(name) as scope:
            if is_reuse is True:
                scope.reuse_variables()
            features = input_tensor
            hidden = tf.contrib.layers.conv2d(
                features,
                num_outputs=64,
                kernel_size=5,
                stride=2,
                activation_fn=tf.nn.leaky_relu,
            )
            if self.config["batch_norm"] is True:
                hidden = tf.contrib.layers.layer_norm(hidden)
            hidden = tf.contrib.layers.conv2d(
                hidden,
                num_outputs=128,
                kernel_size=5,
                stride=2,
                activation_fn=tf.nn.leaky_relu,
            )
            if self.config["batch_norm"] is True:
                hidden = tf.contrib.layers.layer_norm(hidden)
            hidden = tf.contrib.layers.conv2d(
                hidden,
                num_outputs=256,
                kernel_size=5,
                stride=2,
                activation_fn=tf.nn.leaky_relu,
            )
            if self.config["batch_norm"] is True:
                hidden = tf.contrib.layers.layer_norm(hidden)
            hidden = tf.contrib.layers.flatten(hidden)
            for i in range(self.config["layers"]):
                hidden = tf.layers.dense(
                    hidden,
                    units=self.config["nodes"],
                    activation=function_from_string(self.config["activation"]),
                    kernel_regularizer=tf.contrib.layers.l2_regularizer(
                        scale=self.config["L2"]
                    ),
                    name="layer_{0:02d}".format(i),
                )
                if self.config["batch_norm"] is True:
                    hidden = tf.contrib.layers.layer_norm(hidden)
            output = tf.layers.dense(hidden, units=1, name="crit_output")
            return output


@tfmpl.figure_tensor
def plot_images(images, figsize=(32, 32)):
    """ Plot some images """
    n_examples = len(images)
    dim = np.ceil(np.sqrt(n_examples))
    fig = tfmpl.create_figures(1, figsize=figsize)[0]
    for i in range(n_examples):
        ax = fig.add_subplot(dim, dim, i + 1)
        img = np.squeeze(images[i])
        ax.imshow(img, cmap=plt.cm.Greys)
        ax.axis("off")
    fig.tight_layout()
    return fig


class WGANTrainer(BaseTrain):
    """Docstring for WGANTrainer """

    training_history = {}
    metrics = [
        "c_loss_combined",
        "c_loss",
        "GP",
        "g_loss",
        "val_c_loss",
        "val_GP",
        "val_c_loss_combined",
    ]

    def __init__(self, sess, model, data, config, logger):
        super(WGANTrainer, self).__init__(sess, model, data, config, logger)
        for m in self.metrics:
            self.training_history[m] = []

    def train_epoch(self):
        cur_it = self.model.cur_epoch_tensor.eval(self.sess)
        ep_c_losses = []
        ep_c_losses_combined = []
        ep_gradient_penalties = []
        ep_g_losses = []

        g_samples = []
        r_samples = []

        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        self.sess.run(
            [self.model.iterator_init, reset_metrics_op],
            feed_dict={self.model.y: self.data["x_train"]},
        )
        """
        Training-Loop
        """
        n_crit = self.config["n_crit"]
        n_gen = self.config["n_gen"]
        if self.config["reduce_n_crit"] is True:
            if cur_it >= 20:
                n_gen = 5
                n_crit = 1
        while True:
            try:
                L2_losses = []
                c_losses = []
                c_losses_combined = []
                gradient_penalties = []
                g_losses = []
                # for histograms of critic-output
                c_out_r = []
                c_out_g = []
                c_out_i = []

                self.sess.run(self.model.increment_global_step_tensor)
                # self.sess.run(self.model.reset_crit_optimizer)
                for n_c in range(n_crit):
                    """
                    Critit-Loop
                    """
                    (
                        _,
                        c_loss,
                        c_loss_combined,
                        gradient_penalty,
                        L2,
                        c_out_gen,
                        c_out_real,
                        c_out_interp,
                        r_sample,
                    ) = self.sess.run(
                        [
                            self.model.crit_optim,
                            self.model.c_loss,
                            self.model.c_loss_combined,
                            self.model.gradient_penalty,
                            self.model.l2_loss,
                            self.model.c_logit_fake,
                            self.model.c_logit_real,
                            self.model.c_logit_interp,
                            self.model.y_iter,
                        ],
                        feed_dict={self.model.is_training: True},
                    )
                    L2_losses.append(L2)
                    c_losses.append(c_loss)
                    c_losses_combined.append(c_loss_combined)
                    gradient_penalties.append(gradient_penalty)
                    ep_c_losses.append(c_loss)
                    ep_c_losses_combined.append(c_loss_combined)
                    ep_gradient_penalties.append(gradient_penalty)
                    c_out_g.append(c_out_gen)
                    c_out_r.append(c_out_real)
                    c_out_i.append(c_out_interp)
                    r_samples.append(r_sample)
                # self.sess.run(self.model.reset_gen_optimizer)
                for n_g in range(n_gen):
                    """
                    Generator-Loop
                    """
                    _, g_loss, g_sample = self.sess.run(
                        [self.model.gen_optim, self.model.g_loss, self.model.g_samples],
                        feed_dict={self.model.is_training: True},
                    )
                    ep_g_losses.append(g_loss)
                    g_losses.append(g_loss)
                    g_samples.append(g_sample)

                """
                Log for each batch
                """
                L2_loss = np.mean(L2_losses)
                c_loss = np.mean(c_losses)
                c_loss_combined = np.mean(c_loss_combined)
                gradient_penalty = np.mean(gradient_penalties)
                g_loss = np.mean(g_losses)
                g_samp = np.array(g_samples)
                r_samp = np.array(r_samples)

                summaries_dict = {
                    "L2_loss": {"type": "scalar", "value": L2_loss},
                    "c_loss": {"type": "scalar", "value": c_loss},
                    "c_loss_tot": {"type": "scalar", "value": c_loss_combined},
                    "GP": {"type": "scalar", "value": gradient_penalty},
                    "g_loss": {"type": "scalar", "value": g_loss},
                }
                global_step = self.model.global_step_tensor.eval(self.sess)
                """
                Add histograms every 50 iterations
                """
                if global_step % 50 == 0 or global_step == 1:
                    """
                    Histograms of critic-outputs
                    """
                    summaries_dict["c_out_real"] = {}

                    summaries_dict["c_out_real"]["value"] = np.array(c_out_r)
                    summaries_dict["c_out_real"]["type"] = "hist"

                    summaries_dict["c_out_gen"] = {}
                    summaries_dict["c_out_gen"]["value"] = np.array(c_out_g)
                    summaries_dict["c_out_gen"]["type"] = "hist"

                    summaries_dict["c_out_interp"] = {}
                    summaries_dict["c_out_interp"]["value"] = np.array(c_out_i)
                    summaries_dict["c_out_interp"]["type"] = "hist"
                    """
                    Image of generator output
                    """
                    image_tensor_g = plot_images(
                        tf.gather_nd(
                            self.model.g_samples,
                            [[i] for i in np.random.choice(256, 25)],
                        )
                    )
                    summaries_dict["sample_output"] = {}
                    summaries_dict["sample_output"]["value"] = image_tensor_g
                    summaries_dict["sample_output"]["type"] = "image"

                    if cur_it == 1:
                        image_tensor_r = plot_images(
                            tf.gather_nd(
                                self.model.y_iter,
                                [[i] for i in np.random.choice(256, 25)],
                            )
                        )
                        summaries_dict["sample_input"] = {}
                        summaries_dict["sample_input"]["value"] = image_tensor_r
                        summaries_dict["sample_input"]["type"] = "image"

                self.logger.summarize(global_step, summaries_dict=summaries_dict)
            except tf.errors.OutOfRangeError:
                break

        self.training_history["c_loss"].append(c_loss)
        self.training_history["GP"].append(gradient_penalty)
        self.training_history["c_loss_combined"].append(c_loss_combined)
        self.training_history["g_loss"].append(g_loss)
        ep_c_loss = np.mean(ep_c_losses)
        ep_c_loss_combined = np.mean(ep_c_losses_combined)
        ep_gradient_penalty = np.mean(ep_gradient_penalties)
        ep_g_loss = np.mean(ep_g_losses)
        print("- - " * 10)
        print("Epoch: {0:3d}".format(cur_it))
        print("Critic:")
        print(
            "Loss: {0:1.2f}\tGP: {1:1.2f}\tComb: {2:1.2f}".format(
                ep_c_loss, ep_gradient_penalty, ep_c_loss_combined
            )
        )
        print("Generator:")
        print("Loss: {0:1.2f}".format(ep_g_loss))

        # self.validation()
        self.model.save(self.sess)

    def validation(self):
        c_losses = []
        c_losses_combined = []
        gradient_penalties = []
        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        self.sess.run(
            [self.model.iterator_init, reset_metrics_op],
            feed_dict={self.model.y: self.data["x_val"]},
        )
        # self.sess.run([self.model.iterator_init, reset_metrics_op], feed_dict={self.model.x: self.data["noise_val"], self.model.y: self.data["x_val"]})
        """
        Training-Loop
        """
        while True:
            try:
                """
                Critic-Loop
                """
                c_loss, c_loss_combined, gradient_penalty = self.sess.run(
                    [
                        self.model.c_loss,
                        self.model.c_loss_combined,
                        self.model.gradient_penalty,
                    ],
                    feed_dict={self.model.is_training: True},
                )
                c_losses.append(c_loss)
                c_losses_combined.append(c_loss_combined)
                gradient_penalties.append(gradient_penalty)
            except tf.errors.OutOfRangeError:
                break

        c_loss = np.mean(c_losses)
        c_loss_combined = np.mean(c_loss_combined)
        gradient_penalty = np.mean(gradient_penalties)

        self.training_history["val_c_loss"].append(c_loss)
        self.training_history["val_GP"].append(gradient_penalty)
        self.training_history["val_c_loss_combined"].append(c_loss_combined)

        reset_metrics_op = tf.variables_initializer(
            tf.get_collection(tf.GraphKeys.METRIC_VARIABLES)
        )
        # self.sess.run([self.model.iterator_init, reset_metrics_op], feed_dict={self.model.x: self.data["noise_val"], self.model.y: self.data["x_val"]})
        self.sess.run(
            [self.model.iterator_init, reset_metrics_op],
            feed_dict={self.model.y: self.data["x_val"]},
        )

        print("Validation")
        print("Critic:")
        print(
            "Loss: {0:1.2f}\tGP: {1:1.2f}\tComb: {2:1.2f}".format(
                c_loss, gradient_penalty, c_loss_combined
            )
        )
        cur_it = self.model.cur_epoch_tensor.eval(self.sess)
        summaries_dict = {
            "val_c_loss": c_loss,
            "val_c_loss_tot": c_loss_combined,
            "val_GP": gradient_penalty,
        }
        self.logger.summarize(cur_it, summarizer="val", summaries_dict=summaries_dict)

    def get_training_history(self):
        """
        Getter Method for Training History
        """
        for key in self.training_history.keys():
            self.training_history[key] = np.array(self.training_history[key])
        return self.training_history
