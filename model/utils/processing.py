import tensorflow as tf


def get_activation_init(act):
    if act in ["ELU", "elu", "Elu"]:
        return 1.55, "fan_in"
    elif act == ["SELU", "Selu", "selu"]:
        return 1.0, "fan_in"
    elif act in ["ReLu", "relu", "RELU", "ReLU", "Relu"] or act == [
        "leaky_relu",
        "LReLu",
        "lrelu",
        "LReLU",
        "LRelu",
    ]:
        return 6.0, "fan_avg"
    else:
        return 1.0, "fan_in"


def function_from_string(func_name):
    if func_name in ["ReLu", "relu", "RELU", "ReLU", "Relu"]:
        return tf.nn.relu
    elif func_name in ["ELU", "elu", "Elu"]:
        return tf.nn.elu
    elif func_name in ["SELU", "Selu", "selu"]:
        return tf.nn.selu
    elif func_name in ["leaky_relu", "LReLu", "lrelu", "LReLU", "LRelu"]:
        return tf.nn.leaky_relu
    elif func_name in ["tanh", "TANH"]:
        return tf.nn.tanh
    elif func_name in ["sigmoid", "SIGMOID"]:
        return tf.nn.sigmoid
    elif func_name in ["softplus", "Softplus"]:
        return tf.nn.softplus
    elif func_name in ["softmax"]:
        return tf.nn.softmax
    elif func_name is None:
        return None
    elif func_name is "None":
        return None
    else:
        raise NotImplementedError


def add_to_update_ops(seq):
    targets = ["batch_normalization"]
    for layer in seq.layers:
        for target in targets:
            if target in layer.name:
                for op in layer.updates:
                    tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, op)


def get_update_ops(seq, tag="sequential"):
    targets = ["batch_normalization"]
    ret = []
    for layer in seq.layers:
        for target in targets:
            if target in layer.name:
                for op in layer.updates:
                    if tag in op.name:
                        ret.append(op)
    return ret


class DotDict(dict):
    def __getattr__(self, key):
        if key in self.keys():
            return self.__getitem__(key)
        else:
            try:
                return getattr(super(DotDict, self), key)
            except AttributeError:
                raise AttributeError("{} is not in DotDict".format(key))

    def __setattr__(self, key, value):
        self.__setitem__(key, value)
