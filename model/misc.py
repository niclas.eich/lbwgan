import tensorflow as tf
import tensorflow_probability as tfp
from tensorflow.python.ops import math_ops
import numpy as np


class momentum_shape_transformation_layer(tf.keras.layers.Layer):
    """
    Custom Layer, that applies a shape transformation, by applying the inverse cumulative distribution function
    onto the "hopefully" gaussian output of the neurons.

    It applies shape normalization to "px", "py", "pz" by using a bucketized tensor of the given slopes and biases of the interpolated iCDF
    """

    def __init__(self, n_particles, rv_hists=None, n_buckets=5000, **kwargs):
        """
        rv_hists should be a list of the particle-rv_hists
        rv_hist = scipy.stats.rv_histogram(np.histogram(data, n_bins))
        given in the order:
        particle_1_p_x
        particle_1_p_y
        particle_1_p_z
        particle_2_p_x
        ...
        """
        self.n_particles = n_particles
        self.n_buckets = n_buckets
        slopes = []
        bias = []
        self.buckets = np.linspace(0.0, 1, n_buckets)
        self.diff = np.mean(np.diff(self.buckets))
        self.slopes = kwargs.pop("slopes", None)
        self.bias = kwargs.pop("bias", None)
        # compute the linear interpolation for each particle
        if rv_hists is not None and self.slopes is None and self.bias is None:
            for rv_hist in rv_hists:
                """
                sicne _bucketize returns n+1 buckets for a grid with n-points
                and diff returns n-1 points, we need to add some values for left/right boundaries
                """
                icdf = rv_hist.ppf(self.buckets)
                slopes.append(
                    np.concatenate(
                        (
                            np.array([0]),
                            np.diff(icdf) / np.diff(self.buckets),
                            np.array([0]),
                        )
                    )
                )
                bias.append(np.concatenate((np.array([icdf[0]]), icdf)))
        self.slopes = np.array(slopes)
        self.bias = np.array(bias)
        super(momentum_shape_transformation_layer, self).__init__()

    def build(self, input_shape):
        """
        Build function, that is called to build the model
        """
        tfd = tfp.distributions
        self.normal_dist = tfd.Normal(loc=0.0, scale=1.0)
        self.tf_slopes = tf.Variable(
            self.slopes, name="interpolated_slopes", dtype=tf.float32, trainable=False
        )
        self.tf_slopes = tf.reshape(
            self.tf_slopes, (self.n_particles, 3, self.tf_slopes.shape[-1],)
        )
        self.tf_bias = tf.Variable(
            self.bias, name="icdf_values", dtype=tf.float32, trainable=False
        )
        self.tf_bias = tf.reshape(
            self.tf_bias, (self.n_particles, 3, self.tf_bias.shape[-1],)
        )
        self.tf_buckets = tf.Variable(
            np.concatenate((np.array([-self.diff]), self.buckets)),
            name="bucket_values",
            dtype=tf.float32,
            trainable=False,
        )
        self.tf_buckets = tf.tile(self.tf_buckets, [self.n_particles * 3])
        self.tf_buckets = tf.reshape(
            self.tf_buckets, (self.n_particles, 3, self.n_buckets + 1)
        )
        super(momentum_shape_transformation_layer, self).build(input_shape)

    def call(self, input):
        particles = tf.reshape(input, (-1, self.n_particles, 4))
        energies = particles[:, :, 0]
        momenta = particles[:, :, 1:]
        # momenta = tf.divide(momenta, tf.math.reduce_std(momenta, axis=-1, keepdims=True), name="normalized_momenta")
        feeded_cdf = tf.transpose(self.normal_dist.cdf(momenta), [1, 2, 0])
        idx = math_ops._bucketize(feeded_cdf, list(self.buckets))
        mapped_momenta = tf.transpose(
            tf.batch_gather(self.tf_bias, indices=idx)
            + (feeded_cdf - tf.batch_gather(self.tf_buckets, indices=idx))
            * tf.batch_gather(self.tf_slopes, indices=idx),
            [2, 0, 1],
        )
        # mapped_momenta = feeded_cdf
        output = tf.concat((tf.expand_dims(energies, axis=-1), mapped_momenta), axis=2)
        output = tf.reshape(
            output,
            [-1, self.n_particles * 4],
            name="momentum_shape_transformation_layer_output",
        )
        return output

    def get_config(self):
        config = {
            "slopes": self.slopes,
            "bias": self.bias,
            "n_particles": self.n_particles,
        }
        base_config = super(momentum_shape_transformation_layer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.n_particles * 4)
