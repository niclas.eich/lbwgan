import os
import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Lambda
from tensorflow.keras import Sequential
from model.base_model import BaseModel, BaseWGAN
from model.layers import (
    build_pdu_from_dicts,
    combine_to_resonance,
    constant_mul_layer,
    dense_block,
    feature_statistics_layer,
    fully_connected_block,
    particle_features_layer,
    particle_layer,
    particle_rotation_layer_2d,
    residual_network,
    trained_mass_to_particle,
)
from model.modules import reshape_to_flat, reshape_to_particles
from model.lbn import LBN, LBNLayer
from model.utils.processing import get_activation_init, function_from_string

BUFFER_SIZE = 5000


class BinaryClassifier(BaseModel):
    """
    Binary Classifier, that consist of one dense_block
    """

    def __init__(self, config):
        super(BinaryClassifier, self).__init__(config)

    def build_model(self):
        self.x = tf.placeholder(tf.float32, shape=[None] + list(self.config_general["input_dim"]), name="input_placeholder",)
        self.y = tf.placeholder(tf.uint8, shape=[None,], name="target_placeholder")
        self.is_training = tf.placeholder(tf.bool, name="is_training")
        dataset = (
            tf.data.Dataset.from_tensor_slices((self.x, self.y)).shuffle(buffer_size=BUFFER_SIZE).batch(self.config_general["batch_size"])
        )
        iterator = tf.data.Iterator.from_structure(dataset.output_types, dataset.output_shapes)
        x_iter, self.y_iter = iterator.get_next()
        self.iterator_init = iterator.make_initializer(dataset)
        if self.config["lbn"] is True:
            lbn = LBN(self.config["lbn_particles"], boost_mode=LBN.PAIRS)
            features = lbn(x_iter, features=self.config["lbn_features"])
        else:
            features = x_iter
        hidden = features
        for nlayer in range(self.config["layers"]):
            hidden = tf.layers.dense(
                hidden,
                units=self.config["nodes"],
                activation=function_from_string(self.config["activation"]),
                kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=self.config["L2"]),
            )
            if self.config["batch_norm"] is True:
                hidden = tf.layers.batch_normalization(hidden, training=self.is_training)
        self.output = tf.layers.dense(hidden, units=1)[..., 0]
        self.prob = tf.nn.sigmoid(self.output)
        # with tf.name_scope("train"):
        self.auc, self.update_auc = tf.metrics.auc(self.y_iter, self.prob)
        self.acc, self.update_acc = tf.metrics.accuracy(self.y_iter, tf.round(self.prob))
        # with tf.name_scope("val"):
        self.val_auc, self.update_val_auc = tf.metrics.auc(self.y_iter, self.prob)
        self.val_acc, self.update_val_acc = tf.metrics.accuracy(self.y_iter, tf.round(self.prob))
        self.l2_loss = tf.losses.get_regularization_loss()
        with tf.name_scope("loss"):
            # update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS) + [self.update_auc, self.update_acc]
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS) + [
                self.update_auc,
                self.update_acc,
                self.update_val_acc,
                self.update_val_auc,
            ]
            with tf.control_dependencies(update_ops):
                self.loss = tf.losses.sigmoid_cross_entropy(self.y_iter, self.output)
                self.loss = tf.add(self.loss, self.l2_loss, name="loss")
                optimizer = tf.train.AdamOptimizer(self.config["learning_rate"])
                self.train = optimizer.minimize(self.loss, global_step=self.global_step_tensor)


class WGAN(BaseWGAN):
    """Docstring for GAN. """

    def __init__(self, config, load=False, **kwargs):
        super(WGAN, self).__init__(config, load, **kwargs)

    def build_generator(self, name="generator", **kwargs):
        """
        Generator definition
        """
        if "pretrained" in self.config_generator["callbacks"]:
            c_o = {
                "particle_layer": particle_layer,
                "particle_rotation_layer_2d": particle_rotation_layer_2d,
                "residual_network": residual_network,
            }
            self.pretrained_z_block = tf.keras.models.load_model(str(os.getenv("PRETRAINED_MODEL")), custom_objects=c_o)
            self.pretrained_z_block.trainable = False

        if "trained_mass" in  self.config_generator["callbacks"]:
            c_o = {
                "fully_connected_block": fully_connected_block,
            }
            self.z_mass = tf.keras.models.load_model(str(os.getenv("PRETRAINED_MASS")), custom_objects=c_o)
            self.z_mass.trainable = False

        with tf.name_scope(name):
            if not ("pretrained" in self.config_generator["callbacks"]):
                """
                input tensor
                """
                input_gen = tf.keras.Input(shape=(self.config_generator["latent_dim"]), batch_size=self.config_general["batch_size"],)
                """
                fully connected block
                """
                first_dense = tf.keras.layers.Dense(self.config_generator["nodes"], activation=self.config_generator["activation"],)(
                    input_gen
                )
                d_block = residual_network(
                    nodes=self.config_generator["nodes"],
                    layers=self.config_generator["layers"],
                    activation=self.config_generator["activation"],
                    kernel_regularizer=tf.keras.regularizers.l2(l=self.config_generator["L2"]),
                    blocks=self.config_generator["blocks"],
                )(first_dense)
            else:
                input_gen = self.pretrained_z_block.input
                scaling_mul = constant_mul_layer(const=107.34331512451172 / self.config_data["scaling_factor"])
                p_layer = scaling_mul(self.pretrained_z_block.output)
            """
            check if pdu will be built
            """
            if "pdu" in self.config_generator["callbacks"]:
                if not ("pretrained" in self.config_generator["callbacks"]):
                    # Z mass initialization
                    cauchy_shape = "cauchy_shape" in self.config_generator["callbacks"]
                    # connect fc-block to one 4-vec
                    if "trained_mass" in  self.config_generator["callbacks"]:
                        mass = self.z_mass(input_gen)
                        mass_shifted = constant_mul_layer(const=10.520539283752441 / self.config_data["scaling_factor"])(mass)
                        p_layer = trained_mass_to_particle()([mass_shifted, d_block])
                    else:
                        if self.config_data["particle_process"] in ["zbb_gen_01"]:
                            zero_pt = True
                        else:
                            zero_pt = False
                        p_layer = particle_layer(
                            1, mass_init=np.array([90.0]) / self.config_data["scaling_factor"], cauchy_shape=cauchy_shape, pt_repr=False, zero_pt=zero_pt,
                        )(d_block)
                    # rotate 4-vec of input-particle
                    p_layer = particle_rotation_layer_2d(
                        random=True, init_spherical_angles=[0.0, 0.0], global_angle=True, trainable_axis=False,
                    )(p_layer)
                # additional Z observables
                z_features = particle_features_layer(mass=True, eta=False, phi=True)(p_layer)
                # inputs of pdu will be a list
                pdu_inputs = [p_layer]
                # check if rotation-parametrisation network is needed
                if self.config_generator.get("param_network", None) is not None:
                    param_block = dense_block(
                        layers=self.config_generator["param_network"].get("layers", 5),
                        nodes=self.config_generator["param_network"].get("nodes", 40),
                        activation=self.config_generator["param_network"].get("activation", "selu"),
                        out_nodes=7,
                        blocks=self.config_generator["param_network"].get("blocks", 1),
                    )
                    param_network = param_block([input_gen, p_layer, z_features])
                    pdu_inputs += [param_network]
                # feature_network for additional input
                if self.config_generator.get("feature_network", None) is not None and self.config_general.get("pdu", None) is not None:
                    if (
                        self.config_general["pdu"].get("rotation_kernel", None) is not None
                        or self.config_general["pdu"].get("boost_kernel", None) is not None
                    ):
                        feature_block = dense_block(
                            layers=self.config_generator["feature_network"].get("layers", 5),
                            nodes=self.config_generator["feature_network"].get("nodes", 40),
                            activation=self.config_generator["feature_network"].get("activation", "selu"),
                            blocks=self.config_generator["feature_network"].get("blocks", 1),
                        )
                        feature_network = feature_block([input_gen])
                        pdu_inputs += [feature_network]
                pdu_inputs += [z_features]

                # build pdu form configs
                pdu = build_pdu_from_dicts(
                    p_layer, self.config_general, self.config_generator, scaling_factor=self.config_data["scaling_factor"],
                )
                (particles_out, pdu_particles_base, pdu_particles_ref, pdu_rest_0, pdu_rest_1, pdu_masses,) = pdu(
                    pdu_inputs, tuning_par=kwargs.get("tuning_parameter", None)
                )
                particles_out = tf.keras.layers.Reshape((2 * 4,))(particles_out)
            else:
                p_layer = particle_layer(np.prod(self.config_data["input_dim"]) // 4, pt_repr=False)(d_block)
                if "random_rotation" in self.config_generator["callbacks"]:
                    p_layer = particle_rotation_layer_2d(random=True, init_spherical_angles=[0.0, 0.0], global_angle=True)(p_layer)

            if self.config_generator.get("pdu", None) is not None:
                if "shortcut" in self.config_generator["callbacks"]:
                    particles_out = tf.keras.layers.Concatenate()([particles_out, p_layer])
                else:
                    if self.config_data["particle_process"] in ["zbb_gen_01"]:
                        no_pt_res = True
                    else:
                        no_pt_res = False
                    resonance = combine_to_resonance(no_pt_res=no_pt_res)(particles_out)
                    particles_out = tf.keras.layers.Concatenate()([particles_out, resonance])
                output_tensors = [
                    particles_out,
                    pdu_particles_base,
                    pdu_particles_ref,
                    pdu_rest_0,
                    pdu_rest_1,
                    pdu_masses,
                ]
            else:
                output_tensors = [p_layer]
            return tf.keras.Model(inputs=[input_gen], outputs=output_tensors, name="GENERATOR")

    def build_critic(self, name="critic", **kwargs):
        """
        Critic definition
        """
        with tf.name_scope(name):
            if function_from_string(self.config_critic["activation"]) == tf.nn.selu:
                kernel_initializer = "lecun_normal"
            else:
                kernel_initializer = "glorot_normal"
            input_critic = tf.keras.Input(shape=self.config_data["input_dim"], batch_size=self.config_general["batch_size"],)
            particles = input_critic

            # if "resonance_only" in self.config_critic["callbacks"]:
            # particles = combine_to_resonance()(particles)
            # else:
            # particles = tf.keras.layers.Concatenate()([input_critic, combine_to_resonance()(particles)])
            p_features = particle_features_layer(mass=True, phi=True, eta=False)(particles)
            if (
                self.config_critic["lbn"] is True
                and not ("late_interpolation" in self.config_critic["callbacks"])
                and not ("resonance_only" in self.config_critic["callbacks"])
            ):
                lbn_layer = LBNLayer(n_particles=self.config_critic["lbn_particles"], features=self.config_critic["lbn_features"],)(
                    particles
                )
                layer = tf.keras.layers.concatenate([particles, lbn_layer])
            else:
                layer = tf.keras.layers.Dense(
                    units=self.config_critic["nodes"],
                    activation=function_from_string(self.config_critic["activation"]),
                    kernel_regularizer=tf.keras.regularizers.l2(l=self.config_critic["L2"]),
                    input_shape=self.config_data["input_dim"],
                    kernel_initializer=kernel_initializer,
                )(particles)
            if "dense_net" in self.config_critic.keys():
                d_block = dense_block(
                    layers=self.config_critic["dense_net"].get("layers", 3),
                    nodes=self.config_critic["dense_net"].get("nodes", 50),
                    activation=self.config_critic["dense_net"].get("activation", "leaky_relu"),
                    blocks=self.config_critic["dense_net"].get("blocks", "3"),
                )
                layer = d_block([layer, p_features])
            if "feature_statistics" in self.config_critic["callbacks"]:
                layer = feature_statistics_layer()(layer)
            layer = fully_connected_block(
                nodes=self.config_critic["nodes"],
                layers=self.config_critic["layers"],
                activation=self.config_critic["activation"],
                kernel_regularizer=tf.keras.regularizers.l2(l=self.config_critic["L2"]),
            )(layer)
            logit_layer = tf.keras.layers.Dense(units=1, activation=None, name="logit_layer")(layer)
            return tf.keras.Model(inputs=[input_critic], outputs=[logit_layer], name="CRITIC")


class DetectorAcceptanceWGAN(WGAN):
    def __init__(self, config, load=False, **kwargs):
        super(WGAN, self).__init__(config, load, **kwargs)

    def build_generator(self, name="generator", **kwargs):
        with tf.name_scope(name):
            """
            input tensor
            """
            input_gen = tf.keras.Input(shape=self.config_data["input_dim"], batch_size=self.config_general["batch_size"],)
            resonance = combine_to_resonance()(input_gen)
            pdu_inputs = [resonance]
            # additional Z observables
            resonance_features = particle_features_layer(mass=True, eta=False, phi=True)(resonance)
            with tf.name_scope("detector_acceptance"):
                param_block = dense_block(
                    layers=self.config_generator["param_network"].get("layers", 5),
                    nodes=self.config_generator["param_network"].get("nodes", 40),
                    activation=self.config_generator["param_network"].get("activation", "selu"),
                    out_nodes=7,
                    blocks=self.config_generator["param_network"].get("blocks", 1),
                )
                param_network = param_block([resonance, resonance_features])
            pdu_inputs += [param_network]

            # build pdu form configs
            pdu = build_pdu_from_dicts(
                resonance, self.config_general, self.config_generator, scaling_factor=self.config_data["scaling_factor"],
            )
            (p_layer, pdu_particles_base, pdu_particles_ref, pdu_rest_0, pdu_rest_1, pdu_masses,) = pdu(pdu_inputs)
            p_layer = tf.keras.layers.Reshape((2 * 4,))(p_layer)
            if self.config_generator.get("pdu", None) is not None:
                output_tensors = [
                    p_layer,
                    pdu_particles_base,
                    pdu_particles_ref,
                    pdu_rest_0,
                    pdu_rest_1,
                    pdu_masses,
                ]
            else:
                output_tensors = [p_layer]
            return tf.keras.Model(inputs=[input_gen], outputs=output_tensors, name="GENERATOR")


class MassWGAN(BaseWGAN):
    """
    GAN that trains a 1D mass distribution
    """
    def build_generator(self, name="generator", **kwargs):
        with tf.name_scope(name):
            """
            input tensor
            """
            input_gen = tf.keras.Input(shape=self.config_data.get("latent_dim", 30), batch_size=self.config_general["batch_size"],)

            d_block = fully_connected_block(
                nodes=self.config_generator["nodes"],
                layers=self.config_generator["layers"],
                activation=self.config_generator["activation"],
                kernel_regularizer=tf.keras.regularizers.l2(l=self.config_generator["L2"]),
            )(input_gen)
            mass_layer = tf.keras.layers.Dense(1, activation="relu")(d_block)

            return tf.keras.Model(inputs=[input_gen], outputs=mass_layer, name="GENERATOR")

    def build_critic(self, name="critic", **kwargs):
        """
        Critic definition
        """
        with tf.name_scope(name):
            input_critic = tf.keras.Input(shape=(1,), batch_size=self.config_general["batch_size"],)
            main_body = fully_connected_block(
                nodes=self.config_critic["nodes"],
                layers=self.config_critic["layers"],
                activation=self.config_critic["activation"],
                kernel_regularizer=tf.keras.regularizers.l2(l=self.config_critic["L2"]),
            )(input_critic)
            logit_layer = tf.keras.layers.Dense(units=1, activation=None, name="logit_layer")(main_body)
            return tf.keras.Model(inputs=[input_critic], outputs=[logit_layer], name="CRITIC")
