import tensorflow as tf
from model.modules import reshape_to_particles, mass


def breit_wigner_kernel(x, y, sigma=2.0):
    x = tf.keras.layers.Flatten()(x)
    y = tf.keras.layers.Flatten()(y)
    return tf.divide(
        sigma ** 2,
        tf.reduce_sum(tf.square(tf.subtract(x, y)), axis=-1) + sigma ** 2,
        name="breit_wigner_kernel",
    )


def MMD(x, y, kernel=breit_wigner_kernel, **kwargs):
    """
    have to do some "weird" gathering in order to let shuffle work
    for gradients
    """
    mmd_x = tf.reduce_mean(
        kernel(x, tf.gather(x, tf.random.shuffle(tf.range(tf.shape(x)[0]))), **kwargs)
    )
    mmd_y = tf.reduce_mean(
        kernel(y, tf.gather(y, tf.random.shuffle(tf.range(tf.shape(y)[0]))), **kwargs)
    )
    mmd_cross = tf.reduce_mean(
        kernel(
            tf.gather(x, tf.random.shuffle(tf.range(tf.shape(x)[0]))),
            tf.gather(y, tf.random.shuffle(tf.range(tf.shape(y)[0]))),
            **kwargs
        )
    )
    return tf.subtract(tf.add(mmd_x, mmd_y), 2 * mmd_cross, name="MMD")


def combine_particles(vec, idxs):
    if not isinstance(idxs, list):
        raise ValueError("Index {} must be a list of lists!".format(idxs))
    tensors = []
    particles = reshape_to_particles(vec)
    for idx in idxs:
        if len(idx) == 2:
            tensors.append(
                tf.expand_dims(
                    tf.add(particles[..., idx[0], :], particles[..., idx[1], :]),
                    axis=-2,
                )
            )
        elif len(idx) == 3:
            raise NotImplementedError("You knew, this would happen!")
    if len(tensors) == 1:
        return tensors[0]
    elif len(tensors) == 0:
        return 0.0
    else:
        return tf.keras.layers.Concatenate(axis=-2)(tensors)


def MMD_loss(vec_data, vec_gen, combine_idx, **kwargs):
    """
    MMD loss
    """
    if isinstance(kwargs.get("sigma", None), list):
        raise NotImplementedError("currently only one deay width supported!")
    combined_data = combine_particles(vec_data, combine_idx)
    combined_gen = combine_particles(vec_gen, combine_idx)
    m_data = mass(combined_data)
    m_gen = mass(combined_gen)
    loss = MMD(m_data, m_gen, **kwargs)
    return loss
