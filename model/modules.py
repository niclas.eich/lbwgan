import tensorflow as tf
import numpy as np
from model.utils.processing import function_from_string

# this epsilon makes the minimal mass sqrt(eps)
EPSILON = 2 * 1e-5


def residual_unit(nodes=50, layers=3, activation=tf.nn.relu, **kwargs):
    if layers == 0:
        return input
    layer = input
    for i in range(layers):
        layer = tf.keras.layers.Dense(nodes, activation)(layer)
    layer = tf.keras.layers.Dense(input.shape[-1])(layer)
    ret = tf.keras.layers.Add()([input, layer])
    if kwargs.get("mass_activation", None) is not None:
        mass = tf.expand_dims(
            tf.keras.layers.Reshape((n_particles(input), 4))(ret)[..., 0], axis=-1
        )
        momenta = tf.keras.layers.Reshape((n_particles(input), 4))(ret)[..., 1:]
        mass = tf.keras.layers.Activation(
            function_from_string(kwargs.get("mass_activation", None))
        )(mass)
        ret = tf.keras.layers.Concatenate(axis=-1)([mass, momenta])
    return tf.keras.layers.Flatten()(ret)


def two_body_decay(mother_mass, masses):
    with tf.name_scope("two_body_decay"):
        if len(mother_mass.shape) > len(masses.shape):
            masses = tf.expand_dims(masses, axis=-2)

        denominator = tf.multiply(2.0, mother_mass, name="frac_denominator")

        numerator_a = tf.sqrt(
            tf.nn.relu(
                tf.square(
                    tf.square(mother_mass)
                    + tf.square(tf.expand_dims(masses[..., 0], axis=-1))
                    - tf.square(tf.expand_dims(masses[..., 1], axis=-1))
                )
                - 4.0
                * tf.square(mother_mass)
                * tf.square(tf.expand_dims(masses[..., 0], axis=-1))
            ),
            name="frac_numerator_a",
        )
        # numerator_a = tf.sqrt(
        # tf.nn.relu(
        # tf.square(
        # tf.square(mother_mass) - tf.square(tf.expand_dims(masses[..., 0], axis=-1) - tf.expand_dims(masses[..., 1], axis=-1))
        # )
        # * tf.square(
        # tf.square(mother_mass) - tf.square(tf.expand_dims(masses[..., 0], axis=-1) + tf.expand_dims(masses[..., 1], axis=-1))
        # )
        # ),
        # name="frac_numerator_a",
        # )
        momentum_a = tf.divide(numerator_a, denominator, name="momentum_a")

        numerator_b = tf.sqrt(
            tf.nn.relu(
                tf.square(
                    tf.square(mother_mass)
                    + tf.square(tf.expand_dims(masses[..., 1], axis=-1))
                    - tf.square(tf.expand_dims(masses[..., 0], axis=-1))
                )
                - 4.0
                * tf.square(mother_mass)
                * tf.square(tf.expand_dims(masses[..., 1], axis=-1))
            ),
            name="frac_numerator_b",
        )
        # numerator_b = tf.sqrt(
        # tf.nn.relu(
        # tf.square(
        # tf.square(mother_mass) - tf.square(tf.expand_dims(masses[..., 1], axis=-1) - tf.expand_dims(masses[..., 0], axis=-1))
        # )
        # * tf.square(
        # tf.square(mother_mass) - tf.square(tf.expand_dims(masses[..., 1], axis=-1) + tf.expand_dims(masses[..., 0], axis=-1))
        # )
        # ),
        # name="frac_numerator_b",
        # )

        momentum_b = tf.divide(numerator_b, denominator, name="momentum_b")

        vec_4_a = tf.concat(
            (
                tf.expand_dims(masses[..., 0], axis=-1),
                momentum_a,
                tf.zeros_like(momentum_a),
                tf.zeros_like(momentum_a),
            ),
            axis=-1,
            name="vec_4_a",
        )
        vec_4_b = tf.concat(
            (
                tf.expand_dims(masses[..., 1], axis=-1),
                -momentum_b,
                tf.zeros_like(momentum_b),
                tf.zeros_like(momentum_b),
            ),
            axis=-1,
            name="vec_4_a",
        )
        particles = tf.concat((vec_4_a, vec_4_b), axis=-2)
    return particles


def recompute_energy(vec_m, vec_e):
    """
    this function gets a mass-vector or mass-4-vector "vec_m"
    and recomputes the right energy for a 4-vector "vec_e"
    """
    shape = vec_e.shape
    vec_e = reshape_to_particles(vec_e)
    vec_m = reshape_to_particles(vec_m)

    momenta = vec_e[..., 1:]
    masses = vec_m[..., 0]
    energies = tf.sqrt(
        tf.add(tf.square(masses), tf.reduce_sum(tf.square(momenta), axis=-1))
    )
    energies = tf.expand_dims(energies, axis=2)
    ret_tensor = tf.concat((energies, momenta), axis=2)
    if len(shape) == 2:
        ret_tensor = reshape_to_flat(ret_tensor)
    elif len(shape) == 3:
        ret_tensor = reshape_to_particles(ret_tensor)
    return ret_tensor


def boost(particle, restframe, boost_mode="STANDARD"):
    """
    returns boosted particle
    """
    with tf.name_scope("boost"):
        n_restframes = n_particles(restframe)
        n_p = n_particles(particle)

        beta_vec3 = beta_vec(restframe)
        beta = beta_scalar(restframe)
        gamma = gamma_scalar(restframe)
        energies = reshape_to_particles(restframe)[..., 0]
        e_vector = tf.expand_dims(
            tf.concat(
                [tf.expand_dims(tf.ones_like(energies), axis=-1), -beta_vec3 / beta],
                axis=-1,
            ),
            axis=-1,
        )
        e_vector_T = tf.transpose(e_vector, perm=[0, 1, 3, 2])

        identity = tf.constant(np.identity(4), tf.float32)
        U_matrix = tf.constant([[-1, 0, 0, 0]] + 3 * [[0, -1, -1, -1]], tf.float32)

        identity = tf.reshape(
            tf.tile(identity, [n_restframes, 1]), [n_restframes, 4, 4]
        )
        U_matrix = tf.reshape(
            tf.tile(U_matrix, [n_restframes, 1]), [n_restframes, 4, 4]
        )

        beta = tf.expand_dims(beta, axis=-1)
        gamma = tf.expand_dims(gamma, axis=-1)
        """
        (1, 0) + ((-1+g)*b-1, g*b)
        (0, 1)   (g*b, (-1+g)*b-1)
        """
        Lambda = tf.add(
            identity,
            (U_matrix + gamma)
            * ((U_matrix + 1) * beta - U_matrix)
            * tf.matmul(e_vector, e_vector_T),
            name="lambda_matrix",
        )
        particle = reshape_to_particles(particle)
        particle = tf.expand_dims(particle, -1)
        if boost_mode == "STANDARD":
            pass
        elif boost_mode == "COMBINATIONS":
            l_indices = np.tile(np.arange(n_restframes.value), n_p.value)
            p_indices = np.repeat(np.arange(n_p.value), n_restframes.value)
            Lambda = tf.gather(Lambda, l_indices, axis=1)
            particle = tf.gather(particle, p_indices, axis=1)
        boosted_particle = tf.squeeze(
            tf.matmul(Lambda, particle), axis=-1, name="bootesd_particles"
        )
        return boosted_particle


def beta_vec(vec4):
    """
    input a vec4 of E, px, py, pz
    returns the beta-vector of a vector
    """
    vec4 = reshape_to_particles(vec4)
    momenta = vec4[..., 1:]
    energies = tf.expand_dims(vec4[..., 0], -1)
    betavec = tf.divide(momenta, energies + EPSILON, name="beta_vec")
    return betavec


def beta_scalar(vec4):
    """
    input a vec4 of E, px, py, pz
    returns the scalar beta of a vector
    """
    vec4 = reshape_to_particles(vec4)
    momenta = vec4[..., 1:]
    energies = tf.expand_dims(vec4[..., 0], -1)

    # beta = tf.divide(tf.sqrt(tf.reduce_sum(tf.square(momenta), axis=-1)), tf.squeeze(energies, axis=-1) + EPSILON, name="beta")
    beta = tf.divide(
        tf.sqrt(tf.reduce_sum(tf.square(momenta), axis=-1)),
        tf.squeeze(energies, axis=-1),
        name="beta",
    )
    return tf.expand_dims(beta, axis=-1)


def gamma_scalar(vec4):
    """
    input a vec4 of E, px, py, pz
    returns the scalar gamma of a vector
    """
    vec4 = reshape_to_particles(vec4)
    momenta = vec4[..., 1:]
    energies = tf.expand_dims(vec4[..., 0], -1)
    beta = beta_scalar(vec4)
    # gamma = tf.divide(1., tf.sqrt(1. - tf.square(beta)), name="gamma")
    gamma = tf.divide(1.0, tf.sqrt(1.0 - tf.square(beta - EPSILON)), name="gamma")
    return gamma


def mass(vec4, eps=EPSILON):
    """
    returns the mass of a 4vector
    4vec has shape: (BATCH-Size, n_particles, 4)
    """
    return tf.expand_dims(
        tf.sqrt(
            tf.clip_by_value(
                tf.subtract(
                    tf.square(vec4[:, :, 0]),
                    tf.reduce_sum(tf.square(vec4[:, :, 1:]), axis=-1),
                ),
                1e-7,
                1e5,
            )
        ),
        axis=-1,
        name="mass",
    )


def phi(vec4, eps=EPSILON):
    particles = reshape_to_particles(vec4)
    with tf.name_scope("phi"):
        phi = tf.atan2(tf_non_zero(particles[..., 2], eps), particles[..., 1])
    return phi


def eta(vec4, eps=EPSILON):
    particles = reshape_to_particles(vec4)
    with tf.name_scope("eta"):
        tot_mom = total_momentum(particles)
        eta = tf.atanh(
            tf.clip_by_value(
                tf.divide(particles[..., 3], tot_mom + eps), eps - 1, 1 - eps
            )
        )
    return eta


def pt(vec4, eps=EPSILON):
    particles = reshape_to_particles(vec4)
    with tf.name_scope("pt"):
        pt = tf.sqrt(particles[..., 0] ** 2 + particles[..., 1] ** 2)
    return pt


def total_momentum(vec4, eps=EPSILON):
    particles = reshape_to_particles(vec4)
    with tf.name_scope("total_momentum"):
        tot_mom = tf.sqrt(tf.reduce_sum(tf.square(particles[..., 1:]), axis=-1))
    return tot_mom


def n_particles(input_tensor):
    """
    checks if tensor is shaped by (Batch, n, 4)
    or (Batch, n*4)
    and returns n
    """
    if hasattr(input_tensor, "shape"):
        shape = input_tensor.shape
    else:
        shape = input_tensor
    if len(shape) == 3:
        return shape[1].value if hasattr(shape[1], "value") else shape[1]
    elif len(shape) == 2:
        if shape[1] % 4 == 0:
            return (
                int(shape[1].value // 4)
                if hasattr(shape[1], "value")
                else int(shape[1] // 4)
            )
        else:
            raise ValueError("Tensor shape does not match n_particles*4!")
    elif len(shape) == 0:
        raise ValueError("BatchDimension missing!")
    else:
        raise ValueError("Tensor is too large to be a particle-tensor (len 2 or 3)")


def reshape_to_particles(input_tensor):
    """
    reshapes a (Batch, n*4) tensor to
    (Batch, n, 4)
    """
    shape = input_tensor.shape
    n_p = n_particles(input_tensor)
    if len(input_tensor.shape) == 3:
        return input_tensor
    elif len(shape) == 2:
        return tf.reshape(input_tensor, (-1, n_p, 4))
    elif len(shape) == 0:
        raise ValueError("BatchDimension missing!")
    else:
        raise ValueError("Tensor is too large to be a particle-tensor (len 2 or 3)")


def reshape_to_flat(input_tensor):
    """
    reshapes a (Batch, n, 4) tensor to
    (Batch, n*4)
    """
    shape = input_tensor.shape
    n_p = n_particles(input_tensor)
    if len(shape) == 2:
        return input_tensor
    elif len(shape) == 3:
        return tf.reshape(input_tensor, (-1, n_p * 4))
    elif len(shape) == 0:
        raise ValueError("BatchDimension missing!")
    else:
        raise ValueError("Tensor is too large to be a particle-tensor (len 2 or 3)")


def energy_to_mass_vec4(input_tensor, eps=EPSILON, **kwargs):
    """
    Changes 4-vectors with 
    E, px, py, pz to
    m, px, py, pz
    """
    shape = input_tensor.shape
    n_p = n_particles(input_tensor)
    particle_tensor = reshape_to_particles(input_tensor)

    momenta = particle_tensor[..., 1:]
    energies = particle_tensor[..., 0]
    masses = tf.sqrt(
        tf.clip_by_value(
            tf.subtract(
                tf.square(energies), tf.reduce_sum(tf.square(momenta), axis=-1)
            ),
            1e-7,
            1e5,
        )
    )
    masses = tf.expand_dims(masses, axis=2)
    ret_tensor = tf.concat(
        (masses, momenta), axis=2, name="mass_vec4{}".format(kwargs.pop("name", ""))
    )
    if len(shape) == 2:
        ret_tensor = reshape_to_flat(ret_tensor)
    elif len(shape) == 3:
        ret_tensor = reshape_to_particles(ret_tensor)
    return ret_tensor


def mass_to_energy_vec4(input_tensor):
    """
    Changes 4-vectors with 
    m, px, py, pz to
    E, px, py, pz
    """
    shape = input_tensor.shape
    n_p = n_particles(input_tensor)
    particle_tensor = reshape_to_particles(input_tensor)

    momenta = particle_tensor[..., 1:]
    masses = particle_tensor[..., 0]
    energies = tf.sqrt(
        tf.add(tf.square(masses), tf.reduce_sum(tf.square(momenta), axis=-1))
    )
    energies = tf.expand_dims(energies, axis=2)
    ret_tensor = tf.concat((energies, momenta), axis=2)
    if len(shape) == 2:
        ret_tensor = reshape_to_flat(ret_tensor)
    elif len(shape) == 3:
        ret_tensor = reshape_to_particles(ret_tensor)
    return ret_tensor


def tf_non_zero(t, epsilon):
    """
    Ensures that all zeros in a tensor *t* are replaced by *epsilon*.
    """
    # use combination of abs and sign instead of a where op
    return t + (1 - tf.abs(tf.sign(t))) * epsilon
