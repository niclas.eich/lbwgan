import os
import tensorflow as tf
import numpy as np
from model.lbn import LBNLayer
from model.utils.processing import get_update_ops
from model.modules import n_particles, energy_to_mass_vec4, mass_to_energy_vec4
from model.losses import MMD_loss
from model.layers import (
    constant_mul_layer,
    combine_to_resonance,
    dense_block,
    feature_statistics_layer,
    fully_connected_block,
    particle_decay_unit,
    particle_features_layer,
    particle_layer,
    particle_rotation_layer_2d,
    residual_layer,
    residual_network,
    trained_mass_to_particle,
)
from model.modules import eta, pt
from abc import ABC, abstractmethod
from os.path import normpath
from model.lbn import LBN

BUFFER_SIZE = 10000
SAVING_SCHEDULE = np.arange(0, 100000, 5000)[1:]


class BaseModel(ABC):
    """ 
    BaseModel adapted from
    https://github.com/MrGemy95/Tensorflow-Project-Template/blob/master/base/base_model.py
    """

    def __init__(self, config):
        self.config_general = config["general"]
        self.config_data = config["data"]
        # init the global step
        self.init_global_step()
        # init the epoch counter
        self.init_cur_epoch()
        # input
        self.data = None
        self.build_input()
        # build model
        self.build_model()
        # init saver
        self.init_saver()

    # save function that saves the checkpoint in the path defined in the config file
    def save(self, sess, name_tag=""):
        cur_epoch = int(sess.run(self.cur_epoch_tensor))
        if cur_epoch == 0:
            print("Saving Meta-graph")
            self.saver.save(
                sess, self.config_general["checkpoint_dir"] + "/model.cpkt", cur_epoch
            )
        else:
            print("Saving model...")
            self.saver.save(
                sess,
                self.config_general["checkpoint_dir"] + "/model.cpkt",
                cur_epoch,
                write_meta_graph=False,
            )
        print("Model saved")

    def save_meta_graph(self, sess):
        print("Saving Meta-graph...")
        cur_epoch = int(sess.run(self.cur_epoch_tensor))
        if cur_epoch == 0:
            self.saver.save(
                sess, self.config_general["checkpoint_dir"] + "/model.cpkt", cur_epoch
            )
        else:
            self.saver.save(
                sess, self.config_general["checkpoint_dir"] + "/model.cpkt", cur_epoch,
            )

    def load_meta_graph(self, sess):
        latest_checkpoint = tf.train.latest_checkpoint(
            self.config_general["checkpoint_dir"]
        )
        if latest_checkpoint:
            print("Loading model checkpoint {} ...\n".format(latest_checkpoint))
            self.saver.restore(sess, latest_checkpoint)
            print("Model loaded")
        else:
            print("There is nothing to load")

    # def load_graph(self, sess):
    # print("Loading Model from Graph\n{}".format(self.config['checkpoint_dir'] + "/model.cpkt-0.meta"))
    # print(self.config['checkpoint_dir'] + "/model.cpkt-0.meta")
    # assert os.path.isfile(self.config['checkpoint_dir'] + "/model.cpkt-0.meta")
    # self.saver = tf.train.import_meta_graph(self.config['checkpoint_dir'] + "/model.cpkt-0.meta")

    # load latest checkpoint from the experiment path defined in the config file
    def load(self, sess):
        latest_checkpoint = tf.train.latest_checkpoint(
            self.config_general["checkpoint_dir"]
        )
        if latest_checkpoint:
            print("Loading model checkpoint {} ...\n".format(latest_checkpoint))
            self.saver.restore(sess, latest_checkpoint)
            print("Model loaded")
        else:
            print("There is nothing to load")

    # just initialize a tensorflow variable to use it as epoch counter
    def init_cur_epoch(self):
        with tf.variable_scope("cur_epoch"):
            self.cur_epoch_tensor = tf.Variable(0, trainable=False, name="cur_epoch")
            self.increment_cur_epoch_tensor = tf.assign(
                self.cur_epoch_tensor, self.cur_epoch_tensor + 1
            )

    # just initialize a tensorflow variable to use it as global step counter
    def init_global_step(self):
        # DON'T forget to add the global step tensor to the tensorflow trainer
        with tf.variable_scope("global_step"):
            self.global_step_tensor = tf.Variable(
                0, trainable=False, name="global_step"
            )
            self.increment_global_step_tensor = tf.assign(
                self.global_step_tensor, self.global_step_tensor + 1
            )

    def init_saver(self):
        self.saver = tf.train.Saver(max_to_keep=1)

    def build_input(self):
        """
        build input-layer that gives an iterator input_iterator
        """
        with tf.name_scope("input"):

            def gen():
                idx = np.random.permutation(self.data.shape[0])
                for i in range(
                    0,
                    self.data.shape[0] - self.config_general["batch_size"] + 1,
                    self.config_general["batch_size"],
                ):
                    yield self.data[idx[i : i + self.config_general["batch_size"]]]

            dataset = tf.data.Dataset.from_generator(
                generator=gen,
                output_types=tf.float32,
                output_shapes=[self.config_general["batch_size"]]
                + list(self.config_data["input_dim"]),
            )

            iterator = tf.data.Iterator.from_structure(
                dataset.output_types, dataset.output_shapes
            )
            self.iterator_init = iterator.make_initializer(dataset)
            self.input_iter = iterator.get_next()

    @abstractmethod
    def build_model(self):
        pass


class BaseWGAN(BaseModel, ABC):
    """
    config has to include several dicts:
    config = {
              critic: dict
              genrator: dict
              general: dict
             }


    BaseWGAN adapted from:
    https://github.com/ChengBinJin/WGAN-TensorFlow/blob/master/src/wgan.py
    https://github.com/tensorpack/tensorpack/blob/master/examples/GAN/Improved-WGAN.py
    """

    def __init__(self, config, load=False, **kwargs):
        self.kwargs = kwargs
        self.generator = None
        self.critic = None
        self.config_critic = config["critic"]
        self.config_generator = config["generator"]
        self.config_general = config["general"]
        self.config_data = config["data"]
        self.total_config = config
        if load is True:
            loading_epoch = kwargs.pop("load_epoch", self.config_general["epochs"])
            # if loading_epoch == self.config_general["epochs"]:
            # loading_epoch = "latest"
            self.load(config, epoch=loading_epoch)
        super(BaseWGAN, self).__init__(config)
        self.init_saver()

    def save(self, sess, name_tag=None):
        if os.path.isdir(self.config_general["checkpoint_dir"]):
            pass
        else:
            os.makedirs(self.config_general["checkpoint_dir"])
        cur_epoch = int(sess.run(self.cur_epoch_tensor))
        if name_tag is not None:
            if name_tag == "fail":
                pass
            else:
                self.save_meta_graph(sess)
            print("Saving model...")
            self.critic.save(
                os.path.join(
                    self.config_general["checkpoint_dir"],
                    "critic_{}.h5".format(name_tag),
                ),
                overwrite=True,
            )
            self.generator.save(
                os.path.join(
                    self.config_general["checkpoint_dir"],
                    "generator_{}.h5".format(name_tag),
                ),
                overwrite=True,
            )
        if cur_epoch in SAVING_SCHEDULE or cur_epoch == self.config_general["epochs"]:
            print("Saving model...")
            self.critic.save(
                os.path.join(
                    self.config_general["checkpoint_dir"],
                    "critic_{}.h5".format(cur_epoch),
                ),
                overwrite=True,
            )
            self.generator.save(
                os.path.join(
                    self.config_general["checkpoint_dir"],
                    "generator_{}.h5".format(cur_epoch),
                ),
                overwrite=True,
            )

    def load(self, config, epoch="latest", weights_only=False):
        if epoch == "latest":
            critic_path = os.path.join(
                os.path.join(config["general"]["checkpoint_dir"], "critic.h5")
            )
            generator_path = os.path.join(
                os.path.join(config["general"]["checkpoint_dir"], "generator.h5")
            )
        else:
            critic_path = os.path.join(
                os.path.join(
                    config["general"]["checkpoint_dir"], "critic_{}.h5".format(epoch)
                )
            )
            generator_path = os.path.join(
                os.path.join(
                    config["general"]["checkpoint_dir"], "generator_{}.h5".format(epoch)
                )
            )
        """
        Build custom objects for loading
        """
        custom_obj_crit = {
            "leaky_relu": tf.nn.leaky_relu,
            "feature_statistics_layer": feature_statistics_layer,
            "dense_block": dense_block,
            "particle_features_layer": particle_features_layer,
            "fully_connected_block": fully_connected_block,
            "combine_to_resonance": combine_to_resonance,
        }
        custom_obj_gen = {
            "constant_mul_layer": constant_mul_layer,
            "particle_layer": particle_layer,
            "particle_rotation_layer_2d": particle_rotation_layer_2d,
            "particle_features_layer": particle_features_layer,
            "residual_network": residual_network,
            "dense_block": dense_block,
            "fully_connected_block": fully_connected_block,
            "combine_to_resonance": combine_to_resonance,
            "particle_decay_unit": particle_decay_unit,
            "residual_unit": residual_layer,
            "trained_mass_to_particle": trained_mass_to_particle,
        }
        if "momentum transformation" in config["critic"]["callbacks"]:
            custom_obj_gen[
                "momentum_shape_transformation_layer"
            ] = momentum_shape_transformation_layer
        if config["critic"]["lbn"] is True:
            custom_obj_crit["LBNLayer"] = LBNLayer

        if os.path.isfile(critic_path):
            if weights_only is False:
                self.critic = tf.keras.models.load_model(
                    critic_path, custom_objects=custom_obj_crit
                )
            else:
                self.critic.load_weights(critic_path)
        else:
            raise FileNotFoundError(
                "There was no Critic model saved in {}".format(critic_path)
            )

        if os.path.isfile(generator_path):
            if weights_only is False:
                self.generator = tf.keras.models.load_model(
                    generator_path, custom_objects=custom_obj_gen
                )
            else:
                self.generator.load_weights(generator_path)
        else:
            raise FileNotFoundError(
                "There was no Generator model saved in {}".format(generator_path)
            )

    def build_model(self):
        with tf.name_scope("input") as scope:
            self.is_training = tf.placeholder(tf.bool, name="is_training")
            # self.x = tf.placeholder(tf.float32, shape=[None] + [(self.config["latent_dim"])], name="latent_placeholde")
            if self.config_generator["latent_shape"] == "normal":
                self.x = tf.random.normal(
                    shape=[self.config_general["batch_size"]]
                    + [self.config_generator["latent_dim"]],
                    mean=0.0,
                    stddev=1.0,
                    name="latent_vector",
                )
            elif self.config_generator["latent_shape"] == "uniform":
                self.x = tf.random_uniform(
                    shape=[self.config_general["batch_size"]]
                    + [self.config_generator["latent_dim"]],
                    minval=0.0,
                    maxval=1.0,
                    name="latent_vector",
                )

        self._gen_train_ops, self._crit_train_ops = [], []
        self._gen_update_ops, self._crit_update_ops = [], []
        """
        check if there is a tuning parameter setup for this model
        """
        # if "tuning_parameter" in self.config_general["callbacks"]:
        # tuning_start = 1e-4
        # decay_steps = 1.0
        # decay_rate_tuning = (1./tuning_start) ** (decay_steps / 2000.0)
        # self.tuning_param = tf.train.exponential_decay(
        # tuning_start,
        # self.cur_epoch_tensor,
        # decay_steps,
        # decay_rate_tuning,
        # staircase=False,
        # name="tuning_param",
        # )
        # self.kwargs["tuning_parameter"] = self.tuning_param

        """
        Build Generator and LBN
        """
        if not "detector_acceptance_only" in self.config_generator["callbacks"]:
            if self.generator is None:
                self.generator = self.build_generator(**self.kwargs)
                self.generator.summary()
            if self.config_generator.get("pdu", None) is not None:
                (
                    self.g_samples,
                    self.pdu_particles_base,
                    self.pdu_particles_refined,
                    self.pdu_particles_rest_0,
                    self.pdu_particles_rest_1,
                    self.pdu_masses,
                ) = self.generator(self.x)
            else:
                (self.g_samples) = self.generator(self.x)
        else:
            if self.generator is None:
                self.generator = self.build_generator(**self.kwargs)
                self.generator.summary()
            if self.config_generator.get("pdu", None) is not None:
                (
                    self.g_samples,
                    self.pdu_particles_base,
                    self.pdu_particles_refined,
                    self.pdu_particles_rest_0,
                    self.pdu_particles_rest_1,
                    self.pdu_masses,
                ) = self.generator(self.input_iter)
            else:
                self.g_samples = self.generator(self.input_iter)
        print(self.generator.summary())
        self._gen_update_ops += get_update_ops(self.generator, "GENERATOR")
        """
        Build Critic
        """
        if self.critic is None:
            self.critic = self.build_critic(**self.kwargs)
        """
        interpolated sampling for gradient penalty
        
        Since all kinematics have to be fulfilled,
        the masses get interpolated, instead of the energies.
        After that, the Energies get computed manually again
        
        1st Step:
        E, px, py, pz -->  m, px, py, pz
        
        2nd Step:
        Interpolate between v_data, v_gen

        3rd Step:
        m, px, py, pz ---> E, px, py, pz
        """
        with tf.name_scope("interpolation") as scope:
            if (
                "late_interpolation" in self.config_critic["callbacks"]
                and self.config_critic["lbn"] is True
                and not ("resonance_only" in self.config_critic["callbacks"])
            ):
                raise NotImplementedError("This needs rework")
                alpha = tf.random_uniform(
                    shape=self.features_real.shape, minval=0.0, maxval=1.0, name="alpha"
                )
                interp = tf.identity(
                    self.features_real
                    + alpha * (self.features_gen - self.features_real),
                    "interpolated_4vec",
                )
            elif self.config_critic["lbn"] is True and not (
                "resonance_only" in self.config_critic["callbacks"]
            ):
                alpha = tf.random_uniform(
                    shape=self.g_samples.shape, minval=0.0, maxval=1.0, name="alpha"
                )
                mass_vec4_gen = energy_to_mass_vec4(self.g_samples, name="_GEN")
                mass_vec4_real = energy_to_mass_vec4(self.input_iter, name="_DATA_")
                interp_mass_vec4 = mass_vec4_real + alpha * (
                    mass_vec4_gen - mass_vec4_real
                )
                interp = tf.identity(
                    mass_to_energy_vec4(interp_mass_vec4), "interpolated_4vec"
                )
            else:
                alpha = tf.random_uniform(
                    shape=self.g_samples.shape, minval=0.0, maxval=1.0, name="alpha"
                )
                interp = tf.identity(
                    self.input_iter + alpha * (self.g_samples - self.input_iter),
                    "interpolated_4vec",
                )

        """
        feed into Critic
        """
        self.c_logit_real = self.critic(self.input_iter)
        self._crit_update_ops += get_update_ops(self.critic, "CRITIC")
        self.c_logit_fake = self.critic(self.g_samples)
        self.c_logit_interp = self.critic(interp)

        # Critic dense vars:
        self.critic_weights = [
            item.weights[0]
            for item in [
                layer if "dense" in layer.name else None for layer in self.critic.layers
            ]
            if item is not None
        ]

        print(self.critic.summary())

        # if self.config_critic["lbn"] is True and not("resonance_only" in self.config_critic["callbacks"]):
        # self.lbn = next(
        # item for item in [layer if "lbn_layer" in layer.name else None for layer in self.critic.layers] if item is not None
        # )
        # self.features_gen = self.lbn(self.g_samples)
        # self.features_real = self.lbn(self.input_iter)
        # self.features_interp = self.lbn(interp)

        # Gradient Penalty Loss
        with tf.name_scope("gradient_penalty") as scope:
            gradients = tf.gradients(self.c_logit_interp, [interp])[0]
            if "logged_GP" in self.config_critic["callbacks"]:
                gradients = tf.sqrt(
                    1e-12
                    + tf.reduce_sum(
                        tf.log(tf.square(gradients) + 1), reduction_indices=[1]
                    )
                )
            else:
                gradients = tf.sqrt(
                    1e-12 + tf.reduce_sum(tf.square(gradients), reduction_indices=[1])
                )
            self.gradient_penalty = self.config_general["GP"] * tf.reduce_mean(
                tf.square(tf.maximum(0.0, gradients - 1.0)), name="gradient_penalty"
            )

        # if self.config_critic["lbn"] is True and not("resonance_only" in self.config_critic["callbacks"]):
        # self.lbn_gradients = tf.gradients(self.features_interp, [interp])[0]
        # self.lbn_gradients = tf.sqrt(1e-12 + tf.reduce_sum(tf.square(self.lbn_gradients), reduction_indices=[1]))
        # self.lbn_gradient_penalty = self.config_general["GP"] * tf.reduce_mean(
        # tf.square(tf.maximum(0.0, self.lbn_gradients - 1.0)), name="lbn_gradient_penalty",
        # )

        with tf.name_scope("losses") as scope:
            c_vars = self.critic.trainable_variables
            g_vars = self.generator.trainable_variables
            print("Critic-Variables:")
            for i, c_var in enumerate(c_vars):
                print("\t{}:\t".format(i), c_var)
            print("Generator-Variables:")
            for i, g_var in enumerate(g_vars):
                print("\t{}:\t".format(i), g_var)
            # print("All-Variables:")
            # for i, a_var in enumerate(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)):
            # print("\t{}:\t".format(i), a_var)

            guard = tf.ones_like(self.g_samples)
            if "no_gradients" in self.config_critic["callbacks"]:
                print("Deleting gradients of values out of cut")
                if self.config_data["particle_process"] in ["Zmumu", "Zmumu_sorted"]:
                    ETA_CUT = 2.40
                    PT_CUT = 10.0 / self.config_data["scaling_factor"]
                elif self.config_data["particle_process"] in ["Zee", "Zee_sorted"]:
                    ETA_CUT = 2.5
                    PT_CUT = 10.0 / self.config_data["scaling_factor"]
                elif self.config_data["particle_process"] in ["zbb_gen_01"]:
                    ETA_CUT = 10. 
                    PT_CUT = 0. / self.config_data["scaling_factor"]
                etas = tf.abs(eta(self.g_samples)[..., 0:1, :]) < ETA_CUT
                pts = tf.abs(pt(self.g_samples)[..., 0:1, :]) > PT_CUT
                guard = etas & pts
                guard = tf.cast(
                    tf.math.logical_and(guard[..., 0], guard[..., 1]), tf.float32
                )

            self.c_regularizer = self.critic.losses
            print("Critic Regularizers:")
            print(self.c_regularizer)
            self.g_regularizer = self.generator.losses
            print("Generator Regularizers:")
            print(self.g_regularizer)
            self.c_loss = tf.reduce_mean(
                self.c_logit_fake - self.c_logit_real, name="c_loss"
            )
            self.g_loss = tf.negative(
                tf.reduce_mean(self.c_logit_fake * guard), name="g_loss"
            )

            self.c_loss_combined = tf.add(self.c_loss, self.gradient_penalty)
            self.c_loss_combined = tf.add(self.c_loss_combined, self.c_regularizer)
            if not (self.g_regularizer == []):
                self.g_loss = tf.add(self.g_loss, self.g_regularizer)

            # compute hinge_loss on eta if so given
            if self.config_generator.get("hinge_loss", None) is not None:
                ETA_CUT = 2.40
                lambda_hinge = self.config_generator.get("hinge_loss", 100.0)
                self.hinge_loss = tf.squeeze(
                    lambda_hinge
                    * tf.reduce_mean(
                        tf.maximum(tf.abs(eta(self.g_samples)) - ETA_CUT, 0.0)
                    )
                )
                self.g_loss = tf.add(self.g_loss, self.hinge_loss)

            if self.config_generator.get("MMD_loss", None) is not None:
                lambda_MMD = self.config_generator.get("MMD_loss", 1.0)
                self.mmd_loss = lambda_MMD * MMD_loss(
                    self.g_samples,
                    self.input_iter,
                    self.config_data["combined_particles"]["idx"],
                )
                self.g_loss = tf.add(self.g_loss, self.mmd_loss)

        if self.config_general["epochs"] > 0:
            """
            Define Minimization Operations
            """
            decay_steps = self.config_general.get("decay_steps", 1.0)
            if self.config_critic.get("lr_decay", None) is not None:
                if self.config_critic["lr_decay"] == "cosine":
                    # decay steps gives the period length of the cosine decay
                    self.lr_crit = tf.train.cosine_decay(
                        self.config_critic["learning_rate"],
                        self.cur_epoch_tensor,
                        decay_steps,
                        alpha=0.1,
                        name="lr_decay_crit",
                    )
                else:
                    decay_rate_crit = (self.config_critic["lr_decay"]) ** (
                        decay_steps / 3000.0
                    )
                    self.lr_crit = tf.train.exponential_decay(
                        self.config_critic["learning_rate"],
                        self.cur_epoch_tensor,
                        decay_steps,
                        decay_rate_crit,
                        staircase=False,
                        name="lr_decay_crit",
                    )
            else:
                self.lr_crit = self.config_critic["learning_rate"]

            if self.config_generator.get("lr_decay", None) is not None:
                if self.config_generator["lr_decay"] == "cosine":
                    # decay steps gives the period length of the cosine decay
                    self.lr_gen = tf.train.cosine_decay(
                        self.config_generator["learning_rate"],
                        self.cur_epoch_tensor,
                        decay_steps,
                        alpha=0.1,
                        name="lr_decay_gen",
                    )
                else:
                    decay_rate_gen = (self.config_generator["lr_decay"]) ** (
                        decay_steps / 3000.0
                    )
                    self.lr_gen = tf.train.exponential_decay(
                        self.config_generator["learning_rate"],
                        self.cur_epoch_tensor,
                        decay_steps,
                        decay_rate_gen,
                        staircase=False,
                        name="lr_decay_gen",
                    )
            else:
                self.lr_gen = self.config_generator["learning_rate"]
            with tf.name_scope("optimizers") as scope:
                # Critic
                opt_args_crit = {
                    "learning_rate": self.lr_crit,
                }
                if self.config_critic["optimizer"] == "RMS":
                    self.crit_optimizer = tf.train.RMSPropOptimizer(**opt_args_crit)
                elif self.config_critic["optimizer"] == "ADAM":
                    self.crit_optimizer = tf.train.AdamOptimizer(
                        beta1=0.5, beta2=0.9, **opt_args_crit
                    )
                elif self.config_critic["optimizer"] == "ADADELTA":
                    self.crit_optimizer = tf.train.AdadeltaOptimizer(**opt_args_crit)
                elif self.config_critic["optimizer"] == "SGD":
                    self.crit_optimizer = tf.train.GradientDescentOptimizer(
                        **opt_args_crit
                    )

                self.reset_crit_optimizer = tf.variables_initializer(
                    self.crit_optimizer.variables()
                )
                self.crit_loss_grad = self.crit_optimizer.compute_gradients(
                    self.c_loss_combined, var_list=c_vars
                )
                clipped_crit_loss_grad = [
                    (tf.clip_by_norm(grad, 1e3), var)
                    for grad, var in self.crit_loss_grad
                ]

                self.crit_train = self.crit_optimizer.apply_gradients(
                    clipped_crit_loss_grad
                )
                self._crit_update_ops += tf.compat.v1.get_collection(
                    tf.GraphKeys.UPDATE_OPS, scope="critic"
                )
                crit_ops = [self.crit_train] + self._crit_train_ops

                # Generator
                opt_args_gen = {"learning_rate": self.lr_gen}
                if self.config_general["epochs"] > 0:
                    if self.config_generator["optimizer"] == "RMS":
                        self.gen_optimizer = tf.train.RMSPropOptimizer(**opt_args_gen)
                    elif self.config_generator["optimizer"] == "ADAM":
                        self.gen_optimizer = tf.train.AdamOptimizer(
                            beta1=0.5, beta2=0.9, **opt_args_gen
                        )
                    elif self.config_generator["optimizer"] == "ADADELTA":
                        self.gen_optimizer = tf.train.AdadeltaOptimizer(**opt_args_gen)
                    elif self.config_generator["optimizer"] == "SGD":
                        self.gen_optimizer = tf.train.GradientDescentOptimizer(
                            **opt_args_gen
                        )

                    self.reset_gen_optimizer = tf.variables_initializer(
                        self.gen_optimizer.variables()
                    )
                    self.gen_loss_grad = self.gen_optimizer.compute_gradients(
                        self.g_loss, var_list=g_vars
                    )
                    # print("\n"*4, "DEBUG:\n", "Generator grad: {}".format(self.gen_loss_grad))
                    clipped_gen_loss_grad = [
                        (tf.clip_by_norm(grad, 1e3), var)
                        for grad, var in self.gen_loss_grad
                    ]
                    self.gen_train = self.gen_optimizer.apply_gradients(
                        clipped_gen_loss_grad
                    )
                    self._gen_update_ops += tf.compat.v1.get_collection(
                        tf.GraphKeys.UPDATE_OPS, scope="generator"
                    )
                    gen_ops = [self.gen_train] + self._gen_train_ops

                numeric_check = [
                    tf.check_numerics(
                        test_tensor, "Tensor {} is broken!".format(t_name)
                    )
                    for t_name, test_tensor in zip(
                        ["G_Loss", "C_Loss"], [self.g_loss, self.c_loss_combined]
                    )
                ]
                with tf.control_dependencies(numeric_check):
                    print("Critic Update Operations:")
                    print(self._crit_update_ops)
                    with tf.control_dependencies(self._crit_update_ops):
                        self.crit_optim = tf.group(*crit_ops)
                    print("Generator Update Operations:")
                    print(self._gen_update_ops)
                    with tf.control_dependencies(self._gen_update_ops):
                        self.gen_optim = tf.group(*gen_ops)

    @abstractmethod
    def build_generator(self, name="generator", **kwargs):
        """
        Generator definition
        """

    @abstractmethod
    def build_critic(self, name="critic", **kwargs):
        """
        Critic definition
        """
